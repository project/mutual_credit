<?php

use Drupal\mcapi\Entity\Wallet;

/**
 * Implements hook_views_query_alter().
 *
 * Because when transactions are loaded, the child transactions are placed as
 * children in the larger ones, and views loses track of them, so filter them
 * out of all views, except aggregated views
 */
function mcapi_views_query_alter($view, $query) {
  $base_tables = $view->getBaseTables();
  if (isset($base_tables['mc_transaction'])) {
    $exclude_erased = 1;
  }
  elseif (isset($base_tables['mc_transactions_index'])) {
    if (!$view->query->groupby) {
      return;
    }
    $exclude_erased = 1;
  }
  if (isset($exclude_erased)) {
    // Unless the user is admin, only show transactions that count.
    if (!\Drupal::currentUser()->hasPermission('manage mcapi')) {
      $query->where[] = [
        'conditions' => [['field' => 'state', 'value' => \CreditCommons\Workflow::STATE_ERASED, 'operator' => '<>']],
        'type' => 'AND'
      ];
    }
  }
}

/**
 * Implements hook_views_pre_render().
 */
function mcapi_views_pre_render(Drupal\views\ViewExecutable $view) {
  // Set the title of all views with a wallet argument.
  foreach (['payer', 'payee', 'wallet_id'] as $tok) {
    if (isset($view->argument[$tok])) {
      $pos = array_search($tok, $view->argument);
      $wallet = Wallet::load($view->args[$pos]);
      $view->setTitle(t('Wallet: @name', ['@name' => $wallet->label()]));
    }
  }
  // Re-render transaction quantities in aggregated fields.
  if ($view->id() == 'wallet_rankings') {
    foreach ($view->result as $row) {
      if ($view->getDisplay()->useGroupBy()) {
        $affected_fields = ['incoming', 'outgoing'];
        foreach (['incoming', 'outgoing', 'volume'] as $f) {
          if (isset($view->field[$f])) {
            $alias = $view->field[$f]->field_alias;
            /** @var Drupal\views\ResultRow $row */
            $view->field[$f]->options['alter']['alter_text'] = true;
            $view->field[$f]->options['alter']['text'] = '('.Functions::formatCurrency($row->{$alias}) .')';
            break;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_views_data_alter().
 */
function mcapi_views_data_alter(&$data) {
//  $data['users_field_data']['wallet'] = [
//    'title' => t('Wallets of user'),
//    'help' => t('Require this relationship to get only the first wallet'),
//    'relationship' => [
//      'label' => t('User wallet relationship'),
//      'group' => t('Community accounting'),
//      'base' => 'mc_wallet',
//      'base field' => 'owner',
//      'relationship field' => 'uid',
//      'id' => 'standard'
//    ],
//  ];
  $data['users_field_data']['uid']['relationship'] = [
    'id' => 'first_join',
    'label' => t('First wallet'),
    'title' => t('First wallet'),
    'description' => t('Join to the wallets table, ensuring only one wallet is linked.'),
    'id' => 'first_join',
    'base' => 'mc_wallet',
    'base field' => 'owner',
    'table' => 'users_field_data',
  ];
}

