<?php

/**
 * @file
 * Token hooks.
 */
use \Drupal\mcapi\Plugin\Field\FieldType\McState;
use Drupal\mcapi\Entity\Access\AnonSignAccess;
use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi\Functions;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Implements hook_tokens().
 */
function mcapi_tokens($type, $tokens, array $data, array $options, $bubbleable_metadata) {

  if (!array_key_exists($type, $data) || !is_object($data[$type])) {
    return;
  }
  $renderer = \Drupal::service('renderer');
  $replacements = [];
  if ($type == 'mc_transaction' or $type == 'xaction') {
    $transaction = $data[$type];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'state':
          $replacements[$original] = McState::usedStateNames();[$transaction->state->value];
          break;

        case 'workflow':
          $replacements[$original] = $transaction->workflow->entity->label();
          break;

        case 'payee':
        case 'payer':
          $metadata = $transaction->entries->metadata;
          if (isset($metadata[$transaction->{$name}->target_id])) {
            $replacements[$original] = $metadata[$transaction->{$name}->target_id];
          }
          else {
            $replacements[$original] = $transaction->{$name}->toLink()->toString();
          }
          break;

        case 'creator'://deprecated
          $replacements[$original] = $transaction->creator->entity->toLink()->toString();
          break;

        case 'description':
          $replacements[$original] = trim(strip_tags($transaction->description));
          break;

        case 'quant':
        case 'worth': // Deprecated
          $replacements[$original] = Functions::formatCurrency($transaction->quant);
          break;

        case 'created':
          $replacements[$original] = format_date($transaction->created->value, 'medium');
          break;

        case 'changed':
          $replacements[$original] = format_date($transaction->changed->value, 'medium');
          break;

        case 'url':
          $replacements[$original] = $transaction->toUrl('canonical', ['absolute' => TRUE])->toString();
          break;

        case 'sentence':
          $replacements[$original] = $transaction->sentence(); // plain text.
          break;

        case 'links':
          $renderable = \Drupal::entityTypeManager()
            ->getViewBuilder('mc_transaction')
            ->linkList($transaction);
          $replacements[$original] = $renderer->renderPlain($renderable);
          break;

        case 'sign-link':
          $replacements[$original] = Url::fromRoute(
            'mcapi.sign_as_anon',
            [
              'uid' => $data['user']->id(),
              'mc_transaction' => $data['xaction']->id(),
              'hash' => AnonSignAccess::generateHash($data['user']->id(), $data['xaction']->id()),
            ],
            ['absolute' => TRUE]
          )->toString();
      }
    }

    $token_service = \Drupal::token();
    // Handle any entity references - tokens within tokens.
    if ($payer_tokens = $token_service->findWithPrefix($tokens, 'payer')) {
      $replacements += $token_service->generate(
        'mc_wallet',
        $payer_tokens,
        ['mc_wallet' => $transaction->payer->entity],
        $options,
        $bubbleable_metadata
      );
    }
    if ($payee_tokens = $token_service->findWithPrefix($tokens, 'payee')) {
      $replacements += $token_service->generate(
        'mc_wallet',
        $payee_tokens,
        ['mc_wallet' => $transaction->payee->entity],
        $options,
        $bubbleable_metadata
      );
    }
    if ($creator_tokens = $token_service->findWithPrefix($tokens, 'creator')) {
      $replacements += $token_service->generate(
        'user',
        $creator_tokens,
        ['user' => $transaction->uid->entity],
        $options,
        $bubbleable_metadata
      );
    }
    if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate(
        'created',
        $created_tokens,
        ['created' => $transaction->created->value],
        $options,
        $bubbleable_metadata
      );
    }
    if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate(
        'changed',
        $changed_tokens,
        ['changed' => $transaction->changed->value],
        $options,
        $bubbleable_metadata
      );
    }
  }
  elseif ($type == 'mc_entry') {
    $entry = $data[$type];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'payee':
          $replacements[$original] = $entry->payee->toLink()->toString();
          break;
        case 'payer':
          $replacements[$original] = $entry->payer->toLink()->toString();
          break;
        case 'quant':
          $replacements[$original] = Functions::formatCurrency($entry->quant);
          break;
        case 'description':
          $replacements[$original] = strip_tags($entry->description);
          break;
      }
    }

  }
  if ($type == 'wallet' or $type == 'mc_wallet') {
    $wallet = $data[$type];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name':
          $replacements[$original] = $wallet->label();
          break;
        case 'owner':
          $replacements[$original] = $wallet->getOwner()->label();
          break;
        case 'owner-url':
          $replacements[$original] = $wallet->getOwner()->toUrl('canonical', ['absolute' => TRUE])->toString();
          break;
        case 'url':
          $replacements[$original] = $wallet->toUrl('canonical', ['absolute' => TRUE])->toString();
          break;
        case 'balance':
          $replacements[$original] = Markup::create($wallet->balance);
          break;
        case 'volume':
          $replacements[$original] = Markup::create($wallet->volume);
          break;
        case 'gross_in':
          $replacements[$original] = Markup::create($wallet->gross_in);
          break;
        case 'gross_out':
          $replacements[$original] = Markup::create($wallet->gross_out);
          break;
        case 'partners':
          $replacements[$original] = $wallet->partners;
          break;
        case 'trades':
          $replacements[$original] = $wallet->trades;
          break;
        case 'balances':
        case 'balance_bars':
        case 'stats':
          $renderable = [
            '#title' => t('Wallet: %name', ['%name' => $wallet->label()]),
            '#theme' => 'mc_wallet_'.$name,
            '#wallet' => $wallet,
            '#attributes' => ['class' => ['component', $name]],
            '#theme_wrappers' => ['mc_wallet_component']
          ];
          $replacements[$original] = $renderer->render($renderable);
          break;
        case 'history':
          $renderable = mc_wallet_view_balance_history($wallet, 300, 150);
          $replacements[$original] = $renderer->render($renderable);
          break;
        default:
          \Drupal::service('logger.channel.mcapi')->error("Unrecognised transaction token '@token'", ['@token' => $original]);
      }
    }
    if ($owner_tokens = $token_service->findWithPrefix($tokens, 'owner')) {
      $replacements += $token_service->generate(
        'user',
        $owner_tokens,
        ['user' => $wallet->getOwner()],
        $options,
        $bubbleable_metadata
      );
    }
  }
  return $replacements;
}

/**
 * Implements hook_token_info().
 */
function mcapi_token_info() {
  return [
    'types' => [
      'mc_transaction' => [
        'name' => t('Transaction'),
        'description' => t('Tokens related to transactions'),
        'needs-data' => 'mc_transaction',
      ],
      'mc_wallet' => [
        'name' => t('Wallet'),
        'description' => t('Tokens related to wallets'),
        'needs-data' => 'mc_wallet',
      ]
    ],
    'tokens' => [
      'mc_transaction' => [
        'description' => [
          'name' => t('Description'),
        ],
        'payer' => [
          'name' => t("Payer"),
          'type' => 'wallet',
        ],
        'payee' => [
          'name' => t("payee"),
          'type' => 'wallet',
        ],
        'workflow' => [
          'name' => t('Workflow type'),
        ],
        'state' => [
          'name' => t('Workflow state'),
        ],
        'uid' => [
          'name' => t("Owner"),
          'type' => 'user',
        ],
        'created' => [
          'name' => t("Date created"),
          'type' => 'date',
        ],
        'changed' => [
          'name' => t("Date changed"),
          'description' => t("The date the node was most recently updated."),
          'type' => 'date',
        ],
        'url' => [
          'name' => t('Absolute url')
        ],
        'sign-link' => [
          'name' => t("Signature link"),
          'description' => t("A unique url for a user to sign a transaction."),
        ]
      ],
      'mc_wallet' => [
        'name' => [
          'name' => t('Name'),
        ],
        'owner' => [
          'name' => t("Owner"),
          'type' => 'user',
        ],
        'url' => [
          'name' => t('Absolute url'),
        ],
        'owner' => [
          'name' => t('Owner name'),
        ],
        'balance' => [
          'name' => t('Balance'),
        ],
        'volume' => [
          'name' => t('Volume'),
        ],
        'gross_in' => [
          'name' => t('Gross income'),
        ],
        'gross_out' => [
          'name' => t('Gross expenditure'),
        ],
        'trades' => [
          'name' => t('Trades'),
        ],
        'partners' => [
          'name' => t('Partners'),
        ],
        'owner-url' => [
          'name' => t('Absolute url of owner'),
        ],
        'balances' => [
          'name' => t('All balances in a table'),
        ],
        'balance_bars' => [
          'name' => t('Income & expenditure'),
        ],
        'stats' => [
          'name' => t('All stats in one table'),
        ],
        'history' => [
          'name' => t('Balance history'),
        ]
      ]
    ]
  ];
}

function mcapi_token_info_alter(&$info) {
  // Make aliases for transactions & wallets
  $info['tokens']['xaction'] = $info['tokens']['mc_transaction'];
  $info['types']['xaction'] = $info['types']['mc_transaction'];
  $info['tokens']['wallet'] = $info['tokens']['mc_wallet'];
  $info['types']['wallet'] = $info['types']['mc_wallet'];
}
