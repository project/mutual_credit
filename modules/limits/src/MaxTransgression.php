<?php

namespace Drupal\mcapi_limits;

/**
 * Holds data for a transgression of balance limits.
 */
class MaxTransgression extends Transgression {

  /**
   * {@inheritDoc}
   */
  public function limit() : int {
    return $this->wallet->max->limit();
  }

  /**
   * {@inheritDoc}
   */
  public function diff() {
    return $this->projected - $this->wallet->balance;
  }

}
