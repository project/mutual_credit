<?php

namespace Drupal\mcapi_limits;

use Drupal\Core\Form\FormStateInterface;


class Settings extends \Drupal\Core\Form\ConfigFormBase {


  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('mcapi_limits.settings');
    // The fallback plugin still produces a message, so we're putting it here.
    $plugin_name = $form_state->getValue('plugin') ?? $config->get('plugin') ?? 'balanced';
    $plugin_settings = $form_state->getValue('plugin_settings') ??
      $config->get('plugin_settings') ??[];
    $plugin = \Drupal::service('plugin.manager.mcapi_limits')
      ->createInstance($plugin_name, $plugin_settings);

    $form['plugin'] = [
      '#title' => t('Plugin'),
      '#description' => t('How will the limits be determined?'),
      '#type' => 'select',
      '#options' => \Drupal::service('plugin.manager.mcapi_limits')->optionsList(),
      '#default_value' => $plugin->getPluginId(),
      '#weight' => 1,
      '#ajax' => [
        'callback' => [$this, 'ajaxPluginSettings'],
        'wrapper' => 'limits-plugin-settings',
        'effect' => 'fade',
      ]
    ];
    // Show the options form for the selected plugin.
    $form['plugin_settings'] = $plugin->buildConfigurationForm($form, $form_state)
       + [
      '#type' => 'container',
      '#prefix' => '<div id="limits-plugin-settings">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#weight' => 2,
      '#theme_wrappers' => ['form_element']
    ];
    $form['skip'] = [
      '#title' => $this->t('Skip balance limit check for the following transactions'),
      '#description' => $this->t('Especially useful for mass transactions and automated transactions'),
      '#type' => 'checkboxes',
      // Casting it here saves us worrying about the default, which is awkward.
      '#default_value' => array_keys(array_filter($config->get('skip'))),
      // Would be nice if this was pluggable, but not needed for now.
      '#options' => [
        'auto' => $this->t("Automated transactions (such as periodic fees)"), // where user = 0 and workflow = 3rdparty
        'admin' => $this->t("Created by a user with 'manage mcapi' permission"),
        'all' => $this->t("Skip all transactions (warn only)")
      ],
      '#states' => [
        'invisible' => [
          ':input[name="limits[limits_callback]"]' => ['value' => 'limits_none'],
        ],
      ],
      '#weight' => 6,
    ];
        // To move this to the mcapi_mass module we would have to ensure the correct weight of modules
    if (\Drupal::moduleHandler()->moduleExists('mcapi_mass')) {
      $form['skip']['#options']['mass'] = $this->t("Mass transactions"); // NB mass transactions are now a separate module
    }

    $form['notification'] = [
      '#title' => t('Notifications'),
      '#type' => 'details',
      '#open' => FALSE,
      '#tree' => 1,
      '#weight' => 10,
      'prevented_mail' => [
        '#type' => 'fieldset',
        '#title' => t('Notification of a transaction prevented.'),
        'subject' => [
          '#title' => t('Subject'),
          '#type' => 'textfield',
          '#default_value' => $config->get('notification.prevented_mail.subject'),
          '#maxlength' => 180,
          '#weight' => 1,
        ],
        'body' => [
          '#title' => t('Body'),
          '#description' => t("The [user] tokens, represent the email recipient."),
          '#type' => 'textarea',
          '#default_value' => $config->get('notification.prevented_mail.body'),
          '#rows' => 8,
          '#weight' => 2,
        ]
      ],
      'warning_threshhold' => [
        '#title' => $this->t('Warning threshhold'),
        '#description' => $this->t('Trigger a warning when this percent of the max capacity is left. 0 means no warnings. 1 means warn when balances reaches 99% of capacity. 10 means warn when balance reaches 90% of capacity.'),
        '#type' => 'number',
        '#default_value' => $config->get('notification.warning_threshhold') ?? 0,
        '#weight' => 0,
        '#min' => 0,
        '#max' => 99,
        '#field_suffix' => '%',
        '#size' => 2,
        '#required' => TRUE,
      ],
      'warning_mail' => [
        '#type' => 'fieldset',
        '#title' => t('Notification of warning threshhold crossed. this might be shown on screen or sent as an email.'),
        'subject' => [
          '#title' => t('Subject'),
          '#type' => 'textfield',
          '#default_value' => $config->get('notification.warning_mail.subject'),
          '#maxlength' => 180,
          '#weight' => 1,
          '#states' => [
            'required' => [
              ':input[name="notification[warning_threshhold]"]' => ['!value' => '0']
            ]
          ]
        ],
        'body' => [
          '#title' => t('Body'),
          '#description' => t("The [user] tokens, represent the email recipient."),
          '#type' => 'textarea',
          '#default_value' => $config->get('notification.warning_mail.body'),
          '#rows' => 8,
          '#weight' => 2,
          '#states' => [
            'required' => [
              ':input[name="notification[warning_threshhold]"]' => ['!value' => '0']
            ]
          ]
        ],
        '#states' => [
          'visible' => [
            ':input[name="notification[warning_threshhold]"]' => ['!value' => '0']
          ]
        ]
      ]
    ];
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['warning']['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['current-user', 'user', 'transgression', ],
        '#global_types' => FALSE,
        '#weight' => 5
      ];
    }
    $form['override'] = [
      '#title' => $this->t('Allow individual wallet overrides'),
      '#description' => $this->t("Set on each wallet's edit page."),
      '#type' => 'checkbox',
      '#default_value' => $config->get('override'),
      '#weight' => 20
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mcapi_limits.settings')
      ->set('plugin', $form_state->getValue('plugin'))
      ->set('plugin_settings', $form_state->getValue('plugin_settings'))
      ->set('override', $form_state->getValue('override'))
      ->set('skip', $form_state->getValue('skip'))
      ->set('prevent', $form_state->getValue('prevent'))
      ->set('warning', $form_state->getValue('warning'))
      ->save();
    parent::submitForm($form, $form_state);
  }


  protected function getEditableConfigNames(): array {
    return ['mcapi_limits.settings'];
  }

  public function getFormId(): string {
    return 'mcapi_limits_settings_form';
  }

  public function ajaxPluginSettings($form, $form_state) {
    return $form['plugin_settings'];
  }


}
