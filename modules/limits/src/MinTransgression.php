<?php

namespace Drupal\mcapi_limits;

/**
 * Holds data for a transgression of balance limits.
 */
class MinTransgression extends Transgression {

  /**
   * {@inheritDoc}
   */
  public function limit() : int {
    return $this->wallet->min->limit();
  }

  /**
   * {@inheritDoc}
   */
  public function diff() {
    return $this->wallet->balance - $this->projected;
  }

}
