<?php

namespace Drupal\mcapi_limits;

use Drupal\migrate\Event\MigrateImportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use \Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Alterations to migrations
 */
class MigrationSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_import' => [['migratePreImport']],
    ];
  }


  /**
   * Subscribed event callback.
   */
  function migratePreImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_mcapi_limits_global') {
      \Drupal::moduleHandler()->loadInclude('mcapi_limits', 'install');
      // this whole file needed just to call this function. But there's no equivalent hook I think
      mcapi_limits_ensure_field();
    }
  }

}
