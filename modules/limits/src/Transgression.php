<?php

namespace Drupal\mcapi_limits;

use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi\Entity\TransactionInterface;
use Drupal\token\TokenInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Config;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Holds data for a transgression of balance limits.
 */
abstract class Transgression {

  /**
   * @var MessengerInterface
   */
  protected $messenger;

  /**
   * @var TokenInterface
   */
  protected $token;

  /**
   * @var Config
   */
  protected $config;

  /**
   * @var MailManagerInterface
   */
  protected $mailmanager;

  /**
   * The wallet we are working with.
   * @var \Drupal\mcapi\Entity\Wallet
   */
  public $wallet;

  /**
   * The transaction we are working with.
   * @var \Drupal\mcapi\Entity\Transaction
   */
  public $transaction;

  /**
   * The the excessive balance in base units
   * @var int
   */
  public $projected;

  /**
   *
   * @param Messenger $messenger
   * @param TokenInterface $token
   * @param ConfigFactory $config_factory
   * @param TransactionInterface $transaction
   * @param WalletInterface $wallet
   * @param type $projected
   */
  public function __construct(MessengerInterface $messenger, TokenInterface $token, ConfigFactory $config_factory, MailManagerInterface $mail_plugin_manager, TransactionInterface $transaction, WalletInterface $wallet, $projected) {
    $this->messenger = $messenger;
    $this->token = $token;
    $this->config = $config_factory->get('mcapi_limits.settings');
    $this->mailmanager = $mail_plugin_manager;
    $this->transaction = $transaction;
    $this->wallet = $wallet;
    $this->projected = $projected;
  }

  public static function create($transaction, $wallet, $projected) {
    return new static(
      \Drupal::messenger(),
      \Drupal::token(),
      \Drupal::configFactory(),
      \Drupal::service('plugin.manager.mail'),
      $transaction,
      $wallet,
      $projected
    );
  }

  function warn() {
    $recipient = $this->wallet->getOwner();
    if ($recipient->id() == \Drupal::currentUser()->id()) {
      $this->messenger->addWarning(
        $this->token->replace(
          $this->config->get('notifications.warning_mail.body'),
          ['transgression' => $this, 'xaction' => $this->transaction]
        )
      );
    }
    else {
      // This would be a separate mail on top of notifying transactees/ signatories.
      // Not very tidy!
      $this->mailManager->mail(
        'mcapi_limits',
        'warning',
        $recipient->getEmail(),
        $recipient->getPreferredLangcode(),
        [
          'user' => $recipient,
          'transgression' => $this
        ]
      );
    }
  }

  /**
   * Return the limit of the wallet that was hit.
   */
  abstract function limit();

  /**
   * Return the difference in raw units between the wallet limit and what the
   * balance would have been.
   */
  abstract function diff();

  /**
   * Return the current balance of the wallet.
   */
  function balance() {
    return $this->wallet->balance;
  }
  
  /**
   * For debugging.
   */
  function __toString() {
    $wid = $this->wallet->id();
    $limit = $this->limit();
    $quant = $this->transaction->quant;
    $diff = $this->diff();
    $bal = $this->balance();
    return "This transaction for $quant will take Wallet $wid with a current balance of $bal and a limit of $limit beyond by $diff.";
  }


}
