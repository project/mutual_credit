<?php

namespace Drupal\mcapi_limits\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mcapi\Plugin\Field\FieldType\McWorth;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldType;

/**
 * Plugin implementation of the 'limit' field type. Must be either min or max.
 */
#[FieldType(
  id: "mc_wallet_limit",
  label: new TranslatableMarkup("Wallet Limit"),
  description: new TranslatableMarkup("A quantity of community currency."),
  category: new TranslatableMarkup('Community Accounting'),
  default_formatter: 'mc_worth',
  default_widget: 'mc_worth_nullable',
  list_class: '\Drupal\mcapi_limits\Plugin\Field\LimitItemList'
)]
class McWalletLimit extends McWorth {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'minus' => TRUE
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return $form + [
      'minus' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Format as a negative value'),
        '#default_value' => $this->getSetting('minus')
      ]
    ];
  }


  /**
   * {@inheritDoc}
   * @note not used r/n because balance of trade wallet is 0
   */
  public function __isEmpty() {
    return is_null($this->quant);
  }


}
