<?php

namespace Drupal\mcapi_limits\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\mcapi_limits\Plugin\McapiLimitsInterface;

/**
 * Entries field list
 */
class LimitItemList extends FieldItemList {

  /**
   * Get the value of the field or if it is not set, the default determined by the plugin.
   */
  function limit() : int {
    if ($this->isEmpty()) { // Get the global limits.
      //The name of the field is the name of the method (min or max)
      $method = $this->getSetting('minus') ? 'min' : 'max';
      $wallet = $this->getEntity();
      return $this->globalLimitsPlugin()->{$method}($wallet);
    }
    else {
      return $this->getValue()[0]['value'];
    }
  }

  /**
   * Determine the plugin used to set global balance limits.
   * 
   * @return McapiLimitsInterface
   */
  private function globalLimitsPlugin() : McapiLimitsInterface {
    $limits_settings = \Drupal::config('mcapi_limits.settings');
    return \Drupal::service('plugin.manager.mcapi_limits')->createInstance(
      $limits_settings->get('plugin'),
      $limits_settings->get('plugin_settings')
    );
  }

}
