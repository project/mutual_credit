<?php

namespace Drupal\mcapi_limits\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\mcapi\Entity\WalletInterface;

/**
 * Base class for Limits plugins.
 */
abstract class McapiLimitsBase extends PluginBase implements McapiLimitsInterface {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * Stores the limits settings
   *
   * @var array
   */
  protected $configuration;

  /**
   * {@inheritDoc}
   */
  public function getLimits(WalletInterface $wallet) {
    return [
      'min' => $this->min($wallet),
      'max' => $this->max($wallet)
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('min') and $form_state->hasValue('max')) {
      if ($form_state->getValue('min') >= $form_state->getValue('max')) {
        $form_state->setError($form['min'], $this->t('Min must be less than max.'));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // config is saved by the main form into the module config.
  }

}
