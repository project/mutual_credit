<?php

namespace Drupal\mcapi_limits\Plugin\Limits;

use Drupal\mcapi_limits\Plugin\McapiLimitsBase;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi_limits\Attribute\Limits;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Calculate the balance limits with a simple formula.
 */
#[Limits(
  id: 'calculated',
  label: new TranslatableMarkup('Calculated'),
  description: new TranslatableMarkup('Calculate from user summary stats')
)]
class Calculated extends McapiLimitsBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $class = get_class($this);
    $element['max_formula'] = [
      '#title' => $this->t('Formula to calculate maximum limit'),
      '#prefix' => $this->t('N.B Result values are measured in native units, which might be cents, or seconds'),
      '#description' => $this->t('Make a formula from the following variables: @vars', ['@vars' => $this->help()]),
      '#type' => 'textfield',
      '#element_validate' => [
        [$class, 'validateFormula'],
      ],
      '#default_value' => $this->configuration['max_formula'],
    ];
    $element['min_formula'] = [
      '#title' => $this->t('Formula to calculate minimum limit'),
      '#description' => $this->t('Make a formula to output a NEGATIVE result from the following variables: @vars', ['@vars' => $this->help()]),
      '#type' => 'textfield',
      '#element_validate' => [
        [$class, 'validateFormula'],
      ],
      '#default_value' => $this->configuration['min_formula'],
    ];
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function max(WalletInterface $wallet) : int {
    return $this->parse($conf['max_formula'], $wallet);
  }

  /**
   * {@inheritDoc}
   */
  public function min(WalletInterface $wallet) : int {
    $val = $this->parse($conf['min_formula'], $wallet);
    // Ensure the value is negative.
    return - abs($val);
  }


  /**
   * {@inheritDoc}
   */
  private function help() {
    $help[] = $this->t('@in: the total ever income of the user');
    $help[] = $this->t('@out: the total ever expenditure of the user.');
    $help[] = $this->t('@num: the number of trades.');
    $help[] = $this->t('@ptn: the number of trading partners.');
    $help[] = $this->t('Use arithmetic operators, +, -, *, /, and beware division by zero!');
    return implode('; ', $help);
  }

  /**
   * Element validation callback.
   */
  public static function validateFormula(&$element, $form_state) {
    if (!strlen($element['#value'])) {
      return;
    }
    $test_values = (object)[
      'gross_in' => 110,
      'gross_out' => 90,
      'trades' => 10,
      'partners' => 5,
    ];
    try {
      $result = Self::parse($element['#value'], $test_values);
      if (!is_numeric($result)) {
        // @todo display this error properly
        $form_state->setError(
          $element,
          t('Formula does not evaluate to a number: @result', ['@result' => $result])
        );
      }
      return;
    }
    catch (\Error $e) {
      $form_state->setError($element, t('Invalid formula'));
    }
    catch (\Throwable $e) {
      \Drupal::service('logger.channel.mcapi_limits')->error('Unexpected error in Calculated plugin: '.$e->getmessage()); // I've never seen this happen.
    }
    $form_state->setError(
      $element,
      t('There was a problem calculating the limits of the wallet. Please try another day.')
    );
  }


  /**
   * Helper.
   */
  private static function parse($string, WalletInterface $wallet) {
    $tokens = ['@in', '@out', '@num', '@ptn'];
    $replacements = [
      $wallet->gross_in ?: 1,
      $wallet->gross_out ?: 1,
      $wallet->trades ?: 1,
      $wallet->partners ?: 1,
    ];
    $pattern = str_replace($tokens, $replacements, $string);
    return eval('return ' . $pattern . ';');
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    $defaults = [
      'min_formula' => '-@in/2-100',
      'max_formula' => '@in/2+100',
    ];
    return $defaults;
  }

}
