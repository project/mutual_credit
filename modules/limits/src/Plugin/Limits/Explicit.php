<?php

namespace Drupal\mcapi_limits\Plugin\Limits;

use Drupal\mcapi_limits\Plugin\McapiLimitsBase;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi_limits\Attribute\Limits;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use \Drupal\Core\Form\FormStateInterface;

/**
 * State fixed balance limits
 */
#[Limits(
  id: 'explicit',
  label: new TranslatableMarkup('Explicit')
)]
class Explicit extends McapiLimitsBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      'min' => [
        '#type' => 'worth',
        '#description' => $this->t('Blank means no limit.'),
        '#default_value' => $this->configuration['min'] ?? 1000,
      ],
      'max' => [
        '#type' => 'worth',
        '#description' => $this->t('Blank means no limit.'),
        '#default_value' => $this->configuration['max'] ?? 1000,
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function max(WalletInterface $wallet) : int {
    return $this->configuration['max'];
  }

  /**
   * {@inheritDoc}
   */
  public function min(WalletInterface $wallet) : int {
    // ensure the value is negative.
    return -abs($this->configuration['min']);
  }


  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    $defaults = [
      'min' => -1500,
      'max' => 1500,
    ];
    return $defaults;
  }

}
