<?php

namespace Drupal\mcapi_limits\Plugin\Limits;

use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi_limits\Plugin\McapiLimitsBase;
use Drupal\mcapi_limits\Attribute\Limits;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Limits fixed the same above and below zero.
 */
#[Limits(
  id: 'balanced',
  label: new TranslatableMarkup('Balanced'),
  description: new TranslatableMarkup('Min & max limits are the same distance from zero')
)]
class Balanced extends McapiLimitsBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return[
      'liquidity' => [
        '#title' => $this->t('Distance from zero'),
        '#description' => $this->t('Max number of hours above and below zero.'),
        '#type' => 'worth',
        '#default_value' => $this->configuration['liquidity'] ?? 1000,
        '#min' => 0
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getLimits(WalletInterface $wallet) {
    $limit = $this->configuration['liquidity'] ?? NULL;
    return [
      'min' => -$limit,
      'max' => $limit,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function max(WalletInterface $wallet) : int {
    return $this->configuration['liquidity'];
  }

  /**
   * {@inheritDoc}
   */
  public function min(WalletInterface $wallet) : int {
    return -$this->configuration['liquidity'];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    $defaults['liquidity'] = 1500;
    return $defaults;
  }



  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }
}
