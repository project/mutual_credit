<?php

namespace Drupal\mcapi_limits\Plugin;

use Drupal\mcapi\Entity\WalletInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;

/**
 * Interface for Limits object.
 * @todo The use of PluginFormInterface doesn't seem appropriate here.
 */
interface McapiLimitsInterface extends PluginFormInterface, ConfigurableInterface {

  /**
   * Get the limits according to the plugin settings.
   *
   * @param WalletInterface $wallet
   *   The wallet.
   *
   * @return array
   *   Native amounts keyed with 'min' and 'max'.
   */
  public function getLimits(WalletInterface $wallet);

  /**
   * Get the default min/max limit
   * @return int
   *   A negative number
   */
  function min(WalletInterface $wallet) : int;

  /**
   * Get the default min/max limit
   * @return int
   *   A positive number
   */
  function max(WalletInterface $wallet) : int;

}
