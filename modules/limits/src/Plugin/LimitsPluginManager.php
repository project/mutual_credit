<?php

namespace Drupal\mcapi_limits\Plugin;

use Drupal\mcapi_limits\Attribute\Limits;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;

/**
 * Plugin manager for Wallet limits.
 */
class LimitsPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  private $plugins;

  /**
   * Constructs the LimitsPluginManager object.
   */
  public function __construct($namespaces, $moduleHandler, $cache_backend) {
    parent::__construct(
      'Plugin/Limits',
      $namespaces,
      $moduleHandler,
      McapiLimitsInterface::class,
      Limits::class,
      'Drupal\mcapi_limits\Annotation\Limits'
    );
    $this->setCacheBackend($cache_backend, 'mcapi_limits');
  }


  public function optionsList() : array {
    foreach ($this->getDefinitions() as $name => $plugin) {
      $options[$name] = $plugin['label'].': '.$plugin['description'];
    }
    return $options;
  }

  public function getFallbackPluginId($plugin_id, array $configuration = []): string {
    return 'balanced';
  }

}
