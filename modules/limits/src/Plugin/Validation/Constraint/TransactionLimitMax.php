<?php

namespace Drupal\mcapi_limits\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Supports validating payer & payee of parent transactions.
 */
#[Constraint(
  id: 'limits_max',
  label: new TranslatableMarkup('Checks that neither wallet goes beyond its determined credit limits'),
  type: 'entity:mc_transaction'
)]
class TransactionLimitMax extends \Symfony\Component\Validator\Constraint {

  public $message = "The transaction would take wallet '%wallet' %excess above its maximum limit of %limit.";
  public float $diff;
  public int $wid;

  /**
   * {@inheritDoc}
   */
  public function coversFields() {
    return ['entries'];
  }

  public function validatedBy() {
    return '\Drupal\mcapi_limits\Plugin\Validation\Constraint\TransactionLimitsValidator';
  }

}
