<?php

namespace Drupal\mcapi_limits\Plugin\Validation\Constraint;

use Drupal\mcapi\Functions;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Entity level constraint validator checking whether a wallet is beyond its limits.
 */
class TransactionLimitsValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   * Note this is validating max and min
   */
  public function validate($transaction, Constraint $constraint) {
    // Curiously entity field validations runs even if the form field validation
    // fails. So skip if a wallet id is invalid.
    if (!$transaction->payee or !$transaction->payer) {
      return;
    }
    // mcapi_limits check returns transgressions by wallet,
    // Each could be a min or max, so we have to filter them according to which $constraint we are working with.
    $filterFor = (new \ReflectionClass($constraint))->getShortName() == 'TransactionLimitMinConstraint' ?
      'Drupal\mcapi_limits\MinTransgression' :
      'Drupal\mcapi_limits\MaxTransgresstion';
    $transgressions = array_filter(
      mcapi_limits_check($transaction),
      function ($t) use ($filterFor) {return $t instanceof $filterFor;}
    );

    foreach ($transgressions as $wid => $transgression) {
      // These constraint properties are to send the message outside the site
      // i.e. for CreditCommons\Exceptions
      $constraint->wid = $transgression->wallet->id();
      $constraint->diff = intval($transgression->diff());
      // We can't know the path to the quant field unfortunately, so take a guess
      // @todo make a Function to find out the path
      $path = 'quant';
      $res = $this->context
        ->buildViolation($constraint->message)
        ->setParameter('%wallet', $transgression->wallet->label())
        ->setParameter('%limit', strip_tags(Functions::formatBalance($transgression->limit(), FALSE)))
        ->setParameter('%excess', strip_tags(Functions::formatBalance($constraint->diff, FALSE)))
        ->atPath($path)
        ->addViolation();
    }
  }

}
