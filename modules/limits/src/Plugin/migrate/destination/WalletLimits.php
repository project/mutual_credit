<?php

namespace Drupal\mcapi_limits\Plugin\migrate\destination;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\mcapi\Entity\Wallet;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\Core\Database\Connection;
use Drupal\migrate\Attribute\MigrateDestination;

/**
 * Writes the wallet limits into the wallet's new 'min' and 'max' fields.
 */
#[MigrateDestination(id: 'mc_wallet_limits')]
class WalletLimits extends DestinationBase implements ContainerFactoryPluginInterface{

  protected $supportsRollback = TRUE;

  private $database;

  /**
   * Constructor
   * See parents, plus
   * @param Connection $database
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->database = $database;
  }

  public static function create(ContainerInterface $container, array $config, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static (
      $config,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('database')
    );
  }

  /**
   * {@inheritDoc}
   * A row with one field, $limits, which may have many rows thus
   *  [
        'wid' => $wid,
        'max' => $limits['max'],
        'min' => $limits['min'],
      ];
   */
  public function import(Row $row, array $old_destination_id_values = array()) {
    $limits = $row->getDestinationProperty('limits');
    $wallet = Wallet::load($limits['wid']);
    $wallet->max->value = $limits['max'];
    $wallet->min->value = $limits['min'];
    $wallet->save();
    return [$limits['wid']];
  }

  /**
   * {@inheritDoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    // TODO: Implement fields() method.
    // but what does it do??
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    $ids['entity_id']['type'] = 'integer';
    return $ids;
  }
  /**
   * Returns a specific entity key.
   *
   * @param string $key
   *   The name of the entity key to return.
   *
   * @return string|bool
   *   The entity key, or FALSE if it does not exist.
   *
   * @see \Drupal\Core\Entity\EntityTypeInterface::getKeys()
   */
  protected function getKey($key) {
    return [
      'entity-id' => ['type' => 'integer']
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function rollback(array $destination_identifier) {
    $this->database
      ->delete('mc_wallets__max')
      ->condition('entity_id', $destination_identifier['wid'])
      ->execute();
    $this->database
      ->delete('mc_wallets__min')
      ->condition('entity_id', $destination_identifier['wid'])
      ->execute();
  }


}
