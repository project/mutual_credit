<?php

/**
 * @file
 */
namespace Drupal\mcapi_limits\Plugin\migrate\process;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\user\Entity\User;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert the old display setting into the new one-string format.
 */
#[MigrateProcess(id: 'd7_mc_wallet_limit')]
class WalletLimits extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   *
   * $user->data['limits_personal'][$currcode]['min'] = $min;
   * We convert it to go in its own table with fields
   * ['wid', 'curr_id', 'max', 'value', 'editor', 'date']
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    //Incoming value is nothing.
    //would be the $user->data array from D7, except it doesn't come through because it is an array
    $value = $row->getSourceProperty('data');
    $uid = $row->getSourceProperty('uid');
    if (!$uid) {
      throw new MigrateSkipRowException('No uid source property');
    }
    if (!User::load($uid)) {
      throw new MigrateSkipRowException("User $uid not migrated");
    }
    // Just import all limits because it is very tricky to obatain the most used
    // currency without access to the d7 datbase. Will be fine for 95% of cases.
    if (isset($value['limits_personal'])) {
      foreach ($value['limits_personal'] as $currcode => $limits) {
        foreach (WalletStorage::allWalletIdsOf($uid) as $wid) {
          // Transfers the limits to the user's first wallet only.
          return [
            'wid' => $wid,
            'min' => $limits['min'],
            'max' => $limits['max'],
          ];
        }
      }
    }
    throw new MigrateSkipRowException();
  }

}
