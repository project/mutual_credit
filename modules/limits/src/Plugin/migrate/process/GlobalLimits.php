<?php

namespace Drupal\mcapi_limits\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert the global balance limit settings.
 */
#[MigrateProcess(id: 'd7_mcapi_limits_global')]
class GlobalLimits extends ProcessPluginBase {

  /**
   * $value is an object containing the per-currency limit setings like this [
   *   personal (bool) //whether to allow personal overrides
   *   skip -> [ // circumstances on which to skip checking.
   *     auto (bool) // on transactions of type auto
   *     mass (bool) // on transactions of type mass
   *     user1 (bool)// when user 1 is involved
   *     owner (bool) // when the currency owner is involved
   *     reservoir (bool) // when the reservoir account is involved
   *   ]
   *   limits_callback (string)
   *   limits_global (array) //settings for callback with the same name
   *   limits_equations (array) //settings for callback with the same name
   * ]
   *
   * Maps to [
   *   personal (bool)
   *   skip:
   *     auto (bool)
   *     mass (bool)
   *     user1 (bool)
   *     owner (bool)
   *   prevent (bool)
   * ]
   *
   * $value is the 'limits' property of the old $currency object
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $limits = [];
    switch($value->limits_callback) {
      case 'limits_global':
        if ($value->limits_global['max'] == -$value->limits_global['min']) {
          $limits['plugin'] = 'balanced';
          $limits['plugin_settings']['liquidity'] = $value->limits_global['max'];
        }
        else {
          $limits['plugin'] = 'explicit';
          $limits['plugin_settings']['explicit'] = $value->limits_global; //min and max
        }
        break;

      case 'limits_equations':
        $limits['plugin'] = 'calculated';
        $old = ['@gross_in, @gross_out, @trades'];
        $new = ['@in', '@out', '@num'];
        $limits['plugin_settings']['calculated'] = [
          'max_formula' => str_replace($old, $new, $value->limits_equations['max_formula']),
          'min_formula' => str_replace($old, $new, $value->limits_equations['min_formula'])
        ];
        break;

      case 'limits_none':
        \Drupal::service('module.installer')->uninstall(['mcapi_limits']);
        break;

      default:
        throw new MigrateSkipRowException('Unknown limits plugin on currency '.$row->info['name'] .': Limits not migrated');
    }
    return $limits;
  }

}
