<?php

namespace Drupal\mcapi_limits\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler to show a user's balance limits.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("mcapi_limits")]
class Limits extends FieldPluginBase {

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['absolute'] = ['default' => 'absolute'];
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, $form_state) {
    $form['absolute'] = [
      '#title' => $this->t('Range'),
      '#type' => 'radios',
      '#options' => [
        'absolute' => t('Show min, max and current balance'),
        'relative' => t('Show limits for earning and spending, relative to balance'),
      ],
      '#default_value' => $this->options['absolute'],
      '#weight' => '5',
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    $this->addAdditionalFields();
  }

  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    $wallet = $this->getEntity($values);
    return mcapi_view_limits($wallet);
  }

}
