<?php

namespace Drupal\mcapi_limits\Plugin\Block;

use Drupal\mcapi\Plugin\Block\WalletContextBlockBase;
use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Block\Attribute\Block;

/**
 * Provides a block showing the user's balance limits.
 */
#[Block(
  id: 'mcapi_limits',
  admin_label: new TranslatableMarkup('Balance limits'),
  category: new TranslatableMarkup('Community Accounting')
)]
class BalanceLimits extends WalletContextBlockBase {

  /**
   * {@inheritDoc}
   */
  public function build() {
    foreach ($this->wids as $wid) {
      $wallet = Wallet::load($wid);
      $renderable['w'.$wallet->id()] = mcapi_view_limits($wallet);
    }
    return $renderable;
  }

}
