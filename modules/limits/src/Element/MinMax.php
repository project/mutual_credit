<?php

namespace Drupal\mcapi_limits\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Provides a form element for selecting a transaction state.
 */

#[FormElement('minmax')]
class MinMax extends \Drupal\Core\Render\Element\FormElement {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#tree' => TRUE,
      '#process' => [
        [get_class($this), 'processMinmax'],
      ],
    ];
  }

  /**
   * Processor for minmax element.
   */
  public static function processMinmax($element) {
    $element += [
      '#title' => t('Balance limits'),
      '#description_display' => 'before',
      '#type' => 'container',
      'min' => [
        '#title' => t('Minimum'),
        '#description' => t('Must be less than or equal to zero'),
        '#type' => 'worth',
        '#config' => TRUE,
        '#default_value' => $element['#default_value'] ? abs($element['#default_value']['min']) : NULL,
        '#placeholder' => $element['#placeholder']['min']??'',
        '#minus' => TRUE,
        //'#field_suffix' => '('.t('minus').')'
        '#min' => 0,
      ],
      'max' => [
        '#title' => t('Maximum'),
        '#description' => t('Must be greater than 1.'),
        '#type' => 'worth',
        '#config' => TRUE,
        '#default_value' => $element['#default_value']? $element['#default_value']['max'] : NULL,
        '#placeholder' => $element['#placeholder']['max']??'',
        '#weight' => 1,
        '#min' => 0
      ],
    ];
    return $element;
  }


  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input) {
      if (empty($input['min']['main']) and empty($input['min']['subdivision'])) {

      }
      //$input['min']['main'] = intval($input['min']['main']) * -1;
      return $input;
    }
  }



}
