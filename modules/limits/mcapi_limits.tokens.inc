<?php

use Drupal\mcapi\Functions;
use Drupal\Core\Render\BubbleableMetadata;


/**
 * Implements hook_tokens().
 *
 * Adds a new token enabling passing to 2 wallet params to the mail.
 */
function mcapi_limits_token_info() {
  return [
    'types' => [
      'transgression' => [
        'name' => t('Transaction limit transgression'),
        'description' => t('Data pertaining to a transgressed balance limit for a wallet.'),
        'type' => 'transgression'
      ]
    ],
    'tokens' => [
      'transgression' => [
        'diff' => [
          'name' => t('Overshoot'),
          'description' => t('Difference between the balance limit and the projected balance.')
        ],
        'balance' => [
          'name' => t('Balance'),
          'description' => t('Current balance in the wallet')
        ],
        'limit' => [
          'name' => t('Limit'),
          'description' => t('The wallet limit for that currency')
        ],
        'threshhold' => [
          'name' => t('Threshhold'),
          'description' => t('The fraction of the limit which triggers the warning')
        ],
        'wallet' => [
          'name' => t('Wallet'),
          'type' => 'mc_wallet',
        ],
        'transaction' => [
          'name' => t('Transaction as a sentence'),
          'type' => 'mc_transaction',
        ]
      ]
    ]
  ];
}

/**
 * Implements hook_tokens().
 */
function mcapi_limits_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'transgression' and isset ($data['transgression'])) {
    $transgression = $data['transgression'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'diff':
          $replacements[$original] = Functions::formatBalance($transgression->diff());
          break;
        case 'limit':
          $replacements[$original] = Functions::formatBalance($transgression->limit());
          break;
        case 'balance':
          $replacements[$original] = Functions::formatBalance($transgression->balance());
          break;
        case 'threshhold':
          $threshhold = \Drupal::config('mcapi_limits.settings')->getSetting('warning.threshhold');
          $replacements[$original] = $threshhold * 100 . '%';
          break;
      }
    }
    $token_service = \Drupal::token();
    if ($wallet_tokens = $token_service->findWithPrefix($tokens, 'wallet')) {
      $replacements += $token_service->generate(
        'mc_wallet',
        $wallet_tokens,
        ['mc_wallet' => $transgression->wallet],
        $options,
        $bubbleable_metadata
      );
    }
    if ($transaction_tokens = $token_service->findWithPrefix($tokens, 'transaction')) {
      $replacements += $token_service->generate(
        'mc_transaction',
        $transaction_tokens,
        ['mc_transaction' => $transgression->wallet],
        $options,
        $bubbleable_metadata
      );
    }
  }
  return $replacements;
}