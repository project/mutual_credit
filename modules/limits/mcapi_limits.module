<?php

/**
 * @file
 * Hook functions for mcapi_limits module.
 *
 * @todo check using pending balances by default.
 */
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi_limits\Transgression;
use Drupal\mcapi_limits\MaxTransgression;
use Drupal\mcapi_limits\MinTransgression;
use Drupal\Core\Access\AccessResult;

const MCAPI_LIMIT_MIN = 0;
const MCAPI_LIMIT_MAX = 1;

/**
 * Implements hook_help().
 */
function mcapi_limits_help($route_name, $route_match) {
  if ($route_name == 'mcapi_limits.settings') {
    $help[] = '<p>'.t('Balance limits are very important in defining the type of currency and locating the default risk with the right party.').'</p>';
    $help[] = '<p>'.t('LETS and strict mutual credit').'</p><p>';
    $help[] = t('ALL accounts must stay within a tolerable range of zero.');
    $help[] = t('There is no acccount for issuing, but you may have an admin account with a wider tolerance.');
    $help[] = t('Use the Balanced limits plugin');
    $help[] = '<p>'.t('Time banking and and soft mutual credit').'</p><p>';
    $help[] = t('Timebanks usually both issue hours as acknowledgement to volunteers and allow members to transfer hours amongst themselves.');
    $help[] = t('Average members therefore accrue in the long term, more hours than they can spend.');
    $help[] = t('Some members may also be allowed negative balances.');
    $help[] = t('If you need balance limits at all, use the explicit plugin and then make exceptions per member.');
    $help[] = '<p>'.t('Backed currencies').'</p><p>';
    $help[] = t('When a currency is backed, that means there is one issuing account which is negative, and for every unit issued there must be an equivalent asset in a warehouse or bank account.');
    $help[] = t("On this system you need one account with a max balance of zero, and all other accounts have a min balance of zero.");
    $help[] = t('Then when someone buys a voucher you pay into their account from the first account.');
    return implode(' ', $help);
  }
}

/**
 * Implements hook_entity_type_alter().
 *
 * Append the limits constraint to the transaction constraints
 */
function mcapi_limits_entity_type_alter(array &$entity_types) {
  if (!empty($entity_types['mc_transaction'])) {// may not be during installation
    $entity_types['mc_transaction']->addConstraint('limits_max', []);
    $entity_types['mc_transaction']->addConstraint('limits_min', []);
  }
}

/**
 * Implements hook_theme().
 */
function mcapi_limits_theme($existing, $type, $theme, $path) {
  foreach (['mcapi_limits_absolute', 'mcapi_limits_balanced'] as $callback) {
    $hooks[$callback] = [
      'variables' => [
        'wallet' => NULL
      ]
    ];
  }
  return $hooks;
}

/**
 * Implements hook_entity_extra_field_info().
 */
function mcapi_limits_entity_extra_field_info() {
  return [
    'mc_wallet' => [
      'mc_wallet' => [
        'display' => [
          'balance_limits' => [
            'label' => t('Balance limits'),
            'weight' => 0,
          ],
        ],
      ],
    ],
  ];
}

/**
 * function follows the naming convention setup in WalletViewBuider().
 * @return array
 */
function mc_wallet_view_balance_limits($wallet) :array {
  return mcapi_view_limits($wallet);
}

/**
 * Show a wallets limits.
 *
 * Used in hook_wallet_view, views, & blocks.
 *
 * @param WalletInterface $wallet
 *   The wallet.
 *
 * @return array
 *   A renderable array.
 */
function mcapi_view_limits(WalletInterface $wallet) {
  $max = $wallet->max->limit();
  if ($max && (-$max == $wallet->min->limit())) {
    $theme = 'mcapi_limits_balanced';
  }
  else {
    $theme = 'mcapi_limits_absolute';
  }
  $renderable = [
    '#theme' => $theme,
    '#wallet' => $wallet
  ];
  return $renderable;
}

/**
 * Implements hook_entity_field_access().
 * Ensure only admins can edit the wallet field.
 */
function mcapi_limits_entity_field_access($operation, $field_definition, $account, $items = NULL) {
  $result = AccessResult::neutral();
  if ($field_definition->getType() =='mc_wallet_limits' and $operation == 'edit') {
    $result = AccessResult::forbiddenIf(!$account->hasPermission('manage mcapi'))->cachePerUser();
  }
  return $result;
}

/**
 * Implements hook_mail().
 *
 * Params are exceeded_wallet, partner_wallet, message. Token types
 * are user current-user, recipient, exceeded-wallet.
 */
function mcapi_limits_mail($key, &$message, $params) {
  if ($key == 'warning') {
    $template = \Drupal::config('mcapi_limits.settings')->get('notifications.warning_mail');
    $params['xaction'] = $params['transgression']->transaction;
    $message['subject'] = \Drupal::token()->replace($template['subject'], $params);
    $message['body'][] = \Drupal::token()->replace($template['body'], $params);
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 * Email credit limit warnings to 2ndparty
 */
function mcapi_limits_mc_transaction_insert($transaction) {
  // produce a warning
  $threshhold_val = \Drupal::config('mcapi_limits.settings')->get('warning.threshhold') ?? 100;
  if ($threshhold_val < 100) {
    foreach (mcapi_limits_check($transaction, $threshhold_val) as $transgression) {
      $transgression->warn();
    }
  }
}

/**
 * Check if a transaction takes its participants over their balance limits.
 * @param Transaction $transaction
 * @param int $percent_of_limit
 * @return Transgression[]
 *   Keyed with min, max
 */
function mcapi_limits_check(Transaction $transaction, int $percent_of_limit = 0) : array {
  $transgressions = [];
  $warning_threshhold = 1 - $percent_of_limit/100;
  foreach (mcapi_limits_transaction_diffs($transaction) as $wid => $amount) {
    $wallet = Wallet::load($wid);
    // Pending transactions count here.
    $current_balance = $wallet->balance;
    $projected = $current_balance + $amount;
    if ($amount > 0 && $projected > 0) {
      $max = $wallet->max->limit();
      if (is_numeric($max) && $projected > $max * $warning_threshhold) {
        $transgressions[] = MaxTransgression::create($transaction, $wallet, $projected);
      }
    }
    elseif ($amount < 0 && $projected < 0) {
      $min = $wallet->min->limit();
      if (is_numeric($min) && $projected < $min * $warning_threshhold) {
        $transgressions[] = MinTransgression::create($transaction, $wallet, $projected);
      }
    }
  }
  return $transgressions;
}

/**
 * Determine the balance changes to every wallet involved.
 *
 * @param Transaction $transaction
 * @return array
 *   Differences in Balance keyed by wallet id.
 */
function mcapi_limits_transaction_diffs(Transaction $transaction) :array {
  static $diffs = [];
  if (!$diffs) {
    $raw_diffs = [];
    foreach ($transaction->entries->getValue() as $entry) {
      $raw_diffs[$entry['payee_wid']][] = +$entry['quant'];
      $raw_diffs[$entry['payer_wid']][] = -$entry['quant'];
    }
    foreach ($raw_diffs as $wid => $amounts) {
      $diffs[$wid] = array_sum($amounts);
    }
  }
  return $diffs;
}

/**
 * Preprocessor function for theme callback mcapi_limits_absolute.
 */
function template_preprocess_mcapi_limits_absolute(&$vars) {
  $formatter = \Drupal::Service('mcapi.currency_formatter');
  $min = $vars['wallet']->min->limit();
  $vars['minformatted'] = is_null($min) ? [] : ['#markup' =>  $formatter->format($min)];
  $vars['min'] = is_null($min) ? NULL : $formatter->formatAsFloat($min);

  $max = $vars['wallet']->max->limit();
  $vars['maxformatted'] = is_null($max) ? [] : ['#markup' => $formatter->format($max)];
  $vars['max'] = is_null($max) ? NULL : $formatter->formatAsFloat($max);

  $raw_balance = $vars['wallet']->balance;
  $vars['balanceformatted'] = ['#markup' => $formatter->format($raw_balance)];
  $vars['balance'] =  $formatter->formatAsFloat($raw_balance);
}

/**
 * Preprocessor function for theme callback mcapi_limits_balanced.
 *
 * By design this can only be used if the max and min are equidistant from zero
 * Also because of limitations of the google charts gauge visualisation,
 * this can only be used if the formatted value is numeric.
 */
function template_preprocess_mcapi_limits_balanced(&$vars) {
  $formatter = \Drupal::Service('mcapi.currency_formatter');
  // The google gauge cannot mask the real number with a formatted value.
  $min = $vars['wallet']->min->limit();
  $max = $vars['wallet']->max->limit();
  $vars['min'] =  $formatter->formatAsFloat($min);
  $vars['max'] = $formatter->formatAsFloat($max);
  $vars['balance'] =  $formatter->formatAsFloat($vars['wallet']->balance);
  $vars['minformatted'] = ['#markup' =>  $formatter->format($min)];
  $vars['maxformatted'] = ['#markup' =>  $formatter->format($max)];
}


/**
 * Implements hook_form_FORM_ID_alter().
 * Prevent changing the cardinality of the limits field.
 * @deprecated
 */
function mcapi_limits_form_field_storage_config_edit_form_alter(&$form, $form_state) {
  $id = $form_state->getFormObject()->getEntity()->id();
  if ($id == 'mc_wallet.min' or $id == 'mc_wallet.max') {
    $form['cardinality_container']['cardinality']['#disabled'] = TRUE;
    unset($form['cardinality_container']['cardinality_number']['#states']);
    $form['cardinality_container']['cardinality_number']['#disabled'] = TRUE;// Seems to be overridden later.
    $form['actions']['submit']['#disabled'] = TRUE;
  }
}
