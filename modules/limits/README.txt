The limits module allows global min and max wallet balance limits determined by a plugin, to be overridden by absolute limits on each wallet.
Limits can be ignored in certain circumstances.
When the limits are transgressed, the transaction will be blocked.
A warning threshhold can also trigger an email to the wallet owner. 
A theme callback is provided for showing the wallet balance and the min & max limits.
Three plugins are provided for calculating the balance limits.
 - Calculated allows you to put in a formula
 - Explicit means you just enter the max and min into the currency
 - Balanced means that the min is the max * -1