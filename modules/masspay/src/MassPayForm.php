<?php

namespace Drupal\mcapi_mass;

use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for multiple payments between one and many wallets.
 */
class MassPayForm extends ContentEntityForm {

  const MASSINCLUDE = 0;
  const MASSEXCLUDE = 1;

  /**
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Either '12many' or 'many21'
   * @var string
   */
  protected $side;

  /**
   * Mail template
   * @var array
   */
  protected $configfactory;

  /**
   * The payer and payee field names.
   * @var string
   */
  protected $manyfieldName;
  protected $onefieldName;

  /**
   * The step of the form. either 1 or 2.
   * @var int
   */
  protected $step;

  /**
   * the logger object
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   *
   * @param $entity_repository
   * @param $entity_type_bundle_info
   * @param $time
   * @param $mail_manager
   * @param $route_match
   * @param $config_factory
   * @param $logger
   * @param Connection $database
   */
  public function __construct($entity_repository, $entity_type_bundle_info, $time, MailManagerInterface $mail_manager, RouteMatchInterface $route_match, ConfigFactoryInterface $config_factory, LoggerChannel $logger, Connection $database) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->mailManager = $mail_manager;
    $this->configFactory = $config_factory;
    $this->logger = $logger;

    $this->side = $route_match->getRouteObject()->getOption('side');
    if ($this->side == '12many') {
      $this->onefieldName = 'payer_wid';
      $this->manyfieldName = 'payee_wid';
    }
    else {
      $this->onefieldName = 'payee_wid';
      $this->manyfieldName = 'payer_wid';
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.mail'),
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('logger.channel.mcapi'),
      $container->get('database')
    );
  }

  /**
   * Provide an ID for the transaction form consistent with the keys of hook_transction_form_list().
   */
  function transactionFormId() {
    return 'mass_'.$this->side;
  }


  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $this->init($form_state);

    if (!$this->step) {
      $this->step = 1;
      $this->step1($form, $form_state);
    }
    else {
      $this->step = 2;
      $form_state->setValidationComplete();
      $form['preview'][] = $this->entityTypeManager
        ->getViewBuilder('mc_transaction')
        ->view($this->entity, 'transition');
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    if ($this->step == 1) {
      $element['submit']['#value'] = $this->t('Preview');
    }
    else {
      $element['submit']['#value'] = $this->t('Confirm');
    }
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function step1(array &$form, FormStateInterface $form_state) {
    $display = $this->getFormDisplay($form_state);
    $form['#parents'] = [];
    $form['entries'] = $display->getRenderer('entries')
      ->form($this->entity->get('entries'), $form, $form_state);
    $element = &$form['entries']['widget'][0];
    $element[$this->manyfieldName]['#title'] = $this->manyfieldName == 'payer_wid' ? t('Payers') : t('Payees');
    $element[$this->manyfieldName]['#description'] = $this->t('Max 1000 chars');
    $element[$this->manyfieldName]['#maxlength'] = 1000;
    $element[$this->manyfieldName]['#tags'] = TRUE;
    $element[$this->manyfieldName]['#pattern'] = '([a-zA-Z \-]*\([0-9]+\))(,\s*[a-zA-Z \-]*\([0-9]+\))*';

    $element['invert'] = [
      '#type' => 'radios',
      '#options' => [
        SELF::MASSINCLUDE => $this->t('The named wallets'),
        SELF::MASSEXCLUDE => $this->t("All wallets except those named"),
      ],
      '#default_value' => SELF::MASSINCLUDE,
      '#weight' => 7
    ];

    $element[$this->onefieldName]['#weight'] = 6;
    $element[$this->manyfieldName]['#weight'] = 8;
    // Some cosmetic alterations
    if ($this->side == '12many') {
      $element[$this->onefieldName]['#title'] = $this->t('The one payer');
      $element['invert']['#title'] = $this->t('Will pay');
    }
    else {
      $element[$this->onefieldName]['#title'] = $this->t('The one payee');
      $element['invert']['#title'] = $this->t('Will receive from');
    }
    // TheMany field is required if the invert field is set to include
    $element[$this->manyfieldName]['#states'] = [
      'required' => [':input[name="entries[invert]"]' => ['value' => 0]]
    ];

    $mail_setting  = $this->configFactory->get('mcapi_mass.settings')->get('last_mail');
    //@todo use rules for this
    $form['notification'] = [
      '#title' => $this->t('Notify all parties', [], array('context' => 'accounting')),
      // @todo decide whether to put rules in a different module
      '#description' => $this->moduleHandler->moduleExists('rules') ?
      $this->t('N.B. Ensure this mail does not clash with mails sent by the rules module.') : '',
      '#type' => 'fieldset',
      '#open' => TRUE,
      '#weight' => 20,
      'subject' => [
        '#title' => $this->t('Subject'),
        '#type' => 'textfield',
        // This needs to be stored per-exchange.
        '#default_value' => $mail_setting['subject'],
      ],
      'body' => [
        '#title' => $this->t('Message'),
        // @todo the tokens?
        '#description' => $this->t('The following pseudo-tokens are available: @tokens', ['@tokens' => '[user:name], [xaction:quant], [xaction:description], [xaction:creator]']),
        '#type' => 'textarea',
        '#default_value' => $mail_setting['body'],
        '#weight' => 1,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (($triggering_element = $form_state->getTriggeringElement()) && isset($triggering_element['#ajax']['callback'])) {
      return;
    }
    if ($this->step == 1) {
      if ($form_state->getValue(['entries', 0, 'invert']) == Self::MASSINCLUDE and !$form_state->getValue(['entries', 0, $this->manyfieldName])) {
        $form_state->setError(
          $form['entries']['widget'][0][$this->manyfieldName],
          $this->t("'@field' is required", ['@field' => $form['entries']['widget'][0][$this->manyfieldName]['#title']])
        );
      }

      // Unlike normal one-step entity forms, save the entiry here for step 2
      $this->entity = parent::validateForm($form, $form_state);
      // And save the mail
      $form_state->set('mail', [
        'subject' => $form_state->getValue('subject'),
        'body' => $form_state->getValue('body')
      ]);
      // catch the fieldable errors.
     //if (!$form_state->getErrors()) {
        // Reload the form for the confirmation page.
        $form_state->setRebuild(TRUE); // move to step 2
      //}
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $data = $form_state->getValue(['entries', 0]);
    $theMany = array_map(
      fn ($v) => $v['target_id'],
      $data[$this->manyfieldName]
    );

    if ($data['invert'] == SELF::MASSEXCLUDE) {
      $all_wids = \Drupal::entityQuery('mc_wallet')->accessCheck(TRUE)->execute();
      $theMany = array_diff($all_wids, $theMany, [$data[$this->onefieldName]]);
    }
    $form_state->set('themany', $theMany); // used for notification.
    foreach ($theMany as $wid) {
      $entries[] = [
        $this->onefieldName => $data[$this->onefieldName],
        $this->manyfieldName => $wid,
        'description' => $data['description'],
        'quant' => $data['quant'],
      ];
    }
    $properties = [
      'workflow' => '0|C',
      'entries' => $entries
    ];
    $entity = Transaction::create($properties);
    return $entity;
  }


  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //NOT calling the parent because we don't want to build the entity again
    $form_state->cleanValues();
    $this->updateChangedTime($this->entity);
    $this->configFactory->getEditable('mcapi_mass.settings')
      ->set('last_mail', $form_state->get('mail'))
      ->save();
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->saveVersion($this->entity->workflow->entity->startState());
    // @todo send in a batch
    $params = $form_state->get('mail');

    if ($params['subject'] and $params['body']) {
      $this->notifyMany($form_state->get('themany'));
    }
    // Go to the canonical transaction page
    $form_state->setRedirect(
      'entity.mc_transaction.canonical',
      ['mc_transaction' => $this->entity->id()]
    );

    $this->logger->notice(
      'User @uid created a <a href=":link">mass transaction</a> with @num entries.',
      [
        '@uid' => $this->currentUser()->id(),
        '@num' => count($this->entity->entries),
        ':link' => $this->entity->toUrl()->toString()
      ]
    );
  }

    /**
   * {@inheritDoc}
   * @todo make this work for passing transaction properties.
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $properties = [
      'workflow' => '0|C'
    ];
    return Transaction::create($properties);
  }

  /**
   * @todo make this play with the TransactionSubscriber.
   */
  function notifyMany(array $wids) {
    foreach (array_chunk($wids, 50) as $chunk) {
      foreach (Wallet::loadMultiple($chunk) as $wallet) {
        $uids[] = $wallet->getOwnerId();
      }
    }
    $uids = array_unique($uids);
    $batch_builder = (new BatchBuilder())
      ->setTitle(t('Notifying @count users', ['@count' => count($uids)]));
    $txid = $this->entity->id();
    foreach (array_chunk($uids, 50) as $batched_uids) {
      $batch_builder->addOperation('mass_pay_mail', [$batched_uids, $txid]);
    }
    batch_set($batch_builder->toArray());
  }

}
