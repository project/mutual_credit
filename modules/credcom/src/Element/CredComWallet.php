<?php

namespace Drupal\mcapi_cc\Element;

use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\mcapi\Element\WalletAutocomplete;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Element to choose local or remote wallets with autocomplete.
 */
#[FormElement('credcom_wallet')]
class CredComWallet extends \Drupal\Core\Render\Element\Textfield {

  const CREDCOM_WALLET_WIDGET_EITHER = 'either';
  const CREDCOM_WALLET_WIDGET_REMOTE = 'remote';
  const CREDCOM_WALLET_WIDGET_LOCAL = 'local';
  const REGEX = '([a-zA-Z0-9]+\/)?[a-zA-Z0-9]+\/[a-zA-Z0-9\-]+';
  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    // This callback covers remote wallets only.
    $info['#mode'] = self::CREDCOM_WALLET_WIDGET_EITHER;
    $info['#element_validate'] = [[static::class, 'validate']];
    array_unshift($info['#process'], [static::class, 'determineMode']);
    return $info;
  }

  /**
   * Just as in Drupal\Core\Entity\Element\EntityAutocomplete we use the validate
   * callback to set the final value, so that the given value is still present
   * when the form has to be rebuilt.
   *
   * If the #value has a slash then this is a remote wallet which is not
   * validated but  replaced by the credcom wallet id, otherwise take the number
   * from the brackets.
   */
  public static function validate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (strpos($element['#value'], '/') !== FALSE) {// Remote wallet
      $wid = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
      $field_id = end($element['#parents']);
      // There is no 'entries' value yet, so this must be merged. See
      $form_state->set('remoteMetadata', [$wid => $element['#value']]);
      $form_state->setValueForElement($element, $wid);
    }
    else {
      // Identify a local wallet from the wallet id in brackets
      if(preg_match('/\(([0-9]+)\)/', $element['#value'], $matches))  {
        $wid = $matches[1];
      }
      // Or from the wallet id
      elseif (is_numeric($element['#value'])) {
        $wid = $element['#value'];
      }
      // And set the set the form_state value
      if (isset($wid) and Wallet::load($wid)) {
        $form_state->setValueForElement($element, $wid);
      }
      else {
        $form_state->setError($element, t('Unable to identify wallet %id', ['%id' => $element['#value']]));
      }
    }
  }

  /**
   * Element process callback. Must run before processAutocomplete.
   */
  static function determineMode(&$element, FormStateInterface $form_state, &$complete_form) {
    // Only do something if the trunkward id is set.
    if ($element['#mode'] == self::CREDCOM_WALLET_WIDGET_EITHER) {
      // @see Drupal\mcapi_cc\Controller::allNames().
      $element['#autocomplete_route_name'] = 'mcapi_cc.wallet.autocomplete';
      $element['#placeholder'] = t('A local wallet name or remote wallet path');
      $element['#pattern'] = SELF::REGEX.'|'. WalletAutocomplete::REGEX;
      // Remote wallets MUST have a slash the address and no spaces.
    }
    elseif ($element['#mode'] == self::CREDCOM_WALLET_WIDGET_REMOTE) {
      // @see Drupal\mcapi_cc\Controller::RemoteNames().
      $element['#autocomplete_route_name'] = 'mcapi_cc.wallet.remote_autocomplete';
      $element['#placeholder'] = t('A remote wallet path');
      $element['#pattern'] = SELF::REGEX;
    }
    $complete_form['#entity_builders'][] = ['\Drupal\mcapi_cc\Functions', 'buildRemoteTransaction'];
    return $element;
  }
}
