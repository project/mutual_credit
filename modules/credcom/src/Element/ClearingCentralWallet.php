<?php

namespace Drupal\mcapi_cc\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Element to choose wallets from clearingcentral
 * @deprecated
 */
#[FormElement('clearcent_wallet')]
class ClearingCentralWallet extends \Drupal\Core\Render\Element\Select {

  CONST EXCHANGES = [
    'cen1573' => 'cforge/abrayliens',
    'cen1163' => 'cforge/amay',
    'cen1536' => 'cforge/arcan',
    'cen1118' => 'cforge/ardenne',
    'cen1468' => 'cforge/athens',
    'cen2240' => 'cforge/atoutsel',
    'cen1494' => 'cforge/bastides',
    'cen1475' => 'cforge/beaugensel',
    'cen1543' => 'cforge/beaurepaire',
    'cen1701' => 'cforge/belmont',
    'cen1990' => 'cforge/bigoudsel',
    'cen1474' => 'cforge/biotherapeutes',
    'cen2084' => 'cforge/boutsdephi',
    'cen1512' => 'cforge/calade',
    'cen1352' => 'cforge/chania',
    'cen1714' => 'cforge/chauxdefonds',
    'cen1478' => 'cforge/cirosel',
    'cen1480' => 'cforge/citron',
    'cen1574' => 'cforge/clunisois',
    'cen2168' => 'cforge/cotedargent',
    'cen1614' => 'cforge/cotesaintandre',
    'cen1489' => 'cforge/csrfv',
    'cen1967' => 'cforge/demo',
    'cen1788' => 'cforge/dionys',
    'cen1473' => 'cforge/durbuy',
    'cen1502' => 'cforge/echoduvelay',
    'cen1548' => 'cforge/endurance',
    'cen1440' => 'cforge/entramis',
    'cen1198' => 'cforge/episel',
    'cen1485' => 'cforge/ethik',
    'cen1598' => 'cforge/fontaine',
    'cen1555' => 'cforge/franches',
    'cen1736' => 'cforge/galetdubuech',
    'cen2044' => 'cforge/gascogne',
    'cen1558' => 'cforge/gedisel',
    'cen1787' => 'cforge/genevois',
    'cen1126' => 'cforge/gestionint',
    'cen1185' => 'cforge/grain2sel',
    'cen1586' => 'cforge/grandesterres',
    'cen2167' => 'cforge/grandslacs',
    'cen1184' => 'cforge/habitats',
    'cen1181' => 'cforge/hautgiffre',
    'cen1472' => 'cforge/hb',
    'cen1527' => 'cforge/htglobal',
    'cen1806' => 'cforge/icietailleurs',
    'cen1634' => 'cforge/jallieu',
    'cen1556' => 'cforge/jeu',
    'cen1441' => 'cforge/loire',
    'cen1554' => 'cforge/manna',
    'cen1166' => 'cforge/marchebe',
    'cen1562' => 'cforge/mars',
    'cen2018' => 'cforge/marseille',
    'cen1503' => 'cforge/marsel',
    'cen1585' => 'cforge/mion',
    'cen1511' => 'cforge/mortuasel',
    'cen1194' => 'cforge/nantes',
    'cen1988' => 'cforge/nsw',
    'cen1584' => 'cforge/ondaine',
    'cen1671' => 'cforge/outreforet',
    'cen1119' => 'cforge/powaimes',
    'cen1726' => 'cforge/rds',
    'cen1623' => 'cforge/saintebaume',
    'cen2013' => 'cforge/saintmedard',
    'cen1581' => 'cforge/sansfrontiere',
    'cen1488' => 'cforge/sapins',
    'cen2155' => 'cforge/secluses',
    'cen1495' => 'cforge/sel2mers',
    'cen1133' => 'cforge/selalt',
    'cen1617' => 'cforge/selastuce',
    'cen2245' => 'cforge/selauray56',
    'cen2277' => 'cforge/selbressan',
    'cen2269' => 'cforge/selcocagne',
    'cen1535' => 'cforge/seldmer',
    'cen1545' => 'cforge/seldulac',
    'cen2286' => 'cforge/seldumaconnais 	',
    'cen1727' => 'cforge/seleil',
    'cen2215' => 'cforge/selenjoue',
    'cen2068' => 'cforge/selensemble',
    'cen2265' => 'cforge/selensemblech',
    'cen1557' => 'cforge/seleri',
    'cen1199' => 'cforge/selidaire',
    'cen1510' => 'cforge/seljogne',
    'cen2220' => 'cforge/selpaysdevannes',
    'cen1534' => 'cforge/selstseb',
    'cen1501' => 'cforge/solisel',
    'cen1544' => 'cforge/souri',
    'cen1143' => 'cforge/swapengo',
    'cen2142' => 'cforge/thomery',
    'cen1804' => 'cforge/tournuge',
    'cen2085' => 'cforge/troqueursdelin',
    'cen2010' => 'cforge/tutos',
    'cen1533' => 'cforge/unisverssel',
    'cen1899' => 'cforge/valmont',
    'cen1630' => 'cforge/venir49',
    'cen2022' => 'cforge/verdon',
    'cen1624' => 'cforge/villejuif',
    'cen1453' => 'ices/hera',
    'cen0008' => 'ces/MCGR'
  ];


  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    if (empty($element['#default_value'])) {
      $element['#default_value'] = 'cforge/';
    }
    return [
      '#title' => t('The remote group to which the other party belongs.'),
      '#options' => array_combine(self::EXCHANGES, self::EXCHANGES),
      '#placeholder' => 'server/group/member',
    ] + $element;
  }

}
