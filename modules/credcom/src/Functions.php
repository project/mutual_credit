<?php

namespace Drupal\mcapi_cc;

use CreditCommons\Exceptions\CCFailure;
use CreditCommons\Exceptions\CCError;
use CreditCommons\Exceptions\DoesNotExistViolation;
use CreditCommons\Exceptions\CCViolation;
use CreditCommons\Exceptions\CCOtherViolation;
use CreditCommons\Exceptions\MinLimitViolation;
use CreditCommons\Exceptions\MaxLimitViolation;
use CreditCommons\Exceptions\SameAccountViolation;
use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Entity\Workflow as McWorkflow;
use Drupal\mcapi_forms\Entity\McForm;
use Drupal\mcapi_cc\CreditCommons\TrunkwardAccount;
use Drupal\Core\Render\Markup;
use Drupal\Core\Entity\EntityConstraintViolationList;
use GuzzleHttp\Client;


/**
 * Access check for requests from trunkwards nodes.
 */
class Functions {

  static $remoteAccPath;

  /**
   * @param Transaction $transaction
   * @return bool
   *   TRUE if the balance of trade wallet is used in the transaction.
   */
  static function isRemote(Transaction $transaction) : bool {
    $credcom_wid = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
    return $transaction->involves($credcom_wid);
  }


  /**
   * Get the wallet ID from the credcom path.
   * @param string $cc_path
   *   path of an account provided by the trunkward node, may have dashes for spaces.
   * @return int
   */
  static function convertCCaddressToWalletId(string $cc_path, \stdClass &$metadata) : int {
    $cc_path = str_replace('-', ' ',$cc_path);
    $local_acc_name = static::credcomPathIsLocal($cc_path);
    if (!$local_acc_name) {
      $wid = (int)\Drupal::config('mcapi_cc.settings')->get('wallet_id');
      $metadata->{$wid} = $cc_path;
    }
    elseif (!isset($wid) and is_numeric($local_acc_name)) {
      // Assuming its not a float!
      $wid = intval($local_acc_name);
    }
    if (!isset($wid)){
      $entityTypeManager = \Drupal::entityTypeManager();
      // Wallet name
      $wids = $entityTypeManager->getStorage('mc_wallet')
        ->getQuery()->condition('name', $local_acc_name)
        ->accessCheck(FALSE)
        ->execute();
      if ($wids) {
        $wid = reset($wids);
      }
    }
    if (!isset($wid)) {
      // User email address
      if (filter_var($local_acc_name, FILTER_VALIDATE_EMAIL)) {
        $users = $entityTypeManager->getStorage('user')->loadByProperties(['mail' => $local_acc_name]);
        $user = reset($users);
      }
      // User name
      else {
        $uids = $entityTypeManager->getStorage('user')
          ->getQuery()->condition('name', $local_acc_name)
          ->accessCheck(FALSE)
          ->execute();
        if ($uids) {
          $user = \Drupal\user\Entity\User::load(reset($uids));
        }
      }
      if (isset($user)) {
        $wallets = WalletStorage::allWalletIdsOf($user->id());
        if ($wallets) {
          $wid = reset($wallets)->id();
        }
      }
    }
    if (!isset($wid)) {
      throw new DoesNotExistViolation(id: $cc_path, type: 'account');
    }
    return $wid;
  }

  /**
   * Utility, check if a credcom address refers to a local account, and if so
   * return the localised account name.
   *
   * @param string $cc_path
   * @return string
   */
  static function credcomPathIsLocal(string $cc_path) : string {
    $conf = \Drupal::config('mcapi_cc.settings');
    $local_name = '';
    $parts = explode('/', $cc_path);
    $pos = array_search($conf->get('node_name'), $parts);
    if ($pos !== FALSE) {
      $parts = array_slice($parts, $pos+1);
    }
    if (count($parts) == 1) {// Must be the intertrading wallet
      $first_part = reset($parts);
      if ($conf->get('trunkward.id') <> $first_part) {
        $local_name = $first_part;
      }
    }
    return $local_name;
  }

  /**
   * Exception handler to return credit commons errors to the network
   * Consider replacing with a class that extends Drupal\Core\Exception\ExceptionHandler
   * @param \Throwable $ex
   */
  static function CredcomExceptionHandler(\Throwable $ex) {
    header('Content-Type: application/json');
    $err = CCError::convertException($ex);
    if ($err instanceof \CreditCommons\Exceptions\TransactionLimitViolation) {
      $err->diff *= \Drupal::config('mcapi_cc.settings')->get('trunkward.rate');
    }
    if ($err instanceOf \CreditCommons\Exceptions\CCViolation) {
      header('HTTP/1.1: 400 Client error');
    }
    else {
      header('HTTP/1.1: 500 Server error');
    }
    \Drupal::service('logger.channel.credcom')->error(
      'CredcomExceptionHandler: <pre>@message</pre>',
      ['@message' => print_r($err, 1)]
    );
    print json_encode(['errors' => [$err]]);
    exit; //exceptionhandler
  }

  /**
   * Error handler to return credit commons errors to the network
   * @param \Throwable $ex
   */
  static function CredcomErrorHandler($severity, $message, $filename, $lineno) {
    $trace = array_slice(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 1);
    if ($severity > 8000) {// deprecation messages
      \Drupal::service('logger.channel.credcom')
      ->warning(
        'Deprecated: @message\n@break\n@trace',
        ['@message' => $message, '@break'=> "$filename:$lineno", '@trace' => Markup::Create('<pre>'.print_r($trace, 1).'</pre>')]
      );
    }
    else {
      $err = new CCFailure($message);
      $err->trace = $trace;
      $err->break = $filename . ':'. $lineno;
      \Drupal::service('logger.channel.credcom')
        ->error('CredcomErrorHandler: @message', ['@message' =>  Markup::create('<pre>'.print_r($err, 1).'</pre>')]);
      header('Content-Type: application/json');
      header('HTTP/1.1: 500 Server error');
      print json_encode(['errors' => [$err]], 500);
      exit; //Errorhandler
    }
  }

  /**
   * Edit the ledger and change the transaction workflows retrospectively
   * @param string $old_id
   * @param string $new_id
   */
  public static function updateTransactionTypes(string $old_id, string $new_id) {
    \Drupal::database()->update('mc_transaction')
      ->fields(['workflow' => $new_id])
      ->condition('workflow', $old_id)
      ->execute();
    \Drupal::database()->update('mc_transactions_index')
      ->fields(['workflow' => $new_id])
      ->condition('workflow', $old_id)
      ->execute();
    \Drupal::service('logger.channel.credcom')->notice(
      'Edited the ledger changing transaction workflow @old for @new',
      ['@old' => $old_id, '@new' => $new_id]
    );
    if (class_exists('Drupal\mcapi_forms\Entity\McForm')) {
      foreach (McForm::loadMultiple() as $id => $mc_form) {
        if ($mc_form->getworkflowId() == $old_id) {
          $mc_form->setWorkflowId($new_id)->save();
        }
      }
    }
  }

  /**
   * Change the name of a local transaction workflow
   * @param Workflow $wf
   *
   * @todo this could be moved to TrunkwardNodeRequester?
   */
  public static function localiseTransactionTypeName(McWorkflow $wf) {
    $newWorkflow = clone($wf);
    $newWorkflow->set('id', $wf->id() . '_locl');
    $newWorkflow->save();
    $wf->delete();
    \Drupal::service('logger.channel.credcom')->notice(
      'replaced transaction type @old with @new',
      ['@old' => $wf->id(), '@new' => $newWorkflow->id()]
    );
    static::updateTransactionTypes($wf->id(), $newWorkflow->id());
  }

  /**
   * @param Transaction $transaction
   */
  static function saveHash(Transaction $transaction) : void {
    // only hash the entries that involve the interrading wallet;
    $wid = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
    $entries = array_filter(
      iterator_to_array($transaction->entries),
      function($e) use ($wid) { return $e->payee_wid == $wid xor $e->payer_wid == $wid; }
    );
    if ($transaction->isNew()){
      $new_version = 1;
    }
    else {
      $vxids = \Drupal::database()->select('mc_transaction_revision', 'cr')
        ->fields('cr', ['vxid'])
        ->condition('xid', $transaction->id())
        ->execute()
        ->fetchCol();
      // The revision has already been written.
      $new_version = count($vxids);
    }

    $hash_string = static::getHashString(
      $transaction->uuid(),
      $transaction->state->value,
      $new_version,
      $entries
    );
    // Ideally we'd separate credcom database functions by decorating the transaction storage controller.
    \Drupal::database()->insert('mcapi_cc_hashchain')
      ->fields([
        'uuid' => $transaction->uuid(),
        'version' => $new_version,
        'hash' => md5($hash_string),
        'hash_string' => $hash_string
      ])
      ->execute();
  }

  /**
   *
   * @param string $uuid
   * @param string $state
   * @param int $version
   * @param array $entries
   *
   * @return string
   *
   * @todo replace this with the cc-php-lib?
   */
  private static function getHashString(string $uuid, string $state, int $version, array $entries) : string {
    $rows = [];
    foreach ($entries as $e) { /** @var EntryItem $e */
      if (empty($e->quant_t)) {
        throw new \Exception("no trunkward quant in entry.");
      }
      $rows[] = $e->quant_t.'|'.$e->description;
    }
    return join('|', [
      (new TrunkwardAccount())->getLasthash(),
      $uuid,
      $state,
      join('|', $rows),
      $version,
    ]);
  }

  /**
   * \CreditCommons needs this global to generate error messages.
   *
   * @global $error_context
   */
  static function setErrorContext() : void {
    global $error_context;
    $config = \Drupal::config('mcapi_cc.settings');
    $error_context = (object)[
      'node' => $config->get('node_name') ?? '',
      'method' => \Drupal::request()->getMethod(),
      'path' => \Drupal::request()->getPathInfo(),
      'user' => $config->get('trunkward.id') ?? ''
    ];
  }

  /**
   * Return an entry which will become a \Creditcommons\Entry via json_encode.
   *
   * @param EntryItem $entry
   * @return array
   */
  public static function jsonSerializeEntry(EntryItem $entry) :array {
    $config = \Drupal::config('mcapi_cc.settings');
    $payee = $entry->metadata[$entry->payee_wid] ?? $config->get('node_name') . '/'.$entry->payee_wid;
    $payer = $entry->metadata[$entry->payer_wid] ?? $config->get('node_name') . '/'.$entry->payer_wid;
    $metadata = $entry->metadata;
    unset($metadata[$entry->payee_wid], $metadata[$entry->payer_wid]);
    return [
      'payee' => $payee,
      'payer' => $payer,
      'description' => $entry->description,
      'quant' => $entry->quant * $config->get('trunkward.rate'),
      'metadata' => $metadata,
      'primary' => $entry->primary
    ];
  }

  /**
   * Convert incoming json entry to mcapi entry field value.
   *
   * @param \stdClass $entry
   * @return array
   */
  public static function convertJsonToMcapiEntry(\stdClass $entry, bool $primary = FALSE) : array {
    return [
      'payee_wid' => Functions::convertCCaddressToWalletId($entry->payee, $entry->metadata),
      'payer_wid' => Functions::convertCCaddressToWalletId($entry->payer, $entry->metadata),
      'description' => $entry->description,
      'quant' => $entry->quant / \Drupal::config('mcapi_cc.settings')->get('trunkward.rate'),
      'quant_t' => $entry->quant,
      'metadata' => (array)$entry->metadata,
      'primary' => $primary
    ];
  }

  /**
   * Take a Transaction entity violation list and throw a CreditCommons error,
   * which is then Caught in FinalExceptionSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityConstraintViolationList $violations
   *
   * @throws CCError
   */
  static function throwCCError(EntityConstraintViolationList $violations) {
    $config = \Drupal::config('mcapi_cc.settings');
    // Credcom protocol can handle multiple errors, but we can only 'throw' the first.
    foreach ($violations as $violation) {
      $constraint = $violation->getConstraint();
      if (is_null($constraint)) {
        throw new CCOtherViolation($violation->getMessage());
      }
      // Violations created ad hoc in Transaction::validate don't derive from a constraints but shouldn't show up here!
      /** @var Symfony\Component\Validator\ConstraintViolation $violation */
      switch (get_class($constraint)) {
        case 'Drupal\mcapi_limits\Plugin\Validation\Constraint\TransactionLimitMinConstraint':
          throw new MinLimitViolation(
            diff: $constraint->diff * $config->get('trunkward.rate'),
            acc: $constraint->wid == $config->get('wallet_id') ? '*' : $constraint->wid
          );
          break;
        case 'Drupal\mcapi_limits\Plugin\Validation\Constraint\TransactionLimitMaxConstraint':
          throw new MaxLimitViolation(
            diff: $constraint->diff * $config->get('trunkward.rate'),
            acc: $constraint->wid == $config->get('wallet_id') ? '*' : $constraint->wid
          );
          break;
        case 'Drupal\mcapi\Plugin\Validation\Constraint\DifferentWalletsConstraint':
          throw new SameAccountViolation($constraint->wid);
          break;
        default:
          throw new CCViolation(get_class($constraint).': '.$violation->getMessage());
      }
      // NB when we throw this is written into the log as php
    }
  }

  /**
   * Whether any trades have yet taken place with the parent node.
   * @return bool
   */
  static function hashchainStarted() : bool {
    return (bool)\Drupal::database()->select('mcapi_cc_hashchain')->countQuery()->execute()->fetchField();
  }

  /**
   * Entity_builder callback (for remote transactions).
   * Calculate the trunkward rate when building the main entry.
   */
  static function buildRemoteTransaction($entity_type_id, $entity, &$form, $form_state) {
    if (static::isRemote($entity)) {
      $rate = \Drupal::config('mcapi_cc.settings')->get('trunkward.rate');
      $entity->entries->quant_t = $entity->entries->quant * $rate;
      // remoteMetadata is set in Element\CredComWallet::validate().
      $entity->entries->metadata = $form_state->get('remoteMetadata');
    }
  }

  /**
   * Lookup the xed and rate from clearing central.
   * @param string $cen
   * @return array
   */
  static function clearingcentralCenLookup(string $cen) : array {
    $client = new Client(['base_uri' => 'https://clearingcentral.org', 'timeout' => 2]);
    // Specially created script!
    $result = $client->get("lookup_xid_rate.php?cen=$cen")->getBody()->getContents();
    return explode('|', $result);
  }

}
