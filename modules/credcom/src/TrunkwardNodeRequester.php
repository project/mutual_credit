<?php

namespace Drupal\mcapi_cc;

use CreditCommons\Exceptions\CCFailure;
use CreditCommons\Exceptions\CCError;
use CreditCommons\Exceptions\MaxLimitViolation;
use CreditCommons\Exceptions\MinLimitViolation;
use CreditCommons\Transaction as CCTransaction;
use Drupal\Mcapi\Functions as McapiFunctions;
use Drupal\mcapi_cc\Functions;
use Drupal\mcapi_cc\CreditCommons\TrunkwardAccount;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Render\Markup;
use GuzzleHttp\RequestOptions;
use Drupal\Core\Messenger\Messenger;


/**
 * Service handling outgoing requests to the trunkward node.
 */
class TrunkwardNodeRequester extends \CreditCommons\NodeRequester {

  private $logger;
  private $config;
  private $messenger;
  private $account;

  /**
   * Constructor
   *
   * @param ConfigFactory $config_factory
   * @param LoggerChannel $logger_channel
   * @param Messenger $messenger
   */
  public function __construct(ConfigFactory $config_factory, LoggerChannel $logger_channel, Messenger $messenger) {
    $this->config = $config_factory->get('mcapi_cc.settings');
    $this->logger = $logger_channel;
    $this->messenger = $messenger;
    $this->account = new TrunkwardAccount();
    if ($this->configured()) {
      // Initiate the service from the settings.
      parent::__construct($this->account, $this->config->get('node_name'));
    }
    $this->timeout = $this->config->get('trunkward.timeout') ?: 2;
    Functions::setErrorContext();
  }

  public function configured() : bool {
    return $this->config->get('node_name') and $this->config->get('trunkward.url') and $this->config->get('trunkward.id');
  }

  /**
   * {@inheritDoc}
   *
   * Theoretically this could be disintermediated by javascript calling the
   * trunkward node directly, but then the creds would have to be sent to client.
   */
  public function accountNameFilter(string $acc_path = '', $limit = 10) : array {
    $result = [];
    try {
      $result = parent::accountNameFilter($acc_path, $limit);
    }
    catch (CCError $ex) {
      $this->logger->error(
        "accountNameFilter returned @type @message<br />@fullerror",
        ['@type' => get_class($ex), '@message' => $ex->getMessage(), '@fullerror'=> Markup::create($ex)]
      );
    }
    catch (\Throwable $ex) {
      $this->logger->error(
        "accountNameFilter returned @type @message<br />@fullerror",
        ['@type' => get_class($ex), '@message' => $ex->getMessage(), '@fullerror'=>  Markup::create($ex)]
      );
    }
    //prefix the name of the parent on all the results.
    foreach ($result as $name) {

    }
    return $result;
  }

  /**
   * {@inheritDoc}
   * @todo upgrade the API
   */
  public function convertPrice(string $node_path) : \stdClass {

  }

  /**
   * {@inheritDoc}
   */
  public function buildValidateRelayTransaction(CCTransaction $transaction) : array {
    try {
      $additional = parent::buildValidateRelayTransaction($transaction);
    }
    catch (MinLimitViolation|MaxLimitViolation $e) {
      $rate = \Drupal::config('mcapi_cc.settings')->get('trunkward.rate');
      // make a new exception that formats the value
      $args = [
        '%acc' => ($e->acc == '*') ? t('Balance of Trade') : $e->acc,
        '@diff' => McapiFunctions::formatBalance(round($e->diff / $rate), FALSE),
        '@node' => $e->node
      ];
      // @todo how to render &#188; as 1/4 in the message box?
      if ($e instanceOf MaxLimitViolation) {
        $message = t("This transaction would put account %acc on node @node @diff above its maximum balance.", $args);
      }
      else {
        $message = t("This transaction would put account %acc on node @node @diff below its maximum balance.", $args);
      }
      throw new \Exception(Markup::create(strip_tags($message)));
    }
    catch (CCViolation $e) {
      // This might need better handling.
      $this->logger->warning("ccviolation from trunkwards: ".$e->getMessage());
      throw new \Exception($e->getMessage());
    }
    // Convert remote addresses to local wallets.
    foreach($additional as &$entry) {
      $entry->payee = Functions::convertCCaddressToWalletId($entry->payee, $entry->metadata);
      $entry->payer = Functions::convertCCaddressToWalletId($entry->payer, $entry->metadata);
      $entry = (array)$entry;
      $entry['metadata'] = (array)$entry['metadata'];
    }
    return $additional;
  }

  public function transactionChangeState(string $uuid, string $target_state) : void {
    try {
      parent::transactionChangeState($uuid, $target_state);
    }
    catch(CCError $e) {
      throw new \Exception($e->getMessage());
    }
  }

  /**
   * Log every request to the network.
   * @param string $endpoint
   * @return \stdClass|NULL
   */
  protected function request(string $endpoint = '/') :\stdClass|NULL {
    $this->setHeader('CC-Auth', $this->account->getLastHash());
    // Format the headers in an explodable array
    $headers = $this->options[RequestOptions::HEADERS] ? array_map( // cumbersome...
      function ($v, $k){return "$k: $v";},
      $this->options[RequestOptions::HEADERS],
      array_keys($this->options[RequestOptions::HEADERS])
    ) : [];
    try {
      $data = parent::request($endpoint);
      // log success
      $this->logger->notice(
         "<p>Request @method @domain/@endpoint<br />@headers<br />@body</p><p>Response was:<pre>@response</pre></p>",
        [
          '@headers'  => Markup::create(implode('<br />', $headers)),
          '@method'   => strtoupper($this->method),
          '@domain'   => $this->baseUrl,
          '@endpoint' => $endpoint,
          '@body'     => Markup::Create($this->options[RequestOptions::BODY] ?? ""),
          '@response' => print_r($data, 1)
        ]
      );
    }
    catch(Exception $e) {
      // Logger doesn't cope with the long $e->trace, so format a friendler message.
      foreach ((array)$e as $prop => $val) {
        if ($prop == 'trace' or $prop == 'translated' or substr($prop, 1, 5) == 'Error')continue;
        $response_body[] = "$prop : ".print_r($val, 1);
      }
      $this->logger->error(
        "<p>Request @method @domain/@endpoint<br />@headers<br />@request_body</p><p>Response was @response_code: @response_message<br /><pre>@response_body</pre></p>",
        [
          '@headers'  => Markup::create(implode('<br />', $headers)),
          '@method'   => strtoupper($this->method),
          '@domain'   => $this->baseUrl,
          '@endpoint' => $endpoint,
          '@request_body'     => Markup::Create($this->options[RequestOptions::BODY] ?? ""),
          '@response_code'     => $e->getCode(),
          '@response_message'  => $e->getMessage(),
          '@response_body' => implode('<br />', $response_body)
        ]
      );
      // Don't give any details to the user.
      // Drupal seems to be logging this exception, but the idea is to send a message to the user, but we don't have $form_state
      //$this->messenger->addError('There was a problem on the remote server. See the log for more details.');
      throw $e;
    }
    return $data;
  }

}
