<?php

namespace Drupal\mcapi_cc;

use Drupal\Core\Utility\Error;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use CreditCommons\Exceptions\CCFailure;

/**
 * Last-chance handler for exceptions: the final exception subscriber.
 * @see Drupal\mcapi_cc\Controller\Endpoint
 */
class FinalExceptionSubscriber extends \Drupal\Core\EventSubscriber\FinalExceptionSubscriber implements EventSubscriberInterface {

  /**
   * Handles exceptions for this subscriber.
   * This does not seem to catch thrown errors.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function onException(ExceptionEvent $event) {
    $exception = $event->getThrowable(); // this is the CCError
    $logmethod = ($exception->getCode() == 500) ? 'error' : 'warning';
    // This writes to the log twice, as php error and as the intended credcom error
    \Drupal::service('logger.channel.credcom')->{$logmethod}(
      '@class: @message',
      ['@class' => get_class($exception), '@message' => $exception->getMessage()]
    );
    if (!$exception instanceof CCError) {
      // Decode the error so we can put the backtrace in it.
      // @message, %function, %file, %line, 'backtrace', @backtrace_string, exception
      // Generate a backtrace containing only scalar argument values.
      // Unfortunately this backtrace doesn't contain any useful info.
      $error = Error::decodeException($exception);
      $error = new CCFailure($error['@message']);
      $error->backtrace = $exception->getTrace();
    }

    $response = new JsonResponse(['errors' => [$error ?? $exception]], 500, ['Content-Type' => "application/json"]);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    // Run as the final (very late) KernelEvents::EXCEPTION subscriber.
    $events[KernelEvents::EXCEPTION][] = ['onException', -257];
    return $events;
  }
}
