<?php

namespace Drupal\mcapi_cc\Form;

use CreditCommons\Exceptions\CCError;
use CreditCommons\Exceptions\CCFailure;
use CreditCommons\Exceptions\DoesNotExistViolation;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi_cc\CreditCommons\TrunkwardAccount;
use Drupal\mcapi_cc\Functions;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class for credcom settings
 */
class SettingsForm extends ConfigFormBase {

  protected $config;
  private bool $traded = FALSE;
  protected bool $connected = FALSE;
  protected ModuleHandler $moduleHandler;

  /**
   * {@inheritDoc}
   */
  public function getFormID() {
    return 'mcapi_cc_settings_form';
  }

  /**
   * @param ConfigFactory $config_factory
   * @param EntityTypeManager $entity_type_manager
   * @param ModuleHandler $module_handler
   */
  public function __construct(ConfigFactory $config_factory, EntityTypeManager $entity_type_manager, ModuleHandler $module_handler) {
    parent::__construct($config_factory);
    $this->config = $config_factory->get('mcapi_cc.settings');
    $this->traded = Functions::hashchainStarted();
    $this->moduleHandler = $module_handler;
  }

  static public function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $w = 0;

    $this->connected = $this->testConnection(); // writes a message.

    $form['wallet_id'] = [
      '#title' => t('Balance of Trade wallet'),
      '#description' => t('The wallet used for transactions with the rest of the world.'),
      '#markup'=> Wallet::load($this->config->get('wallet_id'))->toLink()->toString(),
      '#theme_wrappers' => ['form_element'],
      '#weight' => $w++,
    ];
    $form['node_name'] = [
      '#title' => $this->t('The name of the current site on the network'),
      '#description' => $this->t('This field is disabled after the first remote trade.'),
      '#type' => 'textfield',
      '#default_value' => $this->config->get('node_name'),
      '#placeholder' => $this->t('lowercase with _ and no spaces.'),
      '#pattern' => '[a-z1-9_]+',
      '#weight' => $w++,
      '#disabled'=> $this->connected,
      '#required' => TRUE
    ];
    if (!$this->traded) {
      $form['node_name']['#description'] = $this->t('The account must be manually created on the trunkwards node.') .' ' .$form['node_name']['#description'];
    }
    $form['trunkward'] = [
      '#title' => $this->t('Connection to trunkwards node'),
      '#type' => 'details',
      '#open' => !$this->connected,
      '#tree' => TRUE,
      '#weight' => $w++,
      'id' => [
        '#title' => $this->t('Trunkwards node ID'),
        '#description' => $this->t('The cc_user name the trunkward node uses to connect'),
        '#type' => 'textfield',
        '#default_value' => $this->config->get('trunkward.id'),
        '#pattern' => '[a-z1-9]+',
        '#required' => TRUE,
        '#disabled'=> $this->connected,
        '#weight' => 1
      ],
      'url' => [
        '#title' => $this->t('Trunkward node URL'),
        '#description' => $this->t('Must be https to protect the login credentials'),
        '#type' => 'textfield',
        '#default_value' => $this->config->get('trunkward.url'),
        '#placeholder' => 'https://',
        '#pattern' => 'https?:\/\/[a-z/.\-0-9]+', // https enforced except during development
        '#disabled'=> $this->connected,
        '#required' => TRUE,
        '#weight' => 2
      ],
      'rate' => [
        '#title' => $this->t('Conversion rate'),
        '#description' => $this->t('The number of (raw) local units to one unit on the parent ledger. Note that local units are stored as integers of the smallest subdivision, e.g. cents not dollars, minutes, not hours.'),
        '#type' => 'number',
        '#min' => 0,
        '#step' => 0.01,
        '#default_value' => $this->config->get('trunkward.rate'),
        '#disabled'=> $this->traded,
        '#weight' => 3,
      ],
      // Can't remember what this is for
      'symbol'=> [
        '#title' => $this->t('Symbol for the local currency'),
        '#description' => $this->t('An html string which is placed before the numeric value.'),
        '#type' => 'textfield',
        '#default_value' => $this->config->get('trunkward.symbol'),
        '#weight' => 4,
        '#required' => TRUE
      ],
      'timeout'=> [
        '#title' => $this->t('Timeout'),
        '#description' => $this->t('Larger trees may need longer timeouts.'),
        '#type' => 'number',
        '#default_value' => $this->config->get('trunkward.timeout'),
        '#weight' => 5,
        '#required' => TRUE,
        '#min' => 1,
        '#max' => 20
      ]
    ];

    $form['privacy'] = [
      '#title' => $this->t('Privacy'),
      '#description' => $this->t('This site is a leaf on a Credit Commons tree. What account and transaction data can be shared with the rest of the tree?'),
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#weight' => $w++,
      'acc_path_autocomplete' => [
        '#title' => $this->t('Wallet autocomplete'),
        '#description' => $this->t('Incoming transactions can address wallets using the name, with dashes instead of spaces.').' '.$this->t('How much personal information can be seen by traders in other nodes trying to autocomplete wallet addresses?'),
        '#type' => 'select',
        '#options' => [
          CREDCOM_AUTOCOMPLETE_NONE => $this->t('Disable autocomplete'),
          CREDCOM_AUTOCOMPLETE_NUM => $this->t('Numeric wallet ids'),
          CREDCOM_AUTOCOMPLETE_NAME => $this->t('Expose wallet names'),
        ],
        '#default_value' => $this->config->get('privacy.acc_path_autocomplete'),
        '#required' => TRUE,
      ],
      'aggregate_stats' => [
        '#title' => $this->t('Expose aggregate trading statistics') . ' NOT IMPLEMENTED',
        '#description' => $this->t('Share trading statistics with the Credit commons network.'),
        '#type' => 'checkbox',
        '#default_value' => $this->config->get('privacy.aggregate_stats'),
        '#disabled' => TRUE
      ]
    ];
    $form['logging'] = [
      '#title' => $this->t('Log requests/responses'),
      '#type' => 'checkbox',
      '#default_value' => $this->config->get('logging'),
      '#weight' => $w++
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $requester = (new TrunkwardAccount())->getRequester();
      $requester->baseUrl = $form_state->getValue(['trunkward', 'url']);
      $requester->setHeader('CC-User', $form_state->getValue('node_name'));
      $requester->handshake();
    }
    catch (DoesNotExistViolation $e) {
      if ($e->type == 'account') {
        $form_state->setErrorByName('node_name', $e->getMessage());
      }
      $form_state->setErrorByName('', $e->getMessage());
    }
    catch (CCFailure $e) {
      $form_state->setErrorByName('trunkward', $e->getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('mcapi_cc.settings')
      ->set('node_name', $form_state->getValue('node_name'))
      ->set('trunkward', $form_state->getValue('trunkward'))
      ->set('privacy', $form_state->getValue('privacy'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['mcapi_cc.settings'];
  }

  private function testConnection() : bool {
    if ($ccuser = $this->config->get('node_name') and $url = $this->config->get('trunkward.url')) {
      $requester = (new TrunkwardAccount())->getRequester();
      try {
        $requester->handshake();
        return TRUE;
      }
      catch(CCError $e) {
        \Drupal::messenger()->addStatus($e->getMessage());
      }
    }
    return FALSE;
  }

}
