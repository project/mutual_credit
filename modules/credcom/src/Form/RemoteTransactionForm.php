<?php

namespace Drupal\mcapi_cc\Form;

use Drupal\mcapi\Form\TransactionForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for adding remote transactions.
 */
class RemoteTransactionForm extends TransactionForm {

  function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#title'] = $this->getEntity()->workflow->entity->label;
    $form['exchange'] = [
      '#type' => 'clearcent_wallet'
    ];
    $form['remote_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Remote party's ID")
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $entity = parent::buildEntity($form, $form_state);
    // Populate the metadata so the transaction knows where its going.
    $entries = $entity->entries->getValue();
    $wid = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
    $val = $form_state->getValue('exchange'). '/'.$form_state->getValue('remote_id');
    $entries[0]['metadata'][$wid] = $val;
    $entries[0]['metadata'][$wid] = 'hamletsdemo/5';// used to construct the trunkward request
    $entity->entries->setValue($entries);
    return $entity;
  }


  /**
   * Provide an ID for the transaction form consistent with the keys of hook_transction_form_list().
   */
  function transactionFormId() {
    return 'remote_'.$this->entity->workflow->entity->id();
  }

}
