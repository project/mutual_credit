<?php

namespace Drupal\mcapi_cc\CreditCommons;

use CreditCommons\Entry;
use CreditCommons\Workflow;
use CreditCommons\Transaction as CCTransaction;
use Drupal\mcapi_cc\CreditCommons\Account;
use Drupal\mcapi_cc\CreditCommons\TrunkwardAccount;
use Drupal\mcapi\Entity\Transaction as McapiTransaction;

class Transaction extends \CreditCommons\BaseTransaction {

  /**
   *
   * @param McapiTransaction $transaction
   * @return static
   */
  static function createFromMcapi(McapiTransaction $transaction) {
    $wallet_id = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
    $metadata = $transaction->entries->first()->metadata;
    foreach ($transaction->entries as $ent) {
      if ($ent->payee_wid == $wallet_id) {
        $payee = new TrunkwardAccount($metadata[$wallet_id]);
        $payer = new Account($ent->payer_wid);
      }
      elseif ($ent->payer_wid == $wallet_id) {
        $payer = new TrunkwardAccount($metadata[$wallet_id]);
        $payee = new Account($ent->payee_wid);
      }
      unset($metadata[$wallet_id]);
      // Multiply the quant because this transaction is about to be sent trunkwards.
      $entries[] = new Entry(
        $payee,
        $payer,
        $ent->quant * \Drupal::config('mcapi_cc.settings')->get('trunkward.rate'),
        (object)$metadata,
        $ent->description
      );
    }
    return new CCTransaction (
      uuid: $transaction->uuid(),
      workflow: new Workflow($transaction->workflow->entity->getCode()),
      state: $transaction->state->value,
      entries: $entries,
      written: $transaction->created->value,
      version: 0
    );
  }

  function getWorkflow() : \CreditCommons\Workflow {
    // this is never called.
  }

}