<?php

namespace Drupal\mcapi_cc\CreditCommons;

class Account extends \CreditCommons\BaseAccount {

  private $nodeName;

  function __construct(
    public string $id
  ) {
    parent::__construct($id);
    $this->nodeName = \Drupal::config('mcapi_cc.settings')->get('node_name');
  }

  function trunkwardPath(): string {
    return $this->nodeName . '/'. $this->id;
  }

}
