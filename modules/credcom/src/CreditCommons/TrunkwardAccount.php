<?php

namespace Drupal\mcapi_cc\CreditCommons;
use Drupal\mcapi_cc\TrunkwardNodeRequester;
use CreditCommons\AccountRemote;
use CreditCommons\Transaction;
use Drupal\mcapi\Entity\Wallet;


/**
 * These account objects are needed to use the \CreditCommons\Entry class.
 */
class TrunkwardAccount extends AccountRemote {

  function __construct(string $rel_path = '') {
    $config = \Drupal::config('mcapi_cc.settings');
    $wallet = Wallet::load($config->get('wallet_id'));
    parent::__construct(
      $config->get('trunkward.id') ?? 0,
      $wallet->min->limit(), // NB Raw value
      $wallet->max->limit(),
      FALSE,
      $config->get('trunkward.url') ?? '',// for testing it can be set later
      $rel_path
    );
  }

  function getRequester() : TrunkwardNodeRequester {
    return \Drupal::service('mcapi_cc.trunkward_node');
  }

  function getLastHash() : string {
    return \Drupal::database()
      ->select('mcapi_cc_hashchain', 'h')
      ->fields('h', ['hash'])
      ->range(0, 1)
      ->orderBy('id', 'desc')
      ->execute()
      ->fetchField();
  }


  /**
   * {@inheritdoc}
   * @todo
   */
  function storeHash(Transaction $transaction) {

  }

  function getLimits($force_local = false): \stdClass|array {

  }
  function getSummary($force_local = false): \stdClass|array {


  }
  function authenticate(string $string) {

  }
  function leafwardPath() : string {

  }

  function handshake() : string {

  }


}