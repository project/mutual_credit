<?php

namespace Drupal\mcapi_cc\CreditCommons;

use CreditCommons\Exceptions\PasswordViolation;
use CreditCommons\Exceptions\HashMismatchFailure;
use Drupal\mcapi_cc\CreditCommons\TrunkwardAccount;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access check for requests from trunkwards nodes.
 */
class HashChecker implements \Drupal\Core\Routing\Access\AccessInterface {

  /**
   * This is actually setting up drupal to respond like a credit commons node.
   */
  function __construct($route_match, $request_stack) {
    global $error_context;
    $config = \Drupal::Config('mcapi_cc.settings');
    $error_context = (object)[
      'node' => $config->get('node_name'),
      // Headers are stored in lowercase but can be retrieved in any case.
      'user' => (string)$request_stack->getCurrentRequest()->headers->get('cc-user')
    ];
    // TODO instead of checking the route name check the route requirements for _hashes_aligned
    if (substr($route_match->getRouteName(), 0, 8) == 'mcapi_cc') {
      set_exception_handler(['\Drupal\mcapi_cc\Functions', 'CredcomExceptionHandler']);
      set_error_handler(['\Drupal\mcapi_cc\Functions', 'CredcomErrorHandler']);
    }
    // Otherwise its probably just the the route rebuilding.
  }

  /**
   * Checks the hash passed in the headers against the hash in the database.
   *
   * @param RequestStack $request
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function access(Request $request) {
    if (\Drupal::state()->get('system.maintenance_mode')) {
      throw new CCFailure(message: 'Site is offline for maintenance');
    }
    global $error_context;
    $error_context->method = $request->getMethod();
    $error_context->path = substr($request->getPathInfo(), 8);
    $remote_hash = ($request->headers->get('cc-auth') == 'null') ? '' : (string)$request->headers->get('cc-auth');
    $trunkward = new TrunkwardAccount();
    $local_hash = $trunkward->getLasthash();
    if (!$request->headers->has('cc-user')) {
      throw new PasswordViolation($remote_hash);
    }
    elseif ($local_hash <> $remote_hash) {
      throw new HashMismatchFailure($trunkward->id, $local_hash, $remote_hash);
    }
    return AccessResult::allowed();
  }

}
