<?php

namespace Drupal\mcapi_cc\Controller;

use CreditCommons\Exceptions\CCFailure;
use CreditCommons\Exceptions\WorkflowViolation;
use CreditCommons\Exceptions\DoesNotExistViolation;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi_cc\FinalExceptionSubscriber;
use Drupal\mcapi_cc\Functions;
use Drupal\Core\TempStore\SharedTempStore;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Handle all incoming requests from the trunkward node.
 */
class EndPoint extends \Drupal\Core\Controller\ControllerBase {

  private $logger;
  private $config;
  private $tempstore;

  /**
   * See HashChecker for more set up operations.
   */
  public function __construct(Request $request, SharedTempStore $credcom_tempstore) {
    $this->config = $this->config('mcapi_cc.settings');
    Functions::setErrorContext($this->config);
    $this->tempstore = $credcom_tempstore;
    if (1 or $this->config->get('logging')) {
      $this->logger = \Drupal::service('logger.channel.credcom');
      $this->logger->info(
        'Incoming Request @method <pre>@body</pre>',
        ['@method' => $request->getMethod(), '@body' => $request->getContent()]
      );
    }
  }

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    // Generate custom error responses for exceptions inside Drupal.
    $container->set('exception.final', new FinalExceptionSubscriber($container->get('config.factory')));
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('tempstore.shared')->get('mcapi_cc')
    );
  }

  /**
   * Wrap the body in json, log the response
   *
   * @param type $body
   * @param int $code
   * @return JsonResponse
   */
  private function respond($body, int $code = 200) {
    if ($this->logger) {
      $this->logger->info(
        'Response2 <pre>@body</pre>',
        ['@body' => print_r($body, 1)]
      );
    }
    return JsonResponse::fromJsonString(json_encode($body), $code);
  }

  /**
   * Having validated with the hash, return that this leaf node is online.
   */
  function handshake() {
    // This drupal module is always positioned as a twig, which means it has
    // only one possible response
    return $this->respond(['data' => []]);
  }

  /**
   * Return the exchange rate.
   */
  function convert() {
    $result = [
      'data' => [
        'symbol' => $this->config->get('local_curr_symbol'),
        'rate' => 1 * $this->config->get('trunkward.rate')
      ]
    ];
    return $this->respond($result);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   */
  function autocompleteNames(Request $request) {
    $result = [];
    $fragment = $request->query->get('acc_path');
    if ($fragment and substr($fragment, 0, 1) == '/'){
      $fragment = substr($fragment, 1);
    }
    $mode = $this->config->get('privacy.acc_path_autocomplete');
    $q = $this->entityTypeManager()->getStorage('mc_wallet')->getQuery()
      ->accessCheck(TRUE)
      ->range(0, 10);
    if ($mode) {
      if(is_numeric($fragment)) {
        $q->condition('wid', $fragment.'%', 'LIKE');
      }
      elseif ($fragment and $mode == CREDCOM_AUTOCOMPLETE_NAME) {
        $q->condition('name', '%'.str_replace('-', ' ', $fragment).'%', 'LIKE');
      }
    }
    else {
      $q->condition('wid', 0); // No results.
    }
    $wids = $q->execute();
    // Prepare response either wallet names or numbers.
    if ($mode == CREDCOM_AUTOCOMPLETE_NAME) {
      foreach (Wallet::loadMultiple($wids) as $wid => $wallet) {
        // Credit commons wallet names support dashes but not spaces.
        // @see Drupal\mcapi_cc\Functions::credcomPathIsLocal
        $result[] = str_replace(' ', '-', strtolower($wallet->name->value));
      }
    }
    else {
      $result = $wids;
    }
    return $this->respond(['data' => $result]);
  }

  /**
   * Handle incoming transaction request.
   * @return JsonResponse
   */
  function relayTransaction(Request $request) {
    $contents = $request->getContent();
    $json = json_decode($contents);
    $json_entries = (array)$json->entries;
    $count_entries = count($json_entries);

    $transaction_props = [
      'uuid' => $json->uuid,
      'workflow' => $json->workflow,
      'entries' => [Functions::convertJsonToMcapiEntry(array_shift($json_entries), TRUE)],
    ];

    $transaction = Transaction::create($transaction_props);
    foreach ($json_entries as $e) {
      $transaction->addEntry(Functions::convertJsonToMcapiEntry($e, FALSE));
    }
    if ($violationList = $transaction->validate()) {
      Functions::throwCCError($violationList);
    }
    if ($transaction->workflow->entity->requiresConfirmation()) {
      $transaction->set('state', CCWorkflowInterface::STATE_VALIDATED);
      $this->tempstore->set($transaction->uuid(), $transaction);
      $response_code = 200;
    }
    else {
      $transaction->save();
      $response_code = 201;
    }
    $new_entries = array_map(
      function ($e) {return Functions::jsonSerializeEntry($e);},
      array_slice(iterator_to_array($transaction->entries), $count_entries)
    );
    // These entries need converting.
    return $this->respond(['data' => $new_entries], $response_code);
  }

  /**
   * @param string $uuid
   * @param string $target_state
   * @return JsonResponse
   * @throws WorkflowViolation
   */
  function changeState(string $uuid, string $target_state) {
    $intertrading_wallet_id = \Drupal::config('mcapi_cc.settings')->get('wallet_id');
    // Transactions not yet saved are in the temp store and don't rely on workflow.
    if ($transaction = $this->tempstore->get($uuid)) {
      // tempstore should only contain transaction in 'validated' state
      if ($transaction->state->value == CCWorkflowInterface::STATE_VALIDATED or $transaction->state->value == CCWorkflowInterface::STATE_INITIATED) {
        if ($transaction->workflow->entity->direction == 'bill' && $transaction->entries->payee_wid <> $intertrading_wallet_id
        or  $transaction->workflow->entity->direction == 'credit' && $transaction->entries->payer_wid <> $intertrading_wallet_id) {
          throw new WorkflowViolation(from: $transaction->state->value, to: $target_state, type: $transaction->workflow->target_id);
        }
      }
      else {
        // This should never happen.
        throw new \Exception("Transaction found in temp was in '".$transaction->state->value."' state.");
      }
    }
    // Handle all saved transactions
    else {
      $txs = $this->entityTypeManager()->getStorage('mc_transaction')->loadByProperties(['uuid' => $uuid]);
      if (empty($txs)) {
        throw new CCFailure("Transaction $uuid not found in Drupal tempStore.");
      }
      $transaction = reset($txs);
      // Check that the transition exists, and the intertrading wallet is part of the transaction.
      if($transaction->state->value == $target_state) {
        throw new WorkflowViolation(from: $transaction->state->value, to: $target_state, type: $transaction->workflow->target_id);
      }
      if (!isset($transaction->workflow->entity->transitions[$transaction->state->value][$target_state])) {
        $transition_id = $transaction->state->value .'>>'.$target_state;
        throw new DoesNotExistViolation(type: 'transition', id: $transition_id);
      }
      // Remote actors have admin permission on any defined transition they are involved in
      if($entry->payee_wid <> $wallet_id and $entry->payer_wid <> $wallet_id) {
        throw new WorkflowViolation(type: $transaction->workflow->target_id, from: $transaction->state->value, to: $target_state);
      }
    }
    $transaction->saveVersion($target_state);
    $this->tempstore->delete($uuid);
    return $this->respond([], 201);
  }

}
