<?php

namespace Drupal\mcapi_cc\Controller;

use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi_cc\TrunkwardNodeRequester;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Return wallet names to the autocomplete wallet field.
 */
class Names extends \Drupal\Core\Controller\ControllerBase {

  /**
   * @var TrunkwardNodeRequester
   */
  protected $trunkwardNode;

  /**
   * @var Config
   */
  protected $config;


  public function __construct(TrunkwardNodeRequester $trunkward_requester, ConfigFactory $config_factory) {
    $this->trunkwardNode = $trunkward_requester;
    $this->config = $config_factory->get('mcapi_cc.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mcapi_cc.trunkward_node'),
      $container->get('config.factory'),
    );
  }

  /**
   * Relay the account name fragment to the trunkward node.
   * @param Request $request
   * @return JsonResponse
   */
  function remoteNames(Request $request) {
    $fragment = $request->query->get('q');
    $names = $this->trunkwardNode->accountNameFilter($fragment);
    foreach ($names as &$name) {
      // don't do this if the trunkward is the trunk
      // $name = $this->config->get('trunkward.id') .'/'. $name;
    }
    return new JsonResponse($names);
  }

  /**
   * Get local and remote names that match the given fragment.
   * @param Request $request
   * @return JsonResponse
   */
  function allNames(Request $request) {
    $local_names = [];
    $fragment = $request->query->get('q');
    $walletQuery = $this->entityTypeManager()->getStorage('mc_wallet')->getQuery()->accessCheck(TRUE);
    if (is_numeric($fragment)) {
      $walletQuery->condition('wid', "$fragment%", 'LIKE');
    }
    else {
      $walletQuery->condition('name', "%$fragment%", 'LIKE');
    }
    if ($wids = $walletQuery->accesscheck(TRUE)->execute()) {
      foreach (Wallet::loadMultiple($wids) as $wid => $wallet) {
        $local_names[] = $wallet->label() .' ('.$wid.')';
      }
    }
    $trunkward_name = $this->config->get('trunkward.id');
    // If the fragment is part of the trunkward name return the full trunkward name
    if (strlen($fragment) < strlen($trunkward_name) and $fragment == substr($trunkward_name, 0, strlen($fragment))) {
      $remote_names = [$trunkward_name . '/'];
    }
    else {
      // If the fragment is prefixed by the trunk node name, strip it off and submit it.
      $fragment = preg_replace('/^'.$trunkward_name.'\/?/', '', $fragment);
      $remote_names = (array)$this->trunkwardNode->accountNameFilter($fragment);
    }
    return new JsonResponse(array_merge($local_names, $remote_names));
  }

}
