<?php

namespace Drupal\mcapi_cc\Plugin\Derivative;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * If the trunkward node is 'clearingcentral', special transaction forms are needed.
 * Create links in the account menu to these forms.
 */
class ClearingCentralLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Storage controller for McForm entities.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Config
   */
  protected $credcomConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->credcomConfig = $config_factory->get('mcapi_cc.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links= [];
    if ($this->moduleHandler->moduleExists('mcapi_forms')) {
      if ($this->credcomConfig->get('trunkward.id') == 'clearingcentral') {
        $form_storage = $this->entityTypeManager->getStorage('mc_form');
        $forms = $form_storage->loadByProperties(['workflow_id', ['bill_legacy', 'credit_legacy']]);
        foreach ($forms as $mc_form) {
          $links["mcapi_cc.clearingcentral.".$mc_form->id()] = [
            'title' => $this->t('Manage fields'),
            'route_name' => "mcapi_forms.mc_form.default",
            'route_arguments' => $mc_form->id(),
            'weight' => 8
          ] + $base_plugin_definition;
        }
      }
    }
    return $links;
  }

}
