<?php
namespace Drupal\mcapi_cc\Plugin\migrate\process;

use Drupal\mcapi_cc\Functions;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Migrate process plugin for the name of the current credit commons node in the network.
 */
#[MigrateProcess(id: 'mcapi_cc_nodename')]
class NodeName extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   * Get the credcom nodename from clearing central.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $cen = $row->getSourceProperty('mcapi_cc_cenip_user');
    try {
      [$nodeName, $rate] = Functions::clearingcentralCenLookup($cen);
    }
    catch(\Exception $e) {
      throw new MigrateSkipRowException('Failed to retrieve clearingcentral ID: '.$e->getMessage());
    }
    $row->setDestinationProperty('trunkward/rate', $rate);
    return $nodeName;
  }


}
