<?php
namespace Drupal\mcapi_cc\Plugin\migrate\process;

use Drupal\mcapi\Entity\Transaction;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Migrate process plugin for credit commons transaction metadata field.
 */
#[MigrateProcess(id: 'mcapi_cc_metadata')]
class CredcomMetadata extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   * Get the credcom nodename from clearing central.
   */
  public function transform($serial, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $wid = \Drupal::config('mcapi_cc.settings')->get('wallet_id');

    $t = Transaction::load($serial);
    $entry = $t->entries->first();
    [$nodeName] = Functions::clearingcentralCenLookup($row->getSourceProperty('remote_exchange_id'));
    if (!$nodename) {
      throw new MigrateSkipRowException("Clearing central couldn't provide xid for ".$row->getSourceProperty('remote_exchange_id'));
    }
    $entry->metadata[$wid] =  $nodeName. '/'. $row->getSourceProperty('remote_user_id');
    $t->save();
    return $serial;
  }


}
