<?php

namespace Drupal\mcapi_cc\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Drupal 7 remote transactions metadata
 *
 * @MigrateSource(
 *   id = "d7_mc_transaction_credcom",
 *   source_module = "mcapi"
 * )
 */
class TransactionRemote extends FieldableEntity {

  /**
   * {@inheritDoc}
   */
  public function query() {
    return $this->select('mcapi_cc', 't')
      ->fields('t', ['xid', 'serial', 'txid', 'remote_exchange_id', 'remote_user_id', 'remote_user_name']);
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    return [
      'xid' => $this->t('Transaction ID'),
      'serial' => $this->t('Serial number'),
      'txid' => $this->t('Payer user'),
      'remote_exchange_id' => $this->t('Payee user'),
      'remote_user_id' => $this->t('Type'),
      'remote_user_name' => $this->t('State')
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    return [
      'xid' => [
        'type' => 'integer',
        'alias' => 't'
      ]
    ];
  }

}
