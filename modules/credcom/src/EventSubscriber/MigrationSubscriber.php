<?php

namespace Drupal\mcapi_cc\EventSubscriber;

use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alterations to migrations
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_import' => [['migratePreImport']],
      'migrate.post_import' => [['migratePostImport']],
      'migrate.pre_row_save' => [['migratePreRowSave']]
    ];
  }

  /**
   * Subscribed event callback.
   *
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    if ($event->getMigration()->id() == 'd7_mc_transaction') {

    }
  }

  /**
   * Subscribed event callback.
   */
  function migratePreImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_mc_transaction') {

    }
  }

  /**
   * Subscribed event callback.
   * Set the autoadd back to what it was before the user migration
   */
  function migratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_mc_transaction') {

    }
  }
}
