<?php

namespace Drupal\mcapi_bursers;

use Drupal\mcapi\Entity\Access\WalletAccessControlHandler;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access controller option for the mc_wallet entity.
 */
class BurserWalletAccessControlHandler extends WalletAccessControlHandler {

  /**
   * Check if the given entity either owns or is burser of the given wallet.
   *
   * @param WalletInterface $wallet
   * @param AccountInterface $account
   *
   * @return boolean
   *   TRUE if the $account controls the $entity
   */
  private function controlsWallet(WalletInterface $wallet, AccountInterface $account) : bool {
    $uid = $account->id();
    if ($wallet->getOwnerId() == $uid) {
      return TRUE;
    }
    foreach ($wallet->bursers->getValue() as $item) {
      if ($item['target_id'] == $uid) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
