<?php

namespace Drupal\mcapi_bursers;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\mcapi\Entity\Access\TransactionAccessControlHandler;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines an access controller option for the mc_transaction entity.
 */
class BurserTransactionAccessControlHandler extends TransactionAccessControlHandler {

  /**
   * {@inheritDoc}
   */
  public function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if (WalletStorage::allWalletIdsOf($account->id())) {
      $result = AccessResult::allowed();
    }
    elseif ($this->walletQuery->condition('bursers', $account->id())->accessCheck(TRUE)->execute()) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden('User '. $account->id() .' has no wallet, nor is burser to any wallet.');
    }
    return $result->cachePerUser();
  }
}
