<?php

namespace Drupal\mcapi_bursers\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\mcapi_bursers\Plugin\Field\BurserReferenceFieldItemList;

/**
 * Defines the burser user entity reference field type.
 */
#[FieldType(
  id: "burser_reference",
  label: new TranslatableMarkup("Bursers"),
  description: new TranslatableMarkup("Other users who can control the wallet"),
  category: new TranslatableMarkup('Community Accounting'),
  default_formatter: "entity_reference_label",
  default_widget: "entity_reference_autocomplete",
  list_class: BurserReferenceFieldItemList::class
)]
class BurserReferenceItem extends EntityReferenceItem {

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default:mc_wallet_burser'
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'target_type' => 'mc_wallet',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

}
