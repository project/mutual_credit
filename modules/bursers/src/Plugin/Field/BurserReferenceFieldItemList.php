<?php

namespace Drupal\mcapi_bursers\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * A burser reference field list
 */
class BurserReferenceFieldItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritDoc}
   * Ensure the list excludes wallet owner and duplicates.
   */
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);
    $owner = $this->getEntity()->getOwner();
    $already[] = $owner->id();
    foreach ($this->list as $delta => $item) {
      if (in_array($item->target_id, $already)) {
        unset($this->list[$delta]);
        $this->list = array_values($this->list);
      }
      $already[] = $item->target_id;
    }
  }

}
