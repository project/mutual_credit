<?php

namespace Drupal\mcapi_bursers\Plugin\EntityReferenceSelection;

use Drupal\mcapi\Plugin\EntityReferenceSelection\WalletSelection;
use Drupal\Core\Entity\Attribute\EntityReferenceSelection;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provide default Wallet selection handler.
 * Overrides the my_wallets_only setting to include bursered wallets
 */
#[EntityReferenceSelection(
  id: 'default:mc_wallet_burser',
  label: new TranslatableMarkup('Wallet (included bursered) selection'),
  entity_types: ['mc_wallet'],
  group: 'default'
)]
class WalletBurserSelection extends WalletSelection {

  /**
   * {@inheritDoc}
   */
  public function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = $this->entityTypeManager->getStorage('mc_wallet')->getQuery()->accessCheck(TRUE);
    if ($match) {
      $query->condition('name', $match, $match_operator);
    }

    if ($this->configuration['my_wallets_only']) {
      $db_or = $query->orConditionGroup()
        ->condition('owner', $this->currentUser->id(), 'IN')
        ->condition('bursers', $this->currentUser->id());
      $query->condition($db_or);
    }
    return $query;
  }

}