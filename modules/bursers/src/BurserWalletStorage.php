<?php

namespace Drupal\mcapi_bursers;

use Drupal\mcapi\Entity\Storage\WalletStorage;

/**
 * Rewrite allWalletIdsOf function to include bursers
 */
class BurserWalletStorage extends WalletStorage {

  /**
   * {@inheritDoc}
   */
  public static function allWalletIdsOf($uid) : array {
    $burser_wids = \Drupal::entityQuery('mc_wallet')->accessCheck(FALSE)
      ->condition('bursers', $uid)
      ->execute();
    $owned_wids = parent::allWalletIdsOf($uid);
    return array_unique(array_merge($burser_wids, $owned_wids));
  }

}

