<?php

namespace Drupal\mcapi_forms;

use Drupal\mcapi\Functions;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Entity\Wallet;
use Drupal\system\Entity\Menu;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to edit the FormEditor entity.
 */
class McFormEditorForm extends EntityForm {

  const ENTRY_PROPERTIES = ['quant', 'payee', 'payee', 'description'];

  /**
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * @var RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * @var RequestContext
   */
  protected $routerRequestContext;

  /**
   * @var MenuParentFormSelectorInterface
   */
  protected $menuParentFormSelector;

  function __construct(
    ModuleHandlerInterface $module_handler,
    ModuleInstallerInterface $module_installer,
    RouteBuilderInterface $router_builder,
    RequestContext $router_request_context,
    MenuParentFormSelectorInterface $menu_parent_form_selector
  ) {
    $this->moduleHandler = $module_handler;
    $this->moduleInstaller = $module_installer;
    $this->routerBuilder = $router_builder;
    $this->routerRequestContext = $router_request_context;
    $this->menuParentFormSelector = $menu_parent_form_selector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('module_installer'),
      $container->get('router.builder'),
      $container->get('router.request_context'),
      $container->get('menu.parent_form_selector'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $w = 0;
    $form['label'] = [
      '#title' => t('Title of the form'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#size' => 40,
      '#maxlength' => 80,
      '#required' => TRUE,
      '#weight' => $w++
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => 12,
      '#machine_name' => [
        'exists' => '\Drupal\mcapi_forms\Entity\McForm::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['purpose'] = [
      '#title' => t('Purpose of the form'),
      '#description' => t('Used in tooltips and at the top of the form.'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->purpose ?? '',//purpose may be missing after upgrade
      '#size' => 100,
      '#maxlength' => 256,
      '#required' => FALSE,
      '#weight' => $w++
    ];
    $form['wallet_link_title'] = [
      '#title' => t('Link title to show on wallet'),
      '#description' => t('Link to this form from the wallet. Optionally use token [mc_wallet:name]'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->getWalletLinkTitle(),
      '#weight' => $w++
    ];
    $form['workflow_id'] = [
      '#title' => t('Workflow'),
      '#description' => $this->t('Remember users need permissions to use certain workflows.'),
      '#type' => 'select',
      '#options' => Functions::workflowOptions(),
      '#default_value' => $this->entity->getWorkflowId(),
      '#required' => TRUE,
      '#weight' => $w++
    ];

    $form['menu'] = [
      '#title' => t('Menu link'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
      'title' => [
        '#title' => t('Menu link title'),
        '#type' => 'textfield',
        '#default_value' => $this->entity->getMenuConf() ? $this->entity->getMenuConf()->title : '',
        '#weight' => 1,
      ],
      'tab' => [
        '#title' => t('Make menu task'),
        '#description' => t('Show along side other designed with this option selected on path /transact.'),
        '#type' => 'checkbox',
        '#default_value' => $this->entity->showTab(),
        '#weight' => 3,
      ],
      '#weight' => $w++,
    ];

    if (!$this->moduleHandler->moduleExists('menu_ui')) {
      $this->moduleInstaller->install(['menu_ui']);
    }
    $custom_menus = [];
    foreach (Menu::loadMultiple() as $menu_parent => $menu) {
      $custom_menus[$menu_parent] = $menu->label();
    }
    $form['menu']['parent'] = $this->menuParentFormSelector
      ->parentSelectElement($settings['menu_parent'] ?? 'main:');
    $form['menu']['parent']['#empty_value'] = '';
    $form['menu']['parent']['#title'] = $this->t("Parent menu item");
    $form['menu']['parent']['#default_value'] = $this->entity->getMenuConf()->parent??'';
    $form['presets'] = [
      '#type' => 'details',
      //'#open' => !empty(array_filter($this->entity->presets)), //todo open if anything is set
      '#tree' => TRUE,
      '#title' => $this->t('Preset values'),
      '#description' => $this->t('Fields with preset values are not required in the twig template'),
      '#weight' => $w++,
      'description' => [
        '#title' => $this->t('Description'),
        '#type' => 'textfield',
        '#default_value' => $this->entity->getPreset('description'),
      ],
      'quant' => [
        '#title' => $this->t('Quantity'),
        '#type' => 'worth',
        '#default_value' => $this->entity->getPreset('quant'),
        '#prefix' => \Drupal\Core\Render\Markup::Create('<style>input#edit-presets-quant-main,input#edit-presets-quant-subdivision,div.form-item--presets-quant-main{display:inline;}</style>')
      ],
      'payee' => [
        '#title' => $this->t('Payee wallet'),
        '#type' => 'wallet_entity_auto',
        '#default_value' => $this->entity->getPreset('payee') ? Wallet::load($this->entity->getPreset('payee')) : NULL,
      ],
      'payer' => [
        '#title' => $this->t('Payer wallet'),
        '#type' => 'wallet_entity_auto',
        '#default_value' => $this->entity->getPreset('payer') ? Wallet::load($this->entity->getPreset('payer')) : NULL,
      ]
    ];

    $form_display = EntityFormDisplay::load('mc_transaction.mc_transaction.default');
    foreach ($this->getAdditionalFieldDefinitions() as $fname => $field_definition) {
      $items = $this->dummyFieldItems($fname, $this->entity->getPreset($fname));
      $field_def = clone($field_definition);
      $field_def->setRequired(FALSE);
      $widget = \Drupal::service('plugin.manager.field.widget')->getInstance([
        'field_definition' => $field_def,
        'form_mode' => 'default',
        // No need to prepare, defaults have been merged in setComponent().
        'prepare' => TRUE,
        'configuration' => $form_display->getComponent($fname),
      ]);
      $element = ['#parents' => ['presets']];
      $form['presets'][$fname] = $widget->form($items, $element, $form_state)['widget'];
      $form['presets'][$fname]['#weight'] = 20;
    }
    $form['twig'] = [
      '#title' => t('Main form @Twig', ['@Twig' => 'Twig']),
      '#description' => t(
        'Use the following @Twig tokens with HTML & css to design your payment form. Linebreaks will be replaced automatically.',
        ['@Twig' => Link::fromTextAndUrl('Twig', Url::fromUri('http://twig.sensiolabs.org/doc/templates.html'))->toString()]
      ).t('N.B. This module supplies some default css classes and directives.'),
      '#type' => 'textarea',
      '#rows' => 6,
      '#default_value' => str_replace(array('\r\n', '\n', '\r'), "\n", $this->entity->getTwigTemplate()),
      '#element_validate' => [[$this, 'validateTwigTemplate']],
      '#required' => TRUE,
      '#weight' => $w++
    ];

    $required_properties = array_combine(self::ENTRY_PROPERTIES, self::ENTRY_PROPERTIES);
    if ($workflow = $this->entity->getWorkflow()) {
      if (!$workflow->isBill()) {
        unset($form['presets']['payer']);
      }
      if (!$workflow->isCredit()) {
        unset($form['presets']['payee']);
      }
      if ($workflow->isBill()) {
        unset($required_properties['payer']);
      }
      elseif ($workflow->isCredit()) {
        unset($required_properties['payee']);
      }
    }
    list($required_additional_fields, $optional_tokens) = $this->getRequiredFieldAPI();

    $form['twig']['#description'] .= t(
      'These fields must be included AND/OR preset: @fields.',
      ['@fields' => ' {{ ' . implode(' }}, {{ ', array_merge($required_additional_fields, $required_properties)) . ' }}']
    );
    if ($optional_tokens) {
      $form['twig']['#description'] .= ' '.t(
        'These fields are optional: @fields.',
        ['@fields' => ' {{ ' . implode(' }}, {{ ', $optional_tokens) . ' }}']
      );
    }

    $form['quicklink'] = [
      '#title' => t('Show quicklink on wallet'),
      '#descriptions' => t('Only if the wallet extrafield '),
      '#type' => 'checkbox',
      '#default_value' => $this->entity->getQuicklink(),
      '#weight' => $w++
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  private function getTransactionWidget($field_name) {
    return EntityFormDisplay::load('mc_transaction.mc_transaction.default')
      ->getRenderer($field_name);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    Cache::invalidateTags(['route_info']);
    $this->routerBuilder->rebuild();

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus(t('Form %label has been updated.', ['%label' => $this->entity->label()]));
    }
    else {
      $this->messenger()->addStatus(t('Form %label has been added.', ['%label' => $this->entity->label()]));
    }
    // how does this route not exist?
    $form_state->setRedirect('entity.mc_workflow.collection');
  }

  /**
   * Element validation callback ensuring that the given path is unique.
   */
  public static function uniquePath(&$element, $form_state) {
    $path = $form_state->getValue('path');
    $url = Url::fromUserInput($path);
    if ($url->isRouted()) {
      $params = $url->getRouteParameters();
      if (isset($params['mc_workflow']) and $params['mc_workflow'] <> $form_state->getFormObject()->entity->id()) {
        $form_state->setError($element, t('The path %path already exists.', ['%path' =>  $path]));
      }
    }
  }


  /**
   * Check the required transaction form elements are either preset OR in Twig.
   */
  public function validateTwigTemplate(array &$element, $form_state) {
    $twig_template = $element['#value'];
    preg_match_all('/\{\{ ?([a-z_]+) ?\}\}/', $twig_template, $matches);
    $used_tokens = $matches[1] ?? [];
    // Check for duplicate tokens.
    if (count($used_tokens) <> count(array_unique($used_tokens))) {
      $form_state->setError($element, t('Each twig token can only appear once in the form.'));
    }

    list($required_api_fields) = $form_state->getFormObject()->getRequiredFieldAPI();
    $preset_values = array_filter($form_state->getValue('presets'));
    // Check for missing required tokens.
    $additional_fields = array_diff($required_api_fields, array_keys($preset_values), Self::ENTRY_PROPERTIES);
    foreach ($additional_fields as $fname) {
      if (!in_array($fname, $used_tokens)) {
        $field_items = $this->dummyFieldItems($fname, $form_state->getvalue('presets.fname'));
        if ($field_items->isEmpty()) {
          $form_state->setError($element, t('Missing required token @token', ['@token' => '{{ '.$fname.' }}']));
        }
      }
    }
  }

  /**
   * Make a dummy transaction fieldItemList with a value already set.
   */
  private function dummyFieldItems($field_name, $field_value) : FieldItemList {
    return Transaction::create(['workflow' => '+|PC-CE0', $field_name => $field_value])->get($field_name);
  }

  /**
   * Get the transaction entity's required and optional form fields
   * @return array
   */
  public function getRequiredFieldAPI() : array {
    // Fields that cannot be set
    $required_field_api = $optional_field_api = [];
    $fixed = ['xid', 'uuid', 'serial', 'uid', 'workflow', 'state', 'changed', 'created', 'entries', 'vxid', 'revision_default'];
    foreach ($this->getAdditionalFieldDefinitions() as $id => $field_def) {
      /** @var \Drupal\Core\Field\BaseFieldDefinition $field_def */
      if (in_array($id, $fixed)) {
        continue;
      }
      if ($field_def->isRequired()) {
        $required_field_api[] = $id;
      }
      else {
        $optional_field_api[] = $id;
      }
    }
    return [$required_field_api, $optional_field_api];
  }

  /**
   * Convenience
   */
  private function getAdditionalFieldDefinitions() :array {
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $all_fields = $entity_field_manager->getFieldDefinitions('mc_transaction', 'mc_transaction');
    $base_fields = $entity_field_manager->getBasefieldDefinitions('mc_transaction');
    return array_diff_key($all_fields, $base_fields);
  }

}
