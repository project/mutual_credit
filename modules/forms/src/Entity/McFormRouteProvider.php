<?php

namespace Drupal\mcapi_forms\Entity;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Config\Config;
use \Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides routes for the McForm entity.
 */
class McFormRouteProvider extends DefaultHtmlRouteProvider {

  protected $config;

  /**
   * Constructs a new DefaultHtmlRouteProvider.
   */
  public function __construct($entity_type_manager, $entity_field_manager, Config $config) {
    parent::__construct($entity_type_manager, $entity_field_manager);
    $this->config = $config;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory')->get('mcapi.settings')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    // Add a route for the enable/disable form
    $collection->add(
      "entity.mc_form.status_form",
      new Route(
        '/admin/accounting/form/{mc_form}/status',
        ['_entity_form' => 'mc_form.status'],
        ['_permission' => 'configure mcapi'],
        ['_admin_route' => TRUE]
      )
    );
    return $collection;
  }

}

