<?php

namespace Drupal\mcapi_forms\Entity;

use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the designed form entity class.
 *
 * @ConfigEntityType(
 *   id = "mc_form",
 *   label = @Translation("Transaction form"),
 *   config_prefix = "mc_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   handlers = {
 *     "form" = {
 *       "edit" = "Drupal\mcapi_forms\McFormEditorForm",
 *       "add" = "Drupal\mcapi_forms\McFormEditorForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "status" = "Drupal\mcapi_forms\ConfirmMcFormForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\mcapi_forms\Entity\McFormRouteProvider",
 *     }
 *   },
 *   links = {
 *     "add-form" = "/admin/accounting/form/create",
 *     "edit-form" = "/admin/accounting/form/{mc_form}/edit",
 *     "delete-form" = "/admin/accounting/form/{mc_form}/delete",
 *     "status-form" = "/admin/accounting/form/{mc_form}/status"
 *   },
 *   admin_permission = "configure mcapi",
 *   config_export = {
 *     "id",
 *     "status",
 *     "label",
 *     "purpose",
 *     "wallet_link_title",
 *     "workflow_id",
 *     "twig",
 *     "menu",
 *     "presets",
 *     "quicklink",
 *     "dependencies"
 *   }
 * )
 */
class McForm extends ConfigEntityBase {

  /**
   * Unique ID
   * @var string
   */
  public $id;

  /**
   * Name to show in lists
   * @var string
   */
  public $label;

  /**
   * Name to show in lists
   * @var string
   */
  public $purpose;

  /**
   * Short description
   * @var string
   */
  public $wallet_link_title;

  /**
   * Whether this workflow is allowed to be used
   * @var string
   */
  public $workflow_id;

  /**
   * Structured data
   * @var string
   */
  public $twig;

  /**
   * Structured data
   * @var array
   */
  public $presets;

  /**
   * The menu link title, parent menu, and weight
   * @var array
   */
  public $menu;

  /**
   * TRUE to show a link to this form in the wallet extrafields quicklinks.
   * @var bool
   */
  protected $quicklink;


  function getWalletLinkTitle() : string {
    return (string) $this->wallet_link_title;
  }

  function getWorkflowId() : string {
    return (string)$this->workflow_id;
  }

  function setWorkflowId(string $workflow_id) : static {
    $this->workflow_id = $workflow_id;
    return $this;
  }

  function getWorkflow() : Workflow|NULL {
    if ($id = $this->workflow_id) {
      return Workflow::load($id);
    }
    return NULL;
  }
  function getMenuConf() : \stdClass|NULL {
    return $this->menu ? (object)$this->menu : NULL;
  }

  function getPreset($field_name) {
    return $this->presets[$field_name] ?? '';
  }

  function showTab() : bool {
    return $this->menu ? !empty($this->menu['tab']) : FALSE;
  }

  function getTwigTemplate() : string {
    return (string) $this->twig;
  }

  function setTwigTemplate(string $twig) : static {
    $this->twig = $twig;
    return $this;
  }

  function getQuicklink() : bool {
    return (bool)$this->quicklink;
  }

  function getRouteId() {
    return 'mcapi_forms.add_transaction';
  }

  /**
   * If a workflow is deleted this form should be deleted as well.
   */
  public function calculateDependencies() {
    // If we use $this->addDependency($type, $name) it doesn't clear the previous previous setting
    parent::calculateDependencies();
    // this seems to have blocked installation.
    if ($this->workflow_id <> 'fallback') {
      $this->addDependency('config', 'mcapi.mc_workflow.'.$this->workflow_id);
    }
    return $this;
  }

}
