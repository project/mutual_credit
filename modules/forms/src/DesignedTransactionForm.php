<?php

namespace Drupal\mcapi_forms;

use Drupal\mcapi\Functions;
use Drupal\mcapi\Form\TransactionForm;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Plugin\Field\FieldWidget\Entry;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Entity\EntityFieldManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form builder for transaction formed designed via the UI.
 */
class DesignedTransactionForm extends TransactionForm {

  /**
   * @var \Drupal\mcapi_forms\Entity\McForm
   */
  protected $config;

  /**
   * @var Renderer
   */
  protected $renderer;

  /**
   * @var EntityFieldManager
   */
  protected $entityFieldManager;

  private array $addedFields;
  private array $rawElements;

  /**
   * @param $entity_repository
   * @param $entity_type_bundle_info
   * @param $time
   * @param $tempstoreFactory
   * @param $current_request
   * @param Renderer $renderer
   * @param EntityFieldManager $entity_field_manager
   */
  public function __construct($entity_repository, $entity_type_bundle_info, $time, $tempstoreFactory, $current_request, Renderer $renderer, EntityFieldManager $entity_field_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time, $tempstoreFactory, $current_request);
    $this->renderer = $renderer;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('tempstore.private'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('renderer'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  function init(FormStateInterface $form_state) {
    parent::init($form_state);
    if (!isset($this->config)) {
      $this->config = $form_state->get('mc_form');
    }
    else {
      $form_state->set('mc_form', $this->config);
    }
    $this->addedFields = array_filter(
      $this->entityFieldManager->getFieldDefinitions('mc_transaction', 'mc_transaction'),
      function ($f) {return !$f instanceOf \Drupal\Core\Field\BaseFieldDefinition;}
    );
    $this->rawElements = Entry::rawElements($this->entity->entries->first(), $this->entity->workflow->entity->direction());
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->config->label();
    // Replace the new line chars. Belt and Braces.
    $form['#twig_template'] = str_replace(["\r\n","\n","\r", '\r\n', '\r', '\n'], "<br/>", $this->config->getTwigTemplate());
    // Avoid calling parent::form and adding the fields.
    $form['#process'][] = '::processForm';
    $form['#entity_builders']['update_form_langcode'] = '::updateFormLangcode';
    $form['#parents'] = [];
    $this->renderer->addCacheableDependency($form, $form_state->get('form_display'));

    preg_match_all('/\{\{ *(.*?) *\}\}/', $form['#twig_template'], $token_matches);
    $form['#tokens'] = $token_matches[1];
    foreach ($form['#tokens'] as $fieldname) {
      $form[$fieldname] = $this->viewField($form, $form_state, $fieldname, $this->config->getPreset($fieldname)) + [
        '#prefix' => '<span class="form-item">',
        '#suffix' => '</span>'
      ];
    }

    $form['#attached']['library'][] = 'mcapi_forms/display';
    $form['#after_build'][] = '::prepareTemplateForm';

    if ($this->moduleHandler->moduleExists('contextual')) {
      // Seems very difficult to put contextual links on a form. This renders but hidden without wrapper.
      $form['prefix'] = [
        '#type' => 'contextual_links',
        '#contextual_links' => ['mc_form' => ['route_parameters' => ['mc_form' => $this->config->id]]],
        '#weight' => -50,
      ];
      $form['#attached']['library'][] = 'contextual/drupal.contextual'; // not sure if this is needed but no harm putting it anyway.
    }
    return $form;
  }

  /**
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @param string $fieldname
   * @param $default_value
   * @return array
   */
  protected function viewField(array $form, FormStateInterface $form_state, string $fieldname, $default_value = NULL) : array {
    if (isset($this->addedFields[$fieldname])) {// API field...
      $items = $this->getEntity()->get($fieldname);
      $instance_settings = [
        'field_definition' => $this->addedFields[$fieldname],
        'configuration' => [],
        'view_mode' => 'default',
      ];
      if ($default_value) {
        $items->setValue($default_value);
        // We don't have any particular ViewDisplay with any settings for the component.
        $formatter = \Drupal::service('plugin.manager.field.formatter')->getInstance($instance_settings);
        $formatter->prepareview([$items]);
        return $formatter->viewElements($items, NULL);
      }
      else {
        $instance_settings += ['configuration' => $form_state->get('form_display')->getComponent($fieldname)];
        // Show the widget.
        $elements = \Drupal::service('plugin.manager.field.widget')
          ->getInstance($instance_settings)
          ->form($items, $form, $form_state);
        // Hide the field title
        $field_key = key($elements['widget'][0]);
        $elements['widget'][0][$field_key]['#title_display'] = 'hidden';
        return $elements;
      }
    }
    elseif (in_array($fieldname, ['payee', 'payer'])) {
      if ($default_value) {
        return ['#markup' => Wallet::load($default_value)->label()];
      }
      else {
        return $this->rawElements[$fieldname.'_wid'] + ['#required' => TRUE];
      }
    }
    elseif ($fieldname == 'quant') {
      if ($default_value) {
        return ['#markup' => Functions::formatCurrency($default_value)];
      }
      else {
        return $this->rawElements[$fieldname];
      }
    }
    elseif ($fieldname == 'description') {
      if ($default_value) {
        return ['#markup' => Markup::Create($default_value)];
      }
      else {
        return $this->rawElements[$fieldname];
      }
    }
    throw new \Exception("Unknown transaction form field $fieldname. Translation error?");
  }

  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Send');
    // @todo check this is working.
    $form['#cache']['contexts'][] = 'user';
    return $actions;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'designed_transaction_form';
  }

  /**
   * Provide an ID for the transaction form consistent with the keys of hook_transction_form_list().
   */
  function transactionFormId() {
    return 'designed_'.$this->config->id();
  }

  /**
   * {@inheritDoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $presets = $this->config->get('presets');
    $form_state->setValue('entries', [[
      'description' => $form_state->getValue('description') ?? $presets['description'],
      'quant' => $form_state->getValue('quant') ?? $presets['quant'],
      'payee_wid' => $form_state->getValue('payee') ?? $presets['payee'],
      'payer_wid' => $form_state->getValue('payer') ?? $presets['payer'],
      'primary' => TRUE
    ]]);
    unset($presets['payee_wid'], $presets['payer_wid'], $presets['description'], $presets['quant']);
    // If a field was preset but not present in the form...
    foreach ($presets as $field_name => $value) {
      if (!$form_state->getValue($field_name)) { // it probably wasn't rendered the form
        $form_state->setValue($field_name, $value);
      }
    }
    return parent::buildEntity($form, $form_state);
  }

  /**
   * Form #afterbuild callback
   * Break up the template into render elements alternating between markup and form elements.
   */
  function prepareTemplateForm($form, $form_state) {
    // Move the form elements to be placed from tokens out of the form and into
    // $replacements, stripping off the containers.
    foreach ($form['#tokens'] as $fname) {
      $form[$fname]['#array_parents'] = ['payer', 'widget', 0];
      $replacements[$fname] = $form[$fname];
      unset($form[$fname]);
    }
    $w = 0;
    // Break up the template into alternating tokens and html snippents
    $tok = strtok(' '.$form['#twig_template'], '{');// ensure the first is text
    while ($tok !== FALSE) {
      $parts[] = $tok;
      $tok = strtok('{}');
    }
    foreach ($parts as $key => $snippet) {
      $snippet = trim($snippet);
      if ($key % 2 == 0) { //even numbers are text
        $form['snippet-'.$key] = [
          '#markup' => '&nbsp;'.$snippet.'&nbsp;',
          '#weight' => $w++,
          '#parents' => []
        ];
      }

      // Replace the fields
      elseif (isset($replacements[$snippet])) { // odd numbers are tokens
        $element = $replacements[$snippet];
        $element['#weight'] = $w++;
        if (empty($element['#parents'])) {
          $element['#parents'] = $element['#array_parents'] = [$element['#name']];
        }
        $element['#array_parents']= $element['#parents'];
        unset($element['#theme_wrappers']);
        $form[$snippet] = $element;
      }
      else {
        throw new \Exception("Unrecognised token in designed transaction form: $snippet");
      }
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $this->config = $this->request->attributes->get('mc_form');
    if (!$this->config->status()) {
      // @todo have the router access understand if the workflow is disabled.
      // N.B. is only one route for transaction forms
      throw new NotFoundHttpException();
    }
    $this->request->attributes->set('mc_workflow', $this->config->getWorkflow());
    return parent::getEntityFromRouteMatch($route_match, $entity_type_id);
  }

  /**
   * Ensure that errors to the payer, payee, quant and description fields are processed.
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    $fields = parent::getEditedFieldNames($form_state);
    $fields[] = 'entries';
    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    foreach ($violations as $offset => $violation) {
      $propPath = $violation->getPropertyPath();
      if (strrpos($propPath, '.')) {
        $fieldname = substr($propPath, strrpos($propPath, '.') + 1);
      }
      else {
        $fieldname = $propPath;
      }

      switch ($fieldname) {
        // Drupals' form.css expects these fields to be in a <div class="form-item"> to show the error message
        case 'payee_wid':
          $element = $form['payee'];
          break;
        case 'payer_wid':
          $element = $form['payer'];
          break;
        case 'quant':
        case 'description':
          $element = $form[$fieldname];
          break;
        default:
          break 2;
      }
      $form_state->setError($element, $violation->getMessage());
      $violations->remove($offset);
    }
    parent::flagViolations($violations, $form, $form_state);
  }

}
