<?php

namespace Drupal\mcapi_forms\Plugin\Block;

use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi_forms\Entity\McForm;
use Drupal\mcapi\Entity\Transaction;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Component\Plugin\Exception\PluginException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a designed transaction form in a block.
 */
#[Block(
  id: 'mc_form',
  admin_label: new TranslatableMarkup('Transaction form'),
  category: new TranslatableMarkup('Community Accounting')
)]
class DesignedForm extends BlockBase implements ContainerFactoryPluginInterface {

  protected $mcForm;
  protected $mcWorkflow;

  /**
   * Constructor
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $mc_form, $mc_workflow) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mcForm = $mc_form;
    $this->mcWorkflow = $mc_workflow;
  }

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param type $plugin_id
   * @param type $plugin_definition
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    if ($mc_form = McForm::load($configuration['editform_type'])) {
      if ($mc_workflow = Workflow::load($mc_form->getWorkflowId())) {
        return new static ($configuration, $plugin_id, $plugin_definition, $mc_form, $mc_workflow);
      }
      throw new PluginException("Failed to load workflow $mc_form->workflow_id for mc_form: ".$configuration['editform_type']);
    }
    throw new PluginException("DesignedForm block failed to load Form: ".$configuration['editform_type']);
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    return \Drupal::service('entity.form_builder')
      ->getForm(
        Transaction::create(['workflow' => $this->mcWorkflow->getCode()]),
        'designed',
        ['mc_form' => $this->mcForm]
      );
  }

  /**
   * {@inheritDoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $access = parent::access($account, TRUE);
    if ($this->mcWorkflow->isBill()) {
      $perm = 'bill wallets';
    }
    elseif ($this->mcWorkflow->isCredit()) {
      $perm = 'credit wallets';
    }
    else {
      $perm = 'manage mcapi';
    }
    $access->andIf(AccessResult::allowedIfHasPermission($account, $perm));

    $route_name = \Drupal::service('current_route_match')->getRouteName();
    if (!$this->mcWorkflow) {
      $access = AccessResult::forbidden('Designed transaction form not available: '.$this->configuration['editform_type']);
    }
    // Block is available if the main page is not already a transaction form.
    elseif (substr($route_name, 0, 14) == 'mcapi.mc_form.') {
      $access = AccessResult::forbidden('Transaction form block does not show on the form page');
    }
    // Or an operation form)
    elseif (substr($route_name, 0, 22) == 'entity.mc_transaction.') {
      $access = AccessResult::forbidden('Transaction forms do not show on transaction operation pages.');
    }
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $options = [];
    foreach (McForm::loadMultiple() as $id => $mcapiForm) {
      $options[$id] = $mcapiForm->label();
    }
    $form['editform_type'] = [
      '#title' => t('Display'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->configuration['mode']??key($options),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['editform_type'] = $form_state->getValue('editform_type');
  }

}
