<?php

namespace Drupal\mcapi_forms\Plugin\Derivative;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Provides local actions to create smallads of each type.
 */
class TransactionFormTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var ConfigEntityStorage
   */
  private ConfigEntityStorage $mcFormStorage;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->mcFormStorage = $entity_type_manager->getStorage('mc_form');
  }

  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritDoc}
   * Make a top level tab for each of this modules forms.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $w=0;
    //foreach ($this->mcFormStorage->loadMultiple() as $mc_form) {if ($mc_form->menu['tab'])
    foreach ($this->mcFormStorage->loadByProperties(['tab' => 1, 'status' => 1]) as $mc_form) {
      $this->derivatives['mcapi_forms.tabs.'.$mc_form->id()] = [
        'title' => $mc_form->menu['title'],
        'description' => $mc_form->purpose,
        'route_name' => 'mcapi_forms.mc_form.default',
        'route_parameters' => ['mc_form' => $mc_form->id()],
        'base_route' => 'mcapi_forms.mc_form.default',
        'weight' => $w++,
      ] + $base_plugin_definition;
    }
    // Put the forms in order of the menu weight
    usort(
      $this->derivatives,
      function ($a, $b){return $a->menu['weight'] <=> $b->menu['weight'];}
    );
    return $this->derivatives;
  }

}
