<?php

namespace Drupal\mcapi_forms\Plugin\Derivative;

use Drupal\mcapi_forms\Entity\McForm;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides links in the account menu for designed forms
 */
class TransactionFormLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    foreach (McForm::loadMultiple() as $id => $mc_form) {
      $menu_conf = $mc_form->getMenuConf();
      if ($mc_form->status() and $menu_conf->parent and $menu_conf->title) {
        list($menu_name, $parent_link_id) = explode(':', $menu_conf->parent);
        $this->derivatives["mcapi.mcform.$id.link"] = [
          // Couldn't work out how to make this deep bit of config translatable.
          'title' => $menu_conf->title,
          'route_name' => "mcapi_forms.mc_form.default",
          'route_parameters' => ['mc_form' => $id],
          'menu_name' => $menu_name,
          'parent' => $parent_link_id,
        ] + $base_plugin_definition;
      }
    }

    usort(
      $this->derivatives,
      function ($a, $b){return $a['title'] <=> $b['title'];}
    );
    return $this->derivatives;
  }

}
