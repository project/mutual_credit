<?php

namespace Drupal\mcapi_forms\Plugin\Derivative;

use Drupal\mcapi_forms\Entity\McForm;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides links in the account menu for designed forms
 */
class ContextualLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritDoc}
   * Make a default tab for the main views
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $forms = McForm::loadMultiple();

    foreach ($forms as $mcForm) {
      $id = $mcForm->id();
      $menu_item = $mcForm->getMenuItem();
      if (!empty($menu_item->parent) and !empty($menu_item->title)) {
        list($menu_name, $parent_link) = explode(':', $menu_item->parent);
        if (strpos($menu_item->title, '/')) {
          [,$menu_title] = explode('/', $menu_item->title);
        }
        else {
          $menu_title = $menu_item->title;
        }
        $this->derivatives["mcapi_form.$id"] = [
          // Couldn't work out how to make this deep bit of config translatable.
          'title' => 'edit',
          'route_name' => 'mcapi_forms.mc_form.default',
          'route_parameters' => ['mc_form' => $id],
          'group' => 'mcapi_form'
        ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }

}