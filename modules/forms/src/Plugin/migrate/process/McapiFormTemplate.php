<?php

namespace Drupal\mcapi_forms\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\mcapi_forms\McFormFunctions;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert the form template.
 */
#[MigrateProcess(
  id: 'd7_mcapiform_template'
)]
class McapiFormTemplate extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   *
   * Old template is of the form:
   * Payer: [mcapiform:payer] Payee: [mcapiform:payee] [mcapiform:worth] for [mcapiform:description]
   *
   * $value is the 'experience' array from the d7 ctools object.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $template = str_replace('[mcapiform:direction]', '', $value->template);
    // Replace the tokens

    $map = McFormFunctions::MigrateTemplateFieldMap($template);
    $from = ['[mcapiform:worth]'];
    $to = ['{{ quant }}'];
    if ($row->getSourceProperty('perspective') == 1) {
      $from[] = '[mcapiform:direction]';
      $from[] = '[mcapiform:secondperson]';
      if ($row->getSourceProperty('direction')->preset == 'outgoing') {
        $to[] = t("I'm giving them");
        $to[] = '{{ payee }}';
      }
      else {
        $to[] = t("I'm billing them");
        $to[] = '{{ payer }}';
      }
    }
    foreach ($map as $old => $new) {
      $from[] = '[mcapiform:'.$old.']';
      $to[] = "{{ $new }}";
    }
    $template = str_replace($from, $to, $template);
    return $template;
  }

}
