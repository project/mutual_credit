<?php

namespace Drupal\mcapi_forms\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert the form type.
 */
#[MigrateProcess(
  id: 'd7_mcapi_form_type'
)]
class McapiFormType extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   *
   * Old template is of the form:
   * Payer: [mcapiform:payer] Payee: [mcapiform:payee] [mcapiform:worth] for [mcapiform:description]
   *
   * $value should 1 or 3, i.e. the perspective from the d7 form
   * output a workflow code.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value == 3) {
      return 'fallback';
    }
    elseif ($value == 1) {
      $dir = $row->getSourceProperty('direction')->preset;
      if ($dir == 'incoming') {
        return 'default_bill';// +|PC-CE0';
      }
      elseif ($dir == 'outgoing') {
        return 'default_credit';// -|PC+CE0';
      }
    }
    throw new MigrateSkipRowException("Unrecognised direction $dir");
  }

}
