<?php

namespace Drupal\mcapi_forms\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\migrate\Row;

/**
 * Currencies in d7 were built on ctools
 *
 * @MigrateSource(
 *   id = "d7_mcapi_forms",
 *   source_module = "mcapi"
 * )
 */
class TransactionForm extends DrupalSqlBase {

  /**
   * {@inheritDoc}
   */
  public function query() {
    // NB Only overriden ctools objects are even stored in the database.
    return $this->select('mcapi_forms', 'f')
      ->fields('f', ['name']);
  }

  protected function values() {
    return $this->prepareQuery()->execute()->fetchCol();
  }

  /**
   * {@inheritDoc}
   */
  public function count($refresh = false) {
    return intval($this->query()->countQuery()->execute()->fetchField());
  }


  /**
   * {@inheritDoc}
   */
  public function prepareRow(Row $row) {
    static $i = 0;
    $result = $this->select('mcapi_forms', 'f')
      ->fields('f', ['data'])
      ->condition('name', $row->getSourceProperty('name'))
      ->execute()
      ->fetchField();

    $currency = unserialize($result);
    foreach ((array)$currency as $key => $val) {
      if (is_array($val)) {
        $val = (object)$val;
      }
      $row->setSourceProperty($key, $val);
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    $fields = [
      'name' => $this->t('Ctools key'),
      'data' => $this->t('Serialized data'),
    ];
    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    return [
      'name' => [
        'type' => 'string',
      ]
    ];
  }

}
