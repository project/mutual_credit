<?php

namespace Drupal\mcapi_forms;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Provides a form for enabling/disabling Designed transaction forms.
 */
class ConfirmMcFormForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $mc_form = $this->getEntity();
    if ($mc_form->status()) {
      return $this->t("Disable the '@label' form?", ['@label' => $mc_form->label()]);
    }
    else {
      return $this->t("Enable the '@label' form?", ['@label' => $mc_form->label()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Can be changed at any time.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // Return the cancel URL.
    return new Url('entity.mc_workflow.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->getEntity()->status() ? $this->t('Disable') : $this->t('Enable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Perform the action based on the form values.
    $entity = $this->getEntity();
    // Perform your logic to enable or disable the entity.
    if ($entity->status()) {
      $entity->disable();
    }
    else {
      $entity->enable();
    }
    $entity->save();

    if ($entity->status()) {
      $message =  $this->t("Entity '@label' was enabled", ['@label' => $entity->label()]);
    }
    else {
      $message =  $this->t("Entity '@label' was disabled", ['@label' => $entity->label()]);
    }
    \Drupal::messenger()->addStatus($message);

    // Redirect to the entity listing page after the action is completed.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
