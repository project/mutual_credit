<?php

namespace Drupal\mcapi_forms\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\mcapi_forms\Entity\McForm;

/**
 * Tweaks to 'designed form' migration.
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']],
      'migrate.post_import' => [['migratePostImport']]
    ];
  }

  /**
   * @param MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    switch ($event->getMigration()->id()) {
      case 'd7_filter_format':
        if ($row->getSourceProperty('format') == 'mcapiform_string_format') {
          return new MigrateSkipRowException('Deprecated filter format');
        }
        break;

      case 'd7_menu_links':
        $link_path = $row->getSourceProperty('link_path');
        if ($link_path == 'transact/3rdparty')   {
          $row->setDestinationProperty('link_path', 'transaction/add');
        }
        if ($link_path == 'transact/1stparty')   {
          $row->setDestinationProperty('link_path', 'transact/credit');
        }
        break;
      case 'd7_mcapi_form':
        if ($row->getSourceProperty('perspective') == 3) {
          return new MigrateSkipRowException('Not migrating 3rdparty forms'); // Runs but has no effect???
        }
        elseif ($dir = $row->getSourceProperty('direction')->preset) {
          // Removing the default form that the migrated form replaces
          $remove = $dir == 'incoming'? 'standard_bill' : 'standard_credit';
          $this->deleteForm($remove, $row->getSourceProperty('id'));
        }
    }
  }

  /**
   * @param MigrateImportEvent $event
   */
  function migratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_mcapi_form') {
      $event->logMessage('Route access on all custom transaction forms must be re-configured.', 'warning');
      $this->deleteForm('3rdparty');
    }
  }

  /**
   *
   */
  private function deleteForm($old_form_id, $new_form_id = NULL) {
    if ($new_form_id) {
      // There might be an mc_form block relying on this
      $blocks = \Drupal::entityTypeManager()->getStorage('block')->loadByProperties(['plugin' => 'mc_form']);
      foreach ($blocks as $block) {
        /** @var \Drupal\block\Entity\Block $block */
        $settings = $block->get('settings');
        if ($settings['editform_type'] == $old_form_id) {
          $settings['editform_type'] = $new_form_id;
          // This somehow tries to load the designedform and fails it is already deleted.
          $block->set('settings', $settings);
          $block->save;
        }
      }
    }
    if ($f = McForm::load($old_form_id)) {
      $old_form_id->delete(); // Compensates for failed MigrateSkipRowException
    }
  }

}
