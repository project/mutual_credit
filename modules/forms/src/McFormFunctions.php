<?php

namespace Drupal\mcapi_forms;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\Core\Url;

class McFormFunctions {

  /**
   * Create links to each of a wallet's payment forms, prepopulated if possible.
   *
   * Iterate through the transaction form displays and create links to ones using
   * the 'my_wallet' widget.
   *
   * @param WalletInterface $wallet
   *
   * @return array[]
   *   Items each containing a Url and string value
   */
  static function quickLinks(WalletInterface $wallet) : array {
    $links = [];
    // Put a link to specified designed forms
    $mc_forms = \Drupal::entityTypeManager()
      ->getStorage('mc_form')
      ->loadByProperties(['quicklink' => TRUE]);
    foreach ($mc_forms as $id => $mc_form) {
      /** @var Drupal\mcapi\Entity\Workflow $workflow */
      $workflow = $mc_form->getWorkflow();
      $options = [];
      if ($workflow->isBill()) {
        $options['query'] = ['payer' => $wallet->id()];
      }
      elseif ($workflow->isCredit()) {
        $options['query'] = ['payee' => $wallet->id()];
      }

      $links[$id] = [
        'title' => str_replace('[mc_wallet:name]', $wallet->label(), $mc_form->getWalletLinkTitle()),
        'url' => Url::fromUserInput('/transact/'.$mc_form->id(), $options)
      ];
    }
    return $links;
  }

  /**
   * Migration helper.
   * Get only the translated field names used in the template.
   *
   * @param string $template
   * @param string $dir
   *   outgoing, incoming or 3rdparty
   *
   * @return array
   *   The field names used in the templated, mapped to the modern names.
   */
   static function MigrateTemplateFieldMap($template) : array {
    //Get the tokens used in the old template
    preg_match_all('/\[mcapiform:([a-z_]+)\]/', $template, $matches);
    $old_tokens = $matches[1];
    unset($old_tokens[array_search('direction', $old_tokens)]);

    $mapping = [
      "payee" => "payee",
      "payer" => "payer",
      "transaction_description" => "description"
    ] + array_combine($old_tokens, $old_tokens);

    return $mapping;

  }

}
