<?php

namespace Drupal\mcapi_signatures\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the deprecated wallet entity to enable a smooth uprade -  the new wallet entity is mc_wallet
 *
 * @ContentEntityType(
 *   id = "mcapi_wallet",
 *   label = @Translation("Wallet DO NOT USE"),
 *   module = "mcapi_signatures",
 *   handlers = {
 *     "views_data" = "Drupal\mcapi\Entity\Views\WalletViewsData"
 *   },
 *   base_table = "mcapi_wallet",
 *   entity_keys = {
 *     "label" = "name",
 *     "owner" = "uid",
 *     "id" = "wid",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class Wallet extends ContentEntityBase {

//  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
//    $fields = parent::baseFieldDefinitions($entity_type);
//    $fields['holder'] = BaseFieldDefinition::create('dynamic_entity_reference')
//      ->setLabel(t('Holder (dummy field)'))
//      ->setSettings([
//        'user' => [
//          'handler' => 'default:user',
//          'autowallet' => '1'
//        ]
//      ]);
//    return $fields;
//  }
}
