<?php

namespace Drupal\mcapi_signatures\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the transaction entity to enable a smooth upgrade.
 *
 * @ContentEntityType(
 *   id = "mcapi_transaction",
 *   label = @Translation("Transaction DO NOT USE"),
 *   module = "mcapi_signatures",
 *   base_table = "mcapi_transaction",
 *   entity_keys = {
 *     "id" = "xid"
 *   },
 * )
 */
class Transaction extends ContentEntityBase {

}
