<?php

namespace Drupal\mcapi_signatures\Plugin\views\access;

use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\Routing\Route;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Attribute\ViewsAccess;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Access plugin that provides permission-based access control.
 *
 * @ingroup views_access_plugins
 */
#[ViewsAccess(
  id: 'mcapi_wallet_holder',
  title: new TranslatableMarkup("Wallet holder (DO NOT USE)"),
  help: new TranslatableMarkup("Dummy plugin just needed to allow system to bootstrap before upgrading from v5.")
)]
class WalletHolder extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    //return \Drupal\Core\Access\AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {}

}
