<?php

namespace Drupal\mcapi_signatures\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\StringTranslation\TranslatableMarkup;
/**
 * FieldType definition to refer to a wallet e.g. in a transaction.
 */
#[FieldType(
  id: 'wallet_reference',
  label: new TranslatableMarkup('Wallets (DO NOT USE)'),
  description: new TranslatableMarkup('Dummy plugin to enable upgrade from version 5'),
  no_ui: 'true'
)]
class WalletReferenceItem extends EntityReferenceItem {


}
