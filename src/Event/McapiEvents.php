<?php

namespace Drupal\mcapi\Event;

/**
 * Contains all events for transitioning transactions between workflow states.
 */
final class McapiEvents {

  /**
   * The name of the event triggered when a transaction is changing state.
   *
   * This event allows modules to react at two stages of the transaction being
   * saved.
   *
   * @Event
   *
   * @var string
   */
  const ACTION = 'mc_transaction.action';

}
