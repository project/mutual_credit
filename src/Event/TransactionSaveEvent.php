<?php

namespace Drupal\mcapi\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Event which is fired on any transaction operation.
 */
class TransactionSaveEvent extends GenericEvent {

  const EVENT_NAME = 'mc_transaction.save';

  private $messages = [];

  /**
   *
   * @param $subject
   * @param array $arguments
       -old_state: state of the transaction before form was submitted
   */
  public function __construct($subject = null, array $arguments = []) {
    // Clone the transaction to make sure it isn't modified by any of the EventHandlers.
    $this->subject = clone($subject);
    $this->arguments = $arguments;
  }

  /**
   * Store a message with this transaction.
   *
   * @param string $markup
   *   The message to be set.
   * @param string $type
   *   The type of message i.e. status, warning or error.
   */
  public function setMessage($markup, $type = 'status') {
    if (strlen($markup)) {
      $this->messages[$type][] = $markup;
    }
  }

  /**
   * Retrieve the messages associated with this event.
   *
   * @return array
   *   renderable array
   */
  public function getMessages() {
    return $this->messages;
  }

}
