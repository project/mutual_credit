<?php

namespace Drupal\Mcapi;

use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Entity\TransactionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Component\Utility\Html;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\mcapi\Entity\Storage\WalletStorage;


/**
 * Utility class for community accounting.
 */
class Functions {

  /**
   * Get a list of entity labels keyed by entity ID suitable for a select box.
   * @param string $entity_type_id
   * @return string[]
   *
   * @note this is only used for mc_state, mc_workflow ATM
   */
  static function workflowOptions() : array {
    $entities = \Drupal::entityTypeManager()->getStorage('mc_workflow')->loadMultiple();
    $contextRepo = \Drupal::service('entity.repository');
    foreach ($entities as $entity) {
      $label = $contextRepo->getTranslationFromContext($entity)->label();
      $summary = $contextRepo->getTranslationFromContext($entity)->summary;
      $list[$entity->id()] = Html::escape($label) .' - '. Html::escape($summary);
    }
    return $list;
  }

  /**
   * Get a list of all the tokens on the transaction entity.
   *
   * @note This really needs replacing with a core function.
   */
  public static function tokenHelp() : string {
    foreach (array_keys(\Drupal::Token()->getInfo()['tokens']['xaction']) as $token) {
      $tokens[] = "[xaction:$token]";
    }
    return implode(', ', $tokens);
  }

  /**
   * Format a raw currency value and add a link to the stats page.
   *
   * @staticvar CurrencyFormatter $formatter
   * @param float $raw_num
   * @param type $linked
   *   Link it to the metrics page.
   * @return Markup
   */
  public static function formatCurrency(float $raw_num) : Markup {
    if ($raw_num == 0 and $snippet = \Drupal::config('mcapi.settings')->get('zero_snippet')) {
      $markup = Markup::create($snippet);
    }
    else {
      $markup = static::formatBalance($raw_num, FALSE);
    }
    return $markup;
  }

  /**
   * Format a raw currency value and add a link to the stats page.
   *
   * @staticvar CurrencyFormatter $formatter
   * @param float $raw_num
   * @param int $linked
   *   Wallet ID to link to, or 0 for econometrics. No link by default.
   * @return Markup
   */
  public static function formatBalance(float $raw_num, int $target = NULL) : Markup {
    static $formatter;
    /** @var \Drupal\mcapi\CurrencyFormatter $formatter */
    if (!isset($formatter)) {
      $formatter = \Drupal::Service('mcapi.currency_formatter');
    }
    // Replace the parts back into the number format.
    $output = $formatter->format($raw_num);
    // Link it to the currency stats page.
    if ($target === 0) {
      $output = (string)Link::createFromRoute(['#markup' => $output], 'mcapi.metrics')->toString();
    }
    elseif($target) {
      $output = (string)Link::createFromRoute(['#markup' => $output], 'entity.mc_wallet.canonical', ['mc_wallet' => $target])->toString();
    }
    return Markup::create($output);
  }

  /**
   * Get transactions needing signature of the given user.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return int[]
   *   Transaction IDs
   */
  static function transactionsNeedingSigOfUser($uid) : array {
    $needed = [];
    // pending transactions in which the user is involved where the PC transaction role (+-) corresponds to the payer or payee
    $xids = Functions::pendingTransactionsOfUser($uid);
    foreach (Transaction::loadMultiple($xids) as $t) {
      if (
        ($t->payer->getOwnerId() == $uid and $t->workflow->value[3] == '-') or
        ($t->payee->getOwnerId() == $uid and $t->workflow->value[3] == '+') )
      {
        $needed[] = $xid;
      }
    }
    return $needed;
  }

  /**
   * Get all transactions still pending for the users' wallets
   *
   * @param int $uid
   *
   * @return int[]
   *   Transaction IDs
   */
  static function pendingTransactionsOfUser(int $uid) : array {
    $transactions = [];
    if ($wallets = WalletStorage::allWalletIdsOf($uid)) {
      $transactions = \Drupal::entityQuery('mc_transaction')->accessCheck(FALSE)
        ->involving($wallets)
        ->condition('state', CCWorkflowInterface::STATE_PENDING)
        ->execute();
    }
    return $transactions;
  }

  /**
   * Assembles transaction and returns it with any added entries. To ensure the
   * transaction is built only once per page load, it is stored as a static.
   *
   * @staticvar TransactionInterface $assembled
   * @param TransactionInterface $transaction
   * @return TransactionInterface
   *
   * @note this method cannot belong in the transaction object.
   */
  static function getAssembledTransaction(TransactionInterface $transaction = NULL) : TransactionInterface {
    static $assembled = [];
    $uuid = $transaction->uuid();
    if (!$transaction->isNew()) {
       throw new \Exception('Called Functions::getAssembledTransaction() with a non-new transaction.');
    }
    if (isset($assembled[$uuid])) {
      $transaction = $assembled[$uuid];
    }
    else {
      // Set the transaction to its initial state and add signatories.
      // Allow other modules to add entries, but not to modify the transaction.
      $entries = \Drupal::moduleHandler()->invokeAll('transaction_assemble', [clone($transaction)]);
      foreach ($entries as $params) {
        $params['primary'] = FALSE;
        $transaction->addEntry($params);
      }
      $assembled[$uuid] = $transaction;
    }
    return $transaction;
  }

  /**
   * Convert transaction ids into sentences with links.
   * @param array $xids
   * @return array
   *   A renderable array.
   */
  public static function sentenceMarkupWithLinks(array $xids) : array {
    $wids = $sentences = [];
    $renderer = \Drupal::service('renderer');
    foreach (Transaction::loadMultiple($xids) as $tx) {
      $wids[] = $tx->payer_wid;
      $wids[] = $tx->payee_wid;
      $links = $tx->workflow->linksAsRenderable();
      $sentences[]  = $tx->sentence() . $renderer->render($links);
    }
    $renderable = [
      '#markup' => Markup::create(implode('<br />', $sentences)),
      '#attached' => ['library' => ['mcapi/transaction']],
      '#cache' => [
        'contexts' => ['user'],
        'tags' => array_map(function ($wid) {return 'mc_wallet:'.$wid;}, $wids)
      ],
    ];
    return $renderable;
  }

}
