<?php

namespace Drupal\mcapi\EventSubscriber;

use Drupal\mcapi\Event\McapiEvents;
use Drupal\mcapi\Event\TransactionSaveEvent;
use Drupal\mcapi\Entity\Transaction;
use Drupal\user\Entity\User;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\State\State;


/**
 * Subscriber showing the two Mcapi events.
 */
class TransactionSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var int
   */
  private int $currentUserId;

  /**
   * @var MailManager
   */
  private MailManager $mailManager;

  /**
   * mcapi.settings
   * @var Config
   */
  protected $config;

  /**
   * The state service
   * @var State
   */
  protected $state;

  /**
   * Constructor
   *
   * @param ConfigFactoryInterface $config_factory
   * @param AccountInterface $account
   * @param MailManager $mail_manager
   */
  function __construct(ConfigFactoryInterface $config_factory, AccountInterface $account, MailManager $mail_manager, State $state) {
    $this->config = $config_factory->get('mcapi.settings');
    $this->currentUserId = $account->id();
    $this->mailManager = $mail_manager;
    $this->state = $state;
  }

  /**
   * {@inheritDoc}
   * @note there is another transaction event, McapiEvents::ASSEMBLE
   */
  public static function getSubscribedEvents() : array {
    return [
      McapiEvents::ACTION => ['notifyTransactees'],
    ];
  }

  /**
   * Notify transactees when a transaction changes state.
   *
   * @param TransactionSaveEvent $event
   *   $event->getSubject() gives the transaction.
   *   $event->getArguments() yields a two char 'transition'.
   * @param string $eventName
   *   The machine name of the event.
   * @param ContainerAwareEventDispatcher $container
   *   The container.
   */
  public function notifyTransactees(TransactionSaveEvent $event, $eventName, ContainerAwareEventDispatcher $container) {
    if ($this->state->get('mcapi_mail_stop')) {
      return;
    }
    $transaction = $event->getSubject();
    $payee_user = $transaction->payee->getOwner();
    $payer_user = $transaction->payer->getOwner();
    // Email will only be sent if the mail tempate for this transition is filled in.
    $this->notifyOtherParty(
      $payee_user->id() == $this->currentUserId ? $payer_user :  $payee_user,
      $transaction,
      $event->getArgument('transition')
    );
  }

  /**
   *
   * @param User $user
   * @param Transaction $transaction
   * @param $transition
   */
  private function notifyOtherParty(User $user, Transaction $transaction, string $transition) {
    $transition_info = $this->config->get("transitions.$transition.mail_template");
    // If wallet owner is anonymous.
    if ($user->isAuthenticated() and $transition_info['subject'] and $transition_info['body']) {
      $this->mailManager->mail(
        'mcapi',
        'transition_secondparty',
        $user->getEmail(),
        $user->getPreferredLangcode(),
        [
          'user' => $user,
          'xaction' => $transaction,
          'transition' => $transition
        ]
      );
    }
  }

}
