<?php

namespace Drupal\mcapi\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;

/**
 * Because the entity.mc_transaction.collection is also the field ui base route, and
 * because views provides a superior listing to the entity's official
 * list_builder, makes the views route override entity.mc_transaction.collection
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    // Do this earlier than it otherwise would.
    return [
      RoutingEvents::ALTER => [['onAlterRoutes', -100]]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // All the wallet tabs should have the same title.
    // Also works for non-views pages
    foreach ($collection->all() as $rid => $route) {
      if (substr($route->getPath(), 0, 19) == '/wallet/{mc_wallet}') {
        $route->setDefault('_title_callback', '\Drupal\mcapi\Controller\WalletController::entityWalletsTitle');
      }
    }
    // If the field_ui module is enabled, replace the transaction forms editor
    // with this modules own page, which lists the transaction forms.
    if ($collection->get('entity.entity_form_display.mc_transaction.mc_transaction.default')) {
      $collection->add( //overwrite
        'entity.entity_form_display.mc_transaction.mc_transaction.default',
        $collection->get('entity.mc_workflow.collection')
      );
    }
    // Retitle this route from yukky default "Transaction workflow entities"
    $collection->get('entity.mc_workflow.collection')->setDefault('_title', 'Transaction fsorms');

    if ($user_wallets_view = $collection->get('view.mc_wallets.per_user')) {
      $user_wallets_view->setRequirements(['_view_users_wallets' => 'TRUE']);
    }
  }

}
