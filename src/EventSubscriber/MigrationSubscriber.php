<?php

namespace Drupal\mcapi\EventSubscriber;

use Drupal\mcapi\Entity\Wallet;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\mcapi\Entity\Workflow;
use Drupal\migrate\Plugin\Migration;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\mcapi\EventSubscriber\MigrationSubscriber;
use Drupal\Core\Config\FileStorage;
use \Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alterations to migrations
 */
class MigrationSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  private $walletIds = [];

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_import' => [['migratePreImport']],
      'migrate.post_import' => [['migratePostImport']],
      'migrate.pre_row_save' => [['migratePreRowSave']]
    ];
  }

  /**
   * Subscribed event callback.
   *
   * @param MigratePreRowSaveEvent $event
   *
   * Change the name of the transaction description field, which was a variable
   * in d7, but an entity basefield in d8. Change the names of the transaction
   * entitytype and bundle.
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();

    // Replace any d7 users that were deleted with dummy wallets.
    if ($migration->id() == 'd7_mc_transaction') {
      $this->ensureWallet($row->getSourceProperty('payee'));
      $this->ensureWallet($row->getSourceProperty('payer'));
    }

    // This is a bit blunt but efficient.
    if ($row->getSourceProperty('entity_type') == 'transaction') {
      $row->setDestinationProperty('entity_type', 'mc_transaction');
      $row->setDestinationProperty('bundle', 'mc_transaction');
      $row->setDestinationProperty('targetEntityType', 'mc_transaction');
    }

    // Don't migrate worth and transaction field definitions
    $field_migrations = ['d7_field','d7_field_instance', 'd7_field_formatter_settings', 'd7_field_instance_widget_settings', 'uc7_field'];
    if (in_array($migration->id(), $field_migrations)) {
      if ($row->getSourceProperty('entity_type') == 'transaction') {
        $existing_fields = ['worth', MigrationSubscriber::d7DescriptionFieldname($migration)];
        if (in_array($row->getSourceProperty('field_name'), $existing_fields)) {
          throw new MigrateSkipRowException('Already installed field '.$row->getSourceProperty('field_name'));
        }
      }
    }
    // Don't bother migrating transaction view modes.
    if ($migration->getSourcePlugin()->getPluginId() == 'd7_view_mode' and $row->getSourceProperty('entity_type') == 'transaction') {
      throw new MigrateSkipRowException('Transaction view modes already installed');
    }

    // Rename the transaction description field
    if ($migration->id() == 'd7_mc_transaction') {
      $old_desc_field_name = $this->d7DescriptionFieldname($migration);
      // Rename the transaction description field
      $row->setDestinationProperty(
        'description',
        $row->getSourceProperty($old_desc_field_name)
      );
      $row->removeDestinationProperty($old_desc_field_name);
    }

    if ($migration->id() == 'd7_filter_format') {
      if ($row->getSourceProperty('format') == 'mcapiform_string_format') {
        throw new MigrateSkipRowException('mcapiform_string_format no longer needed');
      }
    }

    if ($migration->id() == 'd7_mcapi_currency') {
      $display = $row->getSourceProperty('display');
      if (!empty($display->zero)) {
        \Drupal::configFactory()->getEditable('mcapi.settings')
          ->set('currency.zero_snippet', $display->zero)
          ->save();
      }
    }
    if ($migration->id() == 'd7_mc_wallet') {
      if (!User::load($row->getSourceProperty('uid'))) {
        throw new MigrateSkipRowException();  // Ignoring wallet for non-existent user.
      }
    }

    if ($migration->id() == 'd7_field') {
      if ($row->getSourceProperty('entity_type') == 'transaction' and $row->getSourceProperty('field_name') == 'transaction_description') {
        throw new MigrateSkipRowException();
        // Why was all of this needed? D7 description field is handled in Drupal\mcapi\Plugin\migrate\source\d7\Transaction
        $instances = $row->getSourceProperty('instances');
        $data = unserialize($instances[0]['data']);
        $data['settings']['text_processing'] = 0;
        $instances[0]['data'] = serialize($data);
        $row->setSourceProperty('instances', $instances);
      }
    }

    if ($migration->id() == 'd7_block') {
      if ($row->getDestinationProperty('plugin') == 'views_block:mcapi_pending_signatures-my_sig_needed') {
        $row->setDestinationProperty('plugin', 'mcapi_pending');
        $row->setDestinationProperty('settings', ['show_other', 0]);
      }
      if ($row->getDestinationProperty('plugin') == 'views_block:mcapi_pending_signatures-my_pending') {
        $row->setDestinationProperty('plugin', 'mcapi_pending');
        $row->setDestinationProperty('settings', ['show_other', 1]);
      }
    }
  }


  private function ensureWallet($wid) {
    if (!in_array($wid, $this->walletIds)) {
      $this->walletIds[] = $wid;
      // There's a small chance the wallet might already exist.
      Wallet::Create([
        'wid' => $wid,
        'name' => $this->t("D7 Deleted user @wid", ['@wid' => $wid]),
        'owner' => User::load(1),
        'wid' => $wid
      ])->save();
      // if this fails it doesn't interrupt.
    }
  }

  /**
   * Subscribed event callback.
   */
  function migratePreImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_mc_transaction') {
      $this->walletIds = \Drupal::entityQuery('mc_wallet')
        ->accessCheck(FALSE)
        ->execute();
      \Drupal::State()->set('mcapi_mail_stop', 1);
      // Now import the new workflows.
      $mcapi_module_path = \Drupal::service('extension.path.resolver')->getPath('module', 'mcapi');
      $mcapi_config_dir = new FileStorage($mcapi_module_path . '/config/install');
      $mc_workflow_storage = \Drupal::entityTypeManager()->getStorage('mc_workflow');
      // NB There was a case when admin could not be saved because it already existed...
      foreach(['admin', 'default_bill', 'default_credit'] as $mc_workflow) {
        if (!Workflow::load($mc_workflow)) {
          if ($data_from_file = $mcapi_config_dir->read('mcapi.mc_workflow.'.$mc_workflow)) {
            $mc_workflow_storage->create($data_from_file)->save();
          }
        }
      }

    }
    // Delete wallets to ensure no clash with incoming wallets
    elseif ($event->getMigration()->id() == 'd7_user') {
      // Disable auto adding wallets
      $this->setWalletUserAutowallet(FALSE);
    }
  }

  /**
   * Subscribed event callback.
   * Set the autoadd back to what it was before the user migration
   */
  function migratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_user') {
      // Restore auto adding wallets
      $this->setWalletUserAutowallet(TRUE);
    }
    // Finish the permission migration, because it didn't map 1:1
    elseif ($event->getMigration()->id() == 'd7_user_roles') {
      foreach (Role::loadMultiple() as $role) {
        if ($role->hasPermission('create credit transaction')) {
          $role->grantPermission('create bill transaction');
          $role->grantPermission('view all wallets');
          $role->grantPermission('view all transactions');
        }
        if ($role->hasPermission('manage mcapi')) {
          $role->grantPermission('create 3rdparty transaction');
        }
        $role->save();
      }
    }
    if ($event->getMigration()->id() == 'd7_mcapi_transaction') {
      \Drupal::State()->delete('mcapi_mail_stop', 1);
    }
  }

  /**
   * Change the wallet settings before and after migration.
   *
   * @param bool $switch
   *   first FALSE, then TRUE to Restore the original setting
   */
  private function setWalletUserAutowallet(bool $switch) {
    $config = \Drupal::ConfigFactory()->getEditable('mcapi.settings');
    if (!$switch) {
      \Drupal::state()->set('migrate_autowallet', $config->get('autowallet'));
      $config->set('autowallet', FALSE);
    }
    elseif ($switch) {
      $old_setting = \Drupal::state()->get('migrate_autowallet');
      $config->set('autowallet', $old_setting);
      \Drupal::state()->delete('migrate_autowallet');
    }
    $config->save();
  }

  /**
   * Utility.
   *
   * @param Migration $migration
   *
   * @return string
   *   The fieldname of the d7 transaction description
   */
  static function d7DescriptionFieldname(Migration $migration) {
    $result =  $migration->getSourcePlugin()
     ->getDatabase()
     ->select('variable', 'v')
     ->fields('v', ['value'])
     ->condition('name', 'transaction_description_field')
     ->execute()
     ->fetchField();
    return unserialize($result);
  }


  static function mostUsedCurrencyQuery() : string {
    return "SELECT worth_currcode, COUNT(worth_currcode) AS count "
      . "FROM field_data_worth "
      . "GROUP BY worth_currcode "
      . "ORDER BY count DESC LIMIT 1";
  }
}
