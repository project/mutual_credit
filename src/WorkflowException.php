<?php

namespace Drupal\mcapi;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mcapi\Entity\Transaction;

/**
 * Exception for a very specific and rare circumstance, when there is an attempt to save a transaction prohibited by workflow
 *
 */
class WorkflowException extends \Exception {

  private $template = 'User @uid is not allowed to transition transaction with workflow @code from state @old_state to @dest_state: @entry';

 /**
   * @param string $wf_code
   *   The exception message.
   * @param string $old_state
   *   The exception code.
   * @param string $dest_state
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    protected Transaction $transaction,
    protected string $dest_state
  ) {
    $args = [
      '@uid' => \Drupal::currentuser()->id(),
      '@code' => $transaction->workflow->value,
      '@old_state' => $transaction->state->value,
      '@dest_state' => $dest_state,
      '@entry' => print_r($transaction->entries->first()->getValue(), 1)
    ];
    // I think exceptions are logged automatically
    // \Drupal::service('logger.channel.mcapi')->error($this->template, $args);
    parent::__construct(new TranslatableMarkup($this->template, $args), 400);
  }

}