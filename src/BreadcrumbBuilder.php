<?php

namespace Drupal\mcapi;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a class to build virtual breadcrumbs, i.e. not based on actual paths.
 *
 * @see \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
 *
 * Inject config
 */
class BreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * Routes and virtual paths. Admin paths not covered for now.
   */
  private $routes = [
    'entity.mc_transaction.add_form' =>         ['transactions', 'add'],
    'entity.mc_transaction.add_remote' =>       ['transactions', 'add'],
    'mcapi_mass.pay.12many' =>                  ['transactions', 'add', '12many'],
    'mcapi_mass.pay.many21' =>                  ['transactions', 'add', 'many21'],
    'mcapi_forms.mc_form.default' =>            ['transactions', 'add', 'mcform'],
    'entity.mc_transaction.canonical' =>        ['transactions', 'id'],
    'entity.mc_transaction.transition' =>       ['transactions', 'id', 'transition'],
    'view.mc_wallets.wallets_page' =>           ['wallets'],
    'entity.mc_wallet.canonical' =>             ['wallets', 'id'],
    'entity.mc_wallet.edit_form' =>             ['wallets', 'id', 'manage'],
    'entity.mc_wallet.delete_form' =>           ['wallets', 'id', 'delete'],
    'view.mc_wallet_transactions.wallet_page' =>['wallets', 'id', 'transactions'],
    'view.mc_income_expenditure.wallet_page' => ['wallets', 'id', 'inex'],
    'mcapi.user.mc_wallets' =>                  ['users', 'id', 'wallets'],
  ];

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return in_array($route_match->getRouteName(), array_keys($this->routes));
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $links = [Link::createFromRoute($this->t('Home'), '<front>')];
    $parts = $this->routes[$route_match->getRouteName()];
    switch($parts[0]) {
      case 'transactions':
        $links[] = new Link($this->t('Transactions'), Url::fromRoute('view.mc_transactions.simple'));
        break;
      case 'wallets':
        $links[] = new Link($this->t('Wallets'), Url::fromRoute('view.mc_wallets.all'));
        break;
      case 'users':
        $user = $route_match->getParameter('user');
        $links[] = new Link($user->label(), Url::fromRoute('entity.user.canonical', ['user' => $user]));
        break;
    }

    if (isset($parts[1])) {
      switch ($parts[1]) {
        case 'add':
          $links[] = new Link($this->t('Add'), Url::fromRoute('<current>', []));
          break;
        case 'id':
          if ($parts[0] == 'transactions') {
            $transaction = $route_match->getParameter('mc_transaction');
            if ($transaction->id()) {
              $links[] = new Link('#'.$transaction->id(), Url::fromRoute('entity.mc_transaction.canonical', ['mc_transaction' => $transaction->id()]));
            }
            else {
              $links[] = new Link($this->t('New'), Url::fromRoute('<current>'));
            }
          }
          elseif ($parts[0] == 'wallets') {
            $wallet = $route_match->getParameter('mc_wallet');
            $links[] = new Link($wallet->label(), Url::fromRoute('entity.mc_wallet.canonical', ['mc_wallet' => $wallet->id()]));
          }
          elseif ($parts[0] == 'users') {
            $user = $route_match->getParameter('user');
            $links[] = new Link($user->label(), Url::fromRoute('entity.user.canonical', ['mc_wallet' => $user->id()]));
          }
      }
      if (isset($parts[2])) {
        // these are all the last, so the link is always to the current page.
        switch($parts[2]) {
          case '12many':
            $links[] = new Link($this->t('One to many'), Url::fromRoute('<current>'));
            break;
          case 'many21':
            $links[] = new Link($this->t('Many to one'), Url::fromRoute('<current>'));
            break;
          case 'mcform':
            $mc_form = $route_match->getParameter('mc_form');
            $links[] = new Link($mc_form->label(), Url::fromRoute('<current>'));
            break;
          case 'transition':
            $current_state = $route_match->getParameter('mc_transaction')->state->value;
            $dest_state = $route_match->getParameter('dest_state');
            $label = \Drupal::config('mcapi.settings')->get('transitions.'.$current_state.$dest_state.'.label');
            $links[] = new Link($label, Url::fromRoute('<current>'));
            break;
          case 'manage':
            $links[] = new Link($this->t('Edit'), Url::fromRoute('<current>'));
           break;
          case 'delete':
            $links[] = new Link($this->t('Delete'), Url::fromRoute('<current>'));
            break;
          case 'transactions':
            $links[] = new Link($this->t('Transactions'), Url::fromRoute('<current>'));
           break;
          case 'inex':
            $links[] = new Link($this->t('Statement'), Url::fromRoute('<current>'));
            break;
          case 'wallets':
            $links[] = new Link($this->t('Wallets'), Url::fromRoute('<current>'));
            break;
        }

      }
    }
    return $breadcrumb->setLinks($links)->addCacheContexts(['route']);
  }

}
