<?php

namespace Drupal\mcapi;

use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\ParamConverter\EntityConverter;
use Symfony\Component\Routing\Route;


/**
 * Provides upcasting for the transaction/{xid} path
 *
 * If {serial} is 0, the transaction will be pulled from the private tempstore
 * for the current user.
 */
class McTransactionParamConverter extends EntityConverter {

  /**
   * The private tempstore service.
   *
   * @var Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * Constructs a new EntityConverter.
   *
   * @param PrivateTempStoreFactory $tempstore
   */
  public function __construct($entity_type_manager, $entity_repository, PrivateTempStoreFactory $tempstore) {
    parent::__construct($entity_type_manager, $entity_repository);
    $this->tempStore = $tempstore;
  }
  /**
   * {@inheritDoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if ($value > 0) {
      return parent::convert($value, $definition, $name, $defaults);
    }
    // A $value of zero means that this is the are-you-sure page before the
    // transaction has been saved. The transaction is retrieved therefore not in
    // the normal way from the database but from the tempstore.
    return $this->tempStore
      ->get('TransactionForm')
      ->get('mc_transaction');
  }

  /**
   * {@inheritDoc}
   */
  public function applies($definition, $name, Route $route) {
    return $name == 'mc_transaction' && !empty($definition['xid']);
  }

}
