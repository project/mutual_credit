<?php

namespace Drupal\mcapi\Element;

use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\Core\Render\Element\Radios;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Form element to select the wallet to trade from. Hidden if only wallet is available.
 */
#[FormElement(id: 'my_wallet')]
class MyWallet extends Radios {

  /**
   * {@inheritDoc}
   */
  public static function processRadios(&$element, FormStateInterface $form_state, &$complete_form) {
    $uid = \Drupal::currentuser()->id();
    $wids = WalletStorage::allWalletIdsOf($uid);
    if (count($wids) > 1) {
      foreach (Wallet::loadMultiple($wids) as $wid => $wallet) {
        $element['#options'][$wid] = $wallet->label();
      }
      $element['#title'] = 'My wallet';
      $element['#weight'] = -1;
      $element['#required'] = TRUE;
      if ($element['#default_value'] instanceOf Wallet) {
        $element['#default_value'] = $element['#default_value']->id();
      }
      $element = parent::processRadios($element, $form_state, $complete_form);
    }
    else {
      $element = [
        '#name' => $element['#name'],
        '#type' => 'hidden', // 'value' doesn't seem to work.
        '#value' => reset($wids),
        '#parents' => [$element['#name']] // Needed for validation
      ];
    }
    return $element;
  }

}
