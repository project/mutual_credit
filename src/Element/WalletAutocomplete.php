<?php

namespace Drupal\mcapi\Element;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Provides a widget to select wallet references by name, using autocomplete.
 *
 * Its really just EntityAutocomplete with a lot of defaults.
 */
#[FormElement(id: 'wallet_entity_auto')]
class WalletAutocomplete extends EntityAutocomplete {

  static $multiple;
  const REGEX = '[a-zA-Z0-9 ]* \([0-9]+\)';

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#selection_handler'] = 'default:mc_wallet';
    $info['#target_type'] = 'mc_wallet';
    $info['#maxlength'] = 64;
    $info['#pattern'] = self::REGEX;
    // Appearance should be managed with css.
    $info['#size'] = 60;
    return $info;
  }

  public static function validateEntityAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#value']) || (is_string($element['#value']) && strlen($element['#value']))) {
      // The element['#name'] on a default form is something like "entries[payer_wid]", which NOT the format needed to access the form_state value.
      // However on a Designed form the element name is not nested.
      if (is_null($form_state->getValue($element['#parents']))) {
        $form_state->setError($element, t('Unable to identify wallet %id', ['%id' => $element['#value']]));
        // Skip entity level validation.
        $complete_form['#validate'] = [];
      }
      parent::validateEntityAutocomplete($element, $form_state, $complete_form);
    }
  }

}
