<?php

namespace Drupal\mcapi\Element;

use Drupal\Core\Render\Element\RenderElementBase;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Provides an element showing a wallet's balance over time.
 */
#[FormElement(id: 'mcapi_balance_history')]
class BalanceHistory extends RenderElementBase {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#width' => 400,
      '#height' => 133,
      '#format_curr' => TRUE,
      '#pre_render' => [[$class, 'preRender']]
    ];
  }

  /**
   * preRender callback
   * Load the points.
   */
  public static function preRender(array $element) {
    if ($element['#points'] = $element['#wallet']->history()) {
      $element['#theme'] = 'mc_timeline';
    }
    else {
      $element = [];
    }
    return $element;
  }



}
