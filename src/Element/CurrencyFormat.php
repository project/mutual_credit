<?php

namespace Drupal\mcapi\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Provides an elements for entering the currency format.
 */
#[FormElement(id: 'currency_format')]
class CurrencyFormat extends Textfield {
  use \Drupal\Core\Render\Element\CompositeFormElementTrait;

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    unset($info['#maxlength']);
    $class = get_class($this);
    $info['#element_validate'][] = [$class, 'validate'];
    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE) {
      $parts = self::compileFormat($input);
      if (count($parts) == 1) {
        throw new \Exception('Format must have text and numbers.');
      }
      return $parts;
    }
  }

  public static function compileFormat(string $input) : array {
    // Process in chunks because having a lot of trouble with long regexes
    $regexes = [
      'f1' => '[^0-9]*',
      'main' => '0(\.[0-9]+)?',
      'f2' => ' ?<.*>[^0-9]*|[^0-9]+', // Optional text, might include html tags like img or strong
      'subdivision' => '[0-9]+(\/[0-9]+)?', // Optional
      'f3' => '.*' // Optional
    ];
    $parts = array_fill_keys(array_keys($regexes), NULL);
    $substr = $input;
    foreach ($regexes as $key => $pattern) {
      preg_match("/$pattern/", $substr, $matches);
      // if there is a substring and no match
      if (!isset($matches[0]) and $key <> 'f1') {
        throw new \Exception("Badly formatted currency $key");
      }
      $parts[$key] = $matches[0];
      if ($matches[0] == $substr) {
        break;
      }
      $substr = substr($substr, strlen($matches[0]));
    }
    return $parts;
  }

  /**
   * Validate callback.
   */
  public static function validate(&$element, FormStateInterface $form_state) {
    if (!is_array($element['#value'])) {
      $form_state->setError($element, t('Invalid currency format.'));
    }
  }

}
