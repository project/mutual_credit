<?php

namespace Drupal\mcapi\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Provides an element rendering an entry in oneline
 */
#[FormElement(id: 'entry_oneline')]
class EntryOneLine extends RenderElement {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return [
      '#mc_entry' => NULL,
      '#template' => '',
      '#pre_render' => [
        [static::class, 'preRender'],
      ],
    ];
  }

  public static function preRender($element) {
    $element['#markup'] = \Drupal::token()->replace(
      str_replace('[', '[entry:', $element['#template']),
      ['entry' => $element['#mc_entry']]
    );
    return $element;
  }

}
