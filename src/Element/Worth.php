<?php

namespace Drupal\mcapi\Element;

use Drupal\mcapi\Functions;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Attribute\FormElement as FormElementAtrribute;

/**
 * Provides form element to display amounts.
 */
#[FormElementAtrribute(id: 'worth')]
class Worth extends FormElement {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#value_callback' => [
        [$class, 'valueCallback'],
      ],
      '#element_validate' => [
        [$class, 'validate'],
      ],
      '#process' => [
        [$class, 'process'],
      ],
      '#attached' => ['library' => ['mcapi/worth.element']],
      '#theme_wrappers' => ['form_element'],
      '#tree' => TRUE,
      '#minus' => FALSE, //Render the minus sign in the field prefix
      '#placeholder' => 0
    ];
  }

  /**
   * Form element processor callback for 'worth'.
   */
  public static function process($element) {
    $formatter = \Drupal::Service('mcapi.currency_formatter');
    $config = \Drupal::config('mcapi.settings')->get('currency.format');
    // Render the widget for a known currency
    $element += $formatter->generateFormElements($element['#default_value'], intval($element['#placeholder']), $element['#minus']);
    $element['#attributes']['class'][] = 'mc-worth'; // doesn't show ATM
    // This is a hack that prevents FormValidator::dovalidateForm from,
    // registering the field as empty when the two subfields have been
    // calculated. Validation still takes place normally.
    unset($element['#needs_validation']);
    return $element;
  }

  /**
   * {@inheritDoc}
   * This is called before te two sub elements
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if (is_array($input)) {
      return static::unformat($input);
    }
  }

  /**
   * Convert the field contents back to a raw number. Preserve NULL
   * @param array $parts
   *   array with keys 'main' and 'subdivision'
   * @return int
   */
  private static function unformat(array $parts) : ?int {
    if ($parts['main'] === '' and empty($parts['subdivision'])) {
      return NULL;
    }
    return \Drupal::Service('mcapi.currency_formatter')->unformat($parts['main'], $parts['subdivision']??'');
  }

  /**
   * Validate callback.
   *
   * @note the value is set here, coz valuecallback doesn't work when there are subfields.
   */
  public static function validate(&$element, FormStateInterface $form_state) {
    $value_parts = [
      'main' => $element['main']['#value'],
      'subdivision' => $element['subdivision']['#value']??0
    ];
    $val = $element['#value'] = static::unformat($value_parts);

    $form_state->setValueForElement($element, $val);
    if ($val) {
      unset($element['#required_but_empty']);
    }
    // Check for allowed zero values
    if ($val < 0) { // Should never happen!
      $form_state->setError($element, t('Negative amounts not allowed: %val'));
    }
    if (isset($element['#min']) and $val < $element['#min']) {
      $form_state->setError($element['main'], t('Min value is %val', ['%val' => Functions::formatBalance($element['#min'], FALSE)]));
    }
    if (isset($element['#max']) and $val > $element['#max']) {
      $form_state->setError($element['main'], t('Max value is: %val', ['%val' => Functions::formatBalance($element['#max'], FALSE)]));
    }
  }

}
