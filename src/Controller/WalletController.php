<?php

namespace Drupal\mcapi\Controller;

use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Returns all the wallets held by the given entity, in summary view mode.
 */
class WalletController extends ControllerBase {

  /**
   * Title callback for all pages under path wallet/{mc_wallet}/
   *
   * @return TranslatableMarkup
   *   The entity label of the wallet owner.
   */
  public function entityWalletsTitle(WalletInterface $mc_wallet) : TranslatableMarkup {
    return $this->t('Wallet: @name', ['@name' => $mc_wallet->label()]);
  }

  /**
   * Router callback.
   *
   * Show all an entities wallets in summary mode.
   * this is rather tricky because we don't know what arguments would be passed
   * from the url, so we have to load them from scratch.
   *
   * @return array
   *   A renderable array.
   *
   * @todo wallets need names and some css to be visually separated.
   * @note this is comparable to the user extrafield wallets_summaries.
   */
  public function entityWallets(UserInterface $user) {
    if ($wids = WalletStorage::allWalletIdsOf($user->id())) {
      $renderable = $this->entityTypeManager()
        ->getViewBuilder('mc_wallet')
        ->viewMultiple(Wallet::loadMultiple($wids));
    }
    else {
      $renderable = ['#markup' => t('@name has no wallets', ['@name' => $user->getDisplayName()])];
    }
    return $renderable;
  }

}
