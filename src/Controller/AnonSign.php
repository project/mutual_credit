<?php

namespace Drupal\mcapi\Controller;

use Drupal\mcapi\Entity\Transaction;
use Drupal\Core\Controller\ControllerBase;
use CreditCommons\Workflow;

/**
 * Add a signature from a user who may not be logged in.
 *
 * Depends on a token generated in the 'signhere' mail.
 */
class AnonSign extends ControllerBase {

  /**
   * Sign the transaction and redirect to the canonical page.
   *
   * @param Transaction $mc_transaction
   *   Transaction ID
   * @param int $uid
   *   User ID.
   * @param string $hash
   *   Hash which hopefully corresponds to #xid and $uid.
   */
  public function sign(Transaction $mc_transaction, int $uid, string $hash) {
    // user was logged in by the route requirements
    try {
      $mc_transaction->saveVersion(Workflow::STATE_COMPLETED);
    }
    catch (\Exception $e) {
      $this->messenger()->addStatus($e->getMessage());
    }

    return $this->redirect(
      'entity.mc_transaction.canonical',
      ['mc_transaction' => $mc_transaction->id()]
    );
  }

}
