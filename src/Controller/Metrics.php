<?php

namespace Drupal\mcapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mcapi\Functions;
use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\Form\FormState;
use CreditCommons\Workflow as CCWorkflowInterface;

/**
 * Builds a list of transaction forms.
 */
class Metrics extends ControllerBase {

  public function title() {
    $currency = $this->config('mcapi.settings')->get('currency');
    return $this->t('@currency metrics', ['@currency' => $currency['name']]);
  }

  /**
   * Build a page showing all the trading stats
   */
  public function buildPage() {
    $transactionStorage = $this->entityTypeManager()->getStorage('mc_transaction');
    $currency = $this->config('mcapi.settings')->get('currency');
    $form_state = new FormState;

    $first_transaction_time = \Drupal::database()->select('mc_transaction', 't')
      ->fields('t', ['created'])
      ->condition('state', CCWorkflowInterface::STATE_COMPLETED)
      ->orderBy('created', 'ASC')
      ->range(0, 1)
      ->execute()->fetchField();

    $form_state->set('firstTransactionTime', $first_transaction_time);
    if ($filterform = $this->formBuilder()->buildForm('Drupal\mcapi\Form\TransactionStatsFilterForm', $form_state)) {
      $build['filter'] = $filterform;
    }
    $build['#cache_contexts'] = ['accounting'];

    $conditions = [];
    if (isset($_SESSION['transaction_stats_filter'])) {
      $conditions = array_filter($_SESSION['transaction_stats_filter']);
    }
    elseif ($first_transaction_time) {
      // $since needs to be set to the second previous to the first transaction;
      $conditions['since'] = $first_transaction_time - 1;
    }
    else {
      return ['#markup' => $this->t('No transactions have been registered.')];
    }

    $balances_asc = $trades = $volumes = $incomes = $spending = [];

    $rows = $transactionStorage->ledgerStateByWallet($conditions);
    foreach ($rows as $row) {
      $balances_asc[$row->wid] = (int)$row->balance;
      $trades[$row->wid] = $row->trades;
      $volumes[$row->wid] = (int)$row->income + (int)$row->expenditure;
      $incomes[$row->wid] = (int)$row->income;
      $spending[$row->wid] = (int)$row->expenditure;
    }

    asort($balances_asc);
    // Get the bottom wallets.
    for ($i = 0; $i < min(10, count($balances_asc)/2); $i++) {
      $wid = key($balances_asc);
      $wallet = Wallet::load($wid);
      if (!$wallet) {
        \Drupal::service('logger.channel.mcapi')->error("No wallet $wid in \Drupal\mcapi\Controller\Metrics::buildPage");
        continue;
      }
      $quant = current($balances_asc);
      $bottoms[] = [
        'raw' => abs($quant),
        'link' => $wallet->toUrl(),
        'quant' => Functions::formatBalance($quant, FALSE),
        'name' => $wallet->label(),
      ];
      next($balances_asc);
    }
    // Get the top wallets.
    $balances_desc = array_reverse($balances_asc, TRUE);
    for ($i = 0; $i < min(10, count($balances_asc)/2); $i++) {
      $wid = key($balances_desc);
      $wallet = Wallet::load($wid);
      if (!$wallet) {
        \Drupal::service('logger.channel.mcapi')->error("No wallet $wid in \Drupal\mcapi\Controller\Metrics::buildPage");
        continue;
      }
      $quant = current($balances_desc);
      $tops[] = [
        'raw' => $quant,
        'link' => $wallet->toUrl(),
        'quant' => Functions::formatBalance($quant, FALSE),
        'name' => $wallet->label(),
      ];
      next($balances_desc);
    }

    if (!empty($balances_desc)) {
      $build['balances_chart'] = [
        '#theme' => 'mc_extreme_balances',
        '#largest' => max(max($balances_asc), abs(min($balances_asc))),
        '#tops' => array_filter($tops, function($t){return $t['raw'] > 0;}),
        '#bottoms' => array_filter($bottoms, function($t){return $t['raw'] > 0;}),
        '#weight' => -1,
        '#class' => ['extreme-balances'],
      ];
    }
    arsort($trades);
    $build['trades_per_user_wallet'] = [
      '#theme' => 'mc_wallets_ordered',
      '#title' => $this->t('Trades per wallet'),
      '#users_only' => TRUE,
      '#format_vals' => FALSE,
      '#items' => array_slice($trades, 0, 5, TRUE),
      '#width' => 400,
      '#height' => 200,
    ];

    arsort($volumes);
    $build['volume_per_user_wallet'] = [
      '#theme' => 'mc_wallets_ordered',
      '#title' => $this->t('Volume per wallet'),
      '#id' => 'volume_per_user_wallet',
      '#users_only' => TRUE,
      '#format_vals' => TRUE,
      '#items' => array_slice($volumes, 0, 5, TRUE),
      '#width' => 400,
      '#height' => 200,
    ];

    arsort($incomes);
    $build['income_per_user_wallet'] = [
      '#theme' => 'mc_wallets_ordered',
      '#title' => $this->t('Income per wallet'),
      '#id' => 'income_per_user_wallet',
      '#users_only' => TRUE,
      '#format_vals' => TRUE,
      '#items' => array_slice($incomes, 0, 5, TRUE),
      '#width' => 400,
      '#height' => 200,
    ];
    arsort($spending);
    $build['spending_per_user_wallet'] = [
      '#theme' => 'mc_wallets_ordered',
      '#title' => $this->t('Expenditure per wallet'),
      '#id' => 'spending_per_user_wallet',
      '#users_only' => TRUE,
      '#format_vals' => TRUE,
      '#items' => array_slice($spending, 0, 5, TRUE),
      '#width' => 400,
      '#height' => 200,
    ];

    //@todo support time periods
    list(, , $period, $period_name) = $this->periodQueryParams($conditions);
    list($dates, $trades, $volumes, $wallets) = $transactionStorage->historyPeriodic($period, $conditions);
    // Note this doesn't list empty periods
    $params = [
      '@span' => strtolower($period_name), // this should be already translated, somehow.
    ];
    $build['trades_history'] = [
      '#theme' => 'mc_timeline',
      '#title' => $this->t('Numbers of trades per @span', $params),
      '#points' => array_combine($dates, $trades),
      '#width' => 800,
      '#height' => 200,
      '#format_curr' => FALSE
    ];
    $build['participation_history'] = [
      '#theme' => 'mc_timeline',
      '#title' => $this->t('Numbers of traders per @span', $params),
      '#points' => array_combine($dates, $wallets),
      '#width' => 800,
      '#height' => 200,
      '#format_curr' => FALSE
    ];
    $build['volume_history'] = [
      '#theme' => 'mc_timeline',
      '#title' => $this->t('Volumes traded per @span', $params),
      '#points' => array_combine($dates, $volumes),
      '#width' => 800,
      '#height' => 200,
      '#format_curr' => TRUE
    ];
    $build['#attached']['library'][] = 'mcapi/currency';
    return $build;
  }

  /**
   * Work out the start, end and frequency to poll the db.
   *
   * Less than a week, invalid.
   * Less than 100 days.
   * Less than 100 weeks, weekly.
   * Less than 100 months, monthly.
   * Otherwise annual.
   *
   * @param array $conditions
   * @return array
   *   $from, $to, the time period, and the timeperiod translated
   */
  public function periodQueryParams(array $conditions) : array {
    $since = isset($conditions['since']) ? $conditions['since'] : 0;
    $until = isset($conditions['until']) ? $conditions['until'] : \Drupal::time()->getRequestTime();
    $span = $until - $since;
    $day = 86400;
    // $year = 31560192;
    if ($span < $day * 7) {
      // This won't produce very good results.
      $from = date('d-m-Y', $since);
      $to = date('d-m-Y', $until + $day);
      $groupby_translated = $this->t('Day');
      $groupby = 'day';
    }
    // 100 days.
    elseif ($span < $day * 100) {
      $from = date('d-m-Y', $since);
      $to = date('d-m-Y', $until + $day);
      $groupby_translated = $this->t('Day');
      $groupby = 'day';
    }
    // 100 weeks.
    elseif ($span < $day * 700) {
      // Get the beginning of the week.
      $from = date('Y', $since) . 'W' . date('W', $since);
      // The beginning of the week after the end.
      $to = date('Y', $since) . 'W' . (date('W', $since) + 1);
      $groupby_translated = $this->t('Week');
      $groupby = 'week';
    }
    // 100 months.
    elseif ($span < $day * 3000) {
      $from = date('Y', $since);
      // The beginning of the week after the end.
      $to = date('Y', $since) + 1;
      $groupby_translated = $this->t('Month');
      $groupby = 'month';
    }
    else {
      $from  = date('Y', $since);
      $to = date('Y', $until) + 1;
      $groupby_translated = $this->t('Year');
      $groupby = 'year';
    }
    return [strtotime($from), strtotime($to), $groupby, $groupby_translated];
  }

}
