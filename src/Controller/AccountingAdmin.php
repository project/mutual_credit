<?php

namespace Drupal\mcapi\Controller;

use Drupal\system\Controller\SystemController;

/**
 * Community accounting menu / admin page showing workflow info at the top.
 */
class AccountingAdmin extends SystemController {

  /**
   * {@inheritDoc}
   */
  public function systemAdminMenuBlockPage() {
    // Show a warning if there aren't enough wallets to trade
    $wallets = $this->entityTypeManager()->getStorage('mc_wallet')->getQuery()
      ->accessCheck(FALSE)
      ->count()->execute();
    if ($wallets < 2) {
      $message = $this->t("There aren't enough wallets for you to create a transaction. Wallets are at the same time as, or on the canonical page of, the entities that will hold them.");
      $this->messenger()->addWarning($message);
    }

    return parent::systemAdminMenuBlockPage();
  }

}
