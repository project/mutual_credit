<?php

namespace Drupal\mcapi;
use Drupal\Core\Render\Markup;

/**
 * Provides upcasting for the transaction/{xid} path
 *
 * If {serial} is 0, the transaction will be pulled from the private tempstore
 * for the current user.
 */
class CurrencyFormatter {

  /**
   * The currency format, retrieved from config. An array with keys
   * f1 - Formatting to put before the value
   * main - a zero, optionally followed optionally by a decimal point and any number of nines.
   * f2 - Formatting string to put between the units and subdivisions.
   * subdivision - the max number of subunits followed optionally by a slash and the number to divide the unit into e.g. 59/4
   * f3 - Formatting to put after the value.
   * @var array
   */

  /**
   * The 'main' part of the formatting string
   * @var string
   */
  private ?string $prefix;
  private ?string $separator;
  private ?string $suffix;
  private bool $fractions;

  /**
   * The 'subdivision' part of the formatting string
   * @var string
   * @example 99, 59, 59/4,
   */
  private ?int $subdivision = 0;
  /**
   * How many parts to divide the unit into, for UI purposes.
   * @var int|null
   */
  private ?int $options = NULL;

  /**
   * Constructs a new CurrencyFormatter.
   */
  public function __construct($config_factory) {
    $currency = $config_factory->get('mcapi.settings')->get('currency');
    $this->fractions = $currency['fractions'] ?? FALSE;
    $format = $currency['format'];
    $this->prefix = $format['f1'];
    $this->separator = $format['f2'] ?? '';
    $this->suffix = $format['f3'] ?? '';
    $subdivision = $format['subdivision'] ?? '';

    if ($slashpos = strpos($subdivision, '/')) {
      $this->options = intval(substr($subdivision, $slashpos + 1));
      $this->subdivision = intval(substr($subdivision, 0, $slashpos) + 1); // E.g. 100, 60
    }
    elseif ($dotpos = strpos($format['main'], '.')) {
      $this->subdivision = intval(substr($format['main'], $dotpos+1 ));
      $this->separator = '.';
    }
    elseif (is_numeric($subdivision)) {
      // Cannot assign string to property Drupal\mcapi\CurrencyFormatter::$subdivision of type ?int
      $this->subdivision = (int)$subdivision++;
    }
  }

  /**
   * Format a currency value.
   * @param int $raw_num
   * @return string (or even Markup?)
   */
  public function format(int|float $raw_num) : Markup {
    $subdivision = '';
    extract(self::formatParts(abs($raw_num))); // $main, $subdivision
    if (!empty($this->subdivision) and $subdivision) {
      if ($this->fractions) { // Specific subdivisions allowed
        $frac = self::decToFraction($subdivision / $this->subdivision);
        // Frac is a unicode string denoting a fraction
        if ($frac == '0') { // Rounded down
          $subdivision = '';
          $this->separator = '';
        }
        if ($frac == '1') {
          $main++;
          $subdivision = '';
        }
        else {
          $subdivision = $frac;
          $this->separator = '';
        }
      }
    }
    elseif ($this->subdivision) {// just zeros after decimal point
      $subdivision = str_pad($subdivision, strlen($this->subdivision), '0', STR_PAD_LEFT);
    }
    $minus = ($raw_num < 0) ? '-' : '';
    $formatted = $minus . $this->prefix . $main . $this->separator . $subdivision . $this->suffix;
    return Markup::Create($formatted);
  }

  /**
   * Format the value as a machine-readable number
   *
   * @param type $raw_num
   * @return float
   */
  public function formatAsFloat(int|float $raw_num) : float {
    extract(self::formatParts(abs($raw_num)));
    $float = $main;
    if (!empty($this->subdivision) and isset($subdivision)) {
      $float += $subdivision/$this->subdivision;
    }
    if ($raw_num < 0) {
      // always render it as a positive number.
      $float *= -1;
    }
    return round($float, 2);
  }

  /**
   * Convert the raw value into numbers for a main widget and subdivision widget.
   *
   * @param NULL|int|string $raw_num
   * @return array
   */
  public function formatParts(NULL|int|float $raw_num) : array {
    if (!is_numeric($raw_num)) {
      $formatted = ['main' => 0, 'subdivision' => NULL];
    }
    else{
      $raw_num = intval(abs($raw_num));
      if ($this->subdivision) {
        $formatted['main'] = intdiv($raw_num, $this->subdivision);
        $formatted['subdivision'] = $raw_num % $this->subdivision;
      }
      else {
        $formatted = ['main' => $raw_num];
      }
    }
    if ($raw_num < 0 ) {
      $formatted['main'] = $formatted['main'] *= -1;
    }
    return $formatted;
  }


  /**
   * Generate the form element from a raw value.
   *
   * @param $default_val
   *   Whatever was passed to the form element #default_value, null, string or number.
   * @return array
   * @todo typehint $minus when all field settings are updated.
   */
  public function generateFormElements($default_val, int $placeholder_val, $minus = FALSE) {
    if (is_numeric($default_val)) {
      extract($this->formatParts($default_val)); // $main and $subdivision
    }
    else {
      $main = '';
      $subdivision = 0;
    }
    $elements['main'] =  [
      '#type' => 'number',
      '#default_value' => strval($main),
      '#size' => 6, // makes no difference because of other css.
      '#weight' => 1,
      '#pattern' => '[0-9]+',
      '#maxlength' => '10',
      '#attributes' => ['style' => 'width:5em'],
      '#field_prefix' => ($minus ? '-' : '') .$this->prefix,
      '#min' => 0
    ];

    if ($this->separator == '.' and $this->subdivision == 99) {
      if ($main) {
        $elements['main']['#default_value'] .= '.' . str_pad($subdivision, strlen($this->subdivision), '0', STR_PAD_LEFT);
      }
      $elements['main']['#step'] = pow(10, -strlen($this->subdivision));
      $elements['main']['#pattern'] .= '(\.[0-9]{'.strlen($this->subdivision).'})?';
    }
    elseif ($this->subdivision) {
      $default_div = intval($default_val) % $this->subdivision;
      $size = strlen($this->subdivision);
      $elements['main']['#suffix'] = $this->separator;
      // If there is a slash in the format, then prepare a dropdown.
      if ($this->options) {
        $options = [];
        $step = $this->subdivision / $this->options;
        for ($j = 0; $j < $this->options; $j++) {
          $v = $j * $step;
          $options[intval($v)] = $v;
        }
        // If a preset value isn't in the $options ignore the options and use
        // the numeric sub-widget.
        if (array_key_exists(intval($default_div), $options)) {
          $elements['subdivision'] = [
            '#type' => 'select',
            '#options' => $options,
            '#weight' => 3
          ];
        }
        else {
          $elements['subdivision'] = [
            '#type' => 'number',
            '#min' => 0,
            '#step' => $step,
            '#size' => $size,
            '#maxlength' => $size,
            '#attributes' => ['style' => 'width:' . ($size + 3) . 'em;'],
            '#max' => $this->subdivision, //Which is already all the nines
          ];
        }
      }
      else {// Just enter the number of sub units
        $elements['subdivision'] = [
          '#type' => 'number',
          '#min' => 0,
          '#step' => 1,
          // #size has no effect in opera.
          '#size' => $size + 2, // Spinner takes about 2 chars
          '#maxlength' => $size,
          '#max' => $this->subdivision, // E.G. 59 minutes
        ];
      }
      $elements['subdivision'] += [
        '#suffix' => $this->suffix,
        '#default_value' => $default_div,
        '#attributes' => ['class' => ['worth-subdivision']],
        '#theme_wrappers' => [],
        '#placeholder' => str_pad('',$size, '0'),
        '#weight' => 3
      ];
    }
    // Placeholder
    extract($this->formatParts($placeholder_val));
    $elements['main']['#placeholder'] = $main;
    if ($this->separator == '.' and $this->subdivision == '99') {
      $elements['main']['#placeholder'] .= '.' . str_pad($subdivision, strlen($this->subdivision), '0', STR_PAD_LEFT);
    }

    return $elements;
  }

  /**
   * Convert the two part of a number (from the form element) into the raw value.
   * @param string $main
   * @param string|null $subdivision
   * @return int
   */
  public function unformat(string $main, ?string $subdivision = '') : int {
    $main = $main ? $main + 0 : 0; //Make this numeric if it is null.
    if (empty($this->subdivision)) {
      $raw_num = $main;
      if ($this->separator == '.') {
        $raw_num *= pow(10, strlen($this->subdivision));
      }
    }
    else {
      list($atoms) = explode('/', $this->subdivision);
      if (!is_numeric($atoms)) {// could happen in migration. @todo in premigration check with a regex.
        trigger_error("problem converting currency format");
      }
      $divisor = (int)$atoms;
      $options = is_numeric($subdivision) ? $subdivision : 0;
      $raw_num = $main * $divisor + $options;
    }
    return intval($raw_num);
  }

  /**
   * Express a float < 1 as half, thirds, quarters or fifths.
   * @param type $float
   * @return string
   */
  private static function decToFraction($float) : string {
    if ($float >= 1) {
      throw new \Exception("Only convert numbers less than one into options: $float");
    }
    $numerator = floor($float * 60);
    $options = [
      0 => '0',
      12 => '&#8533;', //1/5
      15 => '&#188;', //1/4
      20 => '&#8531;', //1/3
      24 => '&#8534;', //2/5
      30 => '&#189;', //1/2
      36 => '&#8535;', //3/5
      40 => '&#8532;', //2/3
      45 => '&#190;', //3/4
      48 => '&#8536;', //4/5
      53 => '&#8536;', //4/5 rounded down
    ];

    $last = 0;
    foreach (array_keys($options) as $sixtieths) {
      if ($numerator >= $sixtieths) {$last = $sixtieths; continue;}
      return $options[$last];
    }
    return '';
  }

  /**
   * @param int $raw_num
   *   $num formatted as float.
   * @return string[]
   *   The formatted axis points, keyed by float value.
   */
  function axisMarkers(int $raw_num) {
    if ($raw_num == 0) {
      $raw_num = $this->unformat(1);
    }
    $float = $this->formatAsFloat($raw_num);
    $exponent = floor(log10(abs($float)));
    $mantissa = abs($float / pow(10, $exponent));
    // Transform the number into a number between 1 and 10
    if ($mantissa > 7.5) $markers = [5, 10];
    elseif ($mantissa > 6) $markers = [2.5, 5, 7.5];
    elseif ($mantissa > 4.5) $markers = [2, 4, 6];
    elseif ($mantissa > 4) $markers = [1.5, 3, 4.5];
    elseif ($mantissa > 3) $markers = [2, 4];
    elseif ($mantissa > 2) $markers = [1, 2, 3];
    elseif ($mantissa > 1.5) $markers = [1, 2];
    else $markers = [1, 2];

    // Transform the markers back to the right power of 10
    if ($this->subdivision) {
      array_walk(
        $markers,
        function(&$n) use ($exponent, $float, $raw_num) {
          $n *= pow(10, $exponent);
          $n *= $this->subdivision;
          if ($float < 0) $n *= -1;
          $n = intval($n);
        }
      );
    }
    $markers = [0] + $markers;

    foreach ($markers as $raw_worth) {
      $label = strip_tags((string)$this->format(intval($raw_worth)));
      $result[$label] = $this->formatAsFloat($raw_worth);
    }
    return $result;
  }

  /**
   * Calculate some good axis labels, using the min & max balance extents.
   *
   * @param $raw_min
   * @param $raw_max
   *
   * @return string[]
   *   $min, $mid, $max axis labels keyed by float values.
   */
  function gchartAxes(int $raw_min, int $raw_max) : array {
    $range = $raw_max - $raw_min;
    if ($raw_min >= 0) { // All positive
      $markers = $this->axisMarkers($raw_max);
    }
    elseif ($raw_max <= 0) {// All negative
      $markers = $this->axisMarkers($raw_min);
    }
    else { // Equal axes either side of zero.
      $markers = [];
      $pos_markers = $this->axisMarkers(max(abs($raw_min), abs($raw_max)));
      foreach ($pos_markers as $string => $float) {
        $markers[$string] = $float;
        $markers["-$string"] = -$float;
      }
      unset($markers['-0']);
      asort($markers);
    }
    return $markers;
  }


  function __gchartAxes($min, $max) : array {
    $range = $max - $min;
    if ($min >= 0) { // All positive
      $markers = $this->axisMarkers($max);
    }
    elseif ($max <= 0) {// All negative
      $markers = $this->axisMarkers($min);
    }
    else { // either side of zero.
      $pos = $this->axisMarkers($max);
      $neg= $pos;
      array_walk($neg, function (&$n) {$n = -$n;});
      unset ($neg[0]);
      $markers = array_merge($neg, $pos);

    }
    return $markers;
  }


}
