<?php

namespace Drupal\mcapi\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an exchange entity.
 *
 * @note This does NOT implement entityOwnerInterface because the owner is derived
 * from the owner, so you can't set the owner
 */
interface WalletInterface extends ContentEntityInterface {

  /**
   * Check whether a wallet has ever been used. i.e. whether the journal
   * references it.
   *
   * @return bool
   *   TRUE if the wallet has never been used.
   */
  public function isEmpty() : bool;

  /**
   * Retrieve the full balance history.
   *
   * N.B if caching running balances remember to clear the cache whenever a
   * transaction changes state or is deleted.
   *
   * @param int $since
   *   A unixtime before which to exclude the transactions.
   *
   * @return array
   *   Balances keyed by timestamp, including zero at the start and current balance.
   */
  public function history(int $since) : array;

}
