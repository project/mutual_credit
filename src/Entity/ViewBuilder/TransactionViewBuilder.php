<?php

namespace Drupal\mcapi\Entity\ViewBuilder;

use Drupal\mcapi\Plugin\Field\FieldType\McState;
use Drupal\Core\Render\Markup;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Render controller for transactions.
 */
class TransactionViewBuilder extends EntityViewBuilder {

  protected $settings;
  protected $routeMatch;
  const STATE_NAMES = [
    'P' => 'pending',
    'C' => 'completed',
    'V' => 'validated',
    'E' => 'erased',
    'T' => 'timedout',
    'I' => 'initiated'
  ];

  /**
   *
   * @param type $entity_type
   * @param type $entity_repository
   * @param type $language_manager
   * @param type $theme_registry
   * @param type $entity_display_repository
   * @param ConfigFactory $config_factory
   * @param CurrentRouteMatch $route_match
   * @param ModuleHandler $module_handler
   */
  public function __construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository, ConfigFactory $config_factory, CurrentRouteMatch $route_match) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->settings = $config_factory->get('mcapi.settings');
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritDoc}
   *
   * @todo update with entity_type.manager when core interface changes
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository'),
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    $lookup = array_combine(McState::STATES, McState::NAMES);
    $build['#attributes']['class'][] = 'state-' . $lookup[$entity->state->value];
    $build['#attributes']['class'][] = 'mc-transaction';
    $build['#attached']['library'][] = 'mcapi/transaction';
    return $build;
  }


  public function buildComponents(&$build, array $entities, array $displays, $view_mode) {

    parent::buildComponents($build, $entities, $displays, $view_mode);
    $display = $displays['mc_transaction'];
    foreach ($entities as $id => $transaction) {

      if ($options = $display->getComponent('other_entries')) {
        foreach ($transaction->entries->other() as $entryItem) {
          $lines[] =  $entryItem->asOneLine();
        }
        if (isset($lines)) {
          $build[$id]['other_entries']['#markup'] = Markup::Create(implode('<br />', $lines));
        }
      }

      if ($options = $display->getComponent('log')) {
        $build[$id]['log'] = [
          '#theme' => 'mc_workflow_log',
          '#transaction' => $transaction,
          '#weight' => $options['weight']
        ];
      }

      if ($options = $display->getComponent('links')) {
        if ($links = $transaction->workflow->linksAsRenderable()) {
          $links['#prefix'] = '<strong>'.t('Actions').'</strong>';
          $build[$id]['links'] = $links;
        }
      }
    }
  }



}
