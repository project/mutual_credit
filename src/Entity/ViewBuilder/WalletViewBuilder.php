<?php

namespace Drupal\mcapi\Entity\ViewBuilder;

use Drupal\views\Entity\View;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Url;

/**
 * Render controller for wallets.
 */
class WalletViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildComponents(&$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);
    $display = $displays['mc_wallet'];
    foreach ($entities as $id => $wallet) {
      if ($view_mode <> 'full') {
        // Show the wallet title at the top
        $build[$id]['title'] = [
          '#type' => 'item',
          '#markup'=> '<strong>'. $wallet->label(). '('. $wallet->id() .')</strong>',
          '#weight' => -2,
        ];
      }
      if ($options = $display->getComponent('balance_bars')) {
        $build[$id]['balance_bars'] = mc_wallet_view_balance_bars($wallet) + [
          '#title' => $this->t('Given and received'),
          '#weight' => $options['weight'],
          '#theme_wrappers' => ['mc_wallet_component'],
          '#cache' => [
            'tags' => ['mc_wallet:'.$wallet->id()]
          ]
        ];
      }
      if ($options = $display->getComponent('balance_viz')) {
        $build[$id]['balance_viz'] = mc_wallet_view_balance_viz($wallet) + [
          '#title' => $this->t('Balance'),
          '#weight' => $options['weight'],
          '#cache' => [
            'tags' => ['mc_wallet:'.$wallet->id()]
          ]
        ];
      }
      if ($options = $display->getComponent('balance')) {
        // Because this callback returns #markup, if conflicts with the wrapper/
        $build[$id]['balance'] = mc_wallet_view_balance($wallet) + [
          '#title' => $this->t('Balance'),
          '#weight' => $options['weight'],
          '#cache' => [
            'tags' => ['mc_wallet:'.$wallet->id()]
          ]
        ];
      }
      if ($options = $display->getComponent('stats')) {
        $build[$id]['stats'] = mc_wallet_view_stats($wallet) + [
          '#title' => $this->t('Summary stats'),
          '#weight' => $options['weight'],
          '#theme_wrappers' => ['mc_wallet_component'],
          '#cache' => [
            'tags' => ['mc_wallet:'.$wallet->id()]
          ]
        ];
      }
      if ($options = $display->getComponent('history')) {
        if ($renderable = mc_wallet_view_balance_history($wallet, 400)) {
          $build[$id]['history'] = $renderable + [
            '#title' => $this->t('Balance history'),
            '#weight' => $options['weight'],
            '#theme_wrappers' => ['mc_wallet_component'],
            '#cache' => [
              'tags' => ['mc_wallet:'.$wallet->id()]
            ]
          ];
        }
      }
      if ($options = $display->getComponent('link_transactions')) {
        $view_names = ['mc_wallet_transactions', 'mc_income_expenditure'];
        foreach (View::loadMultiple($view_names) as $view) {
          if ($view->status()) {
            $build[$id]['transactions'] = [
              '#type' => 'link',
              '#title' => t('Transactions...'),
              // @todo Lookup which view requires which arg names, both is ok.
              '#url' => Url::fromRoute('view.'.$view->id().'.wallet_page', ['mc_wallet' => $wallet->id(), 'arg_0' => $wallet->id()]),
              '#weight' => $options['weight'],
              '#attributes' => ['class' => ['component', 'link']],
              '#cache' => [
                'tags' => ['mc_wallet:'.$wallet->id()]
              ]
            ];

            break;// Just show one.
          }
        }
      }
      if ($view_mode <> 'full') {
        $build[$id]['canonical'] = $wallet->toLink(t('View wallet...'))->toRenderable();
        $build[$id]['canonical']['#prefix'] = '<br />';
        $build[$id]['canonical']['#weight'] = 20;
        $build[$id]['canonical']['#attributes'] = ['class' => ['component', 'link']];
      }
    }
  }

}
