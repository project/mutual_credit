<?php

namespace Drupal\mcapi\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use CreditCommons\Workflow as CCWorkflowInterface;
use CreditCommons\Exceptions\InvalidWorkflowViolation;

/**
 * Defines the transaction workflow entity class, i.e. the workflow definition.
 * Equivalent to CreditCommons\Workflow.
 * @nb These should not be deleteable if transactions depend on them.
 *
 * @ConfigEntityType(
 *   id = "mc_workflow",
 *   label = @Translation("Transaction workflow"),
 *   config_prefix = "mc_workflow",
 *   admin_permission = "configure mcapi",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "storage" = "Drupal\mcapi\Entity\Storage\WorkflowStorage",
 *     "access" = "Drupal\mcapi\Entity\Access\WorkflowAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\mcapi\Form\WorkflowEdit",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\mcapi\Entity\ListBuilder\WorkflowListBuilder"
 *   },
 *   links = {
 *     "add-form" = "/admin/accounting/workflow/add",
 *     "edit-form" = "/admin/accounting/workflow/{mc_workflow}/edit",
 *     "delete-form" = "/admin/accounting/workflow/{mc_workflow}/delete",
 *     "collection" = "/admin/accounting/workflows"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "summary",
 *     "code"
 *   }
 * )
 */
class Workflow extends ConfigEntityBase {

  const TRANSITIONS = ['VP', 'VC', 'VX', 'PC', 'PE', 'CE', 'EC', 'PX', 'CX'];

  /**
   * Unique ID, equivalent to CCWorkflowInterface::code
   * @var string
   */
  public string $id;

  /**
   * Name to show in lists.
   * @var string
   */
  public string $label;

  /**
   * Short description.
   * @var string
   */
  public string $summary = '';

  /**
   * Code containing all the workflow rules.
   * @var string
   */
  public $code = '';

  /**
   * @var array
   */
  public array $transitions = [];

  /**
   * Constructor
   */
  public function __construct(array $values) {
    parent::__construct($values, 'mc_workflow');
    if (isset($values['code'])) {
      if ($info = static::validate($values['code'])) {
        throw new InvalidWorkflowViolation($values['code'], $info[0], implode($info[1]));
      }
      preg_match_all('/[PCE][PCEX][+-=0]/', substr($values['code'], 2), $matches);
      if ($matches) {
        $transitions = $matches[0];
        foreach ($transitions as $transition) {
          $this->transitions[$transition[0]][$transition[1]] = $transition[2];
        }
      }
    }
  }

  /**
   * Check that the workflow code is value
   * @param string $code
   * @return array
   *   empty if it is valid, otherwise the position of the bad char and the valid values
   */
  static function validate(string $code) : array {
    $regex = \Drupal::config('mcapi.settings')->get('delete_transactions') ?
      '/[PCE][PCE][+-=0]/' :
      '/[PCE][PCEX][+-=0]/';

    $directions = [CCWorkflowInterface::PARTY_PAYEE, CCWorkflowInterface::PARTY_PAYER, CCWorkflowInterface::PARTY_ADMINONLY];
    $confirms = [CCWorkflowInterface::CONFIRM_DO, CCWorkflowInterface::CONFIRM_SKIP];
    $start_states = [CCWorkflowInterface::STATE_PENDING, CCWorkflowInterface::STATE_COMPLETED];
    if (!in_array($code[0], $confirms)) {
      return [1, $confirms];
    }
    elseif (!in_array($code[1], $start_states)) {
      return [2, $start_states];
    }
    elseif (!in_array($code[2], $directions)) {
      return [3, $directions];
    }
    elseif (strlen($code) == 3) {
      return [];
    }
    elseif (strlen($code) > 3 and preg_match_all($regex, substr($code, 3), $matches)) {
      return [];
    }
    else {
      // todo give more detail about the bad char.
      return [4, []];
    }
  }

  /**
   * Load any possible workflow as a config entity.
   * @param string $code
   * @return static
   */
  static function loadByCode(string $code) {
    $storage = \Drupal::entityTypeManager()->getStorage('mc_workflow');
    if ($wfs = $storage->loadByProperties(['code' => $code])) {
      $wf = reset($wfs);
    }
    else {
      // Oops this class expects the $error_context variable.
      CCWorkflowInterface::validate($code); // throws invalidWorkflowViolation
      $props = [
        'id' => 'custom',
        'label' => t('Custom workflow'),
        'summary' => t('No information available'),
        'code' => $code
      ];
      $wf = new static($props);
    }
    return $wf;
  }

  function direction() : string {
    return $this->code[2]??CCWorkflowInterface::DIR_THIRDPARTY;
  }

  function isBill() : bool {
    return $this->code[2] == CCWorkflowInterface::PARTY_PAYEE;
  }

  function isCredit() : bool {
    return $this->code[2] == CCWorkflowInterface::PARTY_PAYER;
  }

  function is3rdParty() : bool {
    return $this->code[2] == CCWorkflowInterface::PARTY_ADMINONLY;
  }

  function requiresConfirmation() : bool {
    return $this->code ? $this->code[0] == '|' : TRUE;
  }

  function directionName() {
    switch($this->direction()) {
      case CCWorkflowInterface::DIR_BILL: return 'bill';
      case CCWorkflowInterface::DIR_CREDIT: return 'credit';
      case CCWorkflowInterface::DIR_THIRDPARTY: return 'thirdparty';
    }
  }
  function startState() : string {
    return substr($this->code, 1, 1);
  }

  function getCode() : string {
    return $this->code;
  }

  /**
   * Get all the transitions used in all the workflows.
   * @return array
   *   two letter transition codes.
   *
   * @todo maybe this would be better in functions?
   */
  static function usedTransitions() {
    foreach (Workflow::loadMultiple() as $wf) {
      if ($wf->requiresConfirmation()) {
        $transitions[] = CCWorkflowInterface::STATE_VALIDATED . $wf->startState();
      }
      foreach ($wf->transitions as $from => $info) {
        foreach ($info as $to => $party) {
          $transitions[] = $from.$to;
        }
      }
    }
    return array_unique($transitions);
  }

  /**
   * Get all the transitions the system permits, with or without delete.
   * @return array
   */
  static function allTransitions() : array {
    $res = static::TRANSITIONS;
    if (!\Drupal::config('mcapi.settings')->get('delete_transactions')) {
      $res = array_filter(
        $res,
        function ($t) {return $t[1] <> CCWorkflowInterface::STATE_DELETE;}
      );
    }
    return $res;
  }

}
