<?php

namespace Drupal\mcapi\Entity;

use Drupal\mcapi\Plugin\Field\FieldType\Entry;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi\Functions;
use Drupal\mcapi\Event\McapiEvents;
use Drupal\mcapi\Event\TransactionSaveEvent;
use Drupal\mcapi\WorkflowException;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\Entity\User;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use CreditCommons\Workflow as CCWorkflowInterface;

//ini_set('memory_limit' 500);
/**
 * Defines the Transaction entity.
 * @ContentEntityType(
 *   id = "mc_transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Transactions"),
 *   handlers = {
 *     "storage" = "Drupal\mcapi\Entity\Storage\TransactionStorage",
 *     "storage_schema" = "Drupal\mcapi\Entity\Storage\TransactionStorageSchema",
 *     "view_builder" = "Drupal\mcapi\Entity\ViewBuilder\TransactionViewBuilder",
 *     "list_builder" = "\Drupal\mcapi\Entity\ListBuilder\TransactionListBuilder",
 *     "access" = "Drupal\mcapi\Entity\Access\TransactionAccessControlHandler",
 *     "form" = {
 *     "collection" = "/admin/accounting/transactions",
 *       "default" = "Drupal\mcapi\Form\TransactionForm",
 *       "transition" = "Drupal\mcapi\Form\TransactionConfirmForm"
 *     },
 *     "views_data" = "Drupal\mcapi\Entity\Views\TransactionViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\mcapi\Entity\RouteProvider\TransactionRouteProvider",
 *     }
 *   },
 *   admin_permission = "configure mcapi",
 *   base_table = "mc_transaction",
 *   translatable = FALSE,
 *   revision_table = "mc_transaction_revision",
 *   show_revision_ui = FALSE,
 *   field_ui_base_route = "entity.mc_transaction.collection",
 *   entity_keys = {
 *     "id" = "xid",
 *     "revision" = "vxid",
 *     "uuid" = "uuid",
 *     "name" = "description",
 *     "owner" = "creator",
 *     "uid" = "creator"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/transaction/add/{mc_workflow}",
 *     "canonical" = "/transaction/{mc_transaction}",
 *     "collection" = "/admin/accounting/transactions"
 *   }
 * )
 */
class Transaction extends RevisionableContentEntityBase implements TransactionInterface, EntityOwnerInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @note There seems to be no way to inject anything here.
   *
   * @see SqlContentEntityStorage::mapFromStorageRecords
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = array()) {
    parent::__construct($values, $entity_type, $bundle, $translations);
  }

  /**
   * {@inheritDoc}
   */
  public function label($langcode = NULL) {
    return $this->t("Transaction #@id", ['@id' => $this->id()]);
  }

  /**
   * @inheritDoc
   *
   * The assemble phase must happen after Entity ref fields are validated
   * because entity reference fields only determine their final value during
   * validation. The assemble phase must happen before entity level validation.
   * Therefore this function validates the entity, assembles it, and validates
   * it again.
   */
  public function validate() : EntityConstraintViolationListInterface {
    $violations = parent::validate();
    /** @var EntityConstraintViolationListInterface $violations */
    if (!$violations->count()) {
      // Credcom module might generate exceptions during assembly
      try {
        // This also puts the transaction into its first defined 'state'
        Functions::getAssembledTransaction($this);
        $violations = parent::validate();
      }
      catch(\Exception $e) {
        $violation = new \Symfony\Component\Validator\ConstraintViolation(
          message: $e->getMessage(),
          messageTemplate: 'message template',
          parameters: [],
          propertyPath: 'payer_wid',
          invalidValue: $this->entries[0]->getValue(),
          root: $this->entries[0]->getValue(),
          code: $e->getCode(),
        );
        $violations->add($violation);
      }
    }
    if ($violations->count() == 0) {
      $this->state->value = CCWorkflowInterface::STATE_VALIDATED;
    }
    return $violations;
  }

  /**
   * {@inheritDoc}
   * Payee and payer are handled as arrays for mass transactions because there's nowhere better to do it.
   * @throws InvalidWorkflowViolation
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    $values += [
      // If creator is 0 that means drush or credcom must have created it.
      'creator' => static::getDefaultEntityOwner(), // Currentuser
      'payee' => [],
      'payer' => [],
    ];
    Workflow::validate($values['workflow']);

    $first = TRUE;
    if (isset($values['entries'])) {
      foreach ($values['entries'] as &$e) {
        $e['primary'] = $first;
        $first = FALSE;
      }
    }
    else {
      // Multiple payers or payees indicates a mass transaction.
      foreach ((array)$values['payee'] as $payee_wid) {
        foreach ((array)$values['payer'] as $payer_wid) {
          $values['entries'][] = [
            'payer_wid' => $payer_wid,
            'payee_wid' => $payee_wid,
            'description' => $values['description'] ?? '',
            'quant' => $values['quant'] ?? 0,
            'primary' => $first
          ];
          $first = FALSE;
        }
      }
    }
    unset($values['payee'], $values['payer']);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTagsToInvalidate() {
    $tags = parent::getCacheTagsToInvalidate();
    // Invalidate tags of wallets
    foreach (array_chunk($this->allWalletIds(), 50) as $wids) {
      foreach (Wallet::loadMultiple($wids) as $wallet) {
        $tags = array_merge_recursive($tags, $wallet->getCacheTagsToInvalidate());
      }
    }
    return $tags;
  }

  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    // Add the user context because links are shown per user.
    $contexts[] = 'user:'.\Drupal::currentUser()->id();
    $contexts[] = 'user';
    return $contexts;
  }

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type)
      + static::ownerBaseFieldDefinitions($entity_type);
    $fields['revision_log'] // Never use this
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', ['region' => 'hidden']);

    $fields['entries'] = BaseFieldDefinition::create('entry')
      ->setLabel(t('Entries'))
      ->setDescription(t('One or more flows of currency between accounts.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->addConstraint('DifferentWallets')
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['weight' => 4]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('On'))
      ->setDescription(t('The time that the transaction was created.'))
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ]);

    $fields['workflow'] = BaseFieldDefinition::create('mc_workflow')
      ->setLabel(t('Workflow'))
      ->setDescription(t('The workflow path for the transaction'))
      ->setDefaultValue('0_C')
      ->addConstraint('mc_workflow')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayOptions('view', ['region' => 'hidden'])
      ->setRevisionable(FALSE)
      ->setReadOnly(TRUE)
      ->setRequired(TRUE);

    $fields['state'] = BaseFieldDefinition::create('mc_state')
      ->setLabel(t('State'))
      ->setDescription(t('Completed, pending, erased, etc.'))
      ->setDefaultValue(CCWorkflowInterface::STATE_INITIATED)
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', ['type' => 'mc_stamp'])
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['choices' => \Drupal\mcapi\Plugin\Field\FieldType\McState::STATES],
      ]);

    return $fields;
  }

  /**
   * {@inheritDoc}
   *
   * Workaround for when the content_translation module tries to link to the
   * original language before the entity is saved and an id is available on transaction/0/save
   * I don't know why the entity definition 'translatable = FALSE' doesn't apply
   */
  public function hasLinkTemplate($rel) {
    if (!$this->isNew()) {
      return parent::hasLinkTemplate($rel);
    }
    return FALSE;
  }

  /**
   * @param array $params
   * @throws \Exception
   */
  public function addEntry(array $params) : ?Entry {
    if (!isset($params['payee_wid'])){
      $params['payee_wid'] = $params['payee'];
    }
    if (!isset($params['payer_wid'])) {
      $params['payer_wid'] = $params['payer'];
    }
    unset($params['payer'], $params['payee']); // these will be computed fields.
    if ($params['payer_wid'] == $params['payee_wid']) {
      return NULL;
    }
    $params['description'] = (string)$params['description'];
    $this->entries->appendItem($params);
    return $this->entries->offsetGet($this->entries->count()-1);
  }

  /**
   * {@inheritDoc}
   */
  function &__get($name) {
    switch ($name) {
      case 'payer':
        $val = $this->entries->payer;
        break;
      case 'payer_wid':
        $val = $this->entries->payer_wid;
        break;
      case 'payee':
        $val = $this->entries->payee;
        break;
      case 'payee_wid':
        $val = $this->entries->payee_wid;
        break;
      case 'quant':
        $val = $this->entries->quant;
        break;
      case 'description':
        $val = $this->entries->description;
        break;
    }
    if (isset($val)) {
      return $val;
    }
    return parent::__get($name);
  }

  /**
   * @return int[]
   */
  function allWalletIds() : array {
    $wallet_ids = [];
    foreach ($this->entries as $entry_item) {
      $wallet_ids[] = $entry_item->payee_wid;
      $wallet_ids[] = $entry_item->payer_wid;
    }
    // This method could be called on a new transaction with wallet fields not populated.
    return array_unique($wallet_ids);
  }

  /**
   * Determine if a wallet is involved in this transaction.
   *
   * @param int $wid
   *   A wallet id
   * @return bool
   */
  function involves(int $wid) : bool {
    foreach ($this->entries as $entry_item) {
      if ($entry_item->payee_wid == $wid or $entry_item->payer_wid == $wid) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   *
   * @param string $dest_state
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\mcapi\WorkflowException
   */
  function saveVersion($dest_state) {
    $wf = $this->workflow->entity;
    if ($this->isNew()) {
      if ($dest_state <> $wf->startState()) {
        \Drupal::logger('mcapi')->error("Tried to save new transaction with workflow $wf->code to .$dest_state");
        $dest_state = $wf->startState();
      }
    }
    else {// Check that dest_state is allowed.
      if (!in_array($dest_state, $this->workflow->destinations())) {
        // Should never happen via the UI
        throw new WorkflowException($this, $dest_state);
      }
      $this->setNewRevision(TRUE);
    }
    $this->transition = $this->state->value . $dest_state;
    $this->state->value = $dest_state;
    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  function save($transition = '') {
    $this->setRevisionUser(User::load(\Drupal::currentUser()->id()));
    parent::save();
    if ($transition = $this->transition) {
      $event = new TransactionSaveEvent($this, ['transition' => $transition]);
      \Drupal::service('event_dispatcher')->dispatch($event, McapiEvents::ACTION);
\Drupal::messenger('Dispatched TransactionSaveEvent');
    }
  }

  /**
   * Render the transaction as a sentence with or without links.
   * Sentence is configured on the main settings page.
   * @param bool $links
   * @return \Drupal\Core\Render\Markup
   */
  function sentence() : string {
    return $this->entries->first()->asOneLine();
  }

  /**
   * Load the workflow as a configEntity, even if it means creating one on the fly.
   * @return Workflow
   */
  function workflow() : Workflow {
    return Workflow::loadByCode($this->workflow->value);
  }

  /**
   * load all the revisions of this transaction.
   * @return array
   */
  function revisions() : array {
    $storage = \Drupal::entityTypeManager()->getStorage('mc_transaction');
    $ids = $storage->getQuery()->accessCheck(TRUE)
      ->allRevisions()
      ->condition('xid', $this->id())
      ->sort('vxid', 'ASC')
      ->execute();
    return $storage->loadMultipleRevisions(array_keys($ids));
  }

}
