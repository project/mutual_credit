<?php

namespace Drupal\mcapi\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Views Data for Transaction entity.
 */
class TransactionViewsData extends EntityViewsData {

  /**
   * {@inheritDoc}
   *
   * @todo Drupal\taxonomy\TermViewsData shows how to incorporate an index table
   *
   * @note There is currently no way to theme an aggregated field e.g the SUM of
   * all incomes. This issue would define the aggregation functions as plugins
   * which would at least give us something to alter.
   * @see https://www.drupal.org/project/drupal/issues/2662548
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['mc_transaction']['table']['access query tag'] = 'mc_transaction_access';

    $data['mc_transaction']['workflow']['filter'] = [
      'id' => 'in_operator',
      'options callback' => '\Drupal\mcapi\Functions::workflowOptions',
    ];
    $data['mc_transaction']['created']['field']['id'] = 'date';
    $data['mc_transaction']['state']['field']['id'] = 'mc_state';
    $data['mc_transaction']['state']['filter'] = [
      'id' => 'in_operator',
      'options callback' => '\Drupal\mcapi\Plugin\Field\FieldType\McState::storedStateNames'
    ];

    // Payee
    $data['mc_transaction__entries']['entries_payee_wid']['title'] = t('Payee wallet id');
    $data['mc_transaction__entries']['entries_payee_wid']['field'] = [
      'id' => 'standard',
      'label' => t('Payee'),
      'title' => t('Credited wallet'),
      'field_name' => 'entries',
      'property' => 'payee_wid',
    ];
    $data['mc_transaction__entries']['entries_payee_wid']['relationship'] = [
      'base' => 'mc_wallet',
      'base field' => 'wid',
      'label' => t('Payee'),
      'title' => t('Credited wallet'),
      'id' => 'standard',
    ];

    // Payer
    $data['mc_transaction__entries']['entries_payer_wid']['title'] = t('Payer wallet id');
    $data['mc_transaction__entries']['entries_payer_wid']['field'] = [
      'id' => 'standard',
      'label' => t('Payer'),
      'title' => t('Debited wallet'),
      'field_name' => 'entries',
      'property' => 'payer_wid',
    ];
    $data['mc_transaction__entries']['entries_payer_wid']['relationship'] = [
      'base' => 'mc_wallet',
      'base field' => 'wid',
      'label' => t('Payer'),
      'title' => t('Debited wallet'),
      'id' => 'standard',
    ];

    // Quant
    $data['mc_transaction__entries']['entries_quant']['title'] = t('Quantity');
    $data['mc_transaction__entries']['entries_quant']['field'] = [
      'id' => 'mc_quant',
      'label' => t('Quantity'),
      'title' => t('Amount transferred'),
      'field_name' => 'entries',
      'property' => 'quant',
    ];

    // Description
    $data['mc_transaction__entries']['entries_description']['title'] = t('Description');
    $data['mc_transaction__entries']['entries_description']['field'] = [
      'id' => 'standard',
      'label' => t('Description'),
      'field_name' => 'entries',
      'property' => 'description',
    ];
    // Is Primary
    $data['mc_transaction__entries']['entries_primary']['filter']['id'] = 'boolean';
    $data['mc_transaction__entries']['entries_primary']['title'] = t('Is Primary (exclude transaction fees)');

    unset($data['mc_transaction__entries']['entries_primary']['field']);
    unset($data['mc_transaction__entries']['entries_quant_t']);
    unset($data['mc_transaction__entries']['entries_quant']['filter']);


    $data['mc_transactions_index']['primary'] = $data['mc_transaction__entries']['entries_primary'];

    $data['mc_transactions_index']['table'] = [
      'group' => $this->t('Transaction index'),
      'entity type' => 'mc_transaction',
      'base' => [
        'field' => 'xid',
        'title' => $this->t("Transaction index"),
        'help' => $this->t("Views-friendly index of completed transactions"),
        'access query tag' => 'mc_transaction_access',
        'weight' => 5,
        'defaults' => [],
        'cache_contexts' => $this->entityType->getListCacheContexts(),
      ],
      'join' => [
        'mc_wallet' => [
          'left_field' => 'wid',
          'field' => 'wallet_id'
        ],
      ],
      'entity revision' => '',
      // This links it to the wizard plugin.
      'wizard_id' => 'transaction_index',
    ];

    $data['mc_transactions_index']['xid'] = [
    // The item it appears as on the UI,.
      'title' => $this->t('Transaction ID'),
      'help' => $this->t('The unique database key of the transaction'),
      'field' => [
        'id' => 'field',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'entity field' => 'xid',
    ];
    $data['mc_transactions_index']['wallet_id'] = [
      'title' => $this->t('Wallet ID'),
      'help' => $this->t('The wallet we are looking at'),
      'relationship' => [
        'id' => 'standard',
        'base' => 'mc_wallet',
        'base field' => 'wid',
        'label' => t('Wallet'),
        'relationship field' => 'wallet_id',
      ],
      'filter' => [
        'id' => 'wallet_name',
      ],
      'argument' => [
        'id' => 'standard',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];
    $data['mc_transactions_index']['partner_id'] = [
      'title' => $this->t('Partner wallet ID'),
      'help' => $this->t('The other wallet in the transaction.'),
      'relationship' => [
        'id' => 'standard',
        'base' => 'mc_wallet',
        'base field' => 'wid',
        'label' => $this->t('Partner wallet'),
        'relationship field' => 'partner_id',
      ],
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'wallet_name',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];

    $data['mc_transactions_index']['state'] = [
      'title' => $this->t('State'),
      'help' => $this->t('The workflow state of the transaction.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'in_operator',
        'options callback' => '\Drupal\mcapi\Plugin\Field\FieldType\McState::storedStateNames',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];

    $data['mc_transactions_index']['workflow'] = [
      'title' => $this->t('Workflow'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'in_operator',
        'options callback' => '\Drupal\mcapi\Functions::workflowOptions',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];
    $data['mc_transactions_index']['created'] = [
      'title' => $this->t('Date'),
      'help' => $this->t('The time the transaction was created.'),
      'field' => [
        'id' => 'date',
      ],
      'sort' => [
        'id' => 'date',
      ],
      'filter' => [
        'id' => 'date',
      ]
    ];
    $data['mc_transactions_index']['description'] = [
      'title' => $this->t('Date'),
      'help' => $this->t('The time the transaction was created.'),
      'field' => [
        'id' => 'standard',
      ]
    ];
    $data['mc_transactions_index']['incoming'] = [
      'title' => $this->t('Income'),
      'help' => $this->t('The income from the transaction; positive or zero'),
      'field' => [
        'id' => 'mc_quant'
      ],
      'filter' => [
        // @todo make a filter that knows not to show the native value
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
        'group_by' => TRUE,
        'group_type' => 'group',
      ],
    ];

    $data['mc_transactions_index']['outgoing'] = [
      'title' => $this->t('Expenditure'),
      'help' => $this->t('The outgoing quantity of the transaction; positive or zero'),
      'field' => [
        'id' => 'mc_quant'
      ],
      'filter' => [
        // @todo make a filter that knows not to show the native value
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
        'group_by' => TRUE,
        'group_type' => 'group',
      ],
    ];
    $data['mc_transactions_index']['diff'] = [
      'title' => $this->t('Change in balance'),
      'help' => $this->t('The difference to the balance; positive or negative.'),
      'field' => [
        'id' => 'mc_quant',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];
    $data['mc_transactions_index']['volume'] = [
      'title' => $this->t('Volume'),
      'help' => $this->t('The quantity of the transaction; always positive'),
      'field' => [
        'id' => 'mc_quant',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];

    // Virtual fields.
    $data['mc_transactions_index']['running_balance'] = [
      'title' => $this->t('Running balance'),
      'help' => $this->t("The sum of the wallet's previous transactions."),
      'field' => [
        'id' => 'transaction_running_balance',
        'additional fields' => [
          'wallet_id' => [
            'table' => 'mc_transactions_index',
            'field' => 'wallet_id'
          ],
          'xid' => [
            'table' => 'mc_transactions_index',
            'field' => 'xid'
          ],
          'volume' => [
            'table' => 'mc_transactions_index',
            'field' => 'volume'
          ],
          'created' => [
            'table' => 'mc_transactions_index',
            'field' => 'created',
          ]
        ],
      ],
    ];

    //@temp
    //@see https://www.drupal.org/node/2337507
    $data['mc_transaction']['created_fulldate'] = array(
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => array(
        'field' => 'created',
        'id' => 'date_fulldate',
      ),
    );
    $data['mc_transaction']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['mc_transaction']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];
    $data['mc_transactions_index']['created_fulldate'] = array(
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => array(
        'field' => 'created',
        'id' => 'date_fulldate',
      ),
    );
    $data['mc_transactions_index']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['mc_transactions_index']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];

    $data['mc_transaction_totals']['table'] = [
      'group' => $this->t('Transaction totals'),
      'base' => [
        'field' => 'wid',
        'title' => $this->t("Transaction totals"),
        'help' => $this->t("Summary per wallet"),
        'access query tag' => 'mc_wallet_access',
        'weight' => 10
      ],
      'join' => [
        'mc_wallet' => [
          'left_field' => 'wid',
          'field' => 'wid'
        ]
      ]
    ];

    // A special filter would be needed to convert the raw number to displayed value
    $data['mc_transaction_totals']['balance'] = [
      'title' => $this->t('Balance'),
      'help' => $this->t("Sum of all this wallet's credits minus debits"),
      'field' => [
        'id' => 'mc_quant'
      ],
      'sort' => [
        'id' => 'standard'
      ]
    ];
    $data['mc_transaction_totals']['balance'] = [
      'title' => $this->t('Balance'),
      'help' => $this->t("From totals table"),
      'field' => [
        'id' => 'mc_quant'
      ],
      'sort' => [
        'id' => 'standard'
      ]
    ];
    $data['mc_transaction_totals']['volume'] = [
      'title' => $this->t('Volume of transactions'),
      'help' => $this->t("Sum of all this wallet's transactions"),
      'field' => [
        'id' => 'mc_quant',
        'stat' => 'volume'
      ],
      'sort' => [
        'id' => 'standard'
      ]
    ];
    $data['mc_transaction_totals']['gross_in'] = [
      'title' => $this->t('Gross income'),
      'help' => $this->t("Sum of all this wallet's income ever"),
      'field' => [
        'id' => 'mc_quant',
        'stat' => 'gross_in'
      ],
      'sort' => [
        'id' => 'standard'
      ]
    ];
    $data['mc_transaction_totals']['gross_out'] = [
      'title' => $this->t('Gross expenditure'),
      'help' => $this->t("Sum of all this wallet's outgoings ever"),
      'field' => [
        'id' => 'mc_quant',
        'stat' => 'gross_out'
      ],
      'sort' => [
        'id' => 'standard'
      ]
    ];
    $data['mc_transaction_totals']['trades'] = [
      'title' => $this->t('Number of trades'),
      'help' => $this->t("Number of transactions involving this wallet"),
      'field' => [
        'id' => 'standard',
      ],
      'sort' => [
        'id' => 'standard'
      ],
      'filter' => [
        'id' => 'numeric'
      ]
    ];
    $data['mc_transaction_totals']['partners'] = [
      'title' => $this->t('Partner count'),
      'help' => $this->t("Number of unique trading partners"),
      'field' => [
        'id' => 'standard',
      ],
      'sort' => [
        'id' => 'standard'
      ],
      'filter' => [
        'id' => 'numeric'
      ]
    ];
    return $data;
  }

}


