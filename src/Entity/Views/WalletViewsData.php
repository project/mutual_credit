<?php

namespace Drupal\mcapi\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Views data for wallet entity.
 */
class WalletViewsData extends EntityViewsData {

  /**
   * {@inheritDoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // This joins all wallet queries or wallet relationship queries to the mc_transaction_totals table.
    $data['mc_wallet']['table']['join']['mc_transaction_totals'] = [
      'table' => 'mc_transaction_totals',
      'left_field' => 'wid',
      'field' => 'wid'
    ];
    // this handles missing wallets.
    $data['mc_wallet']['name']['field']['id'] = 'mc_wallet';
    return $data;
  }
}
