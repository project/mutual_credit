<?php

namespace Drupal\mcapi\Entity\Access;

use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Query\WalletQuery;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Config\Config;

/**
 * Defines an access controller option for the mc_transaction entity.
 */
class TransactionAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * @var Drupal\mcapi\Entity\Query\WalletQuery
   */
  protected $walletQuery;

  /**
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $mcapiConfig;

  /**
   *
   * @param EntityTypeInterface $entity_type
   * @param WalletQuery $wallet_query
   * @param Messenger $messenger
   * @param Config $mcapi_settings
   */
  public function __construct(EntityTypeInterface $entity_type, WalletQuery $wallet_query, Messenger $messenger, Config $mcapi_settings) {
    parent::__construct($entity_type);
    $this->walletQuery = $wallet_query;
    $this->messenger = $messenger;
    $this->mcapiConfig = $mcapi_settings;
  }

  /**
   *
   * @param ContainerInterface $container
   * @param EntityTypeInterface $entity_type
   * @return \static
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage('mc_wallet')->getQuery(),
      $container->get('messenger'),
      $container->get('config.factory')->get('mcapi.settings'),

    );
  }

  /**
   * Anyone with a wallet can create a transaction as long as another wallet exists.
   * @param array $context
   *   - entity_type_id and langcode
   */
  public function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Friendly reminder that there must be at least 2 wallets on the system.
    if ($this->walletQuery->accessCheck(TRUE)->count()->execute() < 2) {
      // Would be great not to have to bother with this check every time.
      $this->messenger->addWarning('Transaction forms cannot be accessed until at least 2 wallets exist.');
      $result = AccessResult::forbidden('At least 2 wallets needed to transact.');
    }
    elseif ($this->walletQuery->condition('owner', $account->id())->execute()) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden('User has no wallet.');
    }
    return $result->cachePerUser();
  }

  /**
   * {@inheritDoc}
   *
   * @note There are only 2 operations, view and transition.
   */
  public function access(EntityInterface $transaction, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $this->prepareUser($account);
    $result = AccessResult::forbidden('Access forbidden by TransactionAccessControlHandler')->cachePerUser();
    if ($operation == 'view label' or $operation == 'view') {
      $operation = 'view';
      $cid = 'mc_transaction:' . $transaction->id();
      if (($return = $this->getCache($cid, $operation, 'und', $account)) !== NULL) {
        // Cache hit, no work necessary.
        return $return_as_object ? $return : $return->isAllowed();
      }
      if ($account->hasPermission('view all transactions') or $account->hasPermission('manage mcapi')) {
        $result = AccessResult::allowed()->cachePerUser();
      }
      else {
        // You can view this transaction if you own any of the wallets in it.
        foreach (array_chunk($transaction->allWalletIds(), 50) as $wids) {
          foreach (Wallet::loadMultiple($wids) as $wallet) {
            if ($wallet->getOwnerId() == $account->id()) {
              $result = AccessResult::allowed()->cachePerUser();
              break 2;
            }
          }
        }
      }
    }
    elseif ($transaction->state->value == CCWorkflowInterface::STATE_VALIDATED) {
      // grant access to the use who created the transaction
      $result = AccessResult::allowedif($transaction->getOwnerId() == $account->id());
    }
    elseif ($operation == 'transition') {
      $curr_state = $transaction->state->value;
      $dest_state = \Drupal::request()->attributes->get('dest_state'); // Shame we can't get this from context.
      $workflow = $transaction->workflow->entity;
      $pathways = $workflow->transitions[$curr_state] ?? [];
      if ($curr_state == 'V') {

      }
      elseif ($curr_state == CCWorkflowInterface::STATE_VALIDATED and $transaction->getOwnerId() == $account->id()) {
        $result = AccessResult::allowed();
      }
      elseif ($dest_state == 'X') {// todo this should be a constant
        $result = AccessResult::allowedIf($account->hasPermission('manage mcapi') and $this->mcapiConfig->get('delete_transactions'));
      }
      elseif (isset($pathways[$dest_state])) {
        if ($account->hasPermission('manage mcapi')) {
          // Only allowed because the transition is actually defined.
          $result = AccessResult::allowed()->cachePerUser();
        }
        else{
          $role = $pathways[$dest_state];
          if ($transaction->payee->getOwner()->id() == $account->id() and CCWorkflowInterface::PARTY_PAYEE == $role) {
            $result = AccessResult::allowed();
          }
          elseif ($transaction->payer->getOwner()->id() == $account->id() and CCWorkflowInterface::PARTY_PAYEE == $role) {
            $result = AccessResult::allowed();
          }
        }
      }
    }
    $others = $this->moduleHandler()->invokeAll('mc_transaction_access', [$transaction, $operation, $account]);
    $return = $this->processAccessHookResults(array_merge($others, [$result]));

    if ($operation == 'view') {
      $result = $this->setCache($return, $cid, 'view', 'und', $account);
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

}
