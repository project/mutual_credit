<?php

namespace Drupal\mcapi\Entity\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines an access controller option for the mc_workflow entity.
 */
class WorkflowAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritDoc}
   *
   * @note Prevent admin workflow from being deleted.
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($entity->id() == 'fallback' and ($operation == 'update' or $operation == 'delete')) {
      return $return_as_object ? AccessResult::forbidden('Admin workflow cannot be deleted') : FALSE;
    }
    else {
      return parent::access($entity, $operation, $account, $return_as_object);
    }
  }

}
