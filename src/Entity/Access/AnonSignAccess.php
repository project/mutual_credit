<?php

namespace Drupal\mcapi\Entity\Access;

use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\mcapi\Entity\Transaction;
use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Site\Settings;
use Drupal\Component\Utility\Crypt;

/**
 * Determines access to routes based on login status of current user.
 * @todo inject Messenger
 */
class AnonSignAccess implements AccessInterface {

  /**
   * @param int $mc_transaction
   *   Transaction ID (not upcast)
   * @param int $uid
   * @param string $hash
   * @return AccessResult
   */
   public function access(int $mc_transaction, int $uid, string $hash) {
    if ($hash == Self::generateHash($uid, $mc_transaction)) {
      $account = \Drupal::currentUser();
      // Logging in
      if ($account->isAuthenticated() and $account->id() != $uid) {
        user_logout();
      }
      if ($account->isAnonymous()) {
        if ($user = User::load($uid) and $user->isActive()) {
          user_login_finalize($user);
          \Drupal::messenger()->addStatus(t('You have been logged in.'));
        }
        else {
          \Drupal::messenger()->addStatus(t('Your account has been deleted or blocked.'));
          return AccessResult::forbidden('User is deleted or blocked.');
        }
      }
      // Checking the signature is needed i.e. access to the sign page.
      $dest_states = Transaction::load($mc_transaction)->workflow->destinations($account);
      if (in_array(CCWorkflowInterface::STATE_PENDING, $dest_states)) {
        return AccessResult::allowed();
      }
      \Drupal::messenger()->addWarning(t('The transaction does not need your signature.'));
      return AccessResult::forbidden('Transaction does not need signature of user '.$uid);
    }
    \Drupal::messenger()->addWarning(t('There is something wrong with the login link.'));
    return AccessResult::forbidden('Invalid link');
  }

 /**
  * Helper function to get a hash corresponding to a user and a transaction.
  *
  * @param int $uid
  *   A user ID.
  * @param int $xid
  *   A transaction ID.
  *
  * @note this hash has no expiry date.
  */
  static function generateHash($uid, $xid) {
    return Crypt::hmacBase64($uid . $xid, Settings::getHashSalt());
  }
}
