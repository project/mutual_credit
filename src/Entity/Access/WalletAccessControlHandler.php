<?php

namespace Drupal\mcapi\Entity\Access;

use Drupal\mcapi\Entity\WalletInterface;
use Drupal\mcapi\Functions;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines an access controller option for the mc_wallet entity.
 *
 */
class WalletAccessControlHandler extends EntityAccessControlHandler {

  private $done = FALSE;

  /**
   * @var RouteMatchInterface
   */
  private $routeMatch = FALSE;

  protected $viewLabelOperation = TRUE;

  /**
   * @todo inject $routematch
   */
  public function __construct(EntityTypeInterface $entity_type) {
    parent::__construct($entity_type);
    $this->routeMatch = \Drupal::routeMatch();

  }

  /**
   * {@inheritDoc}
   */
  public function createAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = [], $return_as_object = FALSE) {
    // In the unlikely event that a page might try to display two create wallet buttons, this might reduce confusion.
    if($this->done == TRUE) {
      return AccessResult::forbidden('Only one wallet create button per page');
    }
    $this->done = TRUE;
    if ($account->hasPermission('manage mcapi')) {
      $params = $this->routeMatch->getParameters()->all();
      $owner = reset($params);
      $result = AccessResult::Allowed()->addCacheableDependency($owner);
    }
    else {
      $result = AccessResult::forbidden("This user cannot create wallets")->addCacheableDependency($account);
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  protected function checkAccess(EntityInterface $entity, $op, AccountInterface $account) {
    $this->prepareUser($account);
    if ($account->hasPermission('manage mcapi')) {
      if ($op == 'delete') {
        // Don't cache
        return AccessResult::allowedIf($entity->isEmpty());
      }
      // Includes user 1.
      return AccessResult::allowed()->cachePerPermissions();
    }
    elseif ($op == 'view label') {
      return AccessResult::allowedIf($account->isAuthenticated())->cachePerUser();
    }
    elseif ($op == 'view') {
      if ($entity->getOwnerId() == $account->id()) {
        return AccessResult::allowed()->cachePerUser();
      }
      return AccessResult::allowedIfhasPermission($account, 'view all wallets')
        ->cachePerPermissions();
    }
    elseif ($this->controlsWallet($entity, $account)) {
      if ($op == 'delete') {
        // Don't cache
        return AccessResult::allowedIf($entity->isEmpty());
      }
      // update
      return AccessResult::allowed()->cachePerUser();
    }
    elseif ($op == 'update') {
      return AccessResult::forbidden()->cachePerUser();
    }
  }

  /**
   * Check if the given entity holds the given wallet.
   *
   * @param WalletInterface $wallet
   * @param AccountInterface $account
   *
   * @return boolean
   *   TRUE if the $account controls the $entity
   */
  private function controlsWallet(WalletInterface $wallet, AccountInterface $account) : bool {
    return $wallet->getOwnerId() == $account->id();
  }

}
