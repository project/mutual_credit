<?php

namespace Drupal\mcapi\Entity\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access to route user/{user}/wallets only if the user has more than one wallet.
 */
class UserWalletsAccess implements AccessInterface {

  /**
   * {@inheritDoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $user = $route_match->getParameters()->get('user');
    $wids = \Drupal\mcapi\Entity\Storage\WalletStorage::allWalletIdsOf($user);
    if (count($wids) < 2) {
      return AccessResult::forbidden();// there's no tag to cache this with
    }
    if ($account->hasPermission('manage mcapi') || $account->hasPermission('view all wallets')) {
      return AccessResult::allowed();
    }
    // Can view the wallets if you are the owner.
    if ($user->id() == $account->id()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
