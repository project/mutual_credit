<?php

namespace Drupal\mcapi\Entity\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\mcapi\Entity\Workflow;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Access\AccessResultForbidden;

/**
 * Route requirement service.
 *
 * Access checks that use the Workflow context
 * @deprecated
 */
class CreateTransaction implements AccessInterface  {

  /**
   * @param string $mc_workflow
   *
   * @return AccessResult
   *
   * @todo upcast the workflow
   */
   public function access($mc_workflow, AccountInterface $account) : AccessResult {
     $workflow = Workflow::load($mc_workflow);
     if ($account->hasPermission('manage mcapi')) {
       $result = AccessResult::allowed();
     }
     elseif ($workflow->requiresConfirmation() and !$account->hasPermission('skip transaction confirm')) {
       $result = new AccessResultForbidden("This workflow requires permission 'skip transaction confirm'.");
     }
     elseif ($workflow->startState() == CCWorkflowInterface::STATE_COMPLETED and !$account->hasPermission('skip counterparty confirm')) {
       $result = new AccessResultForbidden("This workflow requires permission 'skip counterparty confirm'.");
     }
     elseif ($workflow->isBill() and !$account->hasPermission('bill wallets')) {
       $result = new AccessResultForbidden("This workflow requires permission 'bill wallets'.");
     }
     elseif ($workflow->isCredit() and !$account->hasPermission('credit wallets')) {
       $result = new AccessResultForbidden("This workflow requires permission 'credit wallets'.");
     }
     else {
       $result = AccessResult::allowed();
     }
     return $result->addCacheableDependency($account);
  }
}
