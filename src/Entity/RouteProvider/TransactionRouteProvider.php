<?php

namespace Drupal\mcapi\Entity\RouteProvider;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides routes for the transaction entity.
 */
class TransactionRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   * Modify the default add_form route.
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    // All transaction forms must have a workflow. The default transaction form takes the builtin-workflow as a default
    $collection->get('entity.mc_transaction.add_form')
      ->setDefault('mc_workflow', 'fallback') // What happens if the user can't access the fallback workflow?
      ->setDefault('_title', 'Add transaction')
    // Upcast the mc_workflow
      ->addOptions(['parameters' => ['mc_workflow' => ['type' => 'entity:mc_workflow']]]);

    return $collection;
  }

}
