<?php

namespace Drupal\mcapi\Entity\ListBuilder;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Build a listing of transactions. This is only used if views is not installed.
 *
 * @ingroup entity_api
 * @todo inject currentuser
 */
class TransactionListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mc_transaction_collection';
  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    $header['id'] = '';
    $header['created'] = $this->t('Created');
    $header['payer'] = [
      'data' => $this->t('Payer'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['payee'] = [
      'data' => $this->t('Payee'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['description'] = $this->t('Description');
    $header['quant'] = $this->t('Quantity', [], ['context' => 'ledger entry']);
    $header['workflow'] = [
      'data' => $this->t('Workflow'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['state'] = [
      'data' => $this->t('State'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    return $header + parent::buildHeader();
  }
    /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {
    $actions = parent::buildRow($entity);
    if ($actions = parent::buildRow($entity)) {
      $row['id'] = '#'.$entity->xid->value;
      $row['created']['data'] = $entity->created->view(['label' => 'hidden']);
      $row['payer']['data'] = $entity->payer->toLink();
      $row['payee']['data'] = $entity->payee->toLink();
      $row['description']['data'] = $entity->description;
      $row['quant']['data'] = $entity->quant;
      $row['workflow']['data'] = $entity->workflow->entity->label();
      $row['state']['data'] = $entity->state->value;
      $row += $actions;
    }
    return $row;
  }

  /**
   * {@inheritDoc}
   *
   * Derive the operations from the current user and the transaction workflow state
   *
   * This looks at the workflow paths available and makes an 'operation' for each.
   * @todo why are the operations for a new transaction handled separately?
   */
  public function getDefaultOperations(EntityInterface $transaction) {
    return $transaction->workflow->links();
  }

}
