<?php

namespace Drupal\mcapi\Entity\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of wallet entities.
 *
 */
class WalletListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    // Enable language column and filter if multiple languages are added.
    $header = [
      'name' => [
        'data' => $this->t('Name')
      ],
      'balance' => $this->t('Balance'),
      'trades' => [
        'data' => $this->t('Trades'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'owner' => [
        'data' => $this->t('Owner'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'operations' => $this->t('Operations'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   * By default wallets are listed in order of ID. See parent::getEntityListQuery().
   */
  public function buildRow(EntityInterface $entity) {
    $row['id']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label() .' (#'.$entity->id().')',
      '#url' => $entity->toUrl(),
    ];

    $row['balance']['data'] = [
      '#markup' => $entity->balance,
    ];
    $row['trades']['data'] = [
      '#markup' => $entity->trades,
    ];
    $row['owner'] = $entity->getownerId() ? $entity->getOwner()->toLink() : 'orphan';
    $row['operations']['data'] = $this->buildOperations($entity);
    return $row + parent::buildRow($entity);
  }

}
