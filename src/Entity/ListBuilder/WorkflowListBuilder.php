<?php

namespace Drupal\mcapi\Entity\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Render\Markup;
use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Workflow config entities.
 *
 */
class WorkflowListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    // Enable language column and filter if multiple languages are added.
    $header = [
      'name' => [
        'data' => $this->t('Form name'),
      ],
      'summary' => [
        'data' => $this->t('Summary'),
        'class' => [RESPONSIVE_PRIORITY_LOW]
      ],
      'path' => $this->t('Path to form'),
      'tab' => [
        'data' => $this->t('Menu tab'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
        'title' => t('Tabs are only visible when there is more than one in the same path.')
      ],
      'workflow_id' => [
        'data' => $this->t('Workflow ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW]
      ]
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $workflow) {
    $row['title'] = t('Default form for the @name workflow, typically not for normal users.', ['@name' => $workflow->id]);
    // Workflows show grayed out with (auto) in contrast to transaction forms.
    // This opacity is a bit of a hack to avoid creating a new css library.
    $row['data']['name']['style'] = 'opacity:0.5';
    $row['data']['name']['data'] = [
      '#markup' => Markup::Create($workflow->label() .' ('.$this->t('Workflow default form') .')')
    ];
    $row['data']['summary'] = [
      'style' => 'opacity:0.5',
      'data' => ['#markup' => Markup::Create($workflow->summary)],
    ];
    $url = Url::fromRoute('entity.mc_transaction.add_form', ['mc_workflow' => $workflow->id]);

    $row['data']['path']['data'] = [
      '#markup' => Link::fromTextAndUrl($url->toString(), $url)->toString()
    ];
    $row['data']['tab'] = [
      'data' => ['#markup' => 'yes'],
    ];
    $row['data']['workflow_id']['data'] = $workflow->id;

    $row['data'] += parent::buildRow($workflow);
    return $row;
  }


  public function load() {
    $entity_ids = $this->getEntityIds();
    $entity_ids[] = 'fallback';
    return Workflow::loadMultiple($entity_ids);
  }

  // Alter the table rendering to take account of the mc_forms module.
  public function render() {
    $build = parent::render();
    $build['table']['#rows'] = \Drupal::moduleHandler()->invokeAll('mc_workflows_table') + $build['table']['#rows'];
    $build['table']['#attributes']['id'] = 'workflow-collection';
    $build['table']['#theme_wrappers'] = ['table__with_sections'];
    $build['table']['footer']['#rows'] = $build['table']['#rows'];
    foreach($build['table']['#rows'] as &$row) {
      $wfid = $row['data']['workflow_id']['data'];
      if ($wf = Workflow::load($wfid)) {
        $code =  $wf->code;
      }
      else {
        $code = $wfid;
        $wfid = 'custom';
      }
      $row['data']['workflow_id']['data'] = ['#markup' => Markup::Create("<span title = \"".t('Workflow code :@code', ['@code' => $code])."\">".$wfid.'</span>')];
      $row['data']['summary']['class'] = [RESPONSIVE_PRIORITY_LOW];
      $row['data']['workflow_id']['class'] = [RESPONSIVE_PRIORITY_LOW];
      $row['data']['tab']['class'] = [RESPONSIVE_PRIORITY_MEDIUM];
      $row['data']['tab']['data'] = $row['data']['tab']['data'] ? ['#markup' => '&#10004;'] : ['#markup' => '&#10006;'];
    }
    return $build;
  }

  function getDefaultOperations($entity) {
    $ops = parent::getDefaultOperations($entity);
    if ($entity instanceOf Workflow and $entity->id <> 'fallback') {
      if ($ops['edit']) {
        $ops['edit']['title'] = t('Edit workflow');
      }
      // Rather this should be done through entity access control.
      if (!$this->isNeededWorkflow($entity->id)) {
        if ($ops['delete']) {
          $ops['delete']['title'] = t('Delete workflow');
        }
      }
    }
    return $ops;
  }

  /**
   * Find out if some other config depends on this workflow.
   * @param string $workflow_id
   * @todo inject
   */
  private function isNeededWorkflow(string $workflow_id) : bool {
    $configFactory = \Drupal::configFactory();
     foreach (\Drupal::service('config.storage')->listAll() as $config_name) {
       $config_dependencies = $configFactory->get($config_name)->get('dependencies');
       if (!empty($config_dependencies['config']) && in_array($workflow_id, $config_dependencies['config'])) {
         return TRUE;
       }
    }
    return FALSE;
  }

}
