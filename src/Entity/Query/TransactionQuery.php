<?php

namespace Drupal\mcapi\Entity\Query;

use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\Core\Entity\Query\QueryInterface;
/**
 * The SQL storage entity query class.
 *
 * Additional methods to filter Transaction queries.
 */
class TransactionQuery extends Query {

  /**
   * Filter the query to include only transactions in which the given
   * wallet is payer or payee
   *
   * @param array|int $wallet_id
   * @return \Drupal\mcapi\Entity\TransactionQuery
   */
  public function involving(array|int $wallet_id) {
    $group = $this->orConditionGroup()
      ->condition('entries.payer_wid', (array)$wallet_id, 'IN')
      ->condition('entries.payee_wid', (array)$wallet_id, 'IN');
    $this->condition($group);
    return $this;
  }

  public function alter() : QueryInterface {
    if (!\Drupal::currentUser()->hasPermission('view all transactions')) {
      $query->involving($current_user->id());
    }
    return parent::alter();
  }


}

