<?php

namespace Drupal\mcapi\Entity\Query;

use Drupal\Core\Entity\Query\Sql\Query as BaseQuery;

/**
 * EntityQuery for Wallet, where access control means excluding wallets of blocked users.
 */
class WalletQuery extends BaseQuery {

  /**
   * {@inheritDoc}
   *
   * Disallow wallets whose owners are blocked.
   */
  public function prepare(){
    parent::prepare();
    if ($this->accessCheck) {
      $this->sqlQuery->join(
        'users_field_data',
        'u',
        'base_table.owner = u.uid AND u.status = 1'
      );
    }
    return $this;
  }

}
