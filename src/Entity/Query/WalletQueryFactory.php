<?php

namespace Drupal\mcapi\Entity\Query;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\Sql\QueryFactory as QueryFactoryBase;

/**
 * Query factory.
 */
class WalletQueryFactory extends QueryFactoryBase {

  /**
   * {@inheritDoc}
   */
  public function get(EntityTypeInterface $entity_type, $conjunction) {
    return new WalletQuery($entity_type, $conjunction, $this->connection, $this->namespaces);
  }

}
