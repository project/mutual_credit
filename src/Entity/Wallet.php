<?php

namespace Drupal\mcapi\Entity;

use Drupal\user\EntityOwnerTrait;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the wallet entity.
 *
 * The metaphor is not perfect because this wallet can hold a negaive balance.
 * A more accurate term, 'account', has another meaning in Drupal.
 *
 * This Entity knows all about the special intertrading wallets though none are
 * installed by default.
 *
 * @ContentEntityType(
 *   id = "mc_wallet",
 *   label = @Translation("Wallet"),
 *   module = "mcapi",
 *   handlers = {
 *     "view_builder" = "Drupal\mcapi\Entity\ViewBuilder\WalletViewBuilder",
 *     "storage" = "Drupal\mcapi\Entity\Storage\WalletStorage",
 *     "storage_schema" = "Drupal\mcapi\Entity\Storage\WalletStorageSchema",
 *     "list_builder" = "Drupal\mcapi\Entity\ListBuilder\WalletListBuilder",
 *     "access" = "Drupal\mcapi\Entity\Access\WalletAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\mcapi\Form\WalletForm",
 *       "create" = "Drupal\mcapi\Form\WalletAddForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "views_data" = "Drupal\mcapi\Entity\Views\WalletViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     }
 *   },
 *   admin_permission = "configure mcapi",
 *   base_table = "mc_wallet",
 *   entity_keys = {
 *     "label" = "name",
 *     "id" = "wid",
 *     "uuid" = "uuid",
 *     "owner" = "owner"
 *   },
 *   translatable = FALSE,
 *   links = {
 *     "canonical" = "/wallet/{mc_wallet}",
 *     "log" = "/wallet/{mc_wallet}/log",
 *     "inex" = "/wallet/{mc_wallet}/inex",
 *     "delete-form" = "/wallet/{mc_wallet}/delete",
 *     "edit-form" = "/wallet/{mc_wallet}/edit",
 *     "collection" = "/admin/accounting/wallets"
 *   },
 *   field_ui_base_route = "entity.mc_wallet.collection"
 * )
 */
class Wallet extends ContentEntityBase implements WalletInterface, EntityOwnerInterface {

  use MessengerTrait;
  use EntityOwnerTrait;

  const STATS = [
    'balance' => 0,
    'volume' => 0,
    'gross_in' => 0,
    'gross_out' => 0,
    'trades' => 0,
    'partners' => 0
  ];

  /**
   * {@inheritDoc}
   */
  public static function precreate($wallet, array &$values) {
    // NB owner is not set when generating a wallet for the field edit page.
    if (!isset($values['name']) and isset($values['owner'])) {
      // N.B. If this wallet is autocreated then the owner isn't saved yet
      $values['name'] = trim($values['owner']->label()) ?: $values['owner']->getAccountName();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return t('Wallet @num', ['@num' => $this->id()]);
    }
    else {
      return $this->name->value;
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type)
      + static::ownerBaseFieldDefinitions($entity_type);
    $fields['owner']->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setLabel(t('Wallet owner', [], ['context' => 'The user who owns the wallet']));
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name or purpose of wallet'))
      ->setDescription(t('Other users need to know or guess this to transact with you.', [], ['context' => 'wallet name help']))
      ->setSetting('max_length', 128)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setConstraints([
        'NotNull' => [],
        'WalletNameUnique' => []
      ]);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the wallet was created.'))
      ->setRevisionable(FALSE);
    return $fields;
  }

  /**
   * Magic method to get stats about the wallet.
   */
  public function &__get($name) {
    if (in_array($name, ['balance', 'volume', 'gross_in', 'gross_out', 'trades', 'partners'])) {
      // Returned by reference, these values can be edited but not saved.
      if (empty($this->values[$name])) {
        $stats = $this->entityTypeManager()
          ->getStorage('mc_wallet')
          ->getStats($this->id());
        $this->values = array_merge((array)$stats, $this->values);
      }
    }
    return parent::__get($name);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTagsToInvalidate() {
    $tags = parent::getCacheTagsToInvalidate();
    $tags[] = 'config:core.entity_view_display.mc_wallet';
    return $tags;
  }

  /**
   * {@inheritDoc}
   * Only virgin wallet can be deleted
   */
  public function isEmpty() : bool {
    $transactions = $this->entityTypeManager()
      ->getStorage('mc_transaction')->getQuery()->accessCheck(FALSE)
      ->involving($this->id())
      ->count()
      ->execute();
    return empty($transactions);
  }


  /**
   * {@inheritDoc}
   */
  public function history(int $since = 0) : array {
    $database = \Drupal::database();
    // This is a way to add up the results as we go along.
    $database->query("SET @csum := 0");
    // Have to calculate the whole history by adding up all the diffs
    // It is cheaper to do stuff in mysql.
    $query = $database->select('mc_transactions_index', 'i')->fields('i', ['created']);
    $query->addExpression('(@csum := @csum + [diff])', '[balance]');
    $all_balances = $query->condition('wallet_id', $this->id())
      ->orderby('xid', 'ASC')
      ->execute()
      // If two transactions happen on the same second, the latter running
      // balance will be shown only.
      ->fetchAllKeyed();
    // First point is zero at unixtime zero.
    $all_balances = [$this->created->value => 0] + $all_balances;
    // Extend the line to the present moment.
    $all_balances[\Drupal::time()->getRequestTime()] = end($all_balances);
    $pos = 0;
    // Having done the addition, we can chop the beginning off the array.
    if ($since) {
      // We know they keys are in chronological order.
      foreach (array_keys($all_balances) as $pos => $created) {
        if ($created > $since) {
          // Now the $pos is set.
          break;
        }
      }
      // Cut the beginning off
      // to the end.
      $all_balances = array_slice($all_balances, $pos);
    }
    return $all_balances;
  }

}
