<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Database\Query\Query;

/**
 * Interface for the transaction storage controller.
 */
interface TransactionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Truncate and rebuild the index table.
   */
  public function indexRebuild() : void;

  /**
   * Check the integrity of the index table.
   *
   * @return bool
   *   TRUE if the table is integral.
   */
  public function indexCheck() : bool;

  /**
   * Return the ids of all the wallets which have traded.
   *
   * @param array $conditions
   *   Conditions keyed by property name suitable for getmcapiIndexQuery().
   */
  public function wallets($conditions = []) : array ;

  /**
   * Get the balance of a given wallet, up to a given transaction id.
   *
   * @param int $wid
   *   A wallet ID.
   * @param string $field_name
   *   Define the transaction order using this field
   *
   * @return int
   *   Raw value.
   */
  public function runningBalance($wid, $field_name = 'created', $before = 0) :int ;

  /**
   * Get all the balances at the moment of the timestamp.
   *
   * Which means adding all transactions from the beginning until then
   * This provides a short of snapshot of the system.
   *
   * @param array $conditions
   *   Conditions keyed by property name suitable for getmcapiIndexQuery().
   *
   * @return array
   *   Various stats, keyed by wallet id.
   */
  public function ledgerStateByWallet(array $conditions) : array ;

  /**
   * Build a query on the transaction index table.
   *
   * @param array $conditions
   *   Conditions keyed by property name suitable for getmcapiIndexQuery().
   *
   * @return Drupal\Core\Database\Query\Query
   *   An unexecuted db query.
   */
  public function ledgerStateQuery(array $conditions) : Query;

  /**
   * Get a load of transaction stats per day, week, month or year.
   *
   * @param string $period
   *   One of 'Day', 'Week', 'Month', or 'Year'.
   *
   * @return array
   *   An array of dates, volumes trades, num wallets used in the periods
   *   preceding those dates.
   */
  public function historyPeriodic($period, $conditions) : array ;

}
