<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Storage interface for wallet entity.
 */
interface WalletStorageInterface extends ContentEntityStorageInterface {

  /**
   * Retrieve all the wallets held by a given ContentEntity.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return int[]
   */
  static function allWalletIdsOf(int $uid) : array;

  /**
   * Get some gerneral purpose stats by adding up the transactions for a wallet.
   *
   * Because this uses the index table, it only works with completed transactions
   * (state 'C'). All this can be accomplished with views but this is one handy method.
   *
   * @param int $wallet_id
   *   The wid of a wallet.
   * @param array $filters
   *   Conditions keyed by property name suitable for getmcapiIndexQuery().
   *
   * @return array
   *   keys are trades, gross_in, gross_out, balance, volume, partners.
   *
   * @see \Drupal\mcapi\Entity\WalletInterface::getAllStats()
   *
   * @note This could be cached but remember it is possible to generate all kinds of stats, between any dates
   */
  public function getStats($wallet_id) : \stdClass;

}
