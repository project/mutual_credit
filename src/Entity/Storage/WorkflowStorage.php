<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Entity\EntityInterface;

/**
 * Storage controller for workflow config entity.
 * Creates a virtual 'default' workflow that cannot be deleted or saved.
 */
class WorkflowStorage extends ConfigEntityStorage {

  /**
   * {@inheritdoc}
   */
  function loadMultiple($ids = NULL) {
    $all = parent::loadMultiple($ids);
    uasort($all, fn ($a, $b) => $a->label <=> $b->label);
    // This will always load something. Can break if you want to import a workflow.
    if (is_array($ids)) {
      // If we name the fallback as the item requested, then you can't import anything because it already exists.
      if (array_diff($ids, array_keys($all))) {
        $builtin = TRUE;
      }
    }
    elseif (is_null($ids) or isset($ids['fallback'])) {
        $builtin = TRUE;
    }
    if (isset($builtin)) {
      $all = ['fallback' => new Workflow(static::builtin())] + $all;
    }
    return $all;
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    if ($entity->id <> 'fallback' and $entity->id <> 'custom') {
      return parent::save($entity);
    }
    return SAVED_UPDATED;
  }

  /**
   * Default workflow cannot be deleted.
   */
  private static function builtin() {
    // 3rdparty transactions moving directly to completed state/
    return [
      'id' => 'fallback',
      'label' => t('Fallback'),
      'summary' => t('Only admins can create transactions with this builtin workflow.'),
      'code' => '_P0PC='
    ];
  }

}

