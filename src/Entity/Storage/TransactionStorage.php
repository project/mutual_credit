<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\mcapi\Entity\Storage\TransactionStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\mcapi\Entity\Transaction;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Database\Query\Query;
use Drupal\Core\Database\Query\Select;
use Drupal\Core\Cache\Cache;
use CreditCommons\Workflow as CCWorkflowInterface;

/**
 * Storage controller for Transaction entity.
 */
class TransactionStorage extends SqlContentEntityStorage implements TransactionStorageInterface {

  /**
   * {@inheritDoc}
   */
  public function getQueryServiceName() {
    return 'mc_transaction.query.sql';
  }

  /**
   * {@inheritDoc}
   * In case the payee was populated with wid instead of payee_wid
   */
  protected function initFieldValues(ContentEntityInterface $entity, array $values = [], array $field_names = []) {
    if (isset($values['payee']) and empty($values['entries']['payee_wid'])) {
      $values['entries']['payee_wid'] = $values['payee'];
      unset($values['payee']);
    }
    if (isset($values['payer']) and empty($values['entries']['payer_wid'])) {
      $values['entries']['payer_wid'] = $values['payer'];
      unset($values['payee']);
    }
    parent::initFieldValues($entity, $values, $field_names);
  }

  /**
   * {@inheritDoc}
   *
   * Save 2 rows per entry into the index tables.
   */
  public function doSave($id, EntityInterface $transaction) {
    $return = parent::doSave($id, $transaction);
    // To be clean delete any existing rows.
    $this->database->delete('mc_transactions_index')
      ->condition('xid', $transaction->id())
      ->execute();
    if ($transaction->state->value == CCWorkflowInterface::STATE_COMPLETED) {
      $record = $this->mapToStorageRecord($transaction);
      $common = [
        'xid' => $record->xid,
        'state' => $record->state,
        'created' => $record->created// $transaction->get('created')->value
      ];
      foreach ($transaction->entries->getValue() as $delta => $entry) {
        $this->database->insert('mc_transactions_index')
          ->fields($common + [
            'delta' => $delta,
            'wallet_id' => $entry['payer_wid'],
            'partner_id' => $entry['payee_wid'],
            'incoming' => 0,
            'outgoing' => $entry['quant'],
            'diff' => -$entry['quant'],
            'volume' => $entry['quant'],
            'primary' => (int)$entry['primary']
          ])
          ->execute();

        $this->database->insert('mc_transactions_index')
          ->fields($common + [
            'xid' => $record->xid,
            'delta' => $delta,
            'wallet_id' => $entry['payee_wid'],
            'partner_id' => $entry['payer_wid'],
            'incoming' => $entry['quant'],
            'outgoing' => 0,
            'diff' => $entry['quant'],
            'volume' => $entry['quant'],
            'primary' => (int)$entry['primary']
          ])
          ->execute();
      }
      // Some transitions don't need to clear the wallet cache,
      $this->resetCache([$transaction->id()]);
    }
    return $return;
  }

  /**
   * {@inheritDoc}
   */
  protected function doDelete($entities) {
    parent::doDelete($entities);
    $xids = array_keys($entities);
    $this->database
      ->delete('mc_transactions_index')
      ->condition('xid', $xids, 'IN')
      ->execute();
      $this->resetCache($xids);
  }

  /**
   * {@inheritDoc}
   * reset the cache of all the wallets involved as well.
   */
  public function resetCache(array $xids = NULL) {
    parent::resetCache($xids);
    // Transactions affect wallets
    $tags = [];
    if ($xids) {
      $wids = $cids = [];
      foreach(Transaction::loadMultiple($xids) as $transaction) {
        $wids = array_merge($wids, $transaction->allWalletIds());
      }
      $wids = array_unique($wids);
      $this->recalcTransactionTotals($wids);

      $wallet_storage = $this->entityTypeManager->getStorage('mc_wallet');

      foreach (array_chunk($wids, 50) as $wids_chunk) {
        foreach ($wallet_storage->loadMultiple($wids_chunk) as $wallet) {
          $tags = array_merge($tags, $wallet->getCacheTagsToInvalidate());
          $cids[] = $wallet_storage->buildCacheId($wallet->id());
        }
      }
      $this->cacheBackend->deleteMultiple($cids);
    }
    Cache::invalidateTags($tags);
  }

  /**
   * {@inheritDoc}
   */
  public function indexRebuild() : void {
    $this->database->truncate('mc_transactions_index')->execute();
    // don't know how to do this with database API.
    $this->database->query("
      INSERT INTO {mc_transactions_index} (
        SELECT
          t.[xid],
          e.[delta],
          e.[entries_payer_wid] AS wallet_id,
          e.[entries_payee_wid] AS partner_id,
          t.[state],
          t.[created],
          0 AS incoming,
          e.[entries_quant] AS outgoing,
          - e.[entries_quant] AS diff,
          e.[entries_quant] AS volume,
          e.[entries_primary] AS `primary`
        FROM {mc_transaction} t
        RIGHT JOIN {mc_transaction__entries} e ON t.[xid] = e.[entity_id]
        WHERE [state] IN ('C')
      )"
    );
    $this->database->query(
      "INSERT INTO {mc_transactions_index} (
        SELECT
          t.[xid],
          e.[delta],
          e.[entries_payee_wid] AS wallet_id,
          e.[entries_payer_wid] AS partner_id,
          t.[state],
          t.[created],
          e.[entries_quant] AS incoming,
          0 AS outgoing,
          e.[entries_quant] AS diff,
          e.[entries_quant] AS volume,
          e.[entries_primary] AS `primary`
        FROM {mc_transaction} t
        RIGHT JOIN {mc_transaction__entries} e ON t.[xid] = e.[entity_id]
        WHERE [state] IN ('C')
      ) "
    );

    $this->database->truncate('mc_transaction_totals')->execute();
    $result = $this->database->select('mc_transactions_index', 'i')
      ->fields('i', ['wallet_id'])
      ->distinct()
      ->execute();
    $wids = [];
    foreach ($result->fetchAll() as $bal) {
      $wids[] = $bal->wallet_id;
    }
    $this->recalcTransactionTotals($wids);
  }

  /**
   * {@inheritDoc}
   */
  public function indexCheck() : bool {
    $q = $this->database->select('mc_transactions_index');
    $q->addExpression('SUM(diff)');
    // If the sum of everything is zero.
    if ($q->execute()->fetchfield() + 0 != 0) {
      \Drupal::messenger()->addError('Transaction index table rows do not add up to zero');
      return FALSE;
    }
    $q = $this->database->select('mc_transactions_index', 'i');
    $q->addExpression('SUM(incoming)');
    //$q->condition('i.state', [CCWorkflowInterface::STATE_COMPLETED, CCWorkflowInterface::STATE_PENDING], 'IN');
    $volume_index = $q->execute()->fetchField();
    // Only index completed transactions.
    $q = $this->database->select('mc_transaction', 't');
    $q->join('mc_transaction__entries', 'e', 't.xid = e.entity_id');
    $q->addExpression('SUM(e.entries_quant)');
    $q->condition('t.state', CCWorkflowInterface::STATE_COMPLETED);
    $volume = $q->execute()->fetchField();
    if ($volume_index == $volume) {
      return TRUE;
    }
    \Drupal::messenger()->addError('Transaction volumes in main table and index table do not correspond');
    return FALSE;
  }

  /**
   * Add an array of conditions to the select query.
   *
   * This is more flexible than entityQuery which runs on the transaction table
   * itself. This has no access control.
   *
   * @param array $conditions
   *   Conditions keyed by property name suitable for getmcapiIndexQuery():
   *   xid, partner_id, wallet_id, workflow, state, involving, since, until, value,
   *   min, max
   *
   * @note This query may return double the expected results, unless filtered appropriately
   */
  private function getMcapiIndexQuery(array $conditions = []) : Select {
    $query = $this->database->select('mc_transactions_index', 'i');
    foreach ($conditions as $field => $value) {
      if ($value === NULL) {
        //\Drupal::service('logger.channel.mcapi')->debug("getmcapiIndexquery $field needs debugging when value = NULL");
        continue;
      }
      switch ($field) {
        // Case 'payer': these can't work
        // case 'payee':
        case 'xid':
        case 'wallet_id':
        case 'partner_id':
          $query->condition($field, $value);
          break;
        case 'involving':
          $value = (array) $value;
          $cond_group = count($value) == 1 ? $query->orConditionGroup() : $query->andConditionGroup();
          $query->condition($cond_group
            ->condition('wallet_id', $value)
            ->condition('partner_id', $value)
          );
          break;

        case 'since':
          $query->condition('created', $value, '>');
          break;

        case 'until':
          $query->condition('created', $value, '<');
          break;

        // Synonyms as far as the index table is concerned.
        case 'value':
          $query->condition('volume', $value);
          break;

        // Synonyms as far as the index table is concerned.
        case 'min':
          $query->condition('volume', $value, '>=');
          break;

        // Synonyms as far as the index table is concerned.
        case 'max':
          $query->condition('volume', $value, '<=');
          break;

        default:
          \Drupal::service('logger.channel.mcapi')->error('Filtering on unknown field: @fieldname', ['@fideldname' => $field]);
      }
    }
    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function wallets($conditions = []) : array {
    return $this->getmcapiIndexQuery($conditions)
      ->fields('i', ['wallet_id'])
      ->distinct()
      ->execute()
      ->fetchCol();
  }

  /**
   * {@inheritDoc}
   *
   * @note The running balance views field isn't used in any default views.
   */
  public function runningBalance($wid, $field_name = 'xid', $before = 0) : int {
    // The running balance depends the order of the transactions. we will assume
    // the order of creation is what's wanted because that corresponds to the
    // order of the xid. NB it is possible to change the apparent creation date.
    $query = $this->database->select('mc_transactions_index');
    $query->addExpression('SUM([diff])');
    $query->condition('wallet_id', $wid);
    if ($before) {
      $query->condition($field_name, $before, '<=');
    }
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritDoc}
   *
   * @note If there are no transactions balance, volume, income, expenditure will be NULL
   */
  public function ledgerStateQuery(array $conditions) : Query {
    $q = $this->getmcapiIndexQuery($conditions);
    $q->addExpression('SUM([diff])', 'balance');
    $q->addExpression('SUM([volume])/2', 'volume');
    $q->addExpression('SUM([incoming])', 'income');
    $q->addExpression('SUM([outgoing])', 'expenditure');
    $q->addExpression('COUNT([partner_id])', 'trades');
    $q->addExpression('COUNT (DISTINCT [partner_id])', 'partners');
    return $q;
  }

  /**
   * {@inheritDoc}
   */
  public function ledgerStateByWallet(array $conditions) : array {
    $q = $this->ledgerStateQuery($conditions);
    $q->addExpression('wallet_id', 'wid');
    $q->groupby('wallet_id');
    return $q->execute()->fetchAll();
  }

  /**
   * {@inheritDoc}
   */
  public function historyPeriodic($period, $conditions) : array {
    $q = $this->getmcapiIndexQuery($conditions);
    $q->addExpression('COUNT(DISTINCT [xid])', 'trades');
    $q->addExpression('SUM([incoming])', 'volumes');
    $q->addExpression('COUNT(DISTINCT [wallet_id])', 'wallets');
    // Unfortunately all these numbers are retrieved as strings.
    $q->condition('incoming', 0, '>=');
    switch ($period) {
      case 'day':
        $q->addExpression('DATE(FROM_UNIXTIME([created]))', 'date');
        $q->groupBy('date');
        break;

      case 'week':
        $q->addExpression('WEEK(FROM_UNIXTIME([created]))', 'weeknum');
        $q->addExpression('MIN(YEAR(FROM_UNIXTIME([created])))', 'year');
        $q->groupBy('weeknum');
        break;

      case 'month':
        $q->addExpression('MONTH(FROM_UNIXTIME([created]))', 'month');
        $q->addExpression('MIN(YEAR(FROM_UNIXTIME([created])))', 'year');
        $q->groupBy('month');
        break;

      case 'year':
        $q->addExpression('YEAR(FROM_UNIXTIME([created]))', 'year');
        $q->groupBy('year');
        break;
    }
    $dates = $trades = $volumes = $wallets = [];
    $thisyear = date('Y');
    foreach ($q->execute()->fetchAll() as $row) {
      switch ($period) {
        case 'day':
          $dates[] = strtotime($row->date);
          break;
        case 'week':
          // The last second of the week.
          $dates[] = strtotime($row->year . 'W' . str_pad($row->weeknum + 1, 2, 0, STR_PAD_LEFT)) - 1;
          break;
        case 'month':
          // The last second of the month using the format YYYY-MM
          $dates[] = strtotime($row->year . '-' . ($row->month + 1)) - 1;
          break;
        case 'year':
          if ($row->year == $thisyear) {
            $dates[] = \Drupal::service('datetime.time')->getCurrentTime();
          }
          else {
            $dates[] = strtotime('01-01-'.$row->year)-1;
          }
      }
      $trades[] = intval($row->trades);
      $volumes[] = intval($row->volumes);
      $wallets[] = intval($row->wallets);
    }
    return [$dates, $trades, $volumes, $wallets];
  }

  /**
   * Update the transaction totals table.
   * Called by doPostSave -> resetcache();
   *
   * @param array $wids
   */
  private function recalcTransactionTotals(array $wids) {
    // why is this called twice on confirming a transaction?
    foreach ($wids as $wid) {
      $q1 = $this->getmcapiIndexQuery(['wallet_id' => $wid]);
      $q1->addExpression('COUNT(DISTINCT i.[xid])', 'trades');
      $q1->addExpression('COALESCE(SUM(i.[incoming]), 0)', 'gross_in');
      $q1->addExpression('COALESCE(SUM(i.[outgoing]), 0)', 'gross_out');
      $q1->addExpression('COALESCE(SUM(i.[diff]), 0)', 'balance');
      $q1->addExpression('COALESCE(SUM(i.[volume]), 0)', 'volume');
      $q1->addExpression('COUNT(DISTINCT i.[partner_id])', 'partners');
      $fields = $q1->execute()->fetch(\PDO::FETCH_ASSOC);
      $this->database->merge('mc_transaction_totals')
        ->keys(['wid' => $wid])
        ->fields($fields)
        ->execute();
    }
  }

}
