<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\EntityInterface;

/**
 * Storage controller for wallet entity.
 */
class WalletStorage extends SqlContentEntityStorage implements WalletStorageInterface {

  /**
   * {@inheritDoc}
   */
  public function getQueryServiceName() {
    return 'mc_wallet.query.sql';
  }

  /**
   * {@inheritDoc}
   */
  public static function allWalletIdsOf(int $uid) : array {
    $wids = \Drupal::entityQuery('mc_wallet')
      ->accessCheck(FALSE) // not to trigger hook_query_mc_wallet_access_alter()
      ->condition('owner', $uid)
      ->execute();
    return $wids;
  }

  /**
   * {@inheritDoc}
   */
  public function getStats($wallet_id) : \stdClass {
    $fields = ['balance', 'volume', 'gross_in', 'gross_out', 'trades', 'partners'];
    $query = $this->database->select('mc_transaction_totals', 'tt')
      ->fields('tt', $fields)
      ->condition('wid', $wallet_id);
    $result = $query->execute();
    if ($res = $result->fetch()) {
      // Unfortunately the results seem to be strings even though they are stored as ints.
    }
    else {
      $res = new \stdClass;
    }
    // Convert from strings.
    foreach ($fields as $f) {
      $res->{$f} = isset($res->{$f}) ? (int)$res->{$f} : 0;
    }
    return $res;
  }


  /**
   * {@inheritDoc}
   */
  public function doPreSave(EntityInterface $entity) {
    // Check the name length.
    if (strlen($entity->name->value) > 128) {
      $entity->name->value = substr($entity->name->value, 0, 128);
      $this->messenger()->addWarning(
        t('Wallet name was truncated to 128 characters: @name', ['@name' => $this->name->value]),
        'warning'
      );
    }
    parent::doPreSave($entity);
  }

}

