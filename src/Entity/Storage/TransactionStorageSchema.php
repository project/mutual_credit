<?php

namespace Drupal\mcapi\Entity\Storage;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the extra tables.
 */
class TransactionStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritDoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $tables = parent::getEntitySchema($entity_type, $reset = FALSE);
    $tables['mc_transaction']['foreign keys'] = [
      'payer' => [
        'table' => 'mc_wallet',
        'columns' => ['wid' => 'wid'],
      ],
      'payee' => [
        'table' => 'mc_wallet',
        'columns' => ['wid' => 'wid'],
      ],
    ];
    // Regardless of whether the TransactionStorage in on-site, this index is
    // kept in Drupal. This allows for some views integration as long as the
    // storage controller respects the index.
    $tables['mc_transactions_index'] = [
      'description' => 'A more queryable way of storing transactions',
      'fields' => [
        'xid' => [
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'The transaction ID'
        ],
        'delta' => [
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'The delta of the entry'
        ],
        'wallet_id' => [
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'description' => 'The main wallet ID'
        ],
        'partner_id' => [
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'description' => 'The partner wallet ID'
        ],
        'state' => [
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 15,
          'description' => 'The transaction state'
        ],
        'created' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'The Unix timestamp when the transaction was created'
        ],
        'incoming' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Amount by which the wallet balance increased'
        ],
        'outgoing' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Amount by which the wallet balance decreased'
        ],
        'diff' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Change in the wallet balance'
        ],
        'volume' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Amount of the transaction'
        ],
        'primary' => [
          'type' => 'int',
          'not null' => TRUE,
          'size' => 'tiny',
          'description' => 'TRUE if the transaction is primary'
        ]
      ],
      'primary key' => ['xid', 'delta', 'wallet_id'],
      'indexes' => [
        'wallet_id' => ['wallet_id'],
        'partner_id' => ['partner_id'],
      ],
    ];
    return $tables;
  }

}
