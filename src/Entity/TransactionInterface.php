<?php

namespace Drupal\mcapi\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi\Plugin\Field\FieldType\Entry;

/**
 * Interface for Transaction entity.
 */
interface TransactionInterface extends ContentEntityInterface, RevisionableInterface {

  /**
   *
   * @param string $dest_state
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\mcapi\WorkflowException
   */
  function saveVersion($dest_state);

  /**
   * @param array $params
   * @throws \Exception
   */
  function addEntry(array $params) : ?Entry;

  /**
   * Get all the IDs of all wallets involved in all the transaction entries.
   * @return int[]
   */
  function allWalletIds() : array;

  /**
   * load all the revisions of this transaction.
   * @return array
   */
  function revisions() : array;

  /**
   * Load the workflow as a configEntity, even if it means creating one on the fly.
   * @return Workflow
   */
  function workflow() : Workflow;

  /**
   * Determine if a wallet is involved in this transaction.
   *
   * @param int $wid
   *   A wallet id
   * @return bool
   */
  function involves(int $wid) : bool;

  /**
   * Render the transaction as a sentence with or without links.
   * Sentence is configured on the main settings page.
   * @param bool $links
   * @return \Drupal\Core\Render\Markup
   */
  function sentence() : string;

}
