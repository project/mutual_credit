<?php

namespace Drupal\mcapi\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Attribute\EntityReferenceSelection;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide default Wallet selection handler, filtering on the wallet name only.
 * @todo inject database
 */
#[EntityReferenceSelection(
  id: 'default:mc_wallet',
  label: new TranslatableMarkup('Wallet selection'),
  entity_types: ['mc_wallet'],
  group: 'default',
  weight: 0
)]
class WalletSelection extends DefaultSelection {

  /**
   *
   * @var Connection
   */
  protected $database;

  public function __construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $module_handler, $current_user, $entity_field_manager, $entity_repository, Connection $database, $entity_type_bundle_info = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $module_handler, $current_user, $entity_field_manager, $entity_type_bundle_info, $entity_repository);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('entity_field.manager'),
      $container->get('entity.repository'),
      $container->get('database'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritDoc}
   *
   * @note This override avoids having to load a large number of wallets into memory just to get the titles.
   *
   * @note this runs BEFORE hook_form_alter
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $entities = [];
    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }
    if ($wids = $query->execute()) {
      // Just get the wallet names.
      $wallets = $this->database
        ->select('mc_wallet', 'w')
        ->fields('w', ['wid', 'name'])
        ->condition('wid', $wids, 'IN')
        ->execute()
        ->fetchAllKeyed(0);
      $entities['mc_wallet'] = array_map(['Drupal\Component\Utility\Html', 'escape'], $wallets);
    }
    return $entities;
  }

  /**
   * {@inheritDoc}
   */
  public function validateReferenceableEntities(array $ids) {
    // User 1 skips validation. this is helpful for importing.
    return array_intersect(
      $ids,
      $this->buildEntityQuery()->accessCheck($this->currentUser->id() > 1)->execute()
    );
  }

}
