<?php

namespace Drupal\mcapi\Plugin\Field;

use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use CreditCommons\Workflow as CCWorkflowInterface;

/**
 * A signature field list
 * @todo inject the user.settings config
 */
class WorkflowFieldItemList extends FieldItemList {

  function __get($name) {
    if ($name == 'entity') {
      return Workflow::loadByCode($this->value);
    }
    return parent::__get($name);
  }

  /**
   * Get the destination states to which the current user can transition.
   * @return array
   *   The states. See \CreditCommons\Workflow
   */
  function destinations(AccountInterface|UserInterface $user = NULL) {
    $transaction = $this->getEntity();
    if (!$user) {
      $user = \Drupal::currentUser();
    }
    $curr_state = $transaction->state->value;
    $workflow = $this->entity;
    $destination_states = [];
    if ($transaction->isNew()) {
      $destination_states = [$workflow->startState()];
    }
    elseif (isset($workflow->transitions[$curr_state])) {
      $state_info = $workflow->transitions[$curr_state];
      $role = '';
      // derive the other links from the workflow.
      if ($payee = $transaction->payee and $payee->getOwnerId() == $user->id()) {
        $role = CCWorkflowInterface::PARTY_PAYEE;
      }
      elseif ($payer = $transaction->payer and $payer->getOwnerId() == $user->id()) {
        $role = CCWorkflowInterface::PARTY_PAYER;
      }
      // This must correspond with the route's permission. See TransactionAccessControlHandler
      foreach ($state_info as $dest_state => $actor) {
        // Permission check according to the workflow directly.
        if ($role == $actor or $user->hasPermission('manage mcapi')) {
          $destination_states[] = $dest_state;
        }
      }
    }
    return $destination_states;
  }

  /**
   *
   * @param Transaction $transaction
   * @return array
   *   titles and urls of permitteed action links.
   */
  function links() : array {
    $links = [];
    $transition_names = \Drupal::config('mcapi.settings')->get('transitions');
    $transaction = $this->getEntity();
    foreach ($this->destinations() as $dest_state) {
      $r = ($dest_state == 'X') ? '<front>' : '<current>';
      $url = Url::fromRoute(
        'entity.mc_transaction.transition',
        ['mc_transaction' => $transaction->id(), 'dest_state' => $dest_state],
        ['query' => ['destination' => Url::fromRoute($r)->toString()]]
      );
      $links[$dest_state] = [
        'title' => $transition_names[$transaction->state->value . $dest_state]['label'],
        'url' => $url
      ];
    }
    return $links;
  }

  function linksAsRenderable() {
    $renderable = [];
    if ($links = $this->links()) {
      $renderable = [
        '#theme' => 'links',
        '#links' => $this->links(),
        '#attributes' => ['class' => ['mc-transaction-links']],
        '#cache' => [
          // Cache per user per transaction
          'tags' => ['user', 'mc_transaction'],
          // Refresh cache when those entities change.
          'max-age' => 0
        ]
      ];
    }
    return $renderable;
  }

  /**
   *
   * @param string $role
   *   Either + -
   */
  function getParticipantName($role) : string {
    $user = NULL;
    if($role == '=') {
      $name = t('Either party', [], ['context' => 'the payer or the payee']);
    }
    elseif ($role == '+') {
      if ($wallet = $this->getEntity()->payee) {
        $name = $wallet->getOwner()->getDisplayName();
      }
    }
    elseif($role = '-') {
      if ($wallet = $this->getEntity()->payer) {
        $name = $wallet->getOwner()->getDisplayName();
      }
    }
    elseif ($role = '0') {// any user with manage mcapi permission.
      $name = t('admin');
    }
    return $name ?? \Drupal::config('user.settings')->get('anonymous');
  }

}
