<?php

namespace Drupal\mcapi\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Plugin implementation of the 'worth' field type. Note this is not used in the
 * Transaction entity, where quant is a property of the entry field type
 */
#[FieldType(
  id: "mc_worth",
  label: new TranslatableMarkup('Worth'),
  description: new TranslatableMarkup('A quantity of community currency.'),
  category: new TranslatableMarkup('Community Accounting'),
  no_ui: TRUE,
  default_formatter: 'mc_worth',
  default_widget: 'mc_worth_nullable',
)]
class McWorth extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function propertydefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['quant'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Worth'))
      ->setDescription(new TranslatableMarkup('A quantity of community currency.'))
      ->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'base units',
          'type' => 'int',
          'size' => 'normal',
          'not null' => FALSE,
        ],
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    return empty($this->value);
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    if (rand(0, 9) > 7) {
      return rand(1000, 5000);
    }
    else {
      return NULL;
    }
  }

}
