<?php

namespace Drupal\mcapi\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\mcapi\Entity\Workflow as WorkflowEntity;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mcapi\Plugin\Field\WorkflowFieldItemList;

/**
 * Defines the transaction 'workflow' entity field type.
 */
#[FieldType(
  id: "mc_workflow",
  label: new TranslatableMarkup("Transaction workflow"),
  description: new TranslatableMarkup("An entity field containing a workflow code which can be upcast to a workflow config entity, similar to entity_reference"),
  category: new TranslatableMarkup('Community Accounting'),
  list_class: WorkflowFieldItemList::class,
  no_ui: TRUE,
  default_formatter: "mc_workflow"
)]
class Workflow extends StringItem {

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $transitions = WorkflowEntity::allTransitions();
    shuffle($transitions);
    $firstparty = ['0', '+', '-'];
    $confirm = ['|', '_'];
    $first_transitions = array_filter(WorkflowEntity::getTr, function ($t) {return ($t[0] = 'P' or $t[0] = 'C') and $t[1] <> 'X';});
    $transition_parties = ['0', '+', '-', '='];

    $value = $firstparty[array_rand($firstparty)];
    $value .= $confirm[array_rand($confirm)];
    $value .= $first_transitions[array_rand($first_transitions)];
    $value .= $transition_parties[array_rand($transition_parties)];
    // Now any number of other transitions, they don't have to make a proper pathway.
    for ($i=0; $i<rand(5); $i++) {
      $value .= array_pop($transitions);
      $value = $transition_parties[array_rand($transition_parties)];
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }
}
