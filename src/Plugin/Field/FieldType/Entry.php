<?php

namespace Drupal\mcapi\Plugin\Field\FieldType;

use Drupal\mcapi\Functions;
use Drupal\mcapi\Plugin\Field\EntryFieldItemList;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;

/**
 * Plugin implementation of the 'worth' field type.
 * @todo shouldn't this be using the mc_worth dataType?
 */
#[FieldType(
  id: "entry",
  label: new TranslatableMarkup("Entry"),
  description: new TranslatableMarkup("A transfer of accounting units between wallets."),
  category: new TranslatableMarkup('Community Accounting'),
  default_formatter: "mc_main_entry",
  default_widget: "mc_entry",
  list_class: EntryFieldItemList::class
)]
class Entry extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function propertydefinitions(FieldStorageDefinitionInterface $field_definition) {
    $wallet_definition = EntityDataDefinition::create('mc_wallet');
    $properties['payee_wid'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Payee wallet ID'))
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE);
    $properties['payer_wid'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Payer wallet ID'))
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE);
    $properties['payee'] = DataReferenceDefinition::create('entity')
      ->setLabel(new TranslatableMarkup('Payee wallet'))
      ->setComputed(TRUE)
      ->setTargetDefinition($wallet_definition)
      ->setClass('\Drupal\mcapi\Plugin\Field\ComputedWallet')
      ->setReadOnly(TRUE);
    $properties['payer'] = DataReferenceDefinition::create('entity')
      ->setLabel(new TranslatableMarkup('Payer wallet'))
      ->setComputed(TRUE)
      ->setTargetDefinition($wallet_definition)
      ->setClass('\Drupal\mcapi\Plugin\Field\ComputedWallet')
      ->setReadOnly(TRUE);
    $properties['quant'] = DataDefinition::create('float')
      ->setLabel(new TranslatableMarkup('A quantity of units'))
      ->setRequired(TRUE);
    $properties['quant_t'] = DataDefinition::create('float')
      ->setLabel(new TranslatableMarkup('Units converted to trunkward units.'))
      ->setRequired(FALSE);
    $properties['description'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Description'));
    $properties['primary'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Boolean value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'payee_wid' => [
          'description' => 'wallet reference',
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'payer_wid' => [
          'description' => 'wallet reference',
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'quant' => [
          'description' => 'quantity of units',
          'type' => 'float',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'quant_t' => [
          'description' => 'trunkward units',
          'type' => 'float',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'description' => [
          'type' => 'varchar',
          'length' => 256,
          'binary' => FALSE,
        ],
        'primary' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
        'metadata' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ]
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    // todo use dot syntax?
    return $this->quant == 0 and empty(\Drupal::config('mcapi.settings')->get('currency')['zero_snippet']);
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $wids = \Drupal::entityQuery('mc_wallet')->accessCheck(TRUE)->execute();
    shuffle($wids);
    return [
      'quant' => 10,
      'payee_wid' => array_pop($wids),
      'payer_wid' => array_pop($wids), //@todo pick random wallets
      'description' => 'example transaction entry',
      'primary' => TRUE
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function mainPropertyName() {
    return 'quant';
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    // There is either a property object or a plain value - possibly for a
    // not-defined property. If we have a plain value, directly return it.
    if (isset($this->properties[$name])) {
      // Payer and payee wallets.
      return $this->properties[$name]->getValue();
    }
    elseif (isset($this->values[$name])) {
      // Actual properties e.g. payer_wid, quant.
      return $this->values[$name];
    }
  }

  /**
   * Render the entry as one line, using metadata to replace wallet names if available.
   * @return string
   * @see Drupal\mcapi\Plugin\Field\FieldFormatter\MainEntry
   */
  public function asOneLine() : string {
    $template = \Drupal::config('mcapi.settings')->get('sentence_template');
    // Allow metadata to store alt-wallet names.
    $quant = Functions::formatCurrency($this->quant);
    // Bizarrely, My browser renders <strong? or <span> tags as a css block, so strip_tags().
    $str = strtr(
      $template,
      [
        '[quant]' => strip_tags($quant),
        '[payee]' => $this->metadata[$this->payee_wid] ?? $this->payee->label(),
        '[payer]' => $this->metadata[$this->payer_wid] ?? $this->payer->label(),
        '[description]' => $this->description
      ]
    );
    return $str;
  }

}
