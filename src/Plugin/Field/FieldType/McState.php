<?php

namespace Drupal\mcapi\Plugin\Field\FieldType;

use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'worth' field type.
 */
#[FieldType(
  id: 'mc_state',
  label: new TranslatableMarkup("Transaction state"),
  description: new TranslatableMarkup("Enumerated field"),
  category: new TranslatableMarkup('Community Accounting'),
  no_ui: TRUE,
  default_formatter: "mc_state_stamp"
)]
class McState extends FieldItemBase {

  const STATES = [
    CCWorkflowInterface::STATE_INITIATED,
    CCWorkflowInterface::STATE_VALIDATED,
    CCWorkflowInterface::STATE_PENDING,
    CCWorkflowInterface::STATE_COMPLETED,
    CCWorkflowInterface::STATE_ERASED,
    CCWorkflowInterface::STATE_DELETE
  ];
  // english used for html class.
  const NAMES = [
    'init',
    'validated',
    'pending',
    'completed',
    'erased',
    'delete' // just to allow array_combine
  ];

  /**
   * Get a link of states. By default it returns only states saved to the db.
   *
   * @param bool $storable
   *   TRUE to return ONLY the states that can be written to the db.
   *
   * @return StringTranslation[]
   *   Counted or not, keyed by state name
   */
  static function stateNames() : array {
    return array_combine(
      static::STATES,
      [t('Initiated'), t('Validated'), t('Pending'), t('Completed'), t('Erased'), 'DELETE']
    );
  }

  /**
   * Get the normally used state names.
   * @return array
   *   State names keyed by one letter code.
   */
  static function usedStateNames() {
    $names = static::stateNames();
    unset($names[CCWorkflowInterface::STATE_DELETE], $names[CCWorkflowInterface::STATE_INITIATED]);
    return $names;
  }

  /**
   * Get the state names which are used in the db.
   * @return array
   *   State names keyed by one letter code.
   */
  static function storedStateNames() : array {
    $names = static::usedStateNames();
    unset($names[CCWorkflowInterface::STATE_VALIDATED]);
    return $names;
  }


  /**
   * {@inheritDoc}
   */
  public static function propertydefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'));
    // consider adding a computer field for the whole user.
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'Code for transaction state',
          'type' => 'varchar',
          'length' => 1,
          'not null' => FALSE,
        ],
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    return empty($this->value);
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    if (rand(0, 9) > 7) {
      return rand(1000, 5000);
    }
    else {
      return NULL;
    }
  }

}
