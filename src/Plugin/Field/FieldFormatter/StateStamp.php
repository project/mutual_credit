<?php

namespace Drupal\mcapi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'mc_state_stamp' formatter.
 */
#[FieldFormatter(
  id: 'mc_state_stamp',
  label: new TranslatableMarkup('Stamp effect'),
  description: new TranslatableMarkup('Place first for large angled text over the following fields.'),
  field_types: [
    'mc_state'
  ],
)]
class StateStamp extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $statenames = \Drupal\mcapi\Plugin\Field\FieldType\McState::usedStateNames();
    return [
      '#type' => 'markup',
      '#prefix' => '<div class = "mcapi-watermark">',
      '#markup' => Markup::create($statenames[$items->value]),
      '#suffix' => '</div>',
      '#attached' => ['library' => ['mcapi/statestamp']]
    ];
  }
}
