<?php

namespace Drupal\mcapi\Plugin\Field\FieldFormatter;

use Drupal\Mcapi\Functions;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'mc_worth' formatter.
 */
#[FieldFormatter(
  id: 'mc_worth',
  label: new TranslatableMarkup('Worth'),
  description: new TranslatableMarkup('A quantity of units'),
  field_types: [
    'mc_worth'
  ]
)]
class Worth extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($this->getSetting('purpose') == 'amount') {
      $markup = Functions::formatCurrency($items->quant);
    }
    else {
      $markup = Functions::formatBalance($items->quant);
    }
    return $markup;
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return ['purpose' => 'balance'];
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'purpose' => [
        '#type' => 'radios',
        '#title' => t('Purpose of field'),
        '#options' => [
          'balance' => t('Format for wallet balance'),
          'amount' => t('Format for transaction amount')
        ],
        '#default_value' => $this->getSetting('purpose')
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary() {
    return [
      '#markup' => $this->getSettings('purpose') == 'balance' ? $this->t('Format for wallet balance') : $this->t('Format for transaction amount')
    ];
  }
}
