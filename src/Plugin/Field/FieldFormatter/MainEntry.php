<?php

namespace Drupal\mcapi\Plugin\Field\FieldFormatter;

use Drupal\mcapi\Functions;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldFormatter;

/**
 * Format the first entry using a theme callback.
 */
#[FieldFormatter(
  id: 'mc_main_entry',
  label: new TranslatableMarkup('Main entry'),
  description: new TranslatableMarkup('Main entry formatted with Twig'),
  field_types: [
    'entry'
  ],
)]
class MainEntry extends BasicStringFormatter {

  /**
   * {@inheritDoc}
   *
   * @see Drupal\mcapi\Plugin\Field\FieldType\Entry
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entry = $items->first();
    $payer_wallet_name = $entry->payer ? $entry->payer->toLink()->toString() : t('Deleted wallet %id', ['%id' => '#'.$entry->payer_wid]);
    $payee_wallet_name = $entry->payee ? $entry->payee->toLink()->toString() : t('Deleted wallet %id', ['%id' => '#'.$entry->payee_wid]);
    $elements[0] = [
      '#type' => 'inline_template',
      '#template' => $this->getSetting('twig_template'),
      '#context' => [
        'payee' => Markup::Create('<span class="mc-payee">'. $payee_wallet_name .'</span>'),
        'payer' => Markup::Create('<span class="mc-payer">'. $payer_wallet_name .'</span>'),
        'quant' => Markup::Create('<span class="mc-quant">'. Functions::formatCurrency($entry->quant) .'</span>'),
        'description' => Markup::Create('<span class="mc-description">'. $entry->description.'</span>'),
      ]
    ];
    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'twig_template' => [
        '#type' => 'textarea',
        '#title' => t('Twig'),
        '#description' => t('Use tokens, {{ payer }}, {{ payee }}, {{ description }} {{ quant }}'),
        '#default_value' => $this->getSetting('twig_template')
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      // Pull out the twig to make it a bit easier for translators.
      'twig_template' => t(
        '@payer paid @payee\n@quant\nfor "@description"',
        ['@payee' => '{{ payee }}', '@payer' => '{{ payer}}', '@quant' => '{{ quant }}', '@payer' => '{{ description }}'],
        ['context' => 'template layout']
      )
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary() {
    return explode("\n", $this->getSetting('twig_template'));
  }

}
