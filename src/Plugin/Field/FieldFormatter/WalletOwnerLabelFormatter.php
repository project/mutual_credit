<?php

namespace Drupal\mcapi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;
/**
 * Plugin implementation of the 'wallet_reference_label_fallback' formatter
 * which provides a fallback for orphaned wallets.
 */
#[FieldFormatter(
  id: 'wallet_reference_label_fallback',
  label: new TranslatableMarkup('Label (with fallback)'),
  description: new TranslatableMarkup('Display the label of the wallet owners, with fallback for orphans'),
  field_types: ['entity_reference']
)]
class WalletOwnerLabelFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if (!$items->entity) {
      $items->applyDefaultValue(); // what is the default value??
      $this->prepareView([$items]);
    }
    return parent::viewElements($items, $langcode);
  }
  
}
