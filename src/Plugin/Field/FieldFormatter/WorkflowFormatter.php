<?php

namespace Drupal\mcapi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldFormatter;

/**
 * Plugin implementation of the 'wallet_reference_label_fallback' formatter
 * which provides a fallback for orphaned wallets.
 */
#[FieldFormatter(
  id: 'mc_workflow',
  label: new TranslatableMarkup('Workflow'),
  description: new TranslatableMarkup('The transaction workflow in various formats.'),
  field_types: [
    'mc_workflow'
  ],
)]
class WorkflowFormatter extends BasicStringFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $workflow = $items->entity;
    return [
      ['#markup' => '<span title="'. $workflow->summary .'">'. $workflow->label(). '<span>']
    ];
  }

}
