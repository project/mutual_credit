<?php

namespace Drupal\mcapi\Plugin\Field\FieldWidget;

use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use CreditCommons\Workflow as CCWorkflowInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the transaction entry widget.
 */

#[FieldWidget(
  id: 'mc_entry',
  label: new TranslatableMarkup('Entry'),
  description: new TranslatableMarkup('Enter the payer, payee, quant and description'),
  field_types: [
    'entry'
  ],
  multiple_values: TRUE
)]
class Entry extends WidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $raw = static::rawElements($items[$delta]?:$items->applyDefaultValue(), $items->getEntity()->workflow->entity->direction());
    // There is only ever one form element. If there are multiple entries they are generated later.
    return $element + [0 => $raw];
    foreach ($raw as &$e) {
      $e['#weight'] = $w++;
    }
    return $element += $raw;
  }

  /**
   * Provide form elements without any field API clutter.
   *
   * @param FieldItemInterface $item
   * @param string $direction
   *   +, means the payee is bill, - means the payer is crediting, 0 means 3rd party
   * @return type
   */
  static function rawElements(FieldItemInterface $item, $direction) {
    return [
      'description' => [
        '#title' => t('Description'),
        '#type' => 'textfield',
        '#default_value' => $item->description??'',
        '#weight' => 2
      ],
      'quant' => [
        '#title' => t('Amount'),
        '#type' => 'worth',
        '#default_value' => $item->quant??'',
        '#weight' => 4
      ],
      'payee_wid' => [
        '#title' => t('Payee'),
        '#type' => $direction == CCWorkflowInterface::PARTY_PAYEE ? 'my_wallet' : 'wallet_entity_auto',
        '#default_value' => $item->payee_wid ? Wallet::load($item->payee_wid) :  '',
        '#placeholder' => t('Wallet name'),
        '#required' => $direction == CCWorkflowInterface::PARTY_PAYEE,
        '#weight' => 6
      ],
      'payer_wid' => [
        '#title' => t('Payer'),
        '#type' => $direction == CCWorkflowInterface::PARTY_PAYER ? 'my_wallet' : 'wallet_entity_auto',
        '#default_value' => $item->payer_wid ? Wallet::load($item->payer_wid) : '',
        '#placeholder' => t('Wallet name'),
        '#weight' => 8,
        '#required' => $direction == CCWorkflowInterface::PARTY_PAYER,
      ]
    ];
  }

  /**
   * {@inheritDoc}
   * The mcapi_forms module breaks the hierarchy.
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    parent::extractFormValues($items, $form, $form_state);
    $field_name = $this->fieldDefinition->getName();
    $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
    $field_state['array_parents'] = [];
    static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values[0]['primary'] = 1;
    foreach ($values as &$value) {
      if (!isset($value['metadata'])) {
        $value['metadata'] = [];
      }
    }
    return $values;
  }

  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    $parts = explode('.', $error->getPropertyPath());
    $element = $element['entries']['widget'];
    foreach ($parts as $key) {
      $element = $element[$key];
    }
    return $element;
  }

}
