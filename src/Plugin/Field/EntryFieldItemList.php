<?php

namespace Drupal\mcapi\Plugin\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Entries field list
 */
class EntryFieldItemList extends FieldItemList {

  /**
   * {@inheritDoc}
   * Ensure the first Entry is primary
   */
  public function setValue($values, $notify = TRUE) {
    if ($this->count() == 0) {
      if (isset($values['payee_wid'])) {
        $values['primary'] = TRUE;
      }
      elseif(isset($values[0]['payee_wid'])) {
        $values[0]['primary'] = TRUE;
      }
    }
    // The mass transaction form uses the entity reference widget which returns a
    // values with a key of target_id for each FieldItem. Convert these to field
    // properties.
    foreach ($values as &$entry) {
      if (isset($entry['payee_wid']['target_id'])) {
        $entry['payee_wid'] = $entry['payee_wid']['target_id'];
      }
      elseif(isset($entry['payer_wid']['target_id'])) {
        $entry['payer_wid'] = $entry['payer_wid']['target_id'];
      }
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   * This is called in EntityStorageBase::initFieldValues() via doCreate
   * @temp until https://www.drupal.org/node/2356623 when the parent will do it
   */
  public function applyDefaultValue($notify = TRUE) {
    $item = $this->first() ?: $this->appendItem();
    $item->applyDefaultValue(FALSE);
    return $this;
  }

  /**
   * Get all the entries except the main (first) one.
   * @return Drupal\mcapi\Plugin\Field\FieldType\Entry[]
   */
  public function other() {
    return array_slice($this->list, 1);
  }

}
