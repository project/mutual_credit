<?php

namespace Drupal\mcapi\Plugin\Field;

use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\mcapi\Entity\Wallet;

/**
 * A computed field showing the wallet entity on a transaction entry.
 */
class ComputedWallet extends ItemList {

  /**
   * Cached processed entity.
   *
   * @var Wallet
   */
  protected $wallet = NULL;

  /**
   * {@inheritDoc}
   */
  public function getValue() {
    if ($this->wallet == NULL) {
      $propname = $this->getName() . '_wid';
      // The parent is an EntryItem
      if ($wid = $this->getParent()->{$propname}) {
        $this->wallet = Wallet::load($wid);
      }
    }
    return $this->wallet;
  }

}

