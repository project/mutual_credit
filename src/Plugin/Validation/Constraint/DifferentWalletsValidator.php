<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Every entry must be between different wallets.
 */
class DifferentWalletsValidator extends ConstraintValidator {

  /**
   * {@inheritDoc}
   */
  public function validate($entries, Constraint $constraint) {
    foreach ($entries as $delta => $entry) {
      if (empty($entry->payee_wid) or empty($entry->payer_wid)) {
        // How do we ensure this before now?
        $this->context
          ->buildViolation($constraint->missingWallet)
          ->addViolation();
      }
      elseif ($entry->payer_wid == $entry->payee_wid) {
        $constraint->wid = $entry->payee_wid;
        $this->context
          ->buildViolation($constraint->sameMessage)
          ->addViolation();
      }
    }
  }


}
