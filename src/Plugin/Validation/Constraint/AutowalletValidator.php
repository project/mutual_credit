<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;


/**
 *
 */
class AutowalletValidator extends ConstraintValidator {

  /**
   * {@inheritDoc}
   */
  public function validate($entity, Constraint $constraint) {
    // N.B This is a clone of the entity.
    if (isset($entity->newWallet)) {
      // Activate the user (new users are sometimes blocked) to pass validation.
      $entity->newWallet->getowner()->activate();
      // No need to deactivate because we are working on a clone.
      $violationList = $entity->newWallet->validate();
      // Tests show that only the first message is displayed.s
      foreach($violationList as $v) { // specific errors
        $this->context->addViolation($v->getMessage());
      }
    }
  }

}
