<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Validate the autocreated wallet - specifically the owner.
 * Supports validating payer & payee of parent transactions.
 */
#[Constraint(
  id: 'Autowallet',
  label: new TranslatableMarkup('Validates the autowallet when an Entity is created.')
)]
class Autowallet extends \Symfony\Component\Validator\Constraint {

  public $invalidWallet = 'The autocreated wallet is invalid.';

}
