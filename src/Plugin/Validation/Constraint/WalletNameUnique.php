<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;

/**
 * Checks if a wallet name is unique on the site.
 */
#[Constraint(
  id: 'WalletNameUnique',
  label: new TranslatableMarkup('Wallet name unique', [], ['context' => "Validation"]),
)]
class WalletNameUnique extends UniqueFieldConstraint {

  public $message = "There is already a wallet called '%value'.";

}
