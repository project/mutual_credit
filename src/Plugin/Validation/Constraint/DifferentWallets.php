<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Check that the two wallets in the transaction are different.
 * Supports validating payer & payee of parent transactions.
 */
#[Constraint(
  id: 'DifferentWallets',
  label: new TranslatableMarkup('Checks that the payer and payee wallets are different')
)]
class DifferentWallets extends CompositeConstraintBase {

  public string $sameMessage = 'The payer and payee wallets must be different';
  public string $missingWallet = 'Both wallets are required.';

  /**
   * The wallet ID
   * @var int
   */
  public int $wid;

  /**
   * {@inheritDoc}
   */
  public function coversFields() {
    return ['entries'];
  }

}
