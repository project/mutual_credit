<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Validate the workflow definition, a string.
 */
#[Constraint(
  id: 'mc_workflow',
  label: new TranslatableMarkup('Ensures the workflow string is valid')
)]
class McWorkflow extends \Symfony\Component\Validator\Constraint {

  public $badlyFormed = 'The workflow string is invalid';

}
