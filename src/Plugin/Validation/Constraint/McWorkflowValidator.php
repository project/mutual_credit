<?php

namespace Drupal\mcapi\Plugin\Validation\Constraint;

use Drupal\mcapi\Entity\Workflow;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;


/**
 *
 */
class McWorkflowValidator extends ConstraintValidator {

  /**
   * {@inheritDoc}
   */
  public function validate($workflow_items, Constraint $constraint) {
    // There is only ever 1 item
    if (Workflow::validate($workflow_items->value)) {
      $this->context->buildViolation($constraint->badlyFormed)->addViolation();
    }
  }

}
