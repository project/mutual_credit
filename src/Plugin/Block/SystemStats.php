<?php

namespace Drupal\mcapi\Plugin\Block;

use Drupal\mcapi\Functions;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Displays stats indicating the overall activity on the site.
 */
#[Block(
  id: 'mcapi_system_stats',
  admin_label: new TranslatableMarkup("System-wide trading stats"),
  category: new TranslatableMarkup('Community Accounting'),
)]
class SystemStats extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var Drupal\mysql\Driver\Database\mysql\Connection
   */
  private $database;

  function __construct($configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  public static function create($container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }
    return $this->t('@period trading stats', ['@period' => $this->configuration['period']]);
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $conf = parent::defaultConfiguration();
    return $conf += [
      'period' => '1 year',
      'stats_link' => TRUE
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['period'] = [
      '#title' => $this->t('Time since (relative to now)'),
      '#description' => $this->t('E.g. 1 year, 6 months, 30 days. See %link', ['%link' => 'https://www.php.net/manual/en/datetime.formats.relative.php']),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $this->configuration['period'],
      '#weight' => 5
    ];
    $form['stats_link'] = [
      '#title' => $this->t('Link to stats page'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['stats_link'],
      '#weight' => 6
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $query = $this->database->select('mc_transactions_index', 't');
    $query->addExpression('COUNT(t.xid)', 'trades');
    $query->addExpression('COUNT(DISTINCT t.partner_id)', 'traders');
    $query->addExpression('SUM(t.volume)', 'volume');
    $result = $query->condition('t.outgoing', 0)
      ->condition('created', strtotime('- '.$this->configuration['period']), '>')
      ->execute()
      ->fetchAssoc();

    $build['#title'] = $this->t('Trading stats');
    $build['trades'] = $this->littleDiv($this->t('Trades'), Markup::create($result['trades']));
    $build['traders'] = $this->littleDiv($this->t('Traders'), Markup::create($result['traders']));
    $build['volume'] = $this->littleDiv($this->t('Volume'), Functions::formatBalance($result['volume'] ?? 0, $this->configuration['stats_link']));
    $build['more'] = [
      '#markup' => \Drupal\Core\Link::createFromRoute($this->t('More stats...'), 'mcapi.metrics')->toString(),
      '#weight' => 100
    ];
    $build['#cache'] = [
      'tags' => ['mc_transaction_list']
    ];
    return $build;
  }

  /**
   * Show a stat as a markup render array with divs.
   */
  function littleDiv(string|Markup $title, Markup $value) : array {
    static $w = 0;
    return [
      '#type' => 'markup',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#weight' => $w++,
      '#markup' => '<strong>'.$title.':</strong> '.$value
    ];
  }

}
