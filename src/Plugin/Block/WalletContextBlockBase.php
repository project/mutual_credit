<?php

namespace Drupal\mcapi\Plugin\Block;

use Drupal\mcapi\Entity\Wallet;
use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base block class for blocks determine a wallet from the context.
 */
abstract class WalletContextBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  const OWNER_CURRENT_USER = 1;
  const OWNER_VISITED_ENTITY = 0;

  /**
   * @var Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var Drupal\Core\Session\AccountProxy
   */
  protected $entityTypeManager;

  /**
   * @var array
   */
  protected $wids = [];

  /**
   * @var Wallet[]
   */
  protected $wallets = [];

  /**
   * @var ContentEntity
   */
  protected $owner;

  /**
   * Constructor
   *
   * @param array $configuration
   * @param type $plugin_id
   * @param array $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param AccountInterface $current_user
   * @param Request $request
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    if ($this->configuration['owner_source'] == SELF::OWNER_CURRENT_USER) {
      $this->owner = User::load($current_user->id());
    }
    elseif ($request->attributes and $request->attributes->has('_entity')) {
      $entity = $request->attributes->get('_entity');
      if ($entity instanceOf ContentEntityInterface) {
        $this->owner = $entity;
      }
    }
    if ($this->owner) {
      $this->wids = $entity_type_manager->getStorage('mc_wallet')->getQuery()
        ->accessCheck(TRUE)
        ->condition('owner', $this->owner->id())
        ->execute();
    }
  }

  /**
   * Injection.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'owner_source' => SELF::OWNER_CURRENT_USER,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function blockAccess(AccountInterface $account) : AccessResult {
    if (empty($this->owner)) {
      $result = AccessResult::forbidden('No wallet owner on this route');
    }
    elseif (empty($this->wids)) {
      // Not sure how to cache this.
      $result = AccessResult::forbidden('User '.$this->owner->id().' has no wallets');
    }
    else {
      $result = AccessResult::allowed()->cachePerUser();
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    parent::blockForm($form, $form_state);
    $form['owner_source'] = [
      '#title' => $this->t('Entity source'),
      '#type' => 'radios',
      '#options' => array(
        static::OWNER_VISITED_ENTITY => $this->t('Wallets of entity on canonical page'),
        static::OWNER_CURRENT_USER => $this->t('Wallets of logged in user'),
      ),
      '#default_value' => $this->configuration['owner_source'],
      '#weight' => 1
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['owner_source'] = (bool)$form_state->getValue('owner_source');
    $form_state->unsetValue('owner_source');
    $form_state->cleanValues();
    parent::blockSubmit($form, $form_state);
    foreach ($form_state->getValues() as $key => $val) {
      $this->configuration[$key] = $val;
    }
  }

  public function build() {
    if ($this->configuration['owner_source'] == SELF::OWNER_CURRENT_USER) {
      $context = 'user';
    }
    else {
      $context = 'url';
    }

    foreach ($this->wids as $wid) {
      $tags[] = 'mc_wallet:'.$wid;
    }
    return [
      '#cache' => [
        'contexts' => [$context],
        'tags' => $tags
      ]
    ];
  }

}
