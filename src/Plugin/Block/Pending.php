<?php

namespace Drupal\mcapi\Plugin\Block;

use Drupal\mcapi\Functions;
use Drupal\mcapi\Plugin\Block\WalletContextBlockBase;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a block for switching users.
 */
#[Block(
  id: 'mcapi_pending',
  admin_label: new TranslatableMarkup("Users' pending transactions"),
  category: new TranslatableMarkup('Community Accounting'),
)]
class Pending extends WalletContextBlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function label() {
    return $label = $this->t('Pending transactions');
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'empty_message' => $this->t('No pending transactions.'),
      'show_other' => TRUE
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['empty_message'] = [
      '#title' => $this->t('Empty message'),
      '#description' => $this->t('Leave blank to hide the block when there are no pending transactions.'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['empty_message'],
      '#placeholder' => $this->t('No pending transactions.')
    ];
    $form['show_other'] = [
      '#title' => $this->t("Show all pending transactions the user is involved in."),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['show_other'],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $uid = $this->owner->id();
    if ($this->configuration['show_other']) {
      $xids = Functions::pendingTransactionsOfUser($uid);
    }
    else {
      $xids = Functions::transactionsNeedingSigOfUser($uid);
    }
    if ($xids) {
      $renderable = Functions::sentenceMarkupWithLinks($xids);
    }
    elseif (!empty($this->configuration['empty_message'])) {
      $renderable = [
        '#markup' => Markup::create($this->configuration['empty_message']),
        '#weight' => -1
      ];
    }
    else {
      $renderable = [];
    }
    return $renderable;// + parent::build(); // Parent just sets #cache tag
  }

}

