<?php

namespace Drupal\mcapi\Plugin\Block;

use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays all the wallets of an entity.
 *
 * Entity being either the current user OR the entity being viewed. Shows the
 * wallet view mode 'mini'.
 */
#[Block(
  id: 'held_wallets',
  admin_label: new TranslatableMarkup('Held wallet(s)'),
  category: new TranslatableMarkup('Community Accounting'),
)]
class HeldWallets extends WalletContextBlockBase{

  /**
   * @var EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;
  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructor
   *
   * @param array $configuration
   * @param type $plugin_id
   * @param array $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param AccountInterface $currentUser
   * @param Request $request
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $current_user, $request, EntityDisplayRepositoryInterface $entity_display_repository, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $current_user, $request);
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Injection.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $conf = parent::defaultConfiguration();
    $conf += [
      'owner_source' => SELF::OWNER_CURRENT_USER,
      'item' => 'viewdisplay:mini',// if this doesn't exist, it'll fall back I'm sure
      'first_only' => FALSE
    ];
    return $conf;
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    foreach ($this->entityDisplayRepository->getViewModeOptions('mc_wallet') as $mode => $label) {
      if ($mode == 'token') continue;
      $options['viewdisplay:'.$mode] = $this->t('Display: @mode', ['@mode' => $label]);
    }
    foreach ($this->entityFieldManager->getExtraFields('mc_wallet', 'mc_wallet')['display'] as $key => $info) {
      $options['extrafield:'.$key] = $this->t('Component: @mode', ['@mode' => $info['label']]);
    }

    $form['item'] = [
      '#title' => $this->t('Component'),
      '#description' => $this->t(
        'The components in each display are configured at <a href=":url">wallet view displays</a>.',
        [':url' => Url::fromRoute("entity.entity_view_display.mc_wallet.default")->toString()]
      ),
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->configuration['item'],
      '#weight' => 2,
      '#required' => TRUE
    ];

    $form['first_only'] = [
      '#title' => $this->t('Which wallets'),
      '#description' => $this->t('Some entities may hold more than one wallet.'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('All wallets'),
        1 => $this->t('First wallet only'),
      ],
      '#default_value' => $this->configuration['first_only'],
      '#weight' => 3
    ];
    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['first_only'] = (bool)$form_state->getValue('first_only');
    $form_state->unsetValue('first_only');
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $renderable = [];
    if ($this->configuration['first_only']) {
      $this->wids = array_slice($this->wids, 0, 1);
    }
    [$type, $name] = explode(':', $this->configuration['item']);
    $w = 0;
    $walletViewBuilder = $this->entityTypeManager->getViewBuilder('mc_wallet');
    foreach ($this->wids as $wid) {
      $wallet = Wallet::load($wid);
      if ($type == 'viewdisplay') {
        $renderable[$wid] = $walletViewBuilder->viewMultiple([$wallet], $name);
        $renderable[$wid]['#weight'] = $w++;
      }
      elseif ($type == 'extrafield') {
        $key = 'wallet_'.$wid;
        $func = 'mc_wallet_view_'.$name;
        $renderable[$wid] = $func($wallet);
        $renderable[$wid]['#weight'] = $w++;
        $renderable[$wid][]['link'] = $wallet->toLink();
      }
    }
    $renderable['#cache']['contexts'][] = $this->configuration['owner_source'] == SELF::OWNER_CURRENT_USER ? 'user' : 'url';
    $renderable['#cache']['tags'] = array_map(
      function ($w){return 'mc_wallet:'.$w;},
      $this->wids
    );
    return $renderable;// cachetags
  }

}
