<?php

namespace Drupal\mcapi\Plugin\views\field;

use Drupal\views\Attribute\ViewsField;
use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\ResultRow;

/**
 * Displays wallet label, even if the wallet is deleted.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("mc_wallet")]
class Wallet extends EntityField {

  /**
   * {@inheritdoc}
   */
  public function advancedRender(ResultRow $values) {
    $output = parent::advancedRender($values);
    if (empty($output)) {
      $wid = $this->missingWalletId($values);
      $output = t("Deleted wallet %id", ['%id' => '#'.$wid]);
    }
    return $output;
  }

  /**
   * The wallet doesn't exist so the query didn't even get the wallet ID, so work it out from the transaction
   * @param ResultRow $row
   * @return int
   */
  function missingWalletId(ResultRow $row) : int {
    $entry = $row->_entity->entries->first();
    if (isset($row->_relationship_entities['entries_payer_wid'])) {
      return $entry->payee_wid;
    }
    elseif (isset($row->_relationship_entities['entries_payee_wid'])) {
      return $entry->payer_wid;
    }
  }
}
