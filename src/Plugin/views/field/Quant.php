<?php

namespace Drupal\mcapi\Plugin\views\field;

use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Functions;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Attribute\ViewsField;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Field handler to provide running balance for a given transaction.
 *
 * @note reads from the transaction index table
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("mc_quant")]
class Quant extends FieldPluginBase {

  protected $config;

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param ConfigFactoryInterface $config_factory
   *   The EntityTypeManager.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory->get('mcapi.settings');
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('config.factory')
    );
  }
    /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['purpose'] = ['default' => 'balance'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['purpose'] = [
      '#type' => 'radios',
      '#title' => t('Format for'),
      '#options' => [
        'balance' => t('Wallet balance'),
        'link' => t('Wallet balance linked to wallet'),
        'amount' => t('Transaction amount')
      ],
      '#default_value' => $this->options['purpose']
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   * This field is used on wallets and transactions.
   */
  public function render(ResultRow $values) {

    $entity = $this->getEntity($values);
    // Entity is from the main table of the view or the relationship if this field has one.
    if ($entity instanceof Transaction) {
      // $transaction->quant isn't working via __get magic method
      $quant = $entity->entries[0]->quant;
    }
    else {
      $quant = intval($this->getValue($values));
    }

    if ($this->options['purpose'] == 'balance') {
      $markup = Functions::formatBalance($quant, FALSE);
    }
    elseif ($this->options['purpose'] == 'link') {
      $balance = Functions::formatBalance($quant, $entity->id());
      $markup = Markup::create('<span title="'.$entity->label().'">'.$balance.'</span>');
    }
    else {
      $markup = Functions::formatCurrency($quant);
    }
    return $markup;
  }


}

