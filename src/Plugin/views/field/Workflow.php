<?php

namespace Drupal\mcapi\Plugin\views\field;

use Drupal\mcapi\Entity\Workflow as WorkflowEntity;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler for the name of the transaction type.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField(id: 'mc_workflow')]
class Workflow extends FieldPluginBase {

  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    $raw = $this->getValue($values);
    return ['#markup' => '<span class = "mc-type-'. $raw .'">' . WorkflowEntity::load($raw)->label() . '</span>'];
  }

}
