<?php

namespace Drupal\mcapi\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler for the name of the transaction state.
 *
 * I would hope for a generic filter to come along to render list key/values.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("mc_state")]
class State extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $raw = $this->getValue($values);
    $state_names = \Drupal\mcapi\Plugin\Field\FieldType\McState::stateNames();
    // Should be translated.
    $output = '<span class = "mcapi-state-'. $raw .'">' . $state_names[$raw] . '</span>';
    return ['#markup' => $output];
  }

}
