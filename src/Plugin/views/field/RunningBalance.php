<?php

namespace Drupal\mcapi\Plugin\views\field;

use Drupal\Mcapi\Functions;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\views\Attribute\ViewsField;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to provide running balance for a given transaction.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("transaction_running_balance")]
class RunningBalance extends FieldPluginBase {

  private $txStorage;
  private $fAlias;

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param EntityTypeManager $entity_type_manager
   *   The EntityTypeManager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->txStorage = $entity_type_manager->getStorage('mc_transaction');
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  function query() {
    $this->addAdditionalFields();
  }


  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    $transaction = $this->getEntity($values);
    if (!$transaction) {
      //something wrong here
      throw new \Exception('No entity in views result');
    }
    $wid = $this->{aliases['wallet_id']};
    $created = $values->{$aliases['created']};
    $val = $this->txStorage->runningBalance($wid, 'created', $created);
    return Functions::formatBalance($val);
  }

}
