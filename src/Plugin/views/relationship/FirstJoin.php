<?php

namespace Drupal\mcapi\Plugin\views\relationship;

use Drupal\views\Attribute\ViewsRelationship;
use Drupal\views\Plugin\views\relationship\Standard;

/**
 * Extend the Standard relationships to ensure only the first users' wallet is selected.
 *
 * @ingroup views_relationship_handlers
 */
#[ViewsRelationship("first_join")]
class FirstJoin extends Standard {

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();
    $base_table = $this->definition['base'];
    $base_field = $this->definition['base field'];
    $main_table = $this->definition['table'];

    $subquery = "SELECT MIN(base.wid) FROM {$base_table} base WHERE base.$base_field = $main_table.uid";

    $this->query->addWhereExpression(0, "{$this->alias}.wid = ($subquery)");
  }

}
