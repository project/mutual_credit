<?php

namespace Drupal\mcapi\Plugin\views\argument_validator;

use Drupal\views\Plugin\views\argument_validator\ArgumentValidatorPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Attribute\ViewsArgumentValidator;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Validates whether user access is allowed.
 * 
 * @note this isn't used in any of the default views.
 */
#[ViewsArgumentValidator(
  id: 'wallet_private',
  title: new TranslatableMarkup('Wallet private'),
  entity_type: 'mc_wallet'
)]
class WalletPrivate extends ArgumentValidatorPluginBase {

  protected $walletStorage;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param array $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param EntityTypeBundleInfoInterface $entity_type_bundle_info
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->walletStorage = $entity_type_manager->getStorage('mc_wallet');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  public function validateArgument($argument) {
    $current_user = \Drupal::currentUser();
    if ($current_user->hasPermission('view all transactions')) {
      return TRUE;
    }
    if ($wallet = $this->walletStorage->load($argument)) {
      if ($current_user->id() == $wallet->getOwnerId()) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
