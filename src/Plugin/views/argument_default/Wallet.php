<?php

namespace Drupal\mcapi\Plugin\views\argument_default;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\UserInterface;
use Drupal\mcapi\Entity\WalletInterface;
use Drupal\views\Attribute\ViewsArgumentDefault;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Default argument plugin to extract a wallet from request.
 */
#[ViewsArgumentDefault(
  id: 'wallet',
  title: new TranslatableMarkup('Wallet ID from route context')
)]
class Wallet extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new User instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    // If there is a wallet in the current route.
    if ($wallet = $this->routeMatch->getParameter('mc_wallet')) {
      if ($wallet instanceof WalletInterface) {
        return $wallet->id();
      }
    }
    // If there is a user in the current route return the user's first wallet
    elseif ($user = $this->routeMatch->getParameter('user')) {
      if ($user instanceof UserInterface) {
        $wids = WalletStorage::allWalletIdsOf($user->id());
        return reset($wids);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
