<?php

namespace Drupal\mcapi\Plugin\views\argument_default;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Attribute\ViewsArgumentDefault;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The fixed argument default handler.
 *
 * @ingroup views_argument_default_plugins
 */
#[ViewsArgumentDefault(
  id: 'current_user_wallets',
  title: new TranslatableMarkup('Wallets of the current user')
)]
class CurrentUserWallets extends ArgumentDefaultPluginBase {

  private $currentUser;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * Return the default argument.
   */
  public function getArgument() {
    $wids = WalletStorage::allWalletIdsOf($this->currentUser->id());
    return implode('+', $wids);
  }

}
