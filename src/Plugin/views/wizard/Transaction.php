<?php

namespace Drupal\mcapi\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;
use Drupal\views\Attribute\ViewsWizard;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a wizard for the mc_transaction table.
 */
#[ViewsWizard(
  id: 'transaction',
  title: new TranslatableMarkup('Transactions'),
  module: 'mcapi',
  base_table: 'mc_transactions_index'
)]
class Transaction extends WizardPluginBase {

  /**
   * Set the created column.
   */
  protected $createdColumn = 'created';

}
