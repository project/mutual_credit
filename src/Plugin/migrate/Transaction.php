<?php

namespace Drupal\mcapi\Plugin\migrate;

use Drupal\migrate_drupal\Plugin\migrate\FieldMigration;

/**
 * Plugin class for Drupal 7 transaction migrations dealing with fields
 */
class Transaction extends FieldMigration {

  /**
   * {@inheritDoc}
   */
  public function getProcess() {
    if (!$this->init) {
      $this->init = TRUE;
      // NB Use the name of the source entity.
      $this->fieldDiscovery->addEntityFieldProcesses($this, 'transaction');
    }
    return parent::getProcess();
  }


}
