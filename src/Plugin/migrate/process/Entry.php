<?php
namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Generate an entry from a d7 transaction.
 */
#[MigrateProcess('mc_entry')]
class Entry extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   * $value is the old 'worth' field
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $format = \Drupal::config('mcapi.settings')->get('currency')['format'];

    $multiplier = 1;
    if (isset($format['subdivision'])) {
      if ($format['subdivision'] == '59/4') {
        $multiplier = 60;
      }
      elseif($format['subdivision'] == 99) {
        $multiplier = 100;
      }
    }
    elseif (strpos($format['main'], '.99')) {
      $multiplier = 100;
    }
    return [[
      'payee_wid' => $row->getSourceProperty('payee'),
      'payer_wid' => $row->getSourceProperty('payer'),
      'quant' => intval($row->getSourceProperty('quantity') * $multiplier),
      'description' => $row->getSourceProperty('transaction_description')[0]['value'],
      'primary' => 1,
    ]];
  }


}
