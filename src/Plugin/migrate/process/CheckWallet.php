<?php

namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\mcapi\Entity\Wallet;
use Drupal\user\Entity\User;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Check the payer or payee user has a wallet and swop the uid for the wallet id.
 * @deprecated since we now make a wallet with the same id as each user.
 */
#[MigrateProcess('check_wallet')]
class CheckWallet extends ProcessPluginBase {

  private $map = [];

  /**
   * {@inheritDoc}
   * Transform the user id from the d7 ledger to a wallet id for the d8 ledger, creating a wallet if necessary.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $user_id = $value;
    if (!isset($this->map[$user_id])) {
      $user = User::load($user_id);
      if (!$user) {
        // Have to use the default wallet...so get user 1
        // NB this would be some other user in group situation!
        $user = User::load(1);
      }
      if ($wids = WalletStorage::allWalletIdsOf($user_id)) {
        $wid = reset($wids);
      }
      else {
        // Create a wallet for this user (should never be necessary)
        $wallet = Wallet::Create(['owner' => $user]);
        $wallet->save();
        $wid = $wallet->id();
      }
      $this->map[$user_id] = $wid;
    }

    return $this->map[$user_id];
  }

}
