<?php

namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Get a property from the supplied object.
 */
#[MigrateProcess('d7_mcapi_object_prop')]
class ObjectProperty extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $result = $row->getSource()[$this->configuration['property']];
    if ($this->configuration['property'] == 'path') {
      // I couldn't get concat to work
      $result = '/'.$result;
    }
    elseif (isset($this->configuration['subproperty'])) {
      $result = (object)$result;
      $result = $result->{$this->configuration['subproperty']};
    }
    return $result;
  }

}
