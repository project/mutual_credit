<?php
namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert (actually generate the workflow from the D7 transaction type)
 */
#[MigrateProcess('mc_workflow')]
class WorkflowField extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   * $value is the old transacton 'type'
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value == '1stparty') {
      $creator_uid = $row->getSourceProperty('creator');
      if ($row->getSourceProperty('payee') == $creator_uid) {
        return '+|PC-CE0';
      }
      elseif ($row->getSourceProperty('payer') == $creator_uid) {
        return '-|PC+CE0';
      }
    }
    // many2one one2many, mass, 3rdparty
    return '0_CE0';
  }

}
