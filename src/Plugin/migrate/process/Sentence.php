<?php

namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Transform the sentence from the d7 adhoc tokens to d8 twig.
 */
#[MigrateProcess('mcapi_sentence')]
class Sentence extends ProcessPluginBase {

  /**
   * From something like
   * [transaction:payer] paid [transaction:payee] [transaction:worth] for transaction:transaction_description. [transaction:links]
   * to
   * [xaction:payer] paid [xaction:payee] [xaction:worth] for [xaction:description].
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = str_replace('transaction_description', 'description', $value);
    $value = str_replace('[transaction:links]', '', $value);
    $value = str_replace('transaction:', '', $value);
    $value = str_replace('xaction:', '', $value);
    return $value;
  }

}
