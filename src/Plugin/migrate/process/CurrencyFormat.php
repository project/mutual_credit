<?php

/**
 * @file
 */
namespace Drupal\mcapi\Plugin\migrate\process;

use Drupal\mcapi\Element\CurrencyFormat as CurrencyFormatElement;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Convert the old display setting into the new one-string format.
 */
#[MigrateProcess('d7_mcapi_currency_format')]
class CurrencyFormat extends ProcessPluginBase {

  /**
   * Constants taken from D7
   */
  const CURRENCY_DIVISION_MODE_NONE = 0;
  const CURRENCY_DIVISION_MODE_CENTS_INLINE = 1;
  const CURRENCY_DIVISION_MODE_CENTS_FIELD = 2;
  const CURRENCY_DIVISION_MODE_CUSTOM = 3;

  /**
   * {@inheritDoc}
   *
   * Division settings is a string with key|value pairs on each line
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    switch($value->divisions) {
      case self::CURRENCY_DIVISION_MODE_CENTS_INLINE:
        $format = '0.00';
        break;
      case self::CURRENCY_DIVISION_MODE_CUSTOM:
        $items = explode("\n", $value->divisions_setting);
        $format = '0'.$value->delimiter.'59/'.count($items);
        preg_match('/.*|[0-9]+(.*)$/', end($items), $matches);
        if (isset($matches[1])) {
          $format .= ' '.$matches[1];
        }
        break;
      case self::CURRENCY_DIVISION_MODE_NONE:
      default:
        $format = '0';
    }
    $string = str_replace('[quantity]', $format, $value->format);

    // f1, main, f2, subdivision, f3
    return CurrencyFormatElement::compileFormat($string);
  }

}
