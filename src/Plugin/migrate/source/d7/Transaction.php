<?php

namespace Drupal\mcapi\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\mcapi\EventSubscriber\MigrationSubscriber;

/**
 * Drupal 7 transactions source from database.
 *
 * @MigrateSource(
 *   id = "d7_mc_transaction",
 *   source_module = "mcapi"
 * )
 */
class Transaction extends FieldableEntity {

  /**
   * {@inheritDoc}
   */
  public function query() {
    $query = $this->select('mcapi_transactions', 't')
      ->fields('t', ['xid', 'serial', 'payer', 'payee', 'type', 'state', 'creator', 'created']);
    // Concatenate these fields so that it returns one entry per serial number
    $query->join(
      'field_data_' . $this->variableGet('transaction_description_field', 'transaction_description'),
      'd',
      "t.xid = d.entity_id AND d.entity_type = 'transaction'"
    );
    $query->join(
      'field_data_worth',
      'w',
      "t.xid = w.entity_id AND w.entity_type = 'transaction'"
    );
    $query->condition(
      'w.worth_currcode',
      $this->database->query(MigrationSubscriber::mostUsedCurrencyQuery())->fetchField()
    );
    $query->addField('w', 'worth_quantity', 'quantity');
    $query->orderBy('serial', 'ASC');
    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function prepareRow(Row $row) {
    // Change the entity type and bundle names from d7
    $row->setSourceProperty('entity_type', 'transaction');
    $row->setSourceProperty('bundle', 'transaction');
    // Get Field API field values (using the d7 entity and bundle names)
    $fields = $this->getFields('transaction', 'transaction');
    foreach (array_keys($fields) as $field) {
      $row->setSourceProperty(
        $field,
        $this->getFieldValues(
          'transaction',
          $field,
          $row->getSourceProperty('xid'),
          $row->getSourceProperty('vid')
        )
      );
    }

    // Get Field API field values.
    foreach (array_keys($this->getFields('transaction')) as $field) {
      $row->setSourceProperty($field, $this->getFieldValues('transaction', $field, $row->getSourceProperty('xid')));
    }

    return parent::prepareRow($row); // This runs hook_migrate_prepare_row
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    return [
      'xid' => $this->t('Transaction ID'),
      'serial' => $this->t('Serial number'),
      'payer' => $this->t('Payer user'),
      'payee' => $this->t('Payee user'),
      'type' => $this->t('Type'),
      'state' => $this->t('State'),
      'creator' => $this->t('Created by user id'),
      'created' => $this->t('Created timestamp'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    return [
      'xid' => [
        'type' => 'integer',
        'alias' => 't'
      ]
    ];
  }

}
