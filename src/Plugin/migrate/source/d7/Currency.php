<?php

namespace Drupal\mcapi\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\migrate\Row;
use Drupal\mcapi\EventSubscriber\MigrationSubscriber;

/**
 * Currencies in d7 were built on ctools
 *
 * @MigrateSource(
 *   id = "d7_currency",
 *   source_module = "mcapi"
 * )
 */
class Currency extends DrupalSqlBase {

  var string $currcode;

  /**
   * {@inheritDoc}
   */
  public function query() {
    // Select then load the one currency ID with the most transactions.
    $this->currcode = $this->getDatabase()->query(MigrationSubscriber::mostUsedCurrencyQuery())->fetchField();
    // NB If the currency has never been edited, it won't be in the database.
    // So nothing will be migrated and default settings will be used.
    return $this->select('mcapi_currencies', 'c')
      ->fields('c', ['currcode', 'data'])
      ->condition('currcode', $this->currcode);
  }

  /**
   * {@inheritDoc}
   */
  protected function values() {
    return $this->prepareQuery()->execute()->fetchCol();
  }

  /**
   * {@inheritDoc}
   */
  public function count($refresh = false) {
    return intval($this->query()->countQuery()->execute()->fetchField() > 0);
  }

  /**
   * {@inheritDoc}
   */
  public function prepareRow(Row $row) {
    static $i = 0;

    $result = $this->select('mcapi_currencies', 'c')
      ->fields('c', ['data'])
      ->condition('currcode', $this->currcode)
      ->execute()
      ->fetchField();


    $currency = unserialize($result);
    foreach ((array)$currency as $key => $val) {
      if (is_array($val)) {
        $val = (object)$val;
      }
      $row->setSourceProperty($key, $val);
    }
    $result = parent::prepareRow($row);
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    $fields = [
      'currcode' => $this->t('Ctools key'),
      'data' => $this->t('Serialized data'),
    ];
    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    return [
      'currcode' => [
        'type' => 'string',
      ]
    ];
  }

}
