<?php
namespace Drupal\mcapi\Plugin\migrate\field\d7;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;
use Drupal\migrate_drupal\Attribute\MigrateField;
use Drupal\migrate\Plugin\MigrationInterface;

#[MigrateField(
  id: 'mc_entry',
  core: [7],
  source_module: 'mcapi',
  destination_module: 'mcapi'
)]
class Entry extends FieldPluginBase {

  /**
   * {@inheritDoc}
   * $value is the quant
   * @note not used AFAICT
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return [
      'payee_wid' => $row->getSourceProperty('payee'),
      'payer_wid' => $row->getSourceProperty('payer'),
      'quant' => $row->getSourceProperty('worth_quantity'),
      'description' => $row->getSourceProperty('description'),
      'primary' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'mc_entry',
      'source' => 'quant'
    ];
    $migration->mergeProcessOfProperty($field_name, $process);
  }

}
