<?php

namespace Drupal\mcapi\Plugin\migrate\destination;

use Drupal\mcapi\Entity\Transaction;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Attribute\MigrateDestination;

/**
 * Plugin to write migrated transactions.
 */
#[MigrateDestination(id: 'entity:mc_transaction')]
class EntityTransaction extends EntityContentBase {

  /**
   * {@inheritDoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $this->rollbackAction = MigrateIdMapInterface::ROLLBACK_DELETE;
    $transaction = $this->getEntity($row, $old_destination_id_values);
    if (!$transaction) {
      throw new MigrateException('Unable to get entity');
    }
    assert($transaction instanceof ContentEntityInterface);
    if (!$transaction->entries->first()->description) {
      $transaction->entries->first()->description = (string)$this->t('No description');
    }
    if ($this->isEntityValidationRequired($transaction)) {
      //throws EntityValidationException ends up in the migration log.
      //$this->validateEntity($transaction);
    }
    $this->storage->resetCache();
    if ($existing = Transaction::load($transaction->id())) {
      $existing->addEntry($transaction->entries->getValue()[0]);
      $existing->save();
      return [$transaction->id()];
    }
    // @todo try to save 2 revisions here.
    return $this->save($transaction, $old_destination_id_values);
  }

  /**
   * {@inheritDoc}
   */
  public function rollback(array $destination_identifier) {
    $xid = reset($destination_identifier);
    $transaction = $this->storage->load($xid);
    // Shortcuts to save implementing an event listener in other modules.
    if (\Drupal::database()->schema()->tableExists('mcapi_cc')) {
      \Drupal::database()->delete('mcapi_cc')->condition('xid', $transaction->id())->execute();
    }
    parent::rollback($destination_identifier);
  }
}
