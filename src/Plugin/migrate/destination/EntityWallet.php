<?php

namespace Drupal\mcapi\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Row;
use Drupal\user\Entity\User;
use Drupal\migrate\Attribute\MigrateDestination;

/**
 * Plugin to write migrated wallets.
 */
#[MigrateDestination(id: 'entity:mc_wallet')]
class EntityWallet extends EntityContentBase {

  /**
   * {@inheritDoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    // Only migrate the wallet if the user was migrated
    $owner_id = $row->getDestinationProperty('owner');
    $owner = User::load($owner_id);
    if (!$owner) {
      $owner = User::load(1);
    }
    $row->setDestinationProperty('owner', $owner); // Don't know why this isn't an integer but it works.
    $row->setDestinationProperty('wid', $owner_id);
    return parent::import($row, $old_destination_id_values);
  }

}
