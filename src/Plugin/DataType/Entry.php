<?php

namespace Drupal\mcapi\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Data type for transaction entries
 * @todo is this used?
 * @see Drupal\mcapi\Plugin\Field\FieldType\Entry::propertyDefinitions().
 */
#[DataType(
  id: 'mc_entry',
  label: new TranslatableMarkup('Entry')
)]
class Entry extends TypedData {

  /**
   * @var string
   */
  protected $payee_wid;
  protected $payer_wid;

  /**
   * @var int
   */
  protected $quant;
  protected $quant_t; // trunkwards quantity

  /**
   * @var string
   */
  protected $description;
  protected $metadata; // blob / stdClass

  /**
   * @var bool
   */
  protected $primary;

}
