<?php

namespace Drupal\mcapi\Plugin\Derivative;

use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Local action definitions to create a transaction for each transaction type.
 */
class TransactionLocalAction extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Button for each of the basic transaction forms, one per workflow.
    foreach (Workflow::loadMultiple() as $wf) {
      $key = "mcapi.transaction.add.{$wf->id}.action";
      $this->derivatives[$key] = [
        'title' => t("@workflow_type transaction", ['@workflow_type' => $wf->label]),
        'options' => ['attributes' => ['title' => $wf->summary]],
        'route_name' => "entity.mc_transaction.add_form",
        'route_parameters' => [
          'mc_workflow' => $wf->id
        ],
        'appears_on' => [
          'mcapi.admin',
          'entity.mc_transaction.collection'
        ]
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
