<?php

namespace Drupal\mcapi\Plugin\Derivative;

use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local tasks to create a transaction of each workflow type.
 */
class TransactionFormTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static();
  }

  /**
   * {@inheritDoc}
   * Make a task for each local Transaction workflow
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Put types in alphabetical order of the label.
    $w = 0;
    foreach (Workflow::loadMultiple() as $workflow) {
      $tab_title = $workflow->label;
      $this->derivatives['mcapi.form.' . $workflow->id] = [
        'title' => $tab_title,
        'description' => $workflow->summary,
        'route_name' => 'entity.mc_transaction.add_form',
        'route_parameters' => [
          'mc_workflow' => $workflow->id
        ],
        'base_route' => 'entity.mc_transaction.add_form',
        'weight' => $w++,
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
