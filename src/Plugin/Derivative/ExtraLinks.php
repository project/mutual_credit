<?php

namespace Drupal\mcapi\Plugin\Derivative;

use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a default implementation for menu link plugins.
 */
class ExtraLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;


  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $weight = 0;
    // The admin_toolbar_tools module doesn't know about our field_ui links.
    if ($this->moduleHandler->moduleExists('field_ui') && $this->moduleHandler->moduleExists('admin_toolbar_tools')) {
      // Adds common links to entities.
      foreach (['mc_transaction' => 'mcapi.admin.transaction.list', 'mc_wallet' => 'mcapi.admin_wallets'] as $entity_type_id => $parent_link_id) {
        $links["entity.$entity_type_id.field_ui_fields_"] = [
          'title' => $this->t('Manage fields'),
          'route_name' => "entity.$entity_type_id.field_ui_fields",
          'parent' => $parent_link_id,
          'weight' => $weight++,
        ] + $base_plugin_definition;
        $links["entity.entity_form_display.$entity_type_id.default_"] = [
          'title' => $this->t('Manage form display'),
          'route_name' => "entity.entity_form_display.$entity_type_id.default",
          'parent' => $parent_link_id,
          'weight' => $weight++,
        ] + $base_plugin_definition;
        $links["entity.entity_view_display.$entity_type_id.default_"] = [
          'title' => $this->t('Manage display'),
          'route_name' => "entity.entity_view_display.$entity_type_id.default",
          'parent' => $parent_link_id,
          'weight' => $weight++,
        ] + $base_plugin_definition;
      }
    }
    return $links;
  }

}
