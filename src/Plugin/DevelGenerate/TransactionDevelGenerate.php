<?php

namespace Drupal\mcapi\Plugin\DevelGenerate;

use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\WorkflowException;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\user\Entity\User;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CreditCommons\Workflow as CCWorkflowInterface;

/**
 * Provides a DevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "mc_transaction",
 *   label = @Translation("transactions"),
 *   description = @Translation("Generate a given number of transactions..."),
 *   url = "transaction",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 100,
 *     "kill" = TRUE,
 *     "type" = "default"
 *   }
 * )
 */
class TransactionDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  const MAX = 100;

  /**
   * The transaction storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $transactionStorage;

  /**
   * @var ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * @var AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * The unixtime of the request
   * @var Int
   */
  protected $requestTime;

  /**
   * All the wallet ids
   *
   * @var array
   */
  protected array $wids;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->moduleHandler = $container->get('module_handler');
    $instance->messenger = $container->get('messenger');
    $instance->extensionPathResolver = $container->get('extension.path.resolver');
    $instance->accountSwitcher = $container->get('account_switcher');
    $instance->transactionStorage = $container->get('entity_type.manager')->getStorage('mc_transaction');
    $instance->entityFieldManager = $container->get('entity_field.manager');


    $wids = $container->get('entity_type.manager')->getstorage('mc_wallet')->getQuery()->accessCheck(TRUE)->execute();
    if (count($wids) < 2) {
      throw new \Exception('Not enough wallets to make a transaction.');
    }
    $instance->wids = $wids;
    $instance->requestTime = $container->get('datetime.time')->getRequestTime();

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) : array {
    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete existing transactions.'),
      '#default_value' => $this->getSetting('kill'),
    ];
    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('How many transactions would you like to generate?'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];
    $form['days'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of days since the first transaction'),
      '#default_value' => $this->getSetting('day'),
      '#required' => TRUE,
      '#min' => 0,
    ];
    $form['#redirect'] = FALSE;
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  protected function generateElements(array $values) : void {
    $this->settings = $values + $this->settings;
    if ($this->getSetting('num') <= static::MAX) {
      if ($this->getSetting('kill')) {
        $this->contentKill();
      }
      $workflows = Workflow::loadMultiple();

      \Drupal::State()->set('mcapi_mail_stop', TRUE);
      for ($i = 0; $i < $this->getSetting('num'); $i++) {
        $this->develGenerateTransactionAdd($workflows[array_rand($workflows)]->getCode());
      }
      \Drupal::State()->set('mcapi_mail_stop', FALSE);
      if (function_exists('drush_log') && $i % drush_get_option('feedback', 1000) == 0) {
        drush_log('Completed '.drush_get_option('feedback', 1000).' transactions', 'ok');
      }
      $this->setMessage($this->t('Created @num transactions.', ['@num' => $this->getSetting('num')]));
    }
    else {
      // setFile doesn't include the file yet and devel_generate_batch_finished must be callable
      $this->moduleHandler->loadInclude('devel_generate', 'batch.inc');
      $batch_builder = (new BatchBuilder())
       ->setTitle($this->t('Generating transactions'))
       ->setFile($this->extensionPathResolver->getPath('module', 'devel_generate') . '/devel_generate.batch.inc')
       ->setFinishCallback('devel_generate_batch_finished');
      // Add the kill operation.
      if ($this->getSetting('kill')) {
        $batch_builder->addOperation('devel_generate_operation', [$this, 'batchContentKill', []]);
      }

      $batches = ceil($this->getSetting('num')/static::MAX);
      for ($num = 0; $num < $batches; $num++) {
        $batch_builder->addOperation('devel_generate_operation', [$this, 'batchContentAddTransactions', []]);
      }
      batch_set($batch_builder->toArray());
    }
  }

  /**
   * batch callback to create one transaction.
   */
  public function batchContentAddTransactions($values, &$context) {
    $i = 0;
    if (!isset($context['results']['num'])) {
      $context['results']['num'] = 0;
    }

    $this->wids = $this->entityTypeManager()
      ->getstorage('mc_wallet')->getQuery()->accessCheck(TRUE)
      ->execute();
    $workflows = Workflow::loadMultiple();

    \Drupal::State()->set('mcapi_mail_stop', TRUE);
    while ($context['results']['num'] < $this->getSetting('num') and $i < Self::MAX) {
      $this->develGenerateTransactionAdd($workflows[array_rand($workflows)]->getCode());
      $context['results']['num']++;
      $i++;
    }
    \Drupal::State()->set('mcapi_mail_stop', FALSE);
  }

  /**
   * batch callback to create one transaction.
   */
  public function batchContentKill($values, &$context) {
    $this->contentKill();
  }

  /**
   * Deletes all transactions .
   */
  public function contentKill() {
     \Drupal::database()->truncate('mc_transaction')->execute();
     \Drupal::database()->truncate('mc_transaction_revision')->execute();
     \Drupal::database()->truncate('mc_transaction_totals')->execute();
     \Drupal::database()->truncate('mc_transactions_index')->execute();
     \Drupal::database()->truncate('mc_transaction__entries')->execute();
     \Drupal::database()->truncate('mc_transaction_revision__entries')->execute();
    $fieldapi_fields = array_diff_key(
      \Drupal::service('entity_field.manager')->getFieldDefinitions('mc_transaction', 'mc_transaction'),
      \Drupal::service('entity_field.manager')->getBasefieldDefinitions('mc_transaction')
    );
    foreach (array_keys($fieldapi_fields) as $field_name) {
      \Drupal::database()->truncate('mc_transaction__'.$field_name)->execute();
      \Drupal::database()->truncate('mc_transaction_revision__'.$field_name)->execute();
    }
  }

  /**
   * Create one transaction. Used by both batch and non-batch code branches.
   *
   * Ideally this would save multiple versions of transactions, progressing through a real workflow
   */
  public function develGenerateTransactionAdd(string $workflow_code) {
    list($w1, $w2) = $this->get2RandWalletIds();
    if (!$w2) {
      return;
    }
    $creator = Wallet::load($w1)->getOwner();
    $props = [
      'payer' => $w1,
      'payee' => $w2,
      // Transactions of type 'inherit' don't show in the default view.
      'workflow' => $workflow_code,
      'uid' => $creator->id(),
      'description' => $this->getRandom()->sentences(1),
      'quant' => rand(10, 250)
    ];
    $transaction = Transaction::create($props);
    // Populate additional fields
    $this->populateFields($transaction);
    // We don't want all transactions created at the same time!
    // Make created time between the newest involved wallet creation and now.
    $latest = 0;
    foreach (Wallet::loadMultiple($transaction->allWalletIds()) as $wallet) {
      $latest = max($latest, $wallet->created->value);
    }
    $transaction->created->value = rand($latest, $this->requestTime);
    global $error_context;
    $error_context = (object)[
      'user' => \Drupal::currentUser()->id(),
      'node' => 'local',
      'route_name' => 'unspecified'
    ];
    $workflow = $transaction->workflow->entity;
    // Pretend to be the payer or payee
    $use_as = NULL;
    if ($workflow->isCredit()) {
      $use_as = $transaction->entries->payer->getOwner();
    }
    elseif ($workflow->isBill()) {
      $use_as = $transaction->entries->payee->getOwner();
    }
    if (isset($use_as)) {
      $this->accountSwitcher->switchTo($use_as);
    }
    // Assemble the transaction.
    $violations = $transaction->validate();

    $num = rand(1, 60*24*$this->getSetting('days'));
    // this is overwritten if defined earlier
    $transaction->created->value = strtotime("-$num mins");

    if ($violations->count()) {
      foreach ($violations as $v) {
        $this->messenger->addWarning($v->getmessage(), 'Transaction failed validation');
        return;
      }
    }

    if (isset($use_as)) {
      $this->accountSwitcher->switchback();
    }
    // Transaction should be in validated state and saves to $workflow->startState()
    $this->saveTransactionAs($transaction, $creator, $workflow->startState());
    // Perform additional transitions
    while (isset($workflow->transitions[$transaction->state->value])) {
      $rand = rand(0, 99);
      $destinations = $workflow->transitions[$transaction->state->value];
      $dest_state = array_rand($destinations);
      $role = $destinations[$dest_state];
      switch ($dest_state) {
        case CCWorkflowInterface::STATE_COMPLETED:
        case CCWorkflowInterface::STATE_ERASED:
          if ($role == '-')$user = $transaction->payee->getOwner();
          elseif ($role == '+')$user = $transaction->payer->getOwner();
          else $user = User::load(1);
          $this->saveTransactionAs($transaction, $user, $dest_state);
          if ($rand < 90) return;
          break;
        case CCWorkflowInterface::STATE_DELETE:
          $this->messenger->addStatus('Deleted transaction '.$transaction->id());
          $transaction->delete();
          return;
        default:
          throw new \Exception('Illegal state in TransactionDevelGenerate: '.$transaction->state->value);
      }
    }
  }

  function getUserFromRole($transaction, $role_symbol) : User {
    switch ($role_symbol) {
      case '=':
        $role_symbol = rand(0,1) ? '+' : '-';
      case '+':
        return $transaction->entries->first()->payee->getowner();
      case '-':
        return $transaction->entries->first()->payer->getowner();
      case '0':
        $admin_roles = array_keys(user_roles(TRUE, NULL, 'manage mcapi'));
        shuffle($admin_roles);
        $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['roles' => reset($admin_roles)]);
        return $users ? $users[array_rand($users)] : User::load(1);
    }
  }

  function saveTransactionAs($transaction, $user, $dest_state) {
    $this->accountSwitcher->switchTo($user);
    try {
      $transaction->saveVersion($dest_state);
    }
    catch (WorkflowException $e) {
      // do nothing for now. Should be logged.
      $this->messenger->addWarning($e->getMessage());
    }
    $this->accountSwitcher->switchBack();
  }

  /**
   * Get two random wallets
   *
   * @return int[]
   *   2 wallet ids
   */
  public function get2RandWalletIds() {
    $wids = $this->wids;
    shuffle($wids);
    return array_slice($wids, -2);
  }

  /**
   * {@inheritDoc}
   */
  public function validateDrushParams($args, $options = []) : array {
    $values['kill'] = drush_get_option('kill');
    $values['num'] = array_shift($args);
    $values['days'] = 180;
    return $values;
  }

}
