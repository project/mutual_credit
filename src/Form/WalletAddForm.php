<?php

namespace Drupal\mcapi\Form;

use Drupal\mcapi\Entity\Wallet;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Form builder to create a new wallet for a given ContentEntity.
 */
class WalletAddForm extends ContentEntityForm {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'wallet_add_form';
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $owner = $this->entity->getOwner(); // cannot be null.
    $form['#title'] = $this->t(
      "New wallet for user '%label'",
      ['%label' => $owner->label()]
    );
    $form = parent::form($form, $form_state);
    $form['name']['widget'][0]['value']['#placeholder'] = $owner->label() . ' ('.$this->t('suggested').')';
    $form['owner']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create');
    return $actions;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
    $this->messenger()->addStatus($this->t('The wallet was created'));
  }

    /**
   * {@inheritDoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $user = $route_match->getParameters()->get('user');
    // Should be upcast
    return Wallet::create(['owner' => $user]);
  }

}
