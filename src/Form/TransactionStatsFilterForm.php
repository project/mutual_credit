<?php

namespace Drupal\mcapi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides a form for forms.
 */
class TransactionStatsFilterForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'transaction_stats_filter';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request_time = \Drupal::time()->getRequestTime();
    $args = func_get_args();
    $previous = isset($_SESSION['transaction_stats_filter']) ?  $_SESSION['transaction_stats_filter'] : [];
    $first_transaction_time = $form_state->get('firstTransactionTime');
    $form['since'] = [
      '#title' => $this->t('Since'),
      '#type' => 'select',
      '#options' => [
        $first_transaction_time => date('Y M', $first_transaction_time)
      ],
      '#default_value' => isset($previous['since']) ? $previous['since'] : 0
    ];
    $form['until'] = [
      '#title' => $this->t('Until'),
      '#type' => 'select',
      '#empty_option' => $this->t('Now'),
      '#required' => FALSE,
      '#default_value' => isset($previous['until']) ? $previous['until'] : $request_time
    ];

    $month = intval(date('M', $first_transaction_time));
    $year = intval(date('Y', $first_transaction_time));

    // @todo This could be tidier
    if ($first_transaction_time > strtotime('-3 months')) {
      unset($form['until'], $form['since']);
    }
    elseif ($first_transaction_time > strtotime('-2 years')) {
      // System is less than two years old, so show months
      $start_of_month = strtotime("01-{$month}-{$year}");
      while ($start_of_month < $request_time) {
        $this->nextMonth($month, $year);
        $start_of_month = strtotime('01-'.$month.'-'.$year);
        $end_of_month = strtotime('01-'.($month+1).'-'.$year) -1;
        $form['since']['#options'][$start_of_month.':'.$end_of_month] = date('M', $start_of_month) .', '.date('Y', $start_of_month);
        $form['until']['#options'][$end_of_month] = date('Y', $end_of_month);}
    }
    elseif ($first_transaction_time < strtotime('-2 years')) {
      // System is more than 2 years old, so show years.
      $start_of_year = strtotime('01-01-'.$year);
      //$form['since']['#options'][$next_year] = date('Y', $next_year);
      while ($start_of_year < $request_time) {
        $form['since']['#options'][$start_of_year] = date('Y', $start_of_year);
        $year++;
        $form['until']['#options'][strtotime('01-01-'.$year)-1] = $year;
        $start_of_year = strtotime('01-01-'.$year);
      }
    }
    // Ideally 'until' would be set by ajax to prevent dates before 'since'
    if (Element::Children($form)) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#weight' => 20
      ];
      $form['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#limit_validation_errors' => [],
        '#submit' => ['::resetForm'],
        '#weight' => 21
      ];
      $form_details = [
        '#title' => $this->t('Filter transactions...'),
        '#type' => 'details',
        'form' => $form,
        '#weight' => -1,
        '#open' => isset($_SESSION['transaction_stats_filter'])
      ];
      return $form_details;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $val) {
      $_SESSION['transaction_stats_filter'][$key] = $val;
    }
  }

  public function resetForm(array &$form, FormStateInterface $form_state) {
    unset($_SESSION['transaction_stats_filter']);
  }

  /**
   *
   * @param int $month
   * @param int $year
   * @return void
   */
  private function nextMonth(int &$month, int &$year) : void {
    if ($month == 12) {
      $month = 1;
      $year++;
    }
    else {
      $month++;
    }
  }
}
