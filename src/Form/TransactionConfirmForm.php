<?php

namespace Drupal\mcapi\Form;

use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Renderer;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form for confirming all operations on a transaction.
 */
class TransactionConfirmForm extends EntityConfirmFormBase {

  /**
   * @var ContainerAwareEventDispatcher
   */
  private $eventDispatcher;

  /**
   * @var Renderer
   */
  private $renderer;

  /**
   * @var Drupal\mcapi\Entity\ViewBuilder\TransactionViewBuilder
   */
  protected $transactionViewBuilder;

  /**
   * Two letters representing the from state, and the to state.
   * @var string
   */
  protected $transition;

  /**
   * @param EntityTypeManager $entity_type_manager
   * @param ContainerAwareEventDispatcher $event_dispatcher
   * @param Renderer $renderer
   *
   * @todo add typehint for $eventDispatcher after 10.3
   */
  public function __construct(EntityTypeManager $entity_type_manager, $event_dispatcher, Renderer $renderer) {
    $this->transactionViewBuilder = $entity_type_manager->getViewBuilder('mc_transaction');
    $this->eventDispatcher = $event_dispatcher;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildform(array $form, FormStateInterface $form_state) {
    $this->transition = $this->entity->state->value . $this->getRequest()->attributes->get('dest_state');
    $form = parent::buildForm($form, $form_state);
    $form['#title'] = $this->getQuestion();
    $form['#attributes']['class'][] = 'mc-transition';

    $form['description'] = $this->transactionViewBuilder->view($this->entity, 'transition');
    $transition_settings = \Drupal::config('mcapi.settings')->get('transitions');
    $form['actions']['submit']['#value'] = $transition_settings[$this->transition]['label'];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Use the entity as is, don't rebuild it
    $transaction = $this->entity;
    // This assumes no novel workflow states.
    switch ($this->transition[1]) {// Destination state
      case CCWorkflowInterface::STATE_PENDING:
        // N.B. this transaction MUST be new.
        // get the name of the user who should do the PC transition
        $role = $transaction->workflow->entity->transitions['P']['C']; // either + or -
        if ($role == '+') $uid = $transaction->payee->getOwnerId();
        elseif ($role == '-')$uid = $transaction->payer->getOwnerId();
        // Remote party is represented with 0.
        $name = $uid ? User::load($uid)->getDisplayName() : t('The remote party');
        $message = $this->t(
          'The transaction will complete when signed by @name',
          ['@name' => $name]
        );
        break;
      case CCWorkflowInterface::STATE_COMPLETED: // Needs refactoring coz signing is not necessarily a transition to a new state
        // Add the signature - See Transaction::presave for the state change.
        // $transaction->signatures->sign();
        $message = $this->t('The transaction has been saved.');
        break;
      case CCWorkflowInterface::STATE_DELETE: // only people with 'manage mcapi' permission.
        $transaction->delete();
        // User might not have access to this.
        $form_state->setRedirectUrl(Url::fromRoute('entity.mc_transaction.collection')); //
        $this->messenger()->addMessage($this->t('The transaction has been completely deleted'));
        return;
      case CCWorkflowInterface::STATE_ERASED:
        $message = $this->t('The transaction has been marked as erased.');
        break;
    }

    try {
      $transaction->saveVersion($this->transition[1]); // Triggers the TransactionSaveEvent
    }
    catch (EntityStorageException $e) {
      // Probably means the user double clicked, so just load the saved transaction and redirect to it.
      if (!str_contains($e->getMessage(), 'Duplicate entry')) {
        throw $e;
      }
      $txs = \Drupal::entityTypeManager()->getStorage('mc_transaction')->loadByProperties(['uuid' => $transaction->uuid()] );
      $transaction = reset($txs);
    }
    catch(\Exception $e) {
      $this->messenger()->addError(t('There was a problem: @message', ['@message' => $e->getMessage()]));
      $form_state->setRebuild();
      return;
    }
    // Only redirect when we are creating the transactions, otherwise there should be a destination in the url and this obliterates it.
    if ($this->transition[0] == 'V') {
       $form_state->setRedirectUrl($transaction->toUrl());
    }
    if (isset($message)) {
      $this->messenger()->addMessage($message);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl(): Url {
    if ($this->entity->isNew()) {
      $cancel_url = new Url('user.page');
    }
    else {
      $cancel_url = $this->entity->toUrl();
    }
    return $cancel_url;
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritDoc}
   */
  public function getConfirmText() {
    return $this->config('mcapi.settings')->get('transition_names.'.$this->transition);
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    // The page title.
    return $this->t('Are you sure?');
  }

  /**
   * {@inheritDoc}
   * $this->op is currently the name of the destination state.
   */
  public function getFormId() {
    return 'mc_transaction_' . $this->transition . '_confirm_form';
  }

  /**
   * {@inheritDoc}
   */
  public function getBaseFormId() {
    return 'mc_transaction_confirm_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function prepareEntity() {
    // Showing the form for the first time.
    return $this->getRouteMatch()->getParameter('mc_transaction');
  }

  /**
   * {@inheritDoc}
   */
  public function buildEntity($element, $form_state) {
    // Submission of this form does not change the entity.
    return $this->entity;
  }

}
