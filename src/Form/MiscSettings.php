<?php

namespace Drupal\mcapi\Form;

use Drupal\views\Entity\View;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for miscellaneous settings.
 */
class MiscSettings extends ConfigFormBase {

  /**
   * @var ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param ConfigFactoryInterface $configFactory
   *   The Configfactory service.
   * @param ModuleHandlerInterface $module_handler
   *   The ModuleHandler service.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configFactory);
    $this->config = $this->config('mcapi.settings');// editable via ConfigFormBaseTrait.
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Injections.
   */
  static public function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $i = 0;
    $help[] = $this->t('E.g. for loaves put 0 loaves; for dollars and cents put $0.99; for Hours divided into quarters put Hr0:59/4mins.');
    $help[] = $this->t('The main unit is always a zero');
    $help[] = $this->t('The optional divisor /n at the end will render the divisions as a dropdown field showing intervals, in the example, of 15 mins.');
    $help[] = $this->t('Special effects can be accomplised with HTML & css.');
    $default_curr= $this->config->get('currency');
    $form['currency'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Currency'),
      '#tree' => TRUE,
      'name' => [
        '#title' => $this->t('Name (pluralised)'),
        '#type' => 'textfield',
        '#default_value' => $default_curr['name'],
        '#size' => 20,
        '#maxlength' => 128,
        '#weight' => $i++
      ],
      'format' => [
        '#title' => $this->t('Display format'),
        '#description' => implode(' ', $help),
        '#type' => 'currency_format',
        '#default_value' => implode($default_curr['format']), // array
        '#size' => 20,
        '#weight' => $i++
      ],
      'fractions' => [
        '#title' => $this->t('Render partial units to the nearest &#189;, &#8531;, &#188; or &#8533;'),
        '#type' => 'checkbox',
        '#default_value' => $default_curr['fractions'] ?? FALSE, // temp
        '#weight' => $i++
      ],
      'zero_snippet' => [
        '#title' => $this->t('Zero snippet'),
        '#description' => $this->t("Allow zero transactions and display them with this html snippet. Leave blank to disallow."),
        '#type' => 'textfield',
        '#default_value' => $default_curr['zero_snippet'],
        '#size' => 20,
        '#maxlength' => 128,
        '#weight' => $i++
      ]
    ];

    $form['sentence_template'] = [
      '#title' => $this->t('One-line template'),
      '#description' => $this->t('Express a transaction in one line, using tokens [payer] [payee] [quant] [description]'),
      '#type' => 'textfield',
      '#default_value' => $this->config->get('sentence_template'),
      '#weight' => $i++
    ];

    $form['autowallet'] = [
      '#title' => $this->t('Autowallet'),
      '#description' => $this->t('New users automatically have a new wallet.'),
      '#type' => 'checkbox',
      '#default_value' => $this->config->get('autowallet'),
      '#weight' => $i++
    ];

    if ($this->moduleHandler->moduleExists('views')) {
      $inex = View::load('mc_income_expenditure');
      $statement = View::load('mc_wallet_transactions');
      // Ideally this would be a list of all transaction or transaction index views which take wallet id as an argument
      if ($inex && $statement) {
        $form['user_transaction_view'] = [
          '#title' => $this->t("Transaction display"),
          '#description' => $this->t('When looking at a wallet (or user).'),
          '#type' => 'radios',
          '#options' => [
            '' => $this->t('None'),
            'mc_income_expenditure' => $this->t('Income & expenditure'),
            'mc_wallet_transactions' => $this->t('Statement', [], ['context' => 'like a bank statement']),
          ],
          '#default_value' => $inex->status() ? 'mc_income_expenditure' : 'mc_wallet_transactions',
          '#weight' => $i++,
          '#disabled' => !$this->moduleHandler->moduleExists('views')
        ];
      }
    }

    $form['rebuild_xaction_index'] = [
      '#title' => $this->t('Rebuild index'),
      '#description' => $this->t('The index table stores the transactions in an views-friendly format. It should never need rebuilding on a working system.'),
      '#type' => 'fieldset',
      '#weight' => $i++,
      'button' => [
        '#type' => 'submit',
        '#value' => $this->t('Rebuild transaction index'),
        '#submit' => [
          [get_class($this), 'rebuildXactionIndex'],
        ]
      ]
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config
      ->set('currency', $form_state->getValue('currency'))
      ->set('sentence_template', $form_state->getValue('sentence_template'))
      ->set('autowallet', $form_state->getValue('autowallet'));
    if ($form_state->hasValue('user_transaction_view')) {
      $this->userTransactionView($form_state->getValue('user_transaction_view'));
    }
    $this->config->save();
    parent::submitForm($form, $form_state);
    Cache::invalidateTags(['mc_wallet_view']);
  }

  /**
   * Submit callback.
   *
   * Rebuild the Transaction index table.
   */
  public static function rebuildXactionIndex(array &$form, FormStateInterface $form_state) {
    \Drupal::entityTypeManager()->getStorage('mc_transaction')->indexRebuild();
    \Drupal::messenger()->addStatus(t("Index table now tallies with transaction table."));
    $form_state->setRedirect('system.status');
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['mcapi.settings'];
  }

  private static function userTransactionView(string $view_name) {
    $inex = View::load('mc_income_expenditure');
    $statement = View::load('mc_wallet_transactions');
    if ($view_name == 'mc_income_expenditure') {
      $inex->setStatus(TRUE);
      $statement->setStatus(FALSE);
    }
    elseif ($view_name == 'mc_wallet_transactions') {
      $inex->setStatus(FALSE);
      $statement->setStatus(TRUE);
    }
    $inex->save();
    $statement->save();
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mcapi_misc_settings';
  }

}
