<?php

namespace Drupal\mcapi\Form;

use Drupal\mcapi\Entity\Workflow;
use Drupal\mcapi\Plugin\Field\FieldType\McState;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for miscellaneous settings.
 */
class TransitionSettings extends ConfigFormBase {

  /**
   * These are all possible transitions, but they may not all be used in the available workflows.
   */

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mcapi.settings');
    $i = 0;

    $form['delete_transactions'] = [
      '#title' => $this->t('Transactions are deletable.'),
      '#description' => $this->t("By users with '@perm' permission", ['@perm' => t('Manage wallets and transactions')]),
      '#type' => 'checkbox',
      '#default_value' => $config->get('delete_transactions'),
      '#weight' => $i++
    ];
    $state_names = McState::stateNames();
    foreach (Workflow::usedTransitions() as $pair) {
      $form[$pair] = [
        '#type' => 'fieldset',
        '#title' => $state_names[$pair[0]] .' -> '. $state_names[$pair[1]],
        '#description' => t('The following tokens are available: ') . '[xaction:url], [xaction:payee], [xaction:payer], [xaction:quant], [xaction:description], [xaction:sentence]',
        '#weight' => $i++,
        '#tree' => 1
      ];
      $form[$pair]['label'] = [
        '#type' => 'textfield',
        '#title' => t('The name of the transition'),
        '#weight' => $i++,
        '#default_value' => $config->get("transitions.$pair.label"),
        '#required' => $this->isRequired($pair),
        '#weight' => $i++,
      ];
      $name = $pair ."[title]";
      $form[$pair]['subject'] = [
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#default_value' => $config->get("transitions.$pair.mail_template.subject"),
        '#weight' => $i++,
      ];
      $form[$pair]['body'] = [
        '#type' => 'textarea',
        '#title' => t('body'),
        '#default_value' => $config->get("transitions.$pair.mail_template.body"),
        '#weight' => $i++,
      ];
      if ($pair == 'VP') {
        $form['VP']['#description'] .= $this->t(
          'This email can have a special link for the recipient to execute @pending -> @completed transition from the email. [xaction:sign-link]',
          ['@pending' => $state_names['P'], '@completed' => $state_names['C']]
        );
      }
    }

    return parent::buildform($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('mcapi.settings')
      ->set('delete_transactions', $form_state->getValue('delete_transactions'));
    foreach (Workflow::usedTransitions() as $pair) {
      $val = $form_state->getValue($pair);
      $config->set("transitions.$pair.label",$val['label']);
      $config->set("transitions.$pair.mail_template.subject", $val['subject']);
      $config->set("transitions.$pair.mail_template.body", $val['body']);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }


  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['mcapi.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mcapi_transition_settings';
  }

  private function isRequired(string $pair) {
    static $transitions;
    if (!$transitions) {
      foreach (Workflow::loadMultiple() as $wf) {
        foreach($wf->transitions as $start_state => $data) {
          foreach($data as $end_state => $role) {
            $transitions[] = $start_state.$end_state;
          }
        }
      }
      $transitions = array_unique($transitions);
    }
    return in_array($pair, $transitions);
  }

}