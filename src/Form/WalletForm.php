<?php

namespace Drupal\mcapi\Form;

use Drupal\user\Entity\User;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Build form to edit, not create, the wallet entity.
 */
class WalletForm extends ContentEntityForm {

  /**
   * The wallet settings.
   */
  protected $walletConfig;

  /**
   * @param EntityRepositoryInterface $entity_repository
   * @param EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param TimeInterface $time
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct($entity_repository, $entity_type_bundle_info, $time, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->walletConfig = $config_factory->get('mcapi.settings');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'wallet_form';
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $wallet = $this->entity;
    $form = parent::form($form, $form_state);

    $form['#attached']['library'][] = 'mcapi/wallets';
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect(
      'entity.mc_wallet.canonical',
      ['mc_wallet' => $this->entity->id()]
    );
    parent::save($form, $form_state);
  }

  /**
   * Get a list of user names from the user IDs.
   *
   * @param array $uids
   *   User IDs.
   *
   * @return string
   *   Comma separated usernames.
   */
  private function getUsernames($uids) {
    $names = [];
    foreach (User::loadMultiple($uids) as $account) {
      $names[] = $account->getDisplayName();
    }
    return implode(', ', $names);
  }

}
