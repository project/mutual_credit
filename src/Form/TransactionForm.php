<?php

namespace Drupal\mcapi\Form;

use Drupal\mcapi\Entity\Transaction;
use Drupal\mcapi\Functions;
use Drupal\mcapi\Entity\Workflow;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Url;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form for adding transactions.
 */
class TransactionForm extends ContentEntityForm {

  /**
   * The tempStore service.
   *
   * @var Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   *
   * @param $entity_repository
   * @param $entity_type_bundle_info
   * @param $time
   * @param PrivateTempStoreFactory $tempstoreFactory
   * @param Request $current_request
   */
  public function __construct($entity_repository, $entity_type_bundle_info, $time, PrivateTempStoreFactory $tempstoreFactory, Request $current_request) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->tempStore = $tempstoreFactory->get('TransactionForm');
    $this->request = $current_request;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('tempstore.private'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritDoc}
   *
   * @note we are overriding here because this form is neither for saving nor
   * deleting and because previewing is not optional.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->entity->workflow->entity->requiresConfirmation() ? $this->t('Preview') : $this->t('Submit'),
        // N.B. NOT ::save
        '#submit' => ['::submitForm'],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  function form(array $form, FormStateInterface $form_state) {
    //$form['#title'] = $this->entity->workflow->entity->label;
    $form = parent::form($form, $form_state);
    $form['entries']['widget']['payee_wid']['#required'] = TRUE;
    $form['entries']['widget']['payer_wid']['#required'] = TRUE;
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The transaction is assembled during entity validation and this shouldn't
    // happen twice. So don't run $this->buildEntity($form, $form_state) again
    // but rather retrieve the built entity
    $workflow = (object)$this->entity->workflow->entity;
    $this->updateChangedTime($this->entity);
    $this->entity->created->value = $this->time->getRequestTime();
    $this->entity = Functions::getAssembledTransaction($this->entity);
    if ($workflow->requiresConfirmation()) {
      // Transaction is in the VALIDATED STATE
      // Put the whole transaction in the userTempStore and go to the confirm page.
      $this->tempStore->set('mc_transaction', $this->entity);
      $redirect = Url::fromRoute(
        'entity.mc_transaction.transition',
        ['mc_transaction' => 0, 'dest_state' => $workflow->startState()]
      );
    }
    else {
      // Transaction is already saved remotely in the start state
      $start_state = $this->entity->workflow->entity->startState();
      $this->entity->state->value = $start_state;
      $this->entity->transition = CCWorkflowInterface::STATE_VALIDATED . $start_state;
      $this->entity->save();
      $this->messenger()->addStatus($this->t('The transaction was saved.'));
      $redirect = Url::fromRoute(
        'entity.mc_transaction.canonical',
        ['mc_transaction' => $this->entity->id()]
      );
    }
    $form_state->setRedirectUrl($redirect);
  }

  /**
   * {@inheritDoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    $entity->description->value = html_entity_decode($entity->description->value);
  }

  /**
   * {@inheritDoc}
   * @todo make this work for passing transaction properties.
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($wf = $this->request->attributes->get('mc_workflow')) {
      $props['workflow'] = $wf->getCode(); // Loaded entity
    }
    else {
      throw new \Exception('No mc_workflow in Request attributes.');
    }
    // The workflow ID is not a path variable, must be set in the route paramters.
    $props += $this->request->query->all();
    return Transaction::create($props);
  }

  /**
   * Provide an ID for the transaction form based on the workflow.
   */
  function transactionFormId() {
    $wf = $this->entity->workflow->target_id;
    if ($workflow = Workflow::load($wf)) {
      return 'basic_'.$this->entity->workflow->entity->id;
    }
    throw new NotFoundHttpException("No such workflow: $wf");
  }

}
