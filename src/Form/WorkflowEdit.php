<?php

namespace Drupal\mcapi\Form;

use Drupal\mcapi\Plugin\Field\FieldType\McState;
use Drupal\mcapi\Entity\Workflow;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use CreditCommons\Workflow as CCWorkflowInterface;
use CreditCommons\Exceptions\InvalidWorkflowViolation;

/**
 * Build form to edit, workflow config entity
 * @todo finish the complicated bits.
 */
class WorkflowEdit extends EntityForm {

  protected array $allStates;

  function init(FormStateInterface $form_state) {
    parent::init($form_state);
    $this->allStates = McState::stateNames();
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mcapi_workflow_form';
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $w = 0;
    $form = parent::form($form, $form_state);
    $title = $this->entity->isNew() ?
      $this->t('New transaction workflow') :
      $this->t("Editing '%name' workflow", ['%name' => $this->entity->label()]);

    $form['#title'] = Markup::create($title);
    $form['label'] = [
      '#title' => $this->t('Workflow name'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#size' => 40,
      '#maxlength' => 80,
      '#required' => TRUE,
      '#weight' => $w++
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => 12,
      '#machine_name' => [
        'exists' => '\Drupal\mcapi\Entity\Workflow::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['summary'] = [
      '#title' => $this->t('Summary'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->summary,
      '#size' => 40,
      '#maxlength' => 80,
      '#required' => TRUE,
      '#weight' => $w++
    ];

    $form['direction'] = [
      '#title' => $this->t('Transaction is initiated by:'),
      '#type' => 'radios',
      '#options' => [
        CCWorkflowInterface::DIR_THIRDPARTY => $this->t('3rdparty (usually admin only)'),
        CCWorkflowInterface::DIR_BILL => $this->t('The payee, requesting a payment'),
        CCWorkflowInterface::DIR_CREDIT => $this->t('The payer, sending a payment.'),
      ],
      '#default_value' => $this->entity->direction(),
      '#required' => TRUE,
      '#weight' => $w++
    ];

    $form['confirm'] = [
      '#title' => t('Confirm validated transaction on a separate page.'),
      '#type' => 'checkbox',
      '#default_value' => $this->entity->requiresConfirmation(),
      '#weight' => $w++
    ];

    $form['start_pending'] = [
      '#title' => t('Require counterparty signature before transaction is counted'),
      '#type' => 'checkbox',
      '#default_value' => $this->entity->startState() == CCWorkflowInterface::STATE_PENDING,
      '#weight' => $w++
    ];
    $form['transitions'] = [
      '#title' => $this->t('Transitions and permissions'),
      '#description' => $this->t('Admin can do all defined transitions.'),
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#attributes' => ['id' => 'transitions-ajax-wrapper'],
      '#weight' => $w++
    ];
    foreach (Workflow::allTransitions() as $pair) {
      [$from, $to] = str_split($pair);
      // Transitions are used to get from an initial validated state, but they don't concern us here
      if ($from == CCWorkflowInterface::STATE_VALIDATED) {
        continue;
      }
      $form['transitions'][$pair] = [
        '#title' => $this->allStates[$from]. ' -> '. $this->allStates[$to],
        '#type' => 'select',
        '#options' => [
          CCWorkflowInterface::PARTY_PAYER => $this->t('Payer'),
          CCWorkflowInterface::PARTY_PAYEE => $this->t('Payee'),
          CCWorkflowInterface::PARTY_EITHER => $this->t('Payer, payee'),
          CCWorkflowInterface::PARTY_ADMINONLY => $this->t('Admin only'),
        ],
        '#empty_option' => $this->t('- undefined -'),
        '#default_value' => isset($this->entity->transitions[$from][$to]) ? $this->entity->transitions[$from][$to] : ''
      ];
      // Transitions from pending state are only visible if the start state is pending.
      if ($from == CCWorkflowInterface::STATE_PENDING) {
        $form['transitions'][$pair]['#states']['visible'] = [
          ':input[name="start_pending"]' => ['checked' => TRUE]
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  function validateForm(array &$form, FormStateInterface $form_state) {
    // Use the given data to make a new workflow
    $entity = static::buildEntity($form, $form_state);
    // Put errors into form_state
    try {
      CCWorkflowInterface::validate($entity->code);
    }
    catch (InvalidWorkflowViolation $e) {
      switch ($e->pos) {
        case 0:
          $form_state->setError('transitions', $this->t("There's something wrong with the transitions."));
          break;
        case 1:
          $form_state->setError('direction', $this->t("There's something wrong with the transitions."));
          break;
        case 2:
          $form_state->setError('confirm', $this->t("There's something wrong with the transitions."));
          break;
        case 3:
          $form_state->setError('start_pending', $this->t("There's something wrong with the transitions."));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirect('entity.mc_workflow.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $code = $form_state->getValue('direction');
    $code .= $form_state->getValue('confirm') ? '|' : '_';
    // Use the start state to get the first transition.
    $transitions = array_filter(
      $form_state->getValue('transitions'),
      function ($val) {return strlen($val) > 0;}
    );
    $s = $form_state->getValue('start_pending');
    uasort($transitions, fn($triplet) => (int)($triplet[0] == $s));
    if ($transitions) {
      foreach ($transitions as $pair => $val) {
        if ($pair) {
          $code .= $pair . $val;
        }
      }
    }
    else {
      $code .= CCWorkflowInterface::STATE_COMPLETED;
    }
    $entity = parent::buildEntity($form, $form_state);
    $entity->code = $code;
    return $entity;
  }

}

