<?php

/**
 * @file
 * Theme preprocessor functions.
 */
use Drupal\mcapi\Functions;
use Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Plugin\Field\FieldType\McState;
use CreditCommons\Workflow as CCWorkflowInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\Element;

/**
 * Theme preprocessor for 'wallet_wrapper'.
 */
function template_preprocess_mc_wallet(&$vars) {
  $vars['canonical'] = $vars['element']['#mc_wallet']->toUrl('canonical')->toString();
  $vars['attributes']['class'] = ['wallet', $vars['element']['#view_mode']];
}

/**
 * Theme preprocessor for 'wallet_component'.
 */
function template_preprocess_mc_wallet_component(&$vars) {
  // Why do some components have title and some not?
  $vars['title'] = @$vars['element']['#title'];
  $vars['attributes']['class'][] = 'component';
  if (isset($vars['element']['#theme'])) {
    $vars['attributes']['class'][] = str_replace('_', '-',$vars['element']['#theme']);
  }
}

/**
 * Theme preprocessor for all of an entity's wallets.
 */
function template_preprocess_mc_wallets(&$vars) {
  $viewbuilder = \Drupal::entityTypeManager()->getViewBuilder('mc_wallet');
  // Convert each wallet into a render array.
  foreach ($vars['wallets'] as $key => $wallet) {
    $vars['wallets'][$key] = $viewbuilder->view($wallet, $vars['view_mode']);
  }
}

/**
 * Theme preprocessor for 'balance_bars'.
 *
 * All values sent to the theme system as numbers should be formatted plainly,
 * all labels formatted normally.
 */
function template_preprocess_mc_wallet_balance_bars(&$vars) {
  $formatter = \Drupal::Service('mcapi.currency_formatter');
  $wallet = $vars['wallet'];
  $id = "given-received" . $wallet->id();
  $axis_markers = $formatter->axisMarkers(max($wallet->gross_in, $wallet->gross_out));
  $vars += [
    'id' => $id,
    'functionname' => str_replace('-', '_', $id),
    'incoming' => $formatter->formatAsFloat($wallet->gross_in),
    'outgoing' => $formatter->formatAsFloat($wallet->gross_out),
    'show_in'  => $formatter->format($wallet->gross_in),
    'show_out' => $formatter->format($wallet->gross_out),
    'max' => $formatter->formatAsFloat(intval(end($axis_markers))),
    'vaxislabels' => []
  ];
  foreach ($axis_markers as $string => $float) {
    $vars['vaxislabels'][] = [
      'value' => $float,
      'label' => $string
    ];
  }
}

/**
 * Theme preprocessor for 'balance_viz'.
 */
function template_preprocess_mc_wallet_balance_viz(&$vars) {
  $wallet = $vars['wallet'];
  if ($wallet->gross_in > $wallet->gross_out) {
    $vars['side'] = 'positive';
  }
  elseif ($wallet->gross_in < $wallet->gross_out) {
    $vars['side'] = 'negative';
  }
  else {
    $vars['side'] = 'balanced';
  }
  $vars['balance'] = $wallet->balance;
  $vars['income_formatted'] = Functions::formatBalance($wallet->gross_in, FALSE);
  $vars['expenditure_formatted'] = Functions::formatBalance($wallet->gross_out, FALSE);
  $vars['income_formatted'] = Functions::formatBalance($wallet->gross_in, FALSE);
  $vars['expenditure_formatted'] = Functions::formatBalance($wallet->gross_out, FALSE);
  $vars['balance_formatted'] = Functions::formatBalance($wallet->balance, FALSE);
  $vars['width'] = '15'; // em.
}

/**
 * Theme preprocessor for 'mc_wallet_stats'.
 */
function template_preprocess_mc_wallet_stats(&$vars) {
  $vars['gross_in'] = Functions::formatBalance($vars['wallet']->gross_in);
  $vars['gross_out'] = Functions::formatBalance($vars['wallet']->gross_out);
  $vars['volume'] = Functions::formatBalance($vars['wallet']->volume);
  $vars['balance'] = Functions::formatBalance($vars['wallet']->balance);
  $vars['partners'] = $vars['wallet']->partners;
  $vars['trades'] = $vars['wallet']->trades;
}

/**
 * Theme preprocessor for 'mc_main_entry'.
 */
function template_preprocess_mc_main_entry(&$vars) {
  $elements = $vars['elements'];
  $vars['payee'] = $elements['#mc_entry']->payee->label();
  $vars['payer'] = $elements['#mc_entry']->payer->label();
  $vars['description'] = $elements['#mc_entry']->description;
  $quant = \Drupal::Service('mcapi.currency_formatter')->format($elements['#mc_entry']->quant);
  $vars['quant'] = Markup::create('<div class="mc-quantity">'.$quant.'</div>');
}

/**
 * Theme preprocessor for 'mc_transaction'.
 */
function template_preprocess_mc_transaction(&$vars) {
  $elements = $vars['elements'];
  $vars['content'] = [];
  foreach (Element::children($elements) as $key) {
    $vars['content'][$key] = $elements[$key];
  }
}

/**
 * Theme preprocessor for 'mc_timeline'.
 *
 * Adds a column to the data with the formatted currency value.
 */
function template_preprocess_mc_timeline(&$vars) {
  $points = &$vars['points'];
  $num_of_points = count($points);
  // Apply smoothing (or even roughing!)
  if ($num_of_points < $vars['width'] / 3) {
    // Step method, for a small number of points - the true value.
    $times = $values = [];
    // Make two values for each one in the keys and values.
    foreach ($points as $time => $bal) {
      $times[] = $time;
      $times[] = $time + 1;
      $bal_float = $bal;
      $values[] = $bal_float;
      $values[] = $bal_float;
    }
    // Now slide the arrays against each other to create steps.
    array_pop($values);
    array_shift($times);
    $points = array_combine($times, $values);
  }
  elseif ($num_of_points > $vars['width']) {
    // For a large number of points, thin the array.
    $ratio = $num_of_points / $vars['width'];
    $factor = intval($ratio + 1);
    // Now iterate through the array taking 1 out of every $factor values.
    $i = 0;
    $points = array_filter(
      $points,
      function ($i) {$i++; return $i % $factor <> 0;}
    );
  }
  foreach ($points as $timestamp => &$balance) {
    $js_date = date('Y-m-d', $timestamp).'T'.date('H:m:s', $timestamp);
    $vars['daterows'][$js_date] = $balance;
  }

  // work out the axis labels
  if ($vars['format_curr']) {// MOVE THIS
    $markers = \Drupal::Service('mcapi.currency_formatter')->gChartAxes(min($vars['points']), max($vars['daterows']));
    foreach ($vars['daterows'] as &$balance) {
      $val = \Drupal::Service('mcapi.currency_formatter')->formatAsFloat($balance);
    }
  }
  else {
    $max = max($vars['daterows']);
    $max_axis = getTick(max($vars['daterows']));
    $halfway = $max_axis/2;
    $markers = [
      0 => 0,
      strval($halfway) => $halfway,
      $max_axis => $max_axis
    ];
  }

  foreach ($markers as $string => $float) {
    $vars['vaxislabels'][] = [
      'value' => $float,
      'label' => $string
    ];
  }
  // Need a unique id if several wallets are showing on the same page.
  $vars['id'] = preg_replace('/[^0-9a-zA-Z]/', '', $vars['title']) . rand(0, 1000);
}

/**
 * Implements template_preprocess_THEMEHOOK().
 */
function template_preprocess_mc_workflow_log(&$vars) {
  $state_classnames = array_combine(McState::STATES, McState::NAMES);
  $state_labels = McState::UsedStateNames();
  $i= 1;
  $vars['transitions'] = [];
  foreach ($vars['transaction']->revisions() as $transaction) {
    $vars['transitions'][] = [
      'version' => $i++,
      'when' => \Drupal::service('date.formatter')->format($transaction->revision_timestamp->value, 'short'),
      'exacttime' => \Drupal::service('date.formatter')->format($transaction->revision_timestamp->value, 'long'),
      'who' => $transaction->getRevisionUser()->getDisplayName(),
      'state' => $state_classnames[$transaction->state->value],
      'translated_state' => $state_labels[$transaction->state->value]
    ];
  }
  if ($transaction->state->value == CCWorkflowInterface::STATE_PENDING) {
    $pathways = $transaction->workflow->entity->transitions[CCWorkflowInterface::STATE_PENDING];
    if (isset($pathways[CCWorkflowInterface::STATE_COMPLETED])) {
      $signer_role = $pathways[CCWorkflowInterface::STATE_COMPLETED];
      $name = $transaction->workflow->getParticipantName($signer_role);
      $vars['transitions'][] = [
        'version' => '',
        'when' => t('Not yet'),
        'exacttime' => '',
        'who' => $name,
        'state' => $state_classnames[CCWorkflowInterface::STATE_COMPLETED],
        'translated_state' => $state_labels[CCWorkflowInterface::STATE_COMPLETED]
      ];
    }
  }
}

/**
 * Implements template_preprocess_THEMEHOOK().
 * Vars are $title, $users_only, $format_vals, $items, $width, $height.
 */
function template_preprocess_mc_wallets_ordered(&$vars) {
  $formatter = \Drupal::Service('mcapi.currency_formatter');
  $wallets = $vals = [];
  foreach ($vars['items'] as $wid => $raw_value) {
    $val = $vars['format_vals'] ?
      $val = $formatter->formatAsFloat($raw_value) :
      $raw_value;
    $vars['wallets'][(string)Wallet::load($wid)->label()] = $val;
    $vals[$raw_value] = $val;//float val
  }
  // Assumed to be unique for the page
  $vars['id'] = 'ordered_wallets_'. preg_replace('/[^0-9a-zA-Z]/', '', strtolower($vars['title']));
  if ($vars['format_vals'] and $vals) {
    $max_val_float = max($vals) ?: 100;
    $max_tick = getTick($max_val_float);
    $max_tick_raw = $formatter->unformat($max_tick);
    $vars['vticks'][$max_tick] = strip_tags($formatter->format($max_tick_raw, FALSE));
  }
}

/**
 * Utility
 * @param float $val
 * @return int
 */
function getTick(float $val) {
  $val = intval($val);
  $diff = str_pad(1, strlen($val), '0');
  $tick = $diff;
  while ($tick < $val) {
    $tick += $diff;
  }
  return $tick;
}
